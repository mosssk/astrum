﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System.Runtime.CompilerServices;
using System.Runtime.Intrinsics;
using Astrum.Maths;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using RowVec = System.Runtime.Intrinsics.Vector128<float>;

namespace Astrum.Benchmarks;


// Benchmarks for matrix types
[Explicit]
[TestFixture]
[GroupBenchmarksBy(BenchmarkLogicalGroupRule.ByCategory), CategoriesColumn]
public class MatrixBenchmarks : BenchmarkFixture
{
	private static readonly Mat4 _M1 = new(1, 3, 5, 7, 9, 2, 4, 6, 8, 1, 1, 2, 2, 3, 3, 4);
	private static readonly Mat4 _M2 = new(9, 8, 7, 6, 5, 4, 3, 2, 1, 2, 3, 4, 5, 6, 7, 8);
	
	
	[Test]
	public void Run() => RunBenchmarks();
	
	
	[Benchmark(Baseline = true), BenchmarkCategory("Multiply")]
	public Mat4 Multiply_Scalar()
	{
		var l = _M1;
		var r = _M2;
		Unsafe.SkipInit(out Mat4 o);
		
		o.M00 = l.M00 * r.M00 + l.M01 * r.M10 + l.M02 * r.M20 + l.M03 * r.M30;
		o.M01 = l.M00 * r.M01 + l.M01 * r.M11 + l.M02 * r.M21 + l.M03 * r.M31;
		o.M02 = l.M00 * r.M02 + l.M01 * r.M12 + l.M02 * r.M22 + l.M03 * r.M32;
		o.M03 = l.M00 * r.M03 + l.M01 * r.M13 + l.M02 * r.M23 + l.M03 * r.M33;
		
		o.M10 = l.M10 * r.M00 + l.M11 * r.M10 + l.M12 * r.M20 + l.M13 * r.M30;
		o.M11 = l.M10 * r.M01 + l.M11 * r.M11 + l.M12 * r.M21 + l.M13 * r.M31;
		o.M12 = l.M10 * r.M02 + l.M11 * r.M12 + l.M12 * r.M22 + l.M13 * r.M32;
		o.M13 = l.M10 * r.M03 + l.M11 * r.M13 + l.M12 * r.M23 + l.M13 * r.M33;
		
		o.M20 = l.M20 * r.M00 + l.M21 * r.M10 + l.M22 * r.M20 + l.M23 * r.M30;
		o.M21 = l.M20 * r.M01 + l.M21 * r.M11 + l.M22 * r.M21 + l.M23 * r.M31;
		o.M22 = l.M20 * r.M02 + l.M21 * r.M12 + l.M22 * r.M22 + l.M23 * r.M32;
		o.M23 = l.M20 * r.M03 + l.M21 * r.M13 + l.M22 * r.M23 + l.M23 * r.M33;
		
		o.M30 = l.M30 * r.M00 + l.M31 * r.M10 + l.M32 * r.M20 + l.M33 * r.M30;
		o.M31 = l.M30 * r.M01 + l.M31 * r.M11 + l.M32 * r.M21 + l.M33 * r.M31;
		o.M32 = l.M30 * r.M02 + l.M31 * r.M12 + l.M32 * r.M22 + l.M33 * r.M32;
		o.M33 = l.M30 * r.M03 + l.M31 * r.M13 + l.M32 * r.M23 + l.M33 * r.M33;
		
		return o;
	}

	[Benchmark, BenchmarkCategory("Multiply")]
	public Mat4 Multiply_SIMD()
	{
		var l = _M1;
		ref var lData = ref l.M00;
		var r = _M2;
		var r0 = Vector128.Create(r.Row0.X, r.Row0.Y, r.Row0.Z, r.Row0.W);
		var r1 = Vector128.Create(r.Row1.X, r.Row1.Y, r.Row1.Z, r.Row1.W);
		var r2 = Vector128.Create(r.Row2.X, r.Row2.Y, r.Row2.Z, r.Row2.W);
		var r3 = Vector128.Create(r.Row3.X, r.Row3.Y, r.Row3.Z, r.Row3.W);
		Unsafe.SkipInit(out Mat4 o);

		for (var off = 0; off < 16; off += 4) {
			Unsafe.As<float, RowVec>(ref Unsafe.Add(ref o.M00, off)) = 
				(r0 * Unsafe.Add(ref lData, off + 0)) + 
				(r1 * Unsafe.Add(ref lData, off + 1)) + 
				(r2 * Unsafe.Add(ref lData, off + 2)) + 
				(r3 * Unsafe.Add(ref lData, off + 3));
		}

		return o;
	}

	[Benchmark, BenchmarkCategory("Multiply")]
	public Mat4 Multiply_Implementation()
	{
		var l = _M1;
		var r = _M2;
		l.Multiply(in r, out var o);
		return o;
	}

	
	[Benchmark(Baseline = true), BenchmarkCategory("Invert")]
	public Mat4 Invert_Scalar()
	{
		const double DET_TOLERANCE = 1e-5;

		ref var l = ref Unsafe.AsRef(in _M1);

		// Solution based on Laplace Expansion, found at https://stackoverflow.com/a/9614511
		// Use doubles to minimize rounding errors, which can have a large effect in this calculation
		double
			s0 = (double)l.M00 * l.M11 - (double)l.M10 * l.M01, s1 = (double)l.M00 * l.M12 - (double)l.M10 * l.M02,
			s2 = (double)l.M00 * l.M13 - (double)l.M10 * l.M03, s3 = (double)l.M01 * l.M12 - (double)l.M11 * l.M02,
			s4 = (double)l.M01 * l.M13 - (double)l.M11 * l.M03, s5 = (double)l.M02 * l.M13 - (double)l.M12 * l.M03,
			c5 = (double)l.M22 * l.M33 - (double)l.M32 * l.M23, c4 = (double)l.M21 * l.M33 - (double)l.M31 * l.M23,
			c3 = (double)l.M21 * l.M32 - (double)l.M31 * l.M22, c2 = (double)l.M20 * l.M33 - (double)l.M30 * l.M23,
			c1 = (double)l.M20 * l.M32 - (double)l.M30 * l.M22, c0 = (double)l.M20 * l.M31 - (double)l.M30 * l.M21;

		var det = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;
		if (det.ApproxZero(DET_TOLERANCE)) throw new("Bad benchmark");
		var iDet = 1 / det;

		Unsafe.SkipInit(out Mat4 o);
		o.M00 = (float)(( l.M11 * c5 - l.M12 * c4 + l.M13 * c3) * iDet);
		o.M01 = (float)((-l.M01 * c5 + l.M02 * c4 - l.M03 * c3) * iDet);
		o.M02 = (float)(( l.M31 * s5 - l.M32 * s4 + l.M33 * s3) * iDet);
		o.M03 = (float)((-l.M21 * s5 + l.M22 * s4 - l.M23 * s3) * iDet);
		o.M10 = (float)((-l.M10 * c5 + l.M12 * c2 - l.M13 * c1) * iDet);
		o.M11 = (float)(( l.M00 * c5 - l.M02 * c2 + l.M03 * c1) * iDet);
		o.M12 = (float)((-l.M30 * s5 + l.M32 * s2 - l.M33 * s1) * iDet);
		o.M13 = (float)(( l.M20 * s5 - l.M22 * s2 + l.M23 * s1) * iDet);
		o.M20 = (float)(( l.M10 * c4 - l.M11 * c2 + l.M13 * c0) * iDet);
		o.M21 = (float)((-l.M00 * c4 + l.M01 * c2 - l.M03 * c0) * iDet);
		o.M22 = (float)(( l.M30 * s4 - l.M31 * s4 + l.M33 * s0) * iDet);
		o.M23 = (float)((-l.M20 * s4 + l.M21 * s2 - l.M23 * s0) * iDet);
		o.M30 = (float)((-l.M10 * c3 + l.M11 * c1 - l.M12 * c0) * iDet);
		o.M31 = (float)(( l.M00 * c3 - l.M01 * c1 + l.M02 * c0) * iDet);
		o.M32 = (float)((-l.M30 * s3 + l.M31 * s1 - l.M32 * s0) * iDet);
		o.M33 = (float)(( l.M20 * s3 - l.M21 * s1 + l.M22 * s0) * iDet);
		return o;
	}

	[Benchmark, BenchmarkCategory("Invert")]
	public Mat4 Invert_Implementation()
	{
		var l = _M1;
		l.Invert(out var o);
		return o;
	}
}
