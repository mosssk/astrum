﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System.Linq;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Running;

namespace Astrum.Benchmarks;


// Base type for test fixtures that run benchmarks instead of unit tests
public abstract class BenchmarkFixture
{
	protected void RunBenchmarks()
	{
		var cfg = ManualConfig.CreateMinimumViable()
			.WithOptions(ConfigOptions.DisableOptimizationsValidator);
		cfg.UnionRule = ConfigUnionRule.AlwaysUseGlobal;
		
		var summary = BenchmarkRunner.Run(GetType(), cfg);
		var badReports = summary.Reports.Where(static r => !r.Success).ToArray();
		Assert.That(summary.HasCriticalValidationErrors, Is.False);
		Assert.That(badReports, Is.Empty);
	}
}
