﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Linq;
// ReSharper disable InconsistentNaming

namespace Astrum.Maths;


// Tests for Angle
public sealed class AngleTests
{
	private static readonly Angle[] Angle_ARR = new float[] { -180, -90, -45, 0, 30, 60, 120 }.Select(Angle.Deg).ToArray();

	[Test]
	public void Angle_Ctor()
	{
		Assert.That(new Angle().Radians, Is.Zero);
		Assert.That(Angle.Rad(MathF.PI).Radians, Is.EqualTo(MathF.PI));
		Assert.That(Angle.Rad(-MathF.PI).Radians, Is.EqualTo(-MathF.PI));
		Assert.That(Angle.Deg(45).Radians, Is.EqualTo(MathF.PI / 4));
		Assert.That(Angle.Deg(-45).Radians, Is.EqualTo(-MathF.PI / 4));
	}

	[Test]
	public void Angle_Fields()
	{
		foreach (var ang in Angle_ARR) {
			Assert.That(ang.Sin, Is.EqualTo(MathF.Sin(ang.Radians)));
			Assert.That(ang.Cos, Is.EqualTo(MathF.Cos(ang.Radians)));
			Assert.That(ang.Tan, Is.EqualTo(MathF.Tan(ang.Radians)));
			var (s, c) = ang.SinCos;
			Assert.That(s.FastApprox(MathF.Sin(ang.Radians), 1e-4f), Is.True);
			Assert.That(c.FastApprox(MathF.Cos(ang.Radians), 1e-4f), Is.True);
		}
		Assert.That(Angle.Deg(359).Normalized.Degrees,  Is.EqualTo(359).Within(1e-4f));
		Assert.That(Angle.Deg(360).Normalized.Degrees,  Is.EqualTo(0).Within(1e-4f));
		Assert.That(Angle.Deg(500).Normalized.Degrees,  Is.EqualTo(140).Within(1e-4f));
		Assert.That(Angle.Deg(-359).Normalized.Degrees, Is.EqualTo(-359).Within(1e-4f));
		Assert.That(Angle.Deg(-360).Normalized.Degrees, Is.EqualTo(-0).Within(1e-4f));
		Assert.That(Angle.Deg(-500).Normalized.Degrees, Is.EqualTo(-140).Within(1e-4f));
	}

	[Test]
	public void Angle_Equality()
	{
		foreach (var a1 in Angle_ARR) {
			foreach (var a2 in Angle_ARR) {
				Assert.That(a1 == a2, Is.EqualTo(a1.Radians.FastApprox(a2.Radians)));
				Assert.That(a1 != a2, Is.EqualTo(!a1.Radians.FastApprox(a2.Radians)));
				Assert.That(a1.Equals(a2), Is.EqualTo(a1.Radians.FastApprox(a2.Radians)));
				Assert.That(a1.Equals((object)a2), Is.EqualTo(a1.Radians.FastApprox(a2.Radians)));
			}
		}
	}

	[Test]
	public void Angle_Operators()
	{
		foreach (var a1 in Angle_ARR) {
			foreach (var a2 in Angle_ARR) {
				Assert.That(a1 == a2, Is.EqualTo(a1.Equals(a2)));
				Assert.That(a1 != a2, Is.EqualTo(!a1.Equals(a2)));
				Assert.That(a1 <= a2, Is.EqualTo(a1.Radians <= a2.Radians));
				Assert.That(a1 <  a2, Is.EqualTo(a1.Radians <  a2.Radians));
				Assert.That(a1 >= a2, Is.EqualTo(a1.Radians >= a2.Radians));
				Assert.That(a1 >  a2, Is.EqualTo(a1.Radians >  a2.Radians));
				Assert.That((a1 + a2).Radians, Is.EqualTo(a1.Radians + a2.Radians));
				Assert.That((a1 - a2).Radians, Is.EqualTo(a1.Radians - a2.Radians));
				Assert.That((a1 * a2.Radians).Radians, Is.EqualTo(a1.Radians * a2.Radians));
				Assert.That((a1 / a2.Radians).Radians, Is.EqualTo(a1.Radians / a2.Radians));
				Assert.That((a1 % a2).Radians, Is.EqualTo(a1.Radians % a2.Radians));
				Assert.That((-a1).Radians, Is.EqualTo(-a1.Radians));
			}
		}
	}

	[Test]
	public void Angle_MathMethods()
	{
		var min = Angle.Deg(-40);
		var max = Angle.Deg(40);
		foreach (var ang in Angle_ARR) {
			Assert.That(Angle.Clamp(ang, min, max).Radians, Is.EqualTo(Math.Clamp(ang.Radians, min.Radians, max.Radians)));
			Assert.That(Angle.Min(ang, min).Radians, Is.EqualTo(Math.Min(ang.Radians, min.Radians)));
			Assert.That(Angle.Max(ang, max).Radians, Is.EqualTo(Math.Max(ang.Radians, max.Radians)));
		}
	}

	private static readonly AngleD[] AngleD_ARR = new double[] { -180, -90, -45, 0, 30, 60, 120 }.Select(AngleD.Deg).ToArray();

	[Test]
	public void AngleD_Ctor()
	{
		Assert.That(new AngleD().Radians, Is.Zero);
		Assert.That(AngleD.Rad(Math.PI).Radians, Is.EqualTo(Math.PI));
		Assert.That(AngleD.Rad(-Math.PI).Radians, Is.EqualTo(-Math.PI));
		Assert.That(AngleD.Deg(45).Radians, Is.EqualTo(Math.PI / 4));
		Assert.That(AngleD.Deg(-45).Radians, Is.EqualTo(-Math.PI / 4));
	}

	[Test]
	public void AngleD_Fields()
	{
		foreach (var ang in AngleD_ARR) {
			Assert.That(ang.Sin, Is.EqualTo(Math.Sin(ang.Radians)));
			Assert.That(ang.Cos, Is.EqualTo(Math.Cos(ang.Radians)));
			Assert.That(ang.Tan, Is.EqualTo(Math.Tan(ang.Radians)));
			var (s, c) = ang.SinCos;
			Assert.That(s.FastApprox(Math.Sin(ang.Radians), 1e-4f), Is.True);
			Assert.That(c.FastApprox(Math.Cos(ang.Radians), 1e-4f), Is.True);
		}
		Assert.That(AngleD.Deg(359).Normalized.Degrees,  Is.EqualTo(359).Within(1e-4f));
		Assert.That(AngleD.Deg(360).Normalized.Degrees,  Is.EqualTo(0).Within(1e-4f));
		Assert.That(AngleD.Deg(500).Normalized.Degrees,  Is.EqualTo(140).Within(1e-4f));
		Assert.That(AngleD.Deg(-359).Normalized.Degrees, Is.EqualTo(-359).Within(1e-4f));
		Assert.That(AngleD.Deg(-360).Normalized.Degrees, Is.EqualTo(-0).Within(1e-4f));
		Assert.That(AngleD.Deg(-500).Normalized.Degrees, Is.EqualTo(-140).Within(1e-4f));
	}

	[Test]
	public void AngleD_Equality()
	{
		foreach (var a1 in AngleD_ARR) {
			foreach (var a2 in AngleD_ARR) {
				Assert.That(a1 == a2, Is.EqualTo(a1.Radians.FastApprox(a2.Radians)));
				Assert.That(a1 != a2, Is.EqualTo(!a1.Radians.FastApprox(a2.Radians)));
				Assert.That(a1.Equals(a2), Is.EqualTo(a1.Radians.FastApprox(a2.Radians)));
				Assert.That(a1.Equals((object)a2), Is.EqualTo(a1.Radians.FastApprox(a2.Radians)));
			}
		}
	}

	[Test]
	public void AngleD_Operators()
	{
		foreach (var a1 in AngleD_ARR) {
			foreach (var a2 in AngleD_ARR) {
				Assert.That(a1 == a2, Is.EqualTo(a1.Equals(a2)));
				Assert.That(a1 != a2, Is.EqualTo(!a1.Equals(a2)));
				Assert.That(a1 <= a2, Is.EqualTo(a1.Radians <= a2.Radians));
				Assert.That(a1 <  a2, Is.EqualTo(a1.Radians <  a2.Radians));
				Assert.That(a1 >= a2, Is.EqualTo(a1.Radians >= a2.Radians));
				Assert.That(a1 >  a2, Is.EqualTo(a1.Radians >  a2.Radians));
				Assert.That((a1 + a2).Radians, Is.EqualTo(a1.Radians + a2.Radians));
				Assert.That((a1 - a2).Radians, Is.EqualTo(a1.Radians - a2.Radians));
				Assert.That((a1 * a2.Radians).Radians, Is.EqualTo(a1.Radians * a2.Radians));
				Assert.That((a1 / a2.Radians).Radians, Is.EqualTo(a1.Radians / a2.Radians));
				Assert.That((a1 % a2).Radians, Is.EqualTo(a1.Radians % a2.Radians));
				Assert.That((-a1).Radians, Is.EqualTo(-a1.Radians));
			}
		}
	}

	[Test]
	public void AngleD_MathMethods()
	{
		var min = AngleD.Deg(-40);
		var max = AngleD.Deg(40);
		foreach (var ang in AngleD_ARR) {
			Assert.That(AngleD.Clamp(ang, min, max).Radians, Is.EqualTo(Math.Clamp(ang.Radians, min.Radians, max.Radians)));
			Assert.That(AngleD.Min(ang, min).Radians, Is.EqualTo(Math.Min(ang.Radians, min.Radians)));
			Assert.That(AngleD.Max(ang, max).Radians, Is.EqualTo(Math.Max(ang.Radians, max.Radians)));
		}
	}


	[Test]
	public void Casting()
	{
		var a1 = Angle.Deg(60);
		var a2 = AngleD.Deg(80);
		Assert.That(((AngleD)a1).Radians, Is.EqualTo((double)a1.Radians));
		Assert.That(((Angle)a2).Radians, Is.EqualTo((float)a2.Radians));
	}
}
