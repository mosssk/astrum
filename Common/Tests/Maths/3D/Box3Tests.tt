﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */
<#@ template language="C#" #>
<#@ import namespace="System.Linq" #>
<#
	(string Name, string CornerField, string ScalarField, string CenterField, string SizeType, string SizeScalar)[] Types_ = [
		("Box3",  "Point3",  "int",    "Vec3",  "Extent3",  "uint"),
		("Box3L", "Point3L", "long",   "Vec3D", "Extent3L", "ulong"),
		("Box3F", "Vec3",    "float",  "Vec3",  "Vec3",     "float"),
		("Box3D", "Vec3D",   "double", "Vec3D", "Vec3D",    "double")
	];
	(string Name, string Scalar)[] Points_ = [
		("Point3",  "int"),
		("Point3L", "long"),
		("Vec3",    "float"),
		("Vec3D",   "double")
	];
#>

using System.Linq;

namespace Astrum.Maths;


// Tests for Box3
public sealed class Box3Tests
{
<# foreach (var (N_, CF_, SF_, Cen_, SzN_, _) in Types_) { #>
	
	[Test]
	public void <#=N_#>_Ctor()
	{
		Assert.That((_,_,_,_,_,_) = new <#=N_#>(), Is.EqualTo((0, 0, 0, 0, 0, 0)));
		Assert.That((_,_,_,_,_,_) = new <#=N_#>(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new <#=N_#>(4, 5, 6, 1, 2, 3), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new <#=N_#>(new(1, 2, 3), new(4, 5, 6)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new <#=N_#>(new(4, 5, 6), new(1, 2, 3)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
	}

	[Test]
	public void <#=N_#>_Constants() 
	{
		Assert.That((_,_,_,_,_,_) = <#=N_#>.Empty, Is.EqualTo((0,0,0,0,0,0)));
		Assert.That((_,_,_,_,_,_) = <#=N_#>.Unit, Is.EqualTo((0,0,0,1,1,1)));
	}

	[Test]
	public void <#=N_#>_Fields()
	{
		var b = new <#=N_#>(1, 3, 4, 6, 9, 11);
		Assert.That(b.Min, Is.EqualTo(new <#=CF_#>(1, 3, 4)));
		Assert.That(b.Max, Is.EqualTo(new <#=CF_#>(6, 9, 11)));
		Assert.That(b.Size, Is.EqualTo(new <#=SzN_#>(5, 6, 7)));
		Assert.That(b.Width, Is.EqualTo(5));
		Assert.That(b.Height, Is.EqualTo(6));
		Assert.That(b.Depth, Is.EqualTo(7));
		Assert.That(b.Center, Is.EqualTo(new <#=Cen_#>(3.5f, 6f, 7.5f)));
		Assert.That(b.Volume, Is.EqualTo(210));
		Assert.That(b.IsEmpty, Is.False);
		Assert.That(<#=N_#>.Empty.IsEmpty, Is.True);
	}

	[Test]
	public void <#=N_#>_Deconstruct()
	{
		Assert.That((_,_,_,_,_,_) = new <#=N_#>(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_) = new <#=N_#>(1, 2, 3, 4, 5, 6), Is.EqualTo((new <#=CF_#>(1, 2, 3), new <#=CF_#>(4, 5, 6))));
	}

	[Test]
	public void <#=N_#>_Equality()
	{
		<#=N_#>[] boxes = [ 
			new(1, 0, 0, 0, 0, 0), new(0, 1, 0, 0, 0, 0), new(0, 0, 1, 0, 0, 0), 
			new(0, 0, 0, 1, 0, 0), new(0, 0, 0, 0, 1, 0), new(0, 0, 0, 0, 0, 1), 
			new(3, 4, 5, 6, 7, 8) 
		];
		foreach (var b1 in boxes) {
			foreach (var b2 in boxes) {
				var same = b1.Min == b2.Min && b1.Max == b2.Max;
				Assert.That(b1 == b2, Is.EqualTo(same));
				Assert.That(b1 != b2, Is.EqualTo(!same));
				Assert.That(b1.Equals(b2), Is.EqualTo(same));
				Assert.That(b1.Equals((object)b2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void <#=N_#>_Transform()
	{
		var b = new <#=N_#>(2, 3, 5, 7, 8, 10);
		Assert.That((_,_,_,_,_,_) = b.Translated(new(-2, 4, 3)), Is.EqualTo((0, 7, 8, 5, 12, 13)));
		Assert.That((_,_,_,_,_,_) = b.Inflated(new(-1, 3, 4)), Is.EqualTo((3, 0, 1, 6, 11, 14)));

		b = new(1, 1, 1, 3, 3, 3);
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(2, 3, 4), new(0, 0, 0)), Is.EqualTo((2, 3, 4, 6, 9, 12)));
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(4, 3, 2), new(3, 3, 3)), Is.EqualTo((-5, -3, -1, 3, 3, 3)));
	}

	[Test]
	public void <#=N_#>_Union()
	{
		<#=N_#> b1 = new(1, 1, 1, 2, 2, 2), b2 = new(3, 3, 3, 5, 5, 5);
		Assert.That((_,_,_,_,_,_) = <#=N_#>.Union(b1, b2), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		Assert.That((_,_,_,_,_,_) = <#=N_#>.Union(b2, b1), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		b1 = new(2, 2, 2, 6, 6, 6);
		Assert.That((_,_,_,_,_,_) = <#=N_#>.Union(b1, b2), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That((_,_,_,_,_,_) = <#=N_#>.Union(b2, b1), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That(<#=N_#>.Union(<#=N_#>.Empty, <#=N_#>.Empty), Is.EqualTo(<#=N_#>.Empty));
	}

	[Test]
	public void <#=N_#>_Intersect()
	{
		<#=N_#> b1 = new(1, 1, 1, 3, 3, 3), b2 = new(2, 2, 2, 4, 4, 4);
		Assert.That((_,_,_,_,_,_) = <#=N_#>.Intersect(b1, b2), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		Assert.That((_,_,_,_,_,_) = <#=N_#>.Intersect(b2, b1), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		b1 = new(0, 0, 0, 1, 1, 1);
		Assert.That((_,_,_,_,_,_) = <#=N_#>.Intersect(b1, b2), Is.EqualTo((0, 0, 0, 0, 0, 0)));
	}

	[Test]
	public void <#=N_#>_Casting()
	{
<# foreach (var o in Types_.Where(o => o.Name != N_)) { #>
		Assert.That((_,_,_,_,_,_) = (<#=N_#>)new <#=o.Name#>(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((<#=SF_#>)2, (<#=SF_#>)4, (<#=SF_#>)5, (<#=SF_#>)7, (<#=SF_#>)8, (<#=SF_#>)9)));
<# } #>
	}

<# } #>
}


// Tests for Box3 in Space3D
public sealed partial class Space3DTests
{
	private static readonly (int X, int Y, int Z)[] _Coords = (
		from x in Enumerable.Range(0, 5)
		from y in Enumerable.Range(0, 5)
		from z in Enumerable.Range(0, 5)
		select (x, y, z)
	).ToArray();

<# foreach (var (N_, _, _, _, _, _) in Types_) { #>
	
	[Test]
	public void <#=N_#>_Contains()
	{
		var box = new <#=N_#>(1, 1, 1, 3, 3, 3);
		foreach (var (x, y, z) in _Coords) {
			var contains = x >= box.Min.X && x <= box.Max.X && y >= box.Min.Y && y <= box.Max.Y &&
				z >= box.Min.Z && z <= box.Max.Z;
<# foreach (var (P_, _) in Points_) { #>
			Assert.That(box.Contains(new <#=P_#>(x, y, z)), Is.EqualTo(contains));
<# } #>
		}
	}

<# } #>
}
