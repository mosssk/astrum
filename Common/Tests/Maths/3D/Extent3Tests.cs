﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

namespace Astrum.Maths;


// Tests for Extent3
public sealed class Extent3Tests
{

	[Test]
	public void Extent3_Ctor()
	{
		Assert.That((_,_,_) = new Extent3(), Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = new Extent3(1, 2, 4), Is.EqualTo((1, 2, 4)));
	}

	[Test]
	public void Extent3_Fields()
	{
		var e = new Extent3(3, 6, 8);
		Assert.That(e.X, Is.EqualTo(3));
		Assert.That(e.Y, Is.EqualTo(6));
		Assert.That(e.Z, Is.EqualTo(8));
		Assert.That(e.Volume, Is.EqualTo(144));
		Assert.That(e.IsEmpty, Is.False);
	}

	[Test]
	public void Extent3_Constants()
	{
		Assert.That(Extent3.Zero.X, Is.EqualTo(0));
		Assert.That(Extent3.Zero.Y, Is.EqualTo(0));
		Assert.That(Extent3.Zero.Z, Is.EqualTo(0));
		Assert.That(Extent3.Zero.IsEmpty, Is.True);
	}

	[Test]
	public void Extent3_Equality()
	{
		Extent3[] exts = [ new(), new(1, 0, 0), new(0, 1, 0), new(0, 0, 1), new(1, 1, 1) ];
		foreach (var e1 in exts) {
			foreach (var e2 in exts) {
				var same = e1.X == e2.X && e1.Y == e2.Y && e1.Z == e2.Z;
				Assert.That(e1.Equals(e2), Is.EqualTo(same));
				Assert.That(e1.Equals((object)e2), Is.EqualTo(same));
				Assert.That(e1 == e2, Is.EqualTo(same));
				Assert.That(e1 != e2, Is.EqualTo(!same));
			}
		}
	}


	[Test]
	public void Extent3L_Ctor()
	{
		Assert.That((_,_,_) = new Extent3L(), Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = new Extent3L(1, 2, 4), Is.EqualTo((1, 2, 4)));
	}

	[Test]
	public void Extent3L_Fields()
	{
		var e = new Extent3L(3, 6, 8);
		Assert.That(e.X, Is.EqualTo(3));
		Assert.That(e.Y, Is.EqualTo(6));
		Assert.That(e.Z, Is.EqualTo(8));
		Assert.That(e.Volume, Is.EqualTo(144));
		Assert.That(e.IsEmpty, Is.False);
	}

	[Test]
	public void Extent3L_Constants()
	{
		Assert.That(Extent3L.Zero.X, Is.EqualTo(0));
		Assert.That(Extent3L.Zero.Y, Is.EqualTo(0));
		Assert.That(Extent3L.Zero.Z, Is.EqualTo(0));
		Assert.That(Extent3L.Zero.IsEmpty, Is.True);
	}

	[Test]
	public void Extent3L_Equality()
	{
		Extent3L[] exts = [ new(), new(1, 0, 0), new(0, 1, 0), new(0, 0, 1), new(1, 1, 1) ];
		foreach (var e1 in exts) {
			foreach (var e2 in exts) {
				var same = e1.X == e2.X && e1.Y == e2.Y && e1.Z == e2.Z;
				Assert.That(e1.Equals(e2), Is.EqualTo(same));
				Assert.That(e1.Equals((object)e2), Is.EqualTo(same));
				Assert.That(e1 == e2, Is.EqualTo(same));
				Assert.That(e1 != e2, Is.EqualTo(!same));
			}
		}
	}


	[Test]
	public void Extent3_Casting()
	{
		Assert.That((_, _, _) = (Extent3L)new Extent3(1, 2, 3), Is.EqualTo((1, 2, 3)));
		Assert.That((_, _, _) = (Extent3)new Extent3L(3, 4, 5), Is.EqualTo((3, 4, 5)));
	}
}
