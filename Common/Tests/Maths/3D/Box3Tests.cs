﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System.Linq;

namespace Astrum.Maths;


// Tests for Box3
public sealed class Box3Tests
{
	
	[Test]
	public void Box3_Ctor()
	{
		Assert.That((_,_,_,_,_,_) = new Box3(), Is.EqualTo((0, 0, 0, 0, 0, 0)));
		Assert.That((_,_,_,_,_,_) = new Box3(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3(4, 5, 6, 1, 2, 3), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3(new(1, 2, 3), new(4, 5, 6)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3(new(4, 5, 6), new(1, 2, 3)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
	}

	[Test]
	public void Box3_Constants() 
	{
		Assert.That((_,_,_,_,_,_) = Box3.Empty, Is.EqualTo((0,0,0,0,0,0)));
		Assert.That((_,_,_,_,_,_) = Box3.Unit, Is.EqualTo((0,0,0,1,1,1)));
	}

	[Test]
	public void Box3_Fields()
	{
		var b = new Box3(1, 3, 4, 6, 9, 11);
		Assert.That(b.Min, Is.EqualTo(new Point3(1, 3, 4)));
		Assert.That(b.Max, Is.EqualTo(new Point3(6, 9, 11)));
		Assert.That(b.Size, Is.EqualTo(new Extent3(5, 6, 7)));
		Assert.That(b.Width, Is.EqualTo(5));
		Assert.That(b.Height, Is.EqualTo(6));
		Assert.That(b.Depth, Is.EqualTo(7));
		Assert.That(b.Center, Is.EqualTo(new Vec3(3.5f, 6f, 7.5f)));
		Assert.That(b.Volume, Is.EqualTo(210));
		Assert.That(b.IsEmpty, Is.False);
		Assert.That(Box3.Empty.IsEmpty, Is.True);
	}

	[Test]
	public void Box3_Deconstruct()
	{
		Assert.That((_,_,_,_,_,_) = new Box3(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_) = new Box3(1, 2, 3, 4, 5, 6), Is.EqualTo((new Point3(1, 2, 3), new Point3(4, 5, 6))));
	}

	[Test]
	public void Box3_Equality()
	{
		Box3[] boxes = [ 
			new(1, 0, 0, 0, 0, 0), new(0, 1, 0, 0, 0, 0), new(0, 0, 1, 0, 0, 0), 
			new(0, 0, 0, 1, 0, 0), new(0, 0, 0, 0, 1, 0), new(0, 0, 0, 0, 0, 1), 
			new(3, 4, 5, 6, 7, 8) 
		];
		foreach (var b1 in boxes) {
			foreach (var b2 in boxes) {
				var same = b1.Min == b2.Min && b1.Max == b2.Max;
				Assert.That(b1 == b2, Is.EqualTo(same));
				Assert.That(b1 != b2, Is.EqualTo(!same));
				Assert.That(b1.Equals(b2), Is.EqualTo(same));
				Assert.That(b1.Equals((object)b2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void Box3_Transform()
	{
		var b = new Box3(2, 3, 5, 7, 8, 10);
		Assert.That((_,_,_,_,_,_) = b.Translated(new(-2, 4, 3)), Is.EqualTo((0, 7, 8, 5, 12, 13)));
		Assert.That((_,_,_,_,_,_) = b.Inflated(new(-1, 3, 4)), Is.EqualTo((3, 0, 1, 6, 11, 14)));

		b = new(1, 1, 1, 3, 3, 3);
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(2, 3, 4), new(0, 0, 0)), Is.EqualTo((2, 3, 4, 6, 9, 12)));
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(4, 3, 2), new(3, 3, 3)), Is.EqualTo((-5, -3, -1, 3, 3, 3)));
	}

	[Test]
	public void Box3_Union()
	{
		Box3 b1 = new(1, 1, 1, 2, 2, 2), b2 = new(3, 3, 3, 5, 5, 5);
		Assert.That((_,_,_,_,_,_) = Box3.Union(b1, b2), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		Assert.That((_,_,_,_,_,_) = Box3.Union(b2, b1), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		b1 = new(2, 2, 2, 6, 6, 6);
		Assert.That((_,_,_,_,_,_) = Box3.Union(b1, b2), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That((_,_,_,_,_,_) = Box3.Union(b2, b1), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That(Box3.Union(Box3.Empty, Box3.Empty), Is.EqualTo(Box3.Empty));
	}

	[Test]
	public void Box3_Intersect()
	{
		Box3 b1 = new(1, 1, 1, 3, 3, 3), b2 = new(2, 2, 2, 4, 4, 4);
		Assert.That((_,_,_,_,_,_) = Box3.Intersect(b1, b2), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		Assert.That((_,_,_,_,_,_) = Box3.Intersect(b2, b1), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		b1 = new(0, 0, 0, 1, 1, 1);
		Assert.That((_,_,_,_,_,_) = Box3.Intersect(b1, b2), Is.EqualTo((0, 0, 0, 0, 0, 0)));
	}

	[Test]
	public void Box3_Casting()
	{
		Assert.That((_,_,_,_,_,_) = (Box3)new Box3L(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((int)2, (int)4, (int)5, (int)7, (int)8, (int)9)));
		Assert.That((_,_,_,_,_,_) = (Box3)new Box3F(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((int)2, (int)4, (int)5, (int)7, (int)8, (int)9)));
		Assert.That((_,_,_,_,_,_) = (Box3)new Box3D(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((int)2, (int)4, (int)5, (int)7, (int)8, (int)9)));
	}

	
	[Test]
	public void Box3L_Ctor()
	{
		Assert.That((_,_,_,_,_,_) = new Box3L(), Is.EqualTo((0, 0, 0, 0, 0, 0)));
		Assert.That((_,_,_,_,_,_) = new Box3L(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3L(4, 5, 6, 1, 2, 3), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3L(new(1, 2, 3), new(4, 5, 6)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3L(new(4, 5, 6), new(1, 2, 3)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
	}

	[Test]
	public void Box3L_Constants() 
	{
		Assert.That((_,_,_,_,_,_) = Box3L.Empty, Is.EqualTo((0,0,0,0,0,0)));
		Assert.That((_,_,_,_,_,_) = Box3L.Unit, Is.EqualTo((0,0,0,1,1,1)));
	}

	[Test]
	public void Box3L_Fields()
	{
		var b = new Box3L(1, 3, 4, 6, 9, 11);
		Assert.That(b.Min, Is.EqualTo(new Point3L(1, 3, 4)));
		Assert.That(b.Max, Is.EqualTo(new Point3L(6, 9, 11)));
		Assert.That(b.Size, Is.EqualTo(new Extent3L(5, 6, 7)));
		Assert.That(b.Width, Is.EqualTo(5));
		Assert.That(b.Height, Is.EqualTo(6));
		Assert.That(b.Depth, Is.EqualTo(7));
		Assert.That(b.Center, Is.EqualTo(new Vec3D(3.5f, 6f, 7.5f)));
		Assert.That(b.Volume, Is.EqualTo(210));
		Assert.That(b.IsEmpty, Is.False);
		Assert.That(Box3L.Empty.IsEmpty, Is.True);
	}

	[Test]
	public void Box3L_Deconstruct()
	{
		Assert.That((_,_,_,_,_,_) = new Box3L(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_) = new Box3L(1, 2, 3, 4, 5, 6), Is.EqualTo((new Point3L(1, 2, 3), new Point3L(4, 5, 6))));
	}

	[Test]
	public void Box3L_Equality()
	{
		Box3L[] boxes = [ 
			new(1, 0, 0, 0, 0, 0), new(0, 1, 0, 0, 0, 0), new(0, 0, 1, 0, 0, 0), 
			new(0, 0, 0, 1, 0, 0), new(0, 0, 0, 0, 1, 0), new(0, 0, 0, 0, 0, 1), 
			new(3, 4, 5, 6, 7, 8) 
		];
		foreach (var b1 in boxes) {
			foreach (var b2 in boxes) {
				var same = b1.Min == b2.Min && b1.Max == b2.Max;
				Assert.That(b1 == b2, Is.EqualTo(same));
				Assert.That(b1 != b2, Is.EqualTo(!same));
				Assert.That(b1.Equals(b2), Is.EqualTo(same));
				Assert.That(b1.Equals((object)b2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void Box3L_Transform()
	{
		var b = new Box3L(2, 3, 5, 7, 8, 10);
		Assert.That((_,_,_,_,_,_) = b.Translated(new(-2, 4, 3)), Is.EqualTo((0, 7, 8, 5, 12, 13)));
		Assert.That((_,_,_,_,_,_) = b.Inflated(new(-1, 3, 4)), Is.EqualTo((3, 0, 1, 6, 11, 14)));

		b = new(1, 1, 1, 3, 3, 3);
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(2, 3, 4), new(0, 0, 0)), Is.EqualTo((2, 3, 4, 6, 9, 12)));
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(4, 3, 2), new(3, 3, 3)), Is.EqualTo((-5, -3, -1, 3, 3, 3)));
	}

	[Test]
	public void Box3L_Union()
	{
		Box3L b1 = new(1, 1, 1, 2, 2, 2), b2 = new(3, 3, 3, 5, 5, 5);
		Assert.That((_,_,_,_,_,_) = Box3L.Union(b1, b2), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		Assert.That((_,_,_,_,_,_) = Box3L.Union(b2, b1), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		b1 = new(2, 2, 2, 6, 6, 6);
		Assert.That((_,_,_,_,_,_) = Box3L.Union(b1, b2), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That((_,_,_,_,_,_) = Box3L.Union(b2, b1), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That(Box3L.Union(Box3L.Empty, Box3L.Empty), Is.EqualTo(Box3L.Empty));
	}

	[Test]
	public void Box3L_Intersect()
	{
		Box3L b1 = new(1, 1, 1, 3, 3, 3), b2 = new(2, 2, 2, 4, 4, 4);
		Assert.That((_,_,_,_,_,_) = Box3L.Intersect(b1, b2), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		Assert.That((_,_,_,_,_,_) = Box3L.Intersect(b2, b1), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		b1 = new(0, 0, 0, 1, 1, 1);
		Assert.That((_,_,_,_,_,_) = Box3L.Intersect(b1, b2), Is.EqualTo((0, 0, 0, 0, 0, 0)));
	}

	[Test]
	public void Box3L_Casting()
	{
		Assert.That((_,_,_,_,_,_) = (Box3L)new Box3(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((long)2, (long)4, (long)5, (long)7, (long)8, (long)9)));
		Assert.That((_,_,_,_,_,_) = (Box3L)new Box3F(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((long)2, (long)4, (long)5, (long)7, (long)8, (long)9)));
		Assert.That((_,_,_,_,_,_) = (Box3L)new Box3D(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((long)2, (long)4, (long)5, (long)7, (long)8, (long)9)));
	}

	
	[Test]
	public void Box3F_Ctor()
	{
		Assert.That((_,_,_,_,_,_) = new Box3F(), Is.EqualTo((0, 0, 0, 0, 0, 0)));
		Assert.That((_,_,_,_,_,_) = new Box3F(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3F(4, 5, 6, 1, 2, 3), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3F(new(1, 2, 3), new(4, 5, 6)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3F(new(4, 5, 6), new(1, 2, 3)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
	}

	[Test]
	public void Box3F_Constants() 
	{
		Assert.That((_,_,_,_,_,_) = Box3F.Empty, Is.EqualTo((0,0,0,0,0,0)));
		Assert.That((_,_,_,_,_,_) = Box3F.Unit, Is.EqualTo((0,0,0,1,1,1)));
	}

	[Test]
	public void Box3F_Fields()
	{
		var b = new Box3F(1, 3, 4, 6, 9, 11);
		Assert.That(b.Min, Is.EqualTo(new Vec3(1, 3, 4)));
		Assert.That(b.Max, Is.EqualTo(new Vec3(6, 9, 11)));
		Assert.That(b.Size, Is.EqualTo(new Vec3(5, 6, 7)));
		Assert.That(b.Width, Is.EqualTo(5));
		Assert.That(b.Height, Is.EqualTo(6));
		Assert.That(b.Depth, Is.EqualTo(7));
		Assert.That(b.Center, Is.EqualTo(new Vec3(3.5f, 6f, 7.5f)));
		Assert.That(b.Volume, Is.EqualTo(210));
		Assert.That(b.IsEmpty, Is.False);
		Assert.That(Box3F.Empty.IsEmpty, Is.True);
	}

	[Test]
	public void Box3F_Deconstruct()
	{
		Assert.That((_,_,_,_,_,_) = new Box3F(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_) = new Box3F(1, 2, 3, 4, 5, 6), Is.EqualTo((new Vec3(1, 2, 3), new Vec3(4, 5, 6))));
	}

	[Test]
	public void Box3F_Equality()
	{
		Box3F[] boxes = [ 
			new(1, 0, 0, 0, 0, 0), new(0, 1, 0, 0, 0, 0), new(0, 0, 1, 0, 0, 0), 
			new(0, 0, 0, 1, 0, 0), new(0, 0, 0, 0, 1, 0), new(0, 0, 0, 0, 0, 1), 
			new(3, 4, 5, 6, 7, 8) 
		];
		foreach (var b1 in boxes) {
			foreach (var b2 in boxes) {
				var same = b1.Min == b2.Min && b1.Max == b2.Max;
				Assert.That(b1 == b2, Is.EqualTo(same));
				Assert.That(b1 != b2, Is.EqualTo(!same));
				Assert.That(b1.Equals(b2), Is.EqualTo(same));
				Assert.That(b1.Equals((object)b2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void Box3F_Transform()
	{
		var b = new Box3F(2, 3, 5, 7, 8, 10);
		Assert.That((_,_,_,_,_,_) = b.Translated(new(-2, 4, 3)), Is.EqualTo((0, 7, 8, 5, 12, 13)));
		Assert.That((_,_,_,_,_,_) = b.Inflated(new(-1, 3, 4)), Is.EqualTo((3, 0, 1, 6, 11, 14)));

		b = new(1, 1, 1, 3, 3, 3);
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(2, 3, 4), new(0, 0, 0)), Is.EqualTo((2, 3, 4, 6, 9, 12)));
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(4, 3, 2), new(3, 3, 3)), Is.EqualTo((-5, -3, -1, 3, 3, 3)));
	}

	[Test]
	public void Box3F_Union()
	{
		Box3F b1 = new(1, 1, 1, 2, 2, 2), b2 = new(3, 3, 3, 5, 5, 5);
		Assert.That((_,_,_,_,_,_) = Box3F.Union(b1, b2), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		Assert.That((_,_,_,_,_,_) = Box3F.Union(b2, b1), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		b1 = new(2, 2, 2, 6, 6, 6);
		Assert.That((_,_,_,_,_,_) = Box3F.Union(b1, b2), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That((_,_,_,_,_,_) = Box3F.Union(b2, b1), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That(Box3F.Union(Box3F.Empty, Box3F.Empty), Is.EqualTo(Box3F.Empty));
	}

	[Test]
	public void Box3F_Intersect()
	{
		Box3F b1 = new(1, 1, 1, 3, 3, 3), b2 = new(2, 2, 2, 4, 4, 4);
		Assert.That((_,_,_,_,_,_) = Box3F.Intersect(b1, b2), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		Assert.That((_,_,_,_,_,_) = Box3F.Intersect(b2, b1), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		b1 = new(0, 0, 0, 1, 1, 1);
		Assert.That((_,_,_,_,_,_) = Box3F.Intersect(b1, b2), Is.EqualTo((0, 0, 0, 0, 0, 0)));
	}

	[Test]
	public void Box3F_Casting()
	{
		Assert.That((_,_,_,_,_,_) = (Box3F)new Box3(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((float)2, (float)4, (float)5, (float)7, (float)8, (float)9)));
		Assert.That((_,_,_,_,_,_) = (Box3F)new Box3L(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((float)2, (float)4, (float)5, (float)7, (float)8, (float)9)));
		Assert.That((_,_,_,_,_,_) = (Box3F)new Box3D(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((float)2, (float)4, (float)5, (float)7, (float)8, (float)9)));
	}

	
	[Test]
	public void Box3D_Ctor()
	{
		Assert.That((_,_,_,_,_,_) = new Box3D(), Is.EqualTo((0, 0, 0, 0, 0, 0)));
		Assert.That((_,_,_,_,_,_) = new Box3D(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3D(4, 5, 6, 1, 2, 3), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3D(new(1, 2, 3), new(4, 5, 6)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_,_,_,_,_) = new Box3D(new(4, 5, 6), new(1, 2, 3)), Is.EqualTo((1, 2, 3, 4, 5, 6)));
	}

	[Test]
	public void Box3D_Constants() 
	{
		Assert.That((_,_,_,_,_,_) = Box3D.Empty, Is.EqualTo((0,0,0,0,0,0)));
		Assert.That((_,_,_,_,_,_) = Box3D.Unit, Is.EqualTo((0,0,0,1,1,1)));
	}

	[Test]
	public void Box3D_Fields()
	{
		var b = new Box3D(1, 3, 4, 6, 9, 11);
		Assert.That(b.Min, Is.EqualTo(new Vec3D(1, 3, 4)));
		Assert.That(b.Max, Is.EqualTo(new Vec3D(6, 9, 11)));
		Assert.That(b.Size, Is.EqualTo(new Vec3D(5, 6, 7)));
		Assert.That(b.Width, Is.EqualTo(5));
		Assert.That(b.Height, Is.EqualTo(6));
		Assert.That(b.Depth, Is.EqualTo(7));
		Assert.That(b.Center, Is.EqualTo(new Vec3D(3.5f, 6f, 7.5f)));
		Assert.That(b.Volume, Is.EqualTo(210));
		Assert.That(b.IsEmpty, Is.False);
		Assert.That(Box3D.Empty.IsEmpty, Is.True);
	}

	[Test]
	public void Box3D_Deconstruct()
	{
		Assert.That((_,_,_,_,_,_) = new Box3D(1, 2, 3, 4, 5, 6), Is.EqualTo((1, 2, 3, 4, 5, 6)));
		Assert.That((_,_) = new Box3D(1, 2, 3, 4, 5, 6), Is.EqualTo((new Vec3D(1, 2, 3), new Vec3D(4, 5, 6))));
	}

	[Test]
	public void Box3D_Equality()
	{
		Box3D[] boxes = [ 
			new(1, 0, 0, 0, 0, 0), new(0, 1, 0, 0, 0, 0), new(0, 0, 1, 0, 0, 0), 
			new(0, 0, 0, 1, 0, 0), new(0, 0, 0, 0, 1, 0), new(0, 0, 0, 0, 0, 1), 
			new(3, 4, 5, 6, 7, 8) 
		];
		foreach (var b1 in boxes) {
			foreach (var b2 in boxes) {
				var same = b1.Min == b2.Min && b1.Max == b2.Max;
				Assert.That(b1 == b2, Is.EqualTo(same));
				Assert.That(b1 != b2, Is.EqualTo(!same));
				Assert.That(b1.Equals(b2), Is.EqualTo(same));
				Assert.That(b1.Equals((object)b2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void Box3D_Transform()
	{
		var b = new Box3D(2, 3, 5, 7, 8, 10);
		Assert.That((_,_,_,_,_,_) = b.Translated(new(-2, 4, 3)), Is.EqualTo((0, 7, 8, 5, 12, 13)));
		Assert.That((_,_,_,_,_,_) = b.Inflated(new(-1, 3, 4)), Is.EqualTo((3, 0, 1, 6, 11, 14)));

		b = new(1, 1, 1, 3, 3, 3);
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(2, 3, 4), new(0, 0, 0)), Is.EqualTo((2, 3, 4, 6, 9, 12)));
		Assert.That((_,_,_,_,_,_) = b.Scaled(new(4, 3, 2), new(3, 3, 3)), Is.EqualTo((-5, -3, -1, 3, 3, 3)));
	}

	[Test]
	public void Box3D_Union()
	{
		Box3D b1 = new(1, 1, 1, 2, 2, 2), b2 = new(3, 3, 3, 5, 5, 5);
		Assert.That((_,_,_,_,_,_) = Box3D.Union(b1, b2), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		Assert.That((_,_,_,_,_,_) = Box3D.Union(b2, b1), Is.EqualTo((1, 1, 1, 5, 5, 5)));
		b1 = new(2, 2, 2, 6, 6, 6);
		Assert.That((_,_,_,_,_,_) = Box3D.Union(b1, b2), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That((_,_,_,_,_,_) = Box3D.Union(b2, b1), Is.EqualTo((2, 2, 2, 6, 6, 6)));
		Assert.That(Box3D.Union(Box3D.Empty, Box3D.Empty), Is.EqualTo(Box3D.Empty));
	}

	[Test]
	public void Box3D_Intersect()
	{
		Box3D b1 = new(1, 1, 1, 3, 3, 3), b2 = new(2, 2, 2, 4, 4, 4);
		Assert.That((_,_,_,_,_,_) = Box3D.Intersect(b1, b2), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		Assert.That((_,_,_,_,_,_) = Box3D.Intersect(b2, b1), Is.EqualTo((2, 2, 2, 3, 3, 3)));
		b1 = new(0, 0, 0, 1, 1, 1);
		Assert.That((_,_,_,_,_,_) = Box3D.Intersect(b1, b2), Is.EqualTo((0, 0, 0, 0, 0, 0)));
	}

	[Test]
	public void Box3D_Casting()
	{
		Assert.That((_,_,_,_,_,_) = (Box3D)new Box3(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((double)2, (double)4, (double)5, (double)7, (double)8, (double)9)));
		Assert.That((_,_,_,_,_,_) = (Box3D)new Box3L(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((double)2, (double)4, (double)5, (double)7, (double)8, (double)9)));
		Assert.That((_,_,_,_,_,_) = (Box3D)new Box3F(2, 4, 5, 7, 8, 9), 
			Is.EqualTo(((double)2, (double)4, (double)5, (double)7, (double)8, (double)9)));
	}

}


// Tests for Box3 in Space3D
public sealed partial class Space3DTests
{
	private static readonly (int X, int Y, int Z)[] _Coords = (
		from x in Enumerable.Range(0, 5)
		from y in Enumerable.Range(0, 5)
		from z in Enumerable.Range(0, 5)
		select (x, y, z)
	).ToArray();

	
	[Test]
	public void Box3_Contains()
	{
		var box = new Box3(1, 1, 1, 3, 3, 3);
		foreach (var (x, y, z) in _Coords) {
			var contains = x >= box.Min.X && x <= box.Max.X && y >= box.Min.Y && y <= box.Max.Y &&
				z >= box.Min.Z && z <= box.Max.Z;
			Assert.That(box.Contains(new Point3(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Point3L(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec3(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec3D(x, y, z)), Is.EqualTo(contains));
		}
	}

	
	[Test]
	public void Box3L_Contains()
	{
		var box = new Box3L(1, 1, 1, 3, 3, 3);
		foreach (var (x, y, z) in _Coords) {
			var contains = x >= box.Min.X && x <= box.Max.X && y >= box.Min.Y && y <= box.Max.Y &&
				z >= box.Min.Z && z <= box.Max.Z;
			Assert.That(box.Contains(new Point3(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Point3L(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec3(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec3D(x, y, z)), Is.EqualTo(contains));
		}
	}

	
	[Test]
	public void Box3F_Contains()
	{
		var box = new Box3F(1, 1, 1, 3, 3, 3);
		foreach (var (x, y, z) in _Coords) {
			var contains = x >= box.Min.X && x <= box.Max.X && y >= box.Min.Y && y <= box.Max.Y &&
				z >= box.Min.Z && z <= box.Max.Z;
			Assert.That(box.Contains(new Point3(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Point3L(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec3(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec3D(x, y, z)), Is.EqualTo(contains));
		}
	}

	
	[Test]
	public void Box3D_Contains()
	{
		var box = new Box3D(1, 1, 1, 3, 3, 3);
		foreach (var (x, y, z) in _Coords) {
			var contains = x >= box.Min.X && x <= box.Max.X && y >= box.Min.Y && y <= box.Max.Y &&
				z >= box.Min.Z && z <= box.Max.Z;
			Assert.That(box.Contains(new Point3(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Point3L(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec3(x, y, z)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec3D(x, y, z)), Is.EqualTo(contains));
		}
	}

}
