﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Maths;


// Tests for Mat4
public sealed class Mat4Tests
{
	public static readonly Mat4 M0 = new();
	public static readonly Mat4 M1 = new(
		1,  2,  3,  4,
		5,  6,  7,  8,
		9,  10, 11, 12,
		13, 14, 15, 16
	);
	public static readonly Mat4 M2 = new(
		new( 2,  4,  6,  8),
		new( 1,  3,  5,  7),
		new(-2, -4, -6, -8),
		new(-1, -3, -5, -7)
	);
	
	
	[Test]
	public unsafe void Mat4_Layout()
	{
		Mat4 m = new();
		Assert.That(sizeof(Mat4), Is.EqualTo(64));
#pragma warning disable NUnit2010
		Assert.True(&m.M00 == (byte*)&m + 0);
		Assert.True(&m.M01 == (byte*)&m + 4);
		Assert.True(&m.M02 == (byte*)&m + 8);
		Assert.True(&m.M03 == (byte*)&m + 12);
		Assert.True(&m.M10 == (byte*)&m + 16);
		Assert.True(&m.M11 == (byte*)&m + 20);
		Assert.True(&m.M12 == (byte*)&m + 24);
		Assert.True(&m.M13 == (byte*)&m + 28);
		Assert.True(&m.M20 == (byte*)&m + 32);
		Assert.True(&m.M21 == (byte*)&m + 36);
		Assert.True(&m.M22 == (byte*)&m + 40);
		Assert.True(&m.M23 == (byte*)&m + 44);
		Assert.True(&m.M30 == (byte*)&m + 48);
		Assert.True(&m.M31 == (byte*)&m + 52);
		Assert.True(&m.M32 == (byte*)&m + 56);
		Assert.True(&m.M33 == (byte*)&m + 60);
#pragma warning restore NUnit2010
	}
	
	[Test]
	public void Mat4_Constants()
	{
		var I = Mat4.Identity;
		Assert.That(I.Row0, Is.EqualTo(Vec4.UnitX));
		Assert.That(I.Row1, Is.EqualTo(Vec4.UnitY));
		Assert.That(I.Row2, Is.EqualTo(Vec4.UnitZ));
		Assert.That(I.Row3, Is.EqualTo(Vec4.UnitW));
		var z = Mat4.Zero;
		Assert.That(z.Row0, Is.EqualTo(Vec4.Zero));
		Assert.That(z.Row1, Is.EqualTo(Vec4.Zero));
		Assert.That(z.Row2, Is.EqualTo(Vec4.Zero));
		Assert.That(z.Row3, Is.EqualTo(Vec4.Zero));
	}

	[Test]
	public void Mat4_Ctor()
	{
		Assert.That(M0.Row0, Is.EqualTo(Vec4.Zero));
		Assert.That(M0.Row1, Is.EqualTo(Vec4.Zero));
		Assert.That(M0.Row2, Is.EqualTo(Vec4.Zero));
		Assert.That(M0.Row3, Is.EqualTo(Vec4.Zero));

		Assert.That(M1.Row0, Is.EqualTo(new Vec4(1,  2,  3,  4)));
		Assert.That(M1.Row1, Is.EqualTo(new Vec4(5,  6,  7,  8)));
		Assert.That(M1.Row2, Is.EqualTo(new Vec4(9,  10, 11, 12)));
		Assert.That(M1.Row3, Is.EqualTo(new Vec4(13, 14, 15, 16)));

		Assert.That(M2.Row0, Is.EqualTo(new Vec4( 2,  4,  6,  8)));
		Assert.That(M2.Row1, Is.EqualTo(new Vec4( 1,  3,  5,  7)));
		Assert.That(M2.Row2, Is.EqualTo(new Vec4(-2, -4, -6, -8)));
		Assert.That(M2.Row3, Is.EqualTo(new Vec4(-1, -3, -5, -7)));
	}

	[Test]
	public void Mat4_Columns()
	{
		Assert.That(M2.Col0, Is.EqualTo(new Vec4(2, 1, -2, -1)));
		Assert.That(M2.Col1, Is.EqualTo(new Vec4(4, 3, -4, -3)));
		Assert.That(M2.Col2, Is.EqualTo(new Vec4(6, 5, -6, -5)));
		Assert.That(M2.Col3, Is.EqualTo(new Vec4(8, 7, -8, -7)));

		var m = new Mat4 {
			Col0 = new(9,  8,   7,  6),
			Col1 = new(5,  4,   3,  2),
			Col2 = new(1,  0,  -1, -2),
			Col3 = new(-3, -4, -5, -6)
		};
		Assert.That(m.Row0, Is.EqualTo(new Vec4(9, 5, 1,  -3)));
		Assert.That(m.Row1, Is.EqualTo(new Vec4(8, 4, 0,  -4)));
		Assert.That(m.Row2, Is.EqualTo(new Vec4(7, 3, -1, -5)));
		Assert.That(m.Row3, Is.EqualTo(new Vec4(6, 2, -2, -6)));
	}

	[Test]
	public void Mat4_Indexers()
	{
		Assert.Throws<ArgumentOutOfRangeException>(() => _ = M0[-1]);
		Assert.Throws<ArgumentOutOfRangeException>(() => _ = M0[16]);
		Assert.Throws<ArgumentOutOfRangeException>(() => _ = M0[4, 3]);
		Assert.Throws<ArgumentOutOfRangeException>(() => _ = M0[3, 4]);
		Assert.Throws<ArgumentOutOfRangeException>(() => _ = M0[-1, 0]);
		Assert.Throws<ArgumentOutOfRangeException>(() => _ = M0[0, -1]);

		Assert.That(M1[0], Is.EqualTo(1));
		Assert.That(M1[1], Is.EqualTo(2));
		Assert.That(M1[2], Is.EqualTo(3));
		Assert.That(M1[3], Is.EqualTo(4));

		Assert.That((M1[0, 0], M1[0, 1], M1[0, 2], M1[0, 3]), Is.EqualTo((1,  2,  3,  4 )));
		Assert.That((M1[1, 0], M1[1, 1], M1[1, 2], M1[1, 3]), Is.EqualTo((5,  6,  7,  8 )));
		Assert.That((M1[2, 0], M1[2, 1], M1[2, 2], M1[2, 3]), Is.EqualTo((9,  10, 11, 12)));
		Assert.That((M1[3, 0], M1[3, 1], M1[3, 2], M1[3, 3]), Is.EqualTo((13, 14, 15, 16)));

		var m = new Mat4 {
			[0] = 9,
			[1] = 8,
			[2] = 7,
			[3] = 6
		};
		Assert.That(m.Row0, Is.EqualTo(new Vec4(9, 8, 7, 6)));
		Assert.That(m.Row1, Is.EqualTo(Vec4.Zero));
		Assert.That(m.Row2, Is.EqualTo(Vec4.Zero));
		Assert.That(m.Row3, Is.EqualTo(Vec4.Zero));

		var m2 = new Mat4 {
			[0, 0] =  9, [0, 1] =  8, [0, 2] =  7, [0, 3] =  6,
			[1, 0] =  5, [1, 1] =  4, [1, 2] =  3, [1, 3] =  2,
			[2, 0] =  1, [2, 1] =  0, [2, 2] = -1, [2, 3] = -2,
			[3, 0] = -3, [3, 1] = -4, [3, 2] = -5, [3, 3] = -6
		};
		Assert.That(m2.Row0, Is.EqualTo(new Vec4(9,  8,  7,   6)));
		Assert.That(m2.Row1, Is.EqualTo(new Vec4(5,  4,  3,   2)));
		Assert.That(m2.Row2, Is.EqualTo(new Vec4(1,  0,  -1, -2)));
		Assert.That(m2.Row3, Is.EqualTo(new Vec4(-3, -4, -5, -6)));
	}

	[Test]
	public void Mat4_Trace()
	{
		Assert.That(Mat4.Identity.Trace, Is.EqualTo(4));
		Assert.That(M1.Trace, Is.EqualTo(34));
		Assert.That(M2.Trace, Is.EqualTo(-8));
	}

	[Test]
	public void Mat4_WorldFields()
	{
		Assert.That(M1.Right,    Is.EqualTo(new Vec3(1, 2, 3)));
		Assert.That(M1.Left,     Is.EqualTo(-new Vec3(1, 2, 3)));
		Assert.That(M1.Up,       Is.EqualTo(new Vec3(5, 6, 7)));
		Assert.That(M1.Down,     Is.EqualTo(-new Vec3(5, 6, 7)));
		Assert.That(M1.Forward,  Is.EqualTo(-new Vec3(9, 10, 11)));
		Assert.That(M1.Backward, Is.EqualTo(new Vec3(9, 10, 11)));
		Assert.That(M1.Translation, Is.EqualTo(new Vec3(13, 14, 15)));

		var m = Mat4.Zero;
		m.Translation = new(2, 5, 9);
		Assert.That(m.Translation, Is.EqualTo(new Vec3(2, 5, 9)));
	}
	
	[Test]
	public void Mat4_Deconstruct()
	{
		var (r0, r1, r2, r3) = M1;
		Assert.That(r0, Is.EqualTo(new Vec4(1,  2,  3,  4)));
		Assert.That(r1, Is.EqualTo(new Vec4(5,  6,  7,  8)));
		Assert.That(r2, Is.EqualTo(new Vec4(9,  10, 11, 12)));
		Assert.That(r3, Is.EqualTo(new Vec4(13, 14, 15, 16)));
	}

	[Test]
	public void Mat4_Equality()
	{
#pragma warning disable CS1718
		Assert.True(M2 == M2);
		Assert.True(M2.Equals(M2));
		Assert.True(M2.Equals(in M2));
		Assert.True(M2.Equals((object)M2));
		Assert.True(M2.ApproxEqual(in M2));

		Assert.False(M2 == M1);
		Assert.False(M2.Equals(M1));
		Assert.False(M2.Equals(in M1));
		Assert.False(M2.Equals((object)M1));
		Assert.False(M2.ApproxEqual(in M1));

		Assert.True(new Mat4().ApproxZero());
		Assert.True(Mat4.Identity.ApproxIdentity());
		Assert.False(M1.ApproxZero());
		Assert.False(M1.ApproxIdentity());
#pragma warning restore CS1718
	}
	
	[Test]
	public void Mat4_Add()
	{
		var exp = new Mat4(
			M1.M00 + M2.M00, M1.M01 + M2.M01, M1.M02 + M2.M02, M1.M03 + M2.M03,	
			M1.M10 + M2.M10, M1.M11 + M2.M11, M1.M12 + M2.M12, M1.M13 + M2.M13,
			M1.M20 + M2.M20, M1.M21 + M2.M21, M1.M22 + M2.M22, M1.M23 + M2.M23,
			M1.M30 + M2.M30, M1.M31 + M2.M31, M1.M32 + M2.M32, M1.M33 + M2.M33
		);
		Assert.That(M1 + M2, Is.EqualTo(exp));
		Assert.That(M1.Add(in M2), Is.EqualTo(exp));
		M1.Add(in M2, out var o);
		Assert.That(o, Is.EqualTo(exp));

		exp = new(
			M1.M00 + 5, M1.M01 + 5, M1.M02 + 5, M1.M03 + 5,	
			M1.M10 + 5, M1.M11 + 5, M1.M12 + 5, M1.M13 + 5,
			M1.M20 + 5, M1.M21 + 5, M1.M22 + 5, M1.M23 + 5,
			M1.M30 + 5, M1.M31 + 5, M1.M32 + 5, M1.M33 + 5
		);
		Assert.That(M1 + 5, Is.EqualTo(exp));
		Assert.That(M1.Add(5), Is.EqualTo(exp));
		M1.Add(5, out o);
		Assert.That(o, Is.EqualTo(exp));
	}

	[Test]
	public void Mat4_Subtract()
	{
		var exp = new Mat4(
			M1.M00 - M2.M00, M1.M01 - M2.M01, M1.M02 - M2.M02, M1.M03 - M2.M03,	
			M1.M10 - M2.M10, M1.M11 - M2.M11, M1.M12 - M2.M12, M1.M13 - M2.M13,
			M1.M20 - M2.M20, M1.M21 - M2.M21, M1.M22 - M2.M22, M1.M23 - M2.M23,
			M1.M30 - M2.M30, M1.M31 - M2.M31, M1.M32 - M2.M32, M1.M33 - M2.M33
		);
		Assert.That(M1 - M2, Is.EqualTo(exp));
		Assert.That(M1.Subtract(in M2), Is.EqualTo(exp));
		M1.Subtract(in M2, out var o);
		Assert.That(o, Is.EqualTo(exp));

		exp = new(
			M1.M00 - 5, M1.M01 - 5, M1.M02 - 5, M1.M03 - 5,	
			M1.M10 - 5, M1.M11 - 5, M1.M12 - 5, M1.M13 - 5,
			M1.M20 - 5, M1.M21 - 5, M1.M22 - 5, M1.M23 - 5,
			M1.M30 - 5, M1.M31 - 5, M1.M32 - 5, M1.M33 - 5
		);
		Assert.That(M1 - 5, Is.EqualTo(exp));
		Assert.That(M1.Subtract(5), Is.EqualTo(exp));
		M1.Subtract(5, out o);
		Assert.That(o, Is.EqualTo(exp));
	}

	[Test]
	public void Mat4_Multiply()
	{
		var m3 = new Mat4(
			1,7,4,5,
			9,2,7,1,
			3,9,1,6,
			2,6,3,5
		);
		var exp = new Mat4(
			 36,  62,  33,  45,	
			 96, 158,  93, 113,
			156, 254, 153, 181,
			216, 350, 213, 249
		);
		Assert.That(M1 * m3, Is.EqualTo(exp));
		Assert.That(M1.Multiply(in m3), Is.EqualTo(exp));
		M1.Multiply(in m3, out var o);
		Assert.That(o, Is.EqualTo(exp));

		exp = new(
			M1.M00 * 2, M1.M01 * 2, M1.M02 * 2, M1.M03 * 2,	
			M1.M10 * 2, M1.M11 * 2, M1.M12 * 2, M1.M13 * 2,
			M1.M20 * 2, M1.M21 * 2, M1.M22 * 2, M1.M23 * 2,
			M1.M30 * 2, M1.M31 * 2, M1.M32 * 2, M1.M33 * 2
		);
		Assert.That(M1 * 2, Is.EqualTo(exp));
		Assert.That(M1.Multiply(2), Is.EqualTo(exp));
		M1.Multiply(2, out o);
		Assert.That(o, Is.EqualTo(exp));
	}

	[Test]
	public void Mat4_Divide()
	{
		var exp = new Mat4(
			M1.M00 / 2, M1.M01 / 2, M1.M02 / 2, M1.M03 / 2,	
			M1.M10 / 2, M1.M11 / 2, M1.M12 / 2, M1.M13 / 2,
			M1.M20 / 2, M1.M21 / 2, M1.M22 / 2, M1.M23 / 2,
			M1.M30 / 2, M1.M31 / 2, M1.M32 / 2, M1.M33 / 2
		);
		Assert.That(M1 / 2, Is.EqualTo(exp));
		Assert.That(M1.Divide(2), Is.EqualTo(exp));
		M1.Divide(2, out var o);
		Assert.That(o, Is.EqualTo(exp));
	}

	[Test]
	public void Mat4_MatCompMulDiv()
	{
		var exp = new Mat4(
			M1.M00 * M2.M00, M1.M01 * M2.M01, M1.M02 * M2.M02, M1.M03 * M2.M03,	
			M1.M10 * M2.M10, M1.M11 * M2.M11, M1.M12 * M2.M12, M1.M13 * M2.M13,
			M1.M20 * M2.M20, M1.M21 * M2.M21, M1.M22 * M2.M22, M1.M23 * M2.M23,
			M1.M30 * M2.M30, M1.M31 * M2.M31, M1.M32 * M2.M32, M1.M33 * M2.M33
		);
		Assert.That(M1.MatCompMul(in M2), Is.EqualTo(exp));
		M1.MatCompMul(in M2, out var o);
		Assert.That(o, Is.EqualTo(exp));
		
		exp = new(
			M1.M00 / M2.M00, M1.M01 / M2.M01, M1.M02 / M2.M02, M1.M03 / M2.M03,	
			M1.M10 / M2.M10, M1.M11 / M2.M11, M1.M12 / M2.M12, M1.M13 / M2.M13,
			M1.M20 / M2.M20, M1.M21 / M2.M21, M1.M22 / M2.M22, M1.M23 / M2.M23,
			M1.M30 / M2.M30, M1.M31 / M2.M31, M1.M32 / M2.M32, M1.M33 / M2.M33
		);
		Assert.That(M1.MatCompDiv(in M2), Is.EqualTo(exp));
		M1.MatCompDiv(in M2, out o);
		Assert.That(o, Is.EqualTo(exp));
	}

	[Test]
	public void Mat4_Transform()
	{
		var vec4 = new Vec4(1, 6, 4, 9);
		var exp4 = new Vec4(184, 204, 224, 244);
		Assert.That(M1 * vec4, Is.EqualTo(exp4));
		Assert.That(M1.Transform(vec4), Is.EqualTo(exp4));
		M1.Transform(vec4, out var o4);
		Assert.That(o4, Is.EqualTo(exp4));
		
		var vec3 = new Vec3(5, 7, 3);
		var exp3 = new Vec3(80, 96, 112);
		Assert.That(M1 * vec3, Is.EqualTo(exp3));
		Assert.That(M1.Transform(vec3), Is.EqualTo(exp3));
		M1.Transform(vec3, out var o3);
		Assert.That(o3, Is.EqualTo(exp3));
		
		vec3 = new(5, 7, 3);
		exp3 = new(67, 82, 97);
		Assert.That(M1.TransformNormal(vec3), Is.EqualTo(exp3));
		M1.TransformNormal(vec3, out o3);
		Assert.That(o3, Is.EqualTo(exp3));
	}
	
	[Test]
	public void Mat4_TransformDouble()
	{
		var vec4 = new Vec4D(1, 6, 4, 9);
		var exp4 = new Vec4D(184, 204, 224, 244);
		Assert.That(M1 * vec4, Is.EqualTo(exp4));
		Assert.That(M1.Transform(vec4), Is.EqualTo(exp4));
		M1.Transform(vec4, out var o4);
		Assert.That(o4, Is.EqualTo(exp4));
		
		var vec3 = new Vec3D(5, 7, 3);
		var exp3 = new Vec3D(80, 96, 112);
		Assert.That(M1 * vec3, Is.EqualTo(exp3));
		Assert.That(M1.Transform(vec3), Is.EqualTo(exp3));
		M1.Transform(vec3, out var o3);
		Assert.That(o3, Is.EqualTo(exp3));
	}
	
	[Test]
	public void Mat4_Invert()
	{
		var inv = Mat4.Identity;
		inv.M33 = 0;
		Assert.True(inv.Invert().ApproxIdentity());

		var m = new Mat4(
			1, 3, 6, 2,
			8, 4, 6, 2,
			9, 1, 7, 4,
			6, 8, 1, 5
		);
		m.Invert(out inv);
		inv = m.Multiply(in inv);
		Assert.True(inv.ApproxIdentity(1e-5f));
	}

	[Test]
	public void Mat4_Determinant()
	{
		Mat4
			m0 = new(
				8, 4, 7, 5,
				1, 2, 9, 8,
				3, 5, 1, 7,
				2, 6, 8, 4
			),
			m1 = new(
				-2,  5, -4, -7,
				4, -2,  4,  7,
				1,  8, -7, -4,
				-6, -4,  3,  7
			);
		Assert.That(m0.Determinant(), Is.EqualTo(2372));
		Assert.That(m1.Determinant(), Is.EqualTo(831));
	}

	[Test]
	public void Mat4_WorldRotationMatrices()
	{
		Vec3 rotStart = new(1, 1, 1);
		var rvx = Mat4.RotationX(Angle.PiO2).Transform(rotStart);
		var rvy = Mat4.RotationY(Angle.PiO2).Transform(rotStart);
		var rvz = Mat4.RotationZ(Angle.PiO2).Transform(rotStart);
		Assert.True(rvx.ApproxEqual(new( 1, -1,  1), 1e-5f));
		Assert.True(rvy.ApproxEqual(new( 1,  1, -1), 1e-5f));
		Assert.True(rvz.ApproxEqual(new(-1,  1,  1), 1e-5f));
	}

#pragma warning disable CS9193 // Argument should be a variable because it is passed to a 'ref readonly' parameter
	[Test]
	public void Mat4_RotationAxisMatrix()
	{
		Assert.True(Mat4.RotationX(Angle.PiO4).ApproxEqual(Mat4.RotationAxis(Vec3.UnitX, Angle.PiO4), 1e-5f));
		Assert.True(Mat4.RotationY(Angle.PiO4).ApproxEqual(Mat4.RotationAxis(Vec3.UnitY, Angle.PiO4), 1e-5f));
		Assert.True(Mat4.RotationZ(Angle.PiO4).ApproxEqual(Mat4.RotationAxis(Vec3.UnitZ, Angle.PiO4), 1e-5f));
	}

	[Test]
	public void Mat4_YawPitchRollMatrix()
	{
		Assert.True(Mat4.RotationX(Angle.PiO4).ApproxEqual(Mat4.YawPitchRoll(new(), Angle.PiO4, new()), 1e-5f));
		Assert.True(Mat4.RotationY(Angle.PiO4).ApproxEqual(Mat4.YawPitchRoll(Angle.PiO4, new(), new()), 1e-5f));
		Assert.True(Mat4.RotationZ(Angle.PiO4).ApproxEqual(Mat4.YawPitchRoll(new(), new(), Angle.PiO4), 1e-5f));
	}
#pragma warning restore CS9193 // Argument should be a variable because it is passed to a 'ref readonly' parameter

	[Test]
	public void Mat4_ScaleMatrix()
	{
		Mat4.Scale(5, out var m1);
		Mat4.Scale(3, 4, 5, out var m2);

		// Scaling matrices are defined as [[x 0 0] [0 y 0] [0 0 z]]
		Mat4
			e1 = new(5, 0, 0, 0,   0, 5, 0, 0,   0, 0, 5, 0,   0, 0, 0, 1),
			e2 = new(3, 0, 0, 0,   0, 4, 0, 0,   0, 0, 5, 0,   0, 0, 0, 1);
		Assert.True(m1.ApproxEqual(in e1));
		Assert.True(m2.ApproxEqual(in e2));
	}

	[Test]
	public void Mat4_TranslateMatrix()
	{
		Mat4.Translate(1, 2, 3, out var m1);
		Mat4.Translate(new(4, 5, 6), out var m2);

		// Translation matrices are identity matrices with [x, y, z] at indices [12, 13, 14]
		Mat4
			e1 = new(1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  1, 2, 3, 1),
			e2 = new(1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  4, 5, 6, 1);
		Assert.True(m1.ApproxEqual(in e1));
		Assert.True(m2.ApproxEqual(in e2));
	}
	
	[Test]
	public void Mat4_WorldMatrix()
	{
		Vec3
			pos = new(10, 11, 12),
			fwd = new(1, 1, 1),
			up = new(0, 1, 0),
			right = new(-1, 0, 1),
			trueup = right.Cross(fwd);
		Mat4.World(pos, fwd, up, out var m);

		Assert.True(m.Forward.ApproxEqual(fwd.Normalized));
		Assert.True(m.Right.ApproxEqual(right.Normalized));
		Assert.True(m.Up.ApproxEqual(trueup.Normalized));
		Assert.True(m.Translation.ApproxEqual(new(10, 11, 12)));
	}

	[Test]
	public void Mat4_BillboardMatrix()
	{
		Vec3
			opos = new(1, 2, 3),
			vpos = new(2, 3, 4),
			up = new(0, 1, 0),
			right = new(-1, 0, 1),
			trueup = right.Cross(vpos - opos);
		Mat4.Billboard(opos, vpos, up, out var m);

		Assert.True(m.Forward.ApproxEqual((vpos - opos).Normalized));
		Assert.True(m.Right.ApproxEqual(right.Normalized));
		Assert.True(m.Up.ApproxEqual(trueup.Normalized));
		Assert.True(m.Translation.ApproxEqual(new(1, 2, 3)));
	}

	[Test]
	public void Mat4_AsNumeric()
	{
		var m = new Mat4(1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 18);
		ref readonly var mRef = ref m.AsNumeric();
		ref var mRef2 = ref m.AsNumericMutable();
		
		Assert.That(mRef.M11,  Is.EqualTo(m.M00));
		Assert.That(mRef.M21,  Is.EqualTo(m.M10));
		Assert.That(mRef2.M11, Is.EqualTo(m.M00));
		Assert.That(mRef2.M21, Is.EqualTo(m.M10));
		m.M00 = -1;
		Assert.That(mRef.M11, Is.EqualTo(-1));
		Assert.That(mRef2.M11, Is.EqualTo(-1));
		mRef2.M21 = -10;
		Assert.That(m.M10, Is.EqualTo(-10));
		Assert.That(mRef.M21, Is.EqualTo(-10));
	}
}
