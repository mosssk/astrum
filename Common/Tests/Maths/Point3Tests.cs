﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Maths;


// Tests for Point3
public sealed class Point3Tests
{

	[Test]
	public void Point3_Ctor()
	{
		Assert.That((_,_,_) = new Point3(), Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = new Point3(5), Is.EqualTo((5, 5, 5)));
		Assert.That((_,_,_) = new Point3(-1, 1, 0), Is.EqualTo((-1, 1, 0)));
		Assert.That((_,_,_) = new Point3(new(-2, -1), 10), Is.EqualTo((-2, -1, 10)));
	}

	[Test]
	public void Point3_Fields()
	{
		var v = new Point3(10, 15, 20);
		Assert.That(v.X, Is.EqualTo(10));
		Assert.That(v.Y, Is.EqualTo(15));
		Assert.That(v.Z, Is.EqualTo(20));
		Assert.That(v.Length, Is.EqualTo(Single.Sqrt(10*10 + 15*15 + 20*20)));
		Assert.That(v.LengthSq, Is.EqualTo(10*10 + 15*15 + 20*20));
	}

	[Test]
	public void Point3_Constants()
	{
		Assert.That((_,_,_) = Point3.Zero, Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = Point3.One, Is.EqualTo((1, 1, 1)));
		Assert.That((_,_,_) = Point3.UnitX, Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Point3.UnitY, Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Point3.UnitZ, Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Point3.Right, Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Point3.Up, Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Point3.Backward, Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Point3.Left, Is.EqualTo((-1, 0, 0)));
		Assert.That((_,_,_) = Point3.Down, Is.EqualTo((0, -1, 0)));
		Assert.That((_,_,_) = Point3.Forward, Is.EqualTo((0, 0, -1)));
	}

	[Test]
	public void Point3_Equality()
	{
		Point3[] points = [ new(1, 0, 0), new(0, 1, 0), new(0, 0, 1), new(10, 10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X == p2.X && p1.Y == p2.Y && p1.Z == p2.Z;
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y, p1.Z <= p2.Z)));
				Assert.That((_,_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y, p1.Z <  p2.Z)));
				Assert.That((_,_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y, p1.Z >= p2.Z)));
				Assert.That((_,_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y, p1.Z >  p2.Z)));
			}
		}
	}

	[Test]
	public void Point3_Operators()
	{
		Point3 p1 = new(2, 8, 9), p2 = new(6, 3, 1);

		Assert.That((_,_,_) = p1 + p2, Is.EqualTo((8, 11, 10)));
		Assert.That((_,_,_) = p1 + 10, Is.EqualTo((12, 18, 19)));
		Assert.That((_,_,_) = p1 - p2, Is.EqualTo((-4, 5, 8)));
		Assert.That((_,_,_) = p1 - 10, Is.EqualTo((-8, -2, -1)));
		Assert.That((_,_,_) = p1 * p2, Is.EqualTo((12, 24, 9)));
		Assert.That((_,_,_) = p1 * 10, Is.EqualTo((20, 80, 90)));
		Assert.That((_,_,_) = p1 / p2, Is.EqualTo((0, 2, 9)));
		Assert.That((_,_,_) = p1 /  2, Is.EqualTo((1, 4, 4)));
		Assert.That((_,_,_) = p1 % p2, Is.EqualTo((2, 2, 0)));
		Assert.That((_,_,_) = p1 %  5, Is.EqualTo((2, 3, 4)));

		(p1, p2) = (new(123, 567, 789), new(426, 846, 258));
		Assert.That((_,_,_) = p1 & p2, Is.EqualTo((123 & 426, 567 & 846, 789 & 258)));
		Assert.That((_,_,_) = p1 & 99, Is.EqualTo((123 &  99, 567 &  99, 789 &  99)));
		Assert.That((_,_,_) = p1 | p2, Is.EqualTo((123 | 426, 567 | 846, 789 | 258)));
		Assert.That((_,_,_) = p1 | 99, Is.EqualTo((123 |  99, 567 |  99, 789 |  99)));
		Assert.That((_,_,_) = p1 ^ p2, Is.EqualTo((123 ^ 426, 567 ^ 846, 789 ^ 258)));
		Assert.That((_,_,_) = p1 ^ 99, Is.EqualTo((123 ^  99, 567 ^  99, 789 ^  99)));
		Assert.That((_,_,_) = ~p1, Is.EqualTo((~123, ~567, ~789)));
		Assert.That((_,_,_) = -p1, Is.EqualTo((-123, -567, -789)));
	}

	[Test]
	public void Point3_PointOps()
	{
		Point3 p1 = new(3, 5, 6), p2 = new(-1, 7, -4);
		Assert.That(p1.Dot(p2), Is.EqualTo(8));
		Assert.That(new Point3(1, 0, 0).AngleWith(new(1, 1, 0)).ApproxEqual(Angle.D45, 1e-3f), Is.True);
		Assert.That(new Point3(1, 0, 0).AngleWith(new(0, 1, 0)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Point3(0, 0, 1).AngleWith(new(0, 1, 0)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Point3(1, 0, 0).AngleWith(new(-1, 1, 0)).ApproxEqual(Angle.D45 + Angle.D90, 1e-3f), Is.True);
		Assert.That((_,_,_) = Point3.Clamp(p1, new(-1, -1, -1), new(0, 1, 2)), Is.EqualTo((0, 1, 2)));
		Assert.That((_,_,_) = Point3.Clamp(p1, new(8, 9, 7), new(10, 10, 10)), Is.EqualTo((8, 9, 7)));
		Assert.That((_,_,_) = Point3.Clamp(p1, new(-1, -1, -1), new(10, 10, 10)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Point3.Min(p1, new(-1, -2, -3)), Is.EqualTo((-1, -2, -3)));
		Assert.That((_,_,_) = Point3.Min(p1, new(10, 10, 10)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Point3.Max(p1, new(-1, -2, -3)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Point3.Max(p1, new(10, 11, 12)), Is.EqualTo((10, 11, 12)));
	}

	[Test]
	public void Point3_VectorCasting()
	{
		Assert.That((Point3)new Vec3(-1.5f, 2.5f, -3.5f), Is.EqualTo(new Point3(-1, 2, -3)));
		Assert.That((Point3)new Vec3D(-1.5, 2.5, -3.5), Is.EqualTo(new Point3(-1, 2, -3)));
	}


	[Test]
	public void Point3L_Ctor()
	{
		Assert.That((_,_,_) = new Point3L(), Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = new Point3L(5), Is.EqualTo((5, 5, 5)));
		Assert.That((_,_,_) = new Point3L(-1, 1, 0), Is.EqualTo((-1, 1, 0)));
		Assert.That((_,_,_) = new Point3L(new(-2, -1), 10), Is.EqualTo((-2, -1, 10)));
	}

	[Test]
	public void Point3L_Fields()
	{
		var v = new Point3L(10, 15, 20);
		Assert.That(v.X, Is.EqualTo(10));
		Assert.That(v.Y, Is.EqualTo(15));
		Assert.That(v.Z, Is.EqualTo(20));
		Assert.That(v.Length, Is.EqualTo(Double.Sqrt(10*10 + 15*15 + 20*20)));
		Assert.That(v.LengthSq, Is.EqualTo(10*10 + 15*15 + 20*20));
	}

	[Test]
	public void Point3L_Constants()
	{
		Assert.That((_,_,_) = Point3L.Zero, Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = Point3L.One, Is.EqualTo((1, 1, 1)));
		Assert.That((_,_,_) = Point3L.UnitX, Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Point3L.UnitY, Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Point3L.UnitZ, Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Point3L.Right, Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Point3L.Up, Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Point3L.Backward, Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Point3L.Left, Is.EqualTo((-1, 0, 0)));
		Assert.That((_,_,_) = Point3L.Down, Is.EqualTo((0, -1, 0)));
		Assert.That((_,_,_) = Point3L.Forward, Is.EqualTo((0, 0, -1)));
	}

	[Test]
	public void Point3L_Equality()
	{
		Point3L[] points = [ new(1, 0, 0), new(0, 1, 0), new(0, 0, 1), new(10, 10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X == p2.X && p1.Y == p2.Y && p1.Z == p2.Z;
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y, p1.Z <= p2.Z)));
				Assert.That((_,_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y, p1.Z <  p2.Z)));
				Assert.That((_,_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y, p1.Z >= p2.Z)));
				Assert.That((_,_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y, p1.Z >  p2.Z)));
			}
		}
	}

	[Test]
	public void Point3L_Operators()
	{
		Point3L p1 = new(2, 8, 9), p2 = new(6, 3, 1);

		Assert.That((_,_,_) = p1 + p2, Is.EqualTo((8, 11, 10)));
		Assert.That((_,_,_) = p1 + 10, Is.EqualTo((12, 18, 19)));
		Assert.That((_,_,_) = p1 - p2, Is.EqualTo((-4, 5, 8)));
		Assert.That((_,_,_) = p1 - 10, Is.EqualTo((-8, -2, -1)));
		Assert.That((_,_,_) = p1 * p2, Is.EqualTo((12, 24, 9)));
		Assert.That((_,_,_) = p1 * 10, Is.EqualTo((20, 80, 90)));
		Assert.That((_,_,_) = p1 / p2, Is.EqualTo((0, 2, 9)));
		Assert.That((_,_,_) = p1 /  2, Is.EqualTo((1, 4, 4)));
		Assert.That((_,_,_) = p1 % p2, Is.EqualTo((2, 2, 0)));
		Assert.That((_,_,_) = p1 %  5, Is.EqualTo((2, 3, 4)));

		(p1, p2) = (new(123, 567, 789), new(426, 846, 258));
		Assert.That((_,_,_) = p1 & p2, Is.EqualTo((123 & 426, 567 & 846, 789 & 258)));
		Assert.That((_,_,_) = p1 & 99, Is.EqualTo((123 &  99, 567 &  99, 789 &  99)));
		Assert.That((_,_,_) = p1 | p2, Is.EqualTo((123 | 426, 567 | 846, 789 | 258)));
		Assert.That((_,_,_) = p1 | 99, Is.EqualTo((123 |  99, 567 |  99, 789 |  99)));
		Assert.That((_,_,_) = p1 ^ p2, Is.EqualTo((123 ^ 426, 567 ^ 846, 789 ^ 258)));
		Assert.That((_,_,_) = p1 ^ 99, Is.EqualTo((123 ^  99, 567 ^  99, 789 ^  99)));
		Assert.That((_,_,_) = ~p1, Is.EqualTo((~123, ~567, ~789)));
		Assert.That((_,_,_) = -p1, Is.EqualTo((-123, -567, -789)));
	}

	[Test]
	public void Point3L_PointOps()
	{
		Point3L p1 = new(3, 5, 6), p2 = new(-1, 7, -4);
		Assert.That(p1.Dot(p2), Is.EqualTo(8));
		Assert.That(new Point3L(1, 0, 0).AngleWith(new(1, 1, 0)).ApproxEqual(Angle.D45, 1e-3f), Is.True);
		Assert.That(new Point3L(1, 0, 0).AngleWith(new(0, 1, 0)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Point3L(0, 0, 1).AngleWith(new(0, 1, 0)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Point3L(1, 0, 0).AngleWith(new(-1, 1, 0)).ApproxEqual(Angle.D45 + Angle.D90, 1e-3f), Is.True);
		Assert.That((_,_,_) = Point3L.Clamp(p1, new(-1, -1, -1), new(0, 1, 2)), Is.EqualTo((0, 1, 2)));
		Assert.That((_,_,_) = Point3L.Clamp(p1, new(8, 9, 7), new(10, 10, 10)), Is.EqualTo((8, 9, 7)));
		Assert.That((_,_,_) = Point3L.Clamp(p1, new(-1, -1, -1), new(10, 10, 10)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Point3L.Min(p1, new(-1, -2, -3)), Is.EqualTo((-1, -2, -3)));
		Assert.That((_,_,_) = Point3L.Min(p1, new(10, 10, 10)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Point3L.Max(p1, new(-1, -2, -3)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Point3L.Max(p1, new(10, 11, 12)), Is.EqualTo((10, 11, 12)));
	}

	[Test]
	public void Point3L_VectorCasting()
	{
		Assert.That((Point3L)new Vec3(-1.5f, 2.5f, -3.5f), Is.EqualTo(new Point3L(-1, 2, -3)));
		Assert.That((Point3L)new Vec3D(-1.5, 2.5, -3.5), Is.EqualTo(new Point3L(-1, 2, -3)));
	}


	[Test]
	public void Point_Casting()
	{
		Point3 p1 = new(-10, 11, -12);
		Point3L p2 = new(-10, 11, -12);
		Assert.That((Point3L)p1, Is.EqualTo(p2));
		Assert.That((Point3)p2, Is.EqualTo(p1));
		p2 = new(Int64.MinValue, Int64.MaxValue, Int64.MinValue);
		Assert.That((Point3)p2, Is.EqualTo(new Point3((int)p2.X, (int)p2.Y, (int)p2.Z)));
	}
}
