﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
// ReSharper disable RedundantCast

#pragma warning disable NUnit2045

namespace Astrum.Maths;


// Tests for Vec3
public sealed class Vec3Tests
{

	[Test]
	public void Vec3_Ctor()
	{
		Assert.That((_,_,_) = new Vec3(), Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = new Vec3(5.5f), Is.EqualTo((5.5, 5.5, 5.5)));
		Assert.That((_,_,_) = new Vec3(-1.5f, 2.5f, -3.5f), Is.EqualTo((-1.5, 2.5, -3.5)));
		Assert.That((_,_,_) = new Vec3(new(-1.5f, 2.5f), -3.5f), Is.EqualTo((-1.5, 2.5, -3.5)));
	}

	[Test]
	public void Vec3_Fields()
	{
		Vec3 v = new(1.5f, 2.5f, 3.5f);
		Assert.That(v.X, Is.EqualTo(1.5));
		Assert.That(v.Y, Is.EqualTo(2.5));
		Assert.That(v.Z, Is.EqualTo(3.5));
		Assert.That(v.Length, Is.EqualTo(Single.Sqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z)));
		Assert.That(v.LengthSq, Is.EqualTo(v.X*v.X + v.Y*v.Y + v.Z*v.Z));

		var ilen = 1 / Single.Sqrt(29);
		var norm = new Vec3(2, -3, 4).Normalized;
		Assert.That(norm.X, Is.EqualTo(2 * ilen).Within(1e-5));
		Assert.That(norm.Y, Is.EqualTo(-3 * ilen).Within(1e-5));
		Assert.That(norm.Z, Is.EqualTo(4 * ilen).Within(1e-5));
	}

	[Test]
	public void Vec3_Constants()
	{
		Assert.That((_,_,_) = Vec3.Zero, Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = Vec3.One, Is.EqualTo((1, 1, 1)));
		Assert.That((_,_,_) = Vec3.UnitX, Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Vec3.UnitY, Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Vec3.UnitZ, Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Vec3.Right, Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Vec3.Up, Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Vec3.Backward, Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Vec3.Left, Is.EqualTo((-1, 0, 0)));
		Assert.That((_,_,_) = Vec3.Down, Is.EqualTo((0, -1, 0)));
		Assert.That((_,_,_) = Vec3.Forward, Is.EqualTo((0, 0, -1)));
	}

	[Test]
	public void Vec3_Equality()
	{
		Vec3[] points = [ new(1, 0, 0), new(0, 1, 0), new(0, 0, 1), new(10, 10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X.FastApprox(p2.X) && p1.Y.FastApprox(p2.Y) && p1.Z.FastApprox(p2.Z);
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y, p1.Z <= p2.Z)));
				Assert.That((_,_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y, p1.Z <  p2.Z)));
				Assert.That((_,_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y, p1.Z >= p2.Z)));
				Assert.That((_,_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y, p1.Z >  p2.Z)));
			}
		}
	}

	[Test]
	public void Vec3_Operators()
	{
		Vec3 p1 = new(2, 8, 9), p2 = new(6, 3, 1);

		Assert.That((_,_,_) = p1 + p2, Is.EqualTo((8, 11, 10)));
		Assert.That((_,_,_) = p1 + 10, Is.EqualTo((12, 18, 19)));
		Assert.That((_,_,_) = p1 - p2, Is.EqualTo((-4, 5, 8)));
		Assert.That((_,_,_) = p1 - 10, Is.EqualTo((-8, -2, -1)));
		Assert.That((_,_,_) = p1 * p2, Is.EqualTo((12, 24, 9)));
		Assert.That((_,_,_) = p1 * 10, Is.EqualTo((20, 80, 90)));
		Assert.That((_,_,_) = p1 / p2, Is.EqualTo((p1.X / p2.X, p1.Y / p2.Y, p1.Z / p2.Z)));
		Assert.That((_,_,_) = p1 /  2, Is.EqualTo((p1.X / 2, p1.Y / 2, p1.Z / 2)));
		Assert.That((_,_,_) = p1 % p2, Is.EqualTo((2, 2, 0)));
		Assert.That((_,_,_) = p1 %  5, Is.EqualTo((2, 3, 4)));
		Assert.That((_,_,_) = -p1, Is.EqualTo((-2, -8, -9)));
	}

	[Test]
	public void Vec3_VectorOps()
	{
		Vec3 p1 = new(3, 5, 6), p2 = new(-1, 7, -4);
		Assert.That(p1.Dot(p2), Is.EqualTo(8));
		Assert.That(new Vec3(1, 0, 0).AngleWith(new(1, 1, 0)).ApproxEqual(Angle.D45, 1e-3f), Is.True);
		Assert.That(new Vec3(1, 0, 0).AngleWith(new(0, 1, 0)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Vec3(0, 0, 1).AngleWith(new(0, 1, 0)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Vec3(1, 0, 0).AngleWith(new(-1, 1, 0)).ApproxEqual(Angle.D45 + Angle.D90, 1e-3f), Is.True);
		Assert.That((_,_,_) = Vec3.One.Project(Vec3.UnitX), Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Vec3.One.Project(Vec3.UnitY), Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Vec3.One.Project(Vec3.UnitZ), Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Vec3.One.Reflect(new(0, -1, 0)), Is.EqualTo((1, -1, 1)));
		Assert.That((_,_,_) = Vec3.One.Reflect(new(-1, 0, 0)), Is.EqualTo((-1, 1, 1)));
		Assert.That((_,_,_) = Vec3.One.Reflect(new(0, 0, -1)), Is.EqualTo((1, 1, -1)));
		Assert.That(Vec3.UnitX.Cross(Vec3.UnitY), Is.EqualTo(Vec3.UnitZ));
		Assert.That(Vec3.UnitY.Cross(Vec3.UnitZ), Is.EqualTo(Vec3.UnitX));
		Assert.That(Vec3.UnitZ.Cross(Vec3.UnitX), Is.EqualTo(Vec3.UnitY));
		Assert.That((_,_,_) = Vec3.Clamp(p1, new(-1, -1, -1), new(0, 1, 2)), Is.EqualTo((0, 1, 2)));
		Assert.That((_,_,_) = Vec3.Clamp(p1, new(8, 9, 7), new(10, 10, 10)), Is.EqualTo((8, 9, 7)));
		Assert.That((_,_,_) = Vec3.Clamp(p1, new(-1, -1, -1), new(10, 10, 10)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Vec3.Min(p1, new(-1, -2, -3)), Is.EqualTo((-1, -2, -3)));
		Assert.That((_,_,_) = Vec3.Min(p1, new(10, 10, 10)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Vec3.Max(p1, new(-1, -2, -3)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Vec3.Max(p1, new(10, 11, 12)), Is.EqualTo((10, 11, 12)));
	}

	[Test]
	public void Vec3_PointCasting()
	{
		Assert.That((Vec3)new Point3(-1, 2, -3), Is.EqualTo(new Vec3(-1, 2, -3)));
		Assert.That((Vec3)new Point3L(-1, 2, -3), Is.EqualTo(new Vec3(-1, 2, -3)));
	}


	[Test]
	public void Vec3D_Ctor()
	{
		Assert.That((_,_,_) = new Vec3D(), Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = new Vec3D(5.5f), Is.EqualTo((5.5, 5.5, 5.5)));
		Assert.That((_,_,_) = new Vec3D(-1.5f, 2.5f, -3.5f), Is.EqualTo((-1.5, 2.5, -3.5)));
		Assert.That((_,_,_) = new Vec3D(new(-1.5f, 2.5f), -3.5f), Is.EqualTo((-1.5, 2.5, -3.5)));
	}

	[Test]
	public void Vec3D_Fields()
	{
		Vec3D v = new(1.5f, 2.5f, 3.5f);
		Assert.That(v.X, Is.EqualTo(1.5));
		Assert.That(v.Y, Is.EqualTo(2.5));
		Assert.That(v.Z, Is.EqualTo(3.5));
		Assert.That(v.Length, Is.EqualTo(Double.Sqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z)));
		Assert.That(v.LengthSq, Is.EqualTo(v.X*v.X + v.Y*v.Y + v.Z*v.Z));

		var ilen = 1 / Double.Sqrt(29);
		var norm = new Vec3D(2, -3, 4).Normalized;
		Assert.That(norm.X, Is.EqualTo(2 * ilen).Within(1e-5));
		Assert.That(norm.Y, Is.EqualTo(-3 * ilen).Within(1e-5));
		Assert.That(norm.Z, Is.EqualTo(4 * ilen).Within(1e-5));
	}

	[Test]
	public void Vec3D_Constants()
	{
		Assert.That((_,_,_) = Vec3D.Zero, Is.EqualTo((0, 0, 0)));
		Assert.That((_,_,_) = Vec3D.One, Is.EqualTo((1, 1, 1)));
		Assert.That((_,_,_) = Vec3D.UnitX, Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Vec3D.UnitY, Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Vec3D.UnitZ, Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Vec3D.Right, Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Vec3D.Up, Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Vec3D.Backward, Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Vec3D.Left, Is.EqualTo((-1, 0, 0)));
		Assert.That((_,_,_) = Vec3D.Down, Is.EqualTo((0, -1, 0)));
		Assert.That((_,_,_) = Vec3D.Forward, Is.EqualTo((0, 0, -1)));
	}

	[Test]
	public void Vec3D_Equality()
	{
		Vec3D[] points = [ new(1, 0, 0), new(0, 1, 0), new(0, 0, 1), new(10, 10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X.FastApprox(p2.X) && p1.Y.FastApprox(p2.Y) && p1.Z.FastApprox(p2.Z);
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y, p1.Z <= p2.Z)));
				Assert.That((_,_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y, p1.Z <  p2.Z)));
				Assert.That((_,_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y, p1.Z >= p2.Z)));
				Assert.That((_,_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y, p1.Z >  p2.Z)));
			}
		}
	}

	[Test]
	public void Vec3D_Operators()
	{
		Vec3D p1 = new(2, 8, 9), p2 = new(6, 3, 1);

		Assert.That((_,_,_) = p1 + p2, Is.EqualTo((8, 11, 10)));
		Assert.That((_,_,_) = p1 + 10, Is.EqualTo((12, 18, 19)));
		Assert.That((_,_,_) = p1 - p2, Is.EqualTo((-4, 5, 8)));
		Assert.That((_,_,_) = p1 - 10, Is.EqualTo((-8, -2, -1)));
		Assert.That((_,_,_) = p1 * p2, Is.EqualTo((12, 24, 9)));
		Assert.That((_,_,_) = p1 * 10, Is.EqualTo((20, 80, 90)));
		Assert.That((_,_,_) = p1 / p2, Is.EqualTo((p1.X / p2.X, p1.Y / p2.Y, p1.Z / p2.Z)));
		Assert.That((_,_,_) = p1 /  2, Is.EqualTo((p1.X / 2, p1.Y / 2, p1.Z / 2)));
		Assert.That((_,_,_) = p1 % p2, Is.EqualTo((2, 2, 0)));
		Assert.That((_,_,_) = p1 %  5, Is.EqualTo((2, 3, 4)));
		Assert.That((_,_,_) = -p1, Is.EqualTo((-2, -8, -9)));
	}

	[Test]
	public void Vec3D_VectorOps()
	{
		Vec3D p1 = new(3, 5, 6), p2 = new(-1, 7, -4);
		Assert.That(p1.Dot(p2), Is.EqualTo(8));
		Assert.That(new Vec3D(1, 0, 0).AngleWith(new(1, 1, 0)).ApproxEqual(Angle.D45, 1e-3f), Is.True);
		Assert.That(new Vec3D(1, 0, 0).AngleWith(new(0, 1, 0)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Vec3D(0, 0, 1).AngleWith(new(0, 1, 0)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Vec3D(1, 0, 0).AngleWith(new(-1, 1, 0)).ApproxEqual(Angle.D45 + Angle.D90, 1e-3f), Is.True);
		Assert.That((_,_,_) = Vec3D.One.Project(Vec3D.UnitX), Is.EqualTo((1, 0, 0)));
		Assert.That((_,_,_) = Vec3D.One.Project(Vec3D.UnitY), Is.EqualTo((0, 1, 0)));
		Assert.That((_,_,_) = Vec3D.One.Project(Vec3D.UnitZ), Is.EqualTo((0, 0, 1)));
		Assert.That((_,_,_) = Vec3D.One.Reflect(new(0, -1, 0)), Is.EqualTo((1, -1, 1)));
		Assert.That((_,_,_) = Vec3D.One.Reflect(new(-1, 0, 0)), Is.EqualTo((-1, 1, 1)));
		Assert.That((_,_,_) = Vec3D.One.Reflect(new(0, 0, -1)), Is.EqualTo((1, 1, -1)));
		Assert.That(Vec3D.UnitX.Cross(Vec3D.UnitY), Is.EqualTo(Vec3D.UnitZ));
		Assert.That(Vec3D.UnitY.Cross(Vec3D.UnitZ), Is.EqualTo(Vec3D.UnitX));
		Assert.That(Vec3D.UnitZ.Cross(Vec3D.UnitX), Is.EqualTo(Vec3D.UnitY));
		Assert.That((_,_,_) = Vec3D.Clamp(p1, new(-1, -1, -1), new(0, 1, 2)), Is.EqualTo((0, 1, 2)));
		Assert.That((_,_,_) = Vec3D.Clamp(p1, new(8, 9, 7), new(10, 10, 10)), Is.EqualTo((8, 9, 7)));
		Assert.That((_,_,_) = Vec3D.Clamp(p1, new(-1, -1, -1), new(10, 10, 10)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Vec3D.Min(p1, new(-1, -2, -3)), Is.EqualTo((-1, -2, -3)));
		Assert.That((_,_,_) = Vec3D.Min(p1, new(10, 10, 10)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Vec3D.Max(p1, new(-1, -2, -3)), Is.EqualTo((3, 5, 6)));
		Assert.That((_,_,_) = Vec3D.Max(p1, new(10, 11, 12)), Is.EqualTo((10, 11, 12)));
	}

	[Test]
	public void Vec3D_PointCasting()
	{
		Assert.That((Vec3D)new Point3(-1, 2, -3), Is.EqualTo(new Vec3D(-1, 2, -3)));
		Assert.That((Vec3D)new Point3L(-1, 2, -3), Is.EqualTo(new Vec3D(-1, 2, -3)));
	}


	[Test]
	public void Vec3_AsNumeric()
	{
		var v = new Vec3(1, 2, 3);
		ref readonly var vRef = ref v.AsNumeric();
		ref var vRef2 = ref v.AsNumericMutable();

		Assert.That(vRef.X,  Is.EqualTo(1));
		Assert.That(vRef.Y,  Is.EqualTo(2));
		Assert.That(vRef.Z,  Is.EqualTo(3));
		Assert.That(vRef2.X, Is.EqualTo(1));
		Assert.That(vRef2.Y, Is.EqualTo(2));
		Assert.That(vRef2.Z, Is.EqualTo(3));
		v.X = 5;
		Assert.That(vRef.X,  Is.EqualTo(5));
		Assert.That(vRef2.X, Is.EqualTo(5));
		vRef2.X = 10;
		Assert.That(v.X,    Is.EqualTo(10));
		Assert.That(vRef.X, Is.EqualTo(10));
	}

	[Test]
	public void Vector_Casting()
	{
		Vec3 p1 = new(-10, 11, -12);
		Vec3D p2 = new(-10, 11, -12);
		Assert.That((Vec3D)p1, Is.EqualTo(p2));
		Assert.That((Vec3)p2, Is.EqualTo(p1));
		p2 = new(Double.MinValue, Double.MaxValue, Double.MinValue);
		Assert.That((Vec3)p2, Is.EqualTo(new Vec3((float)p2.X, (float)p2.Y, (float)p2.Z)));
	}
}
