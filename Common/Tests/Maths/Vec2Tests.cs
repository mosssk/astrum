﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
// ReSharper disable RedundantCast

#pragma warning disable NUnit2045

namespace Astrum.Maths;


// Tests for Vec2
public sealed class Vec2Tests
{

	[Test]
	public void Vec2_Ctor()
	{
		Assert.That((_,_) = new Vec2(), Is.EqualTo((0, 0)));
		Assert.That((_,_) = new Vec2(5.5f), Is.EqualTo((5.5, 5.5)));
		Assert.That((_,_) = new Vec2(-1.5f, 2.5f), Is.EqualTo((-1.5, 2.5)));
	}

	[Test]
	public void Vec2_Fields()
	{
		Vec2 v = new(1.5f, 2.5f);
		Assert.That(v.X, Is.EqualTo(1.5));
		Assert.That(v.Y, Is.EqualTo(2.5));
		Assert.That(v.Length, Is.EqualTo(Single.Sqrt(v.X*v.X + v.Y*v.Y)));
		Assert.That(v.LengthSq, Is.EqualTo(v.X*v.X + v.Y*v.Y));

		var ilen = 1 / Single.Sqrt(13);
		var norm = new Vec2(2, -3).Normalized;
		Assert.That(norm.X, Is.EqualTo(2 * ilen).Within(1e-5));
		Assert.That(norm.Y, Is.EqualTo(-3 * ilen).Within(1e-5));
	}

	[Test]
	public void Vec2_Constants()
	{
		Assert.That((_,_) = Vec2.Zero, Is.EqualTo((0, 0)));
		Assert.That((_,_) = Vec2.One, Is.EqualTo((1, 1)));
		Assert.That((_,_) = Vec2.UnitX, Is.EqualTo((1, 0)));
		Assert.That((_,_) = Vec2.UnitY, Is.EqualTo((0, 1)));
	}

	[Test]
	public void Vec2_Equality()
	{
		Vec2[] vecs = [ new(1, 0), new(0, 1), new(10, 10) ];
		foreach (var p1 in vecs) {
			foreach (var p2 in vecs) {
				var same = p1.X.FastApprox(p2.X) && p1.Y.FastApprox(p2.Y);
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y)));
				Assert.That((_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y)));
				Assert.That((_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y)));
				Assert.That((_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y)));
			}
		}
	}

	[Test]
	public void Vec2_Operators()
	{
		Vec2 p1 = new(2, 8), p2 = new(6, 3);

		Assert.That((_,_) = p1 + p2, Is.EqualTo((8, 11)));
		Assert.That((_,_) = p1 + 10, Is.EqualTo((12, 18)));
		Assert.That((_,_) = p1 - p2, Is.EqualTo((-4, 5)));
		Assert.That((_,_) = p1 - 10, Is.EqualTo((-8, -2)));
		Assert.That((_,_) = p1 * p2, Is.EqualTo((12, 24)));
		Assert.That((_,_) = p1 * 10, Is.EqualTo((20, 80)));
		Assert.That((_,_) = p1 / p2, Is.EqualTo((p1.X / p2.X, p1.Y / p2.Y)));
		Assert.That((_,_) = p1 /  2, Is.EqualTo((p1.X / 2, p1.Y / 2)));
		Assert.That((_,_) = p1 % p2, Is.EqualTo((2, 2)));
		Assert.That((_,_) = p1 %  5, Is.EqualTo((2, 3)));
		Assert.That((_,_) = -p1, Is.EqualTo((-2, -8)));
	}

	[Test]
	public void Vec2_VectorOps()
	{
		Vec2 p1 = new(3, 5), p2 = new(-1, 7);
		Assert.That(p1.Dot(p2), Is.EqualTo(32));
		Assert.That(new Vec2(1, 0).AngleWith(new(1, 1)).ApproxEqual(Angle.D45, 1e-3f), Is.True);
		Assert.That(new Vec2(1, 0).AngleWith(new(0, 1)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Vec2(1, 0).AngleWith(new(-1, 1)).ApproxEqual(Angle.D45 + Angle.D90, 1e-3f), Is.True);
		Assert.That((_,_) = Vec2.One.Project(Vec2.UnitX), Is.EqualTo((1, 0)));
		Assert.That((_,_) = Vec2.One.Project(Vec2.UnitY), Is.EqualTo((0, 1)));
		Assert.That((_,_) = Vec2.One.Project(new(-1, 1)), Is.EqualTo((0, 0)));
		Assert.That((_,_) = Vec2.One.Reflect(new(0, -1)), Is.EqualTo((1, -1)));
		Assert.That((_,_) = Vec2.One.Reflect(new(-1, 0)), Is.EqualTo((-1, 1)));
		Assert.That((_,_) = Vec2.Clamp(p1, new(-1, -1), new(0, 1)), Is.EqualTo((0, 1))); 
		Assert.That((_,_) = Vec2.Clamp(p1, new(8, 9), new(10, 10)), Is.EqualTo((8, 9))); 
		Assert.That((_,_) = Vec2.Clamp(p1, new(-1, -1), new(10, 10)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Vec2.Min(p1, new(-1, -2)), Is.EqualTo((-1, -2)));
		Assert.That((_,_) = Vec2.Min(p1, new(10, 10)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Vec2.Max(p1, new(-1, -2)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Vec2.Max(p1, new(10, 11)), Is.EqualTo((10, 11)));
	}

	[Test]
	public void Vec2_PointCasting()
	{
		Assert.That((Vec2)new Point2(-1, 2), Is.EqualTo(new Vec2(-1, 2)));
		Assert.That((Vec2)new Point2L(-1, 2), Is.EqualTo(new Vec2(-1, 2)));
	}


	[Test]
	public void Vec2D_Ctor()
	{
		Assert.That((_,_) = new Vec2D(), Is.EqualTo((0, 0)));
		Assert.That((_,_) = new Vec2D(5.5f), Is.EqualTo((5.5, 5.5)));
		Assert.That((_,_) = new Vec2D(-1.5f, 2.5f), Is.EqualTo((-1.5, 2.5)));
	}

	[Test]
	public void Vec2D_Fields()
	{
		Vec2D v = new(1.5f, 2.5f);
		Assert.That(v.X, Is.EqualTo(1.5));
		Assert.That(v.Y, Is.EqualTo(2.5));
		Assert.That(v.Length, Is.EqualTo(Double.Sqrt(v.X*v.X + v.Y*v.Y)));
		Assert.That(v.LengthSq, Is.EqualTo(v.X*v.X + v.Y*v.Y));

		var ilen = 1 / Double.Sqrt(13);
		var norm = new Vec2D(2, -3).Normalized;
		Assert.That(norm.X, Is.EqualTo(2 * ilen).Within(1e-5));
		Assert.That(norm.Y, Is.EqualTo(-3 * ilen).Within(1e-5));
	}

	[Test]
	public void Vec2D_Constants()
	{
		Assert.That((_,_) = Vec2D.Zero, Is.EqualTo((0, 0)));
		Assert.That((_,_) = Vec2D.One, Is.EqualTo((1, 1)));
		Assert.That((_,_) = Vec2D.UnitX, Is.EqualTo((1, 0)));
		Assert.That((_,_) = Vec2D.UnitY, Is.EqualTo((0, 1)));
	}

	[Test]
	public void Vec2D_Equality()
	{
		Vec2D[] vecs = [ new(1, 0), new(0, 1), new(10, 10) ];
		foreach (var p1 in vecs) {
			foreach (var p2 in vecs) {
				var same = p1.X.FastApprox(p2.X) && p1.Y.FastApprox(p2.Y);
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y)));
				Assert.That((_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y)));
				Assert.That((_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y)));
				Assert.That((_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y)));
			}
		}
	}

	[Test]
	public void Vec2D_Operators()
	{
		Vec2D p1 = new(2, 8), p2 = new(6, 3);

		Assert.That((_,_) = p1 + p2, Is.EqualTo((8, 11)));
		Assert.That((_,_) = p1 + 10, Is.EqualTo((12, 18)));
		Assert.That((_,_) = p1 - p2, Is.EqualTo((-4, 5)));
		Assert.That((_,_) = p1 - 10, Is.EqualTo((-8, -2)));
		Assert.That((_,_) = p1 * p2, Is.EqualTo((12, 24)));
		Assert.That((_,_) = p1 * 10, Is.EqualTo((20, 80)));
		Assert.That((_,_) = p1 / p2, Is.EqualTo((p1.X / p2.X, p1.Y / p2.Y)));
		Assert.That((_,_) = p1 /  2, Is.EqualTo((p1.X / 2, p1.Y / 2)));
		Assert.That((_,_) = p1 % p2, Is.EqualTo((2, 2)));
		Assert.That((_,_) = p1 %  5, Is.EqualTo((2, 3)));
		Assert.That((_,_) = -p1, Is.EqualTo((-2, -8)));
	}

	[Test]
	public void Vec2D_VectorOps()
	{
		Vec2D p1 = new(3, 5), p2 = new(-1, 7);
		Assert.That(p1.Dot(p2), Is.EqualTo(32));
		Assert.That(new Vec2D(1, 0).AngleWith(new(1, 1)).ApproxEqual(Angle.D45, 1e-3f), Is.True);
		Assert.That(new Vec2D(1, 0).AngleWith(new(0, 1)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Vec2D(1, 0).AngleWith(new(-1, 1)).ApproxEqual(Angle.D45 + Angle.D90, 1e-3f), Is.True);
		Assert.That((_,_) = Vec2D.One.Project(Vec2D.UnitX), Is.EqualTo((1, 0)));
		Assert.That((_,_) = Vec2D.One.Project(Vec2D.UnitY), Is.EqualTo((0, 1)));
		Assert.That((_,_) = Vec2D.One.Project(new(-1, 1)), Is.EqualTo((0, 0)));
		Assert.That((_,_) = Vec2D.One.Reflect(new(0, -1)), Is.EqualTo((1, -1)));
		Assert.That((_,_) = Vec2D.One.Reflect(new(-1, 0)), Is.EqualTo((-1, 1)));
		Assert.That((_,_) = Vec2D.Clamp(p1, new(-1, -1), new(0, 1)), Is.EqualTo((0, 1))); 
		Assert.That((_,_) = Vec2D.Clamp(p1, new(8, 9), new(10, 10)), Is.EqualTo((8, 9))); 
		Assert.That((_,_) = Vec2D.Clamp(p1, new(-1, -1), new(10, 10)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Vec2D.Min(p1, new(-1, -2)), Is.EqualTo((-1, -2)));
		Assert.That((_,_) = Vec2D.Min(p1, new(10, 10)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Vec2D.Max(p1, new(-1, -2)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Vec2D.Max(p1, new(10, 11)), Is.EqualTo((10, 11)));
	}

	[Test]
	public void Vec2D_PointCasting()
	{
		Assert.That((Vec2D)new Point2(-1, 2), Is.EqualTo(new Vec2D(-1, 2)));
		Assert.That((Vec2D)new Point2L(-1, 2), Is.EqualTo(new Vec2D(-1, 2)));
	}


	[Test]
	public void Vec2_AsNumeric()
	{
		var v = new Vec2(1, 2);
		ref readonly var vRef = ref v.AsNumeric();
		ref var vRef2 = ref v.AsNumericMutable();

		Assert.That(vRef.X,  Is.EqualTo(1));
		Assert.That(vRef.Y,  Is.EqualTo(2));
		Assert.That(vRef2.X, Is.EqualTo(1));
		Assert.That(vRef2.Y, Is.EqualTo(2));
		v.X = 5;
		Assert.That(vRef.X,  Is.EqualTo(5));
		Assert.That(vRef2.X, Is.EqualTo(5));
		vRef2.X = 10;
		Assert.That(v.X,    Is.EqualTo(10));
		Assert.That(vRef.X, Is.EqualTo(10));
	}

	[Test]
	public void Vector_Casting()
	{
		Vec2 p1 = new(-10, 11);
		Vec2D p2 = new(-10, 11);
		Assert.That((Vec2D)p1, Is.EqualTo(p2));
		Assert.That((Vec2)p2, Is.EqualTo(p1));
		p2 = new(Double.MinValue, Double.MaxValue);
		Assert.That((Vec2)p2, Is.EqualTo(new Vec2((float)p2.X, (float)p2.Y)));
	}
}
