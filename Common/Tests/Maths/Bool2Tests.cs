﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

namespace Astrum.Maths;


// Tests for Bool2
public sealed class Bool2Tests
{
	private static readonly Bool2[] _All;

	static Bool2Tests()
	{
		_All = new Bool2[4];
		for (uint i = 0; i < 4; ++i) {
			_All[i] = new(i > 2, i % 2 == 0);
		}
	}
	
	
	[Test]
	public void Constructor()
	{
		Bool2 b = new();
		Assert.That((b.X, b.Y), Is.EqualTo((false, false)));
		b = new(true);
		Assert.That((b.X, b.Y), Is.EqualTo((true, true)));
		for (uint i = 0; i < 4; ++i) {
			b = new(i > 2, i % 2 == 0);
			Assert.That((b.X, b.Y), Is.EqualTo((i > 2, i % 2 == 0)));
		}
	}

	[Test]
	public void Fields()
	{
		foreach (var b in _All) {
			Assert.That(b.Any, Is.EqualTo(b.X || b.Y));
			Assert.That(b.All, Is.EqualTo(b.X && b.Y));
			Assert.That(b.None, Is.EqualTo(!(b.X || b.Y)));
			Assert.That(b.Count, Is.EqualTo((b.X ? 1 : 0) + (b.Y ? 1 : 0)));
		}
	}

	[Test]
	public void Equals()
	{
#pragma warning disable 1718
		for (uint i = 0; i < _All.Length - 1; ++i) {
			var b0 = _All[i];
			var b1 = _All[i + 1];
			Assert.True(b0.Equals(b0));
			Assert.True(b0.Equals((object)b0));
			Assert.False(b0.Equals(b1));
			Assert.False(b0.Equals((object)b1));
			Assert.True(b0 == b0);
			Assert.False(b0 != b0);
			Assert.True(b0 != b1);
			Assert.False(b0 == b1);
		}
#pragma warning restore 1718
	}

	[Test]
	public void Deconstruct()
	{
		foreach (var b in _All) {
			var (x, y) = b;
			Assert.That((x, y), Is.EqualTo((b.X, b.Y)));
		}
	}

	[Test]
	public void LogicalOperators()
	{
		for (uint i = 0; i < _All.Length - 1; ++i) {
			var b0 = _All[i];
			var b1 = _All[i + 1];
			var and = b0 & b1;
			Assert.That((and.X, and.Y), Is.EqualTo((b0.X && b1.X, b0.Y && b1.Y)));
			var or = b0 | b1;
			Assert.That((or.X, or.Y), Is.EqualTo((b0.X || b1.X, b0.Y || b1.Y)));
			var xor = b0 ^ b1;
			Assert.That((xor.X, xor.Y), Is.EqualTo((b0.X != b1.X, b0.Y != b1.Y)));
			var not0 = ~b0;
			Assert.That((not0.X, not0.Y), Is.EqualTo((!b0.X, !b0.Y)));
			var not1 = !b0;
			Assert.That((not1.X, not1.Y), Is.EqualTo((!b0.X, !b0.Y)));
		}
	}
}
