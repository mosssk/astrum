﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
// ReSharper disable InconsistentNaming

namespace Astrum.Maths;


// Tests for MathUtils
public sealed class MathUtilsTests
{
	// Float values
	private static readonly float[] F32_ARR = [100, 10, 5, 1, .1f, .001f, .00001f, 1e-8f, 0];
	private static readonly double[] F64_ARR = [100, 10, 5, 1, .1, .001, .00001, 1e-8, 0];
	
	// Eps values
	private static readonly float[] F32_EPS = [10, 1, 0.001f, .00001f, 1e-8f, MathUtils.TOL_32];
	private static readonly double[] F64_EPS = [10, 1, 0.001, .00001, 1e-8, MathUtils.TOL_64];
	
	
	[Test]
	public void FastApprox()
	{
		// Float
		foreach (var l in F32_ARR) {
			foreach (var r in F32_ARR) {
				foreach (var eps in F32_EPS) {
					var eq = l.Equals(r) || Math.Abs(r - l) <= eps;
					Assert.That(l.FastApprox(r, eps), Is.EqualTo(eq));
				}
			}
		}
		Assert.That(Single.PositiveInfinity.FastApprox(Single.PositiveInfinity));
		Assert.That(Single.PositiveInfinity.FastApprox(Single.NegativeInfinity), Is.False);
		Assert.That(Single.NegativeInfinity.FastApprox(Single.NegativeInfinity));
		Assert.That(Single.NaN.FastApprox(Single.NaN));
		
		// Double
		foreach (var l in F64_ARR) {
			foreach (var r in F64_ARR) {
				foreach (var eps in F64_EPS) {
					var eq = l.Equals(r) || Double.Abs(r - l) <= eps;
					Assert.That(l.FastApprox(r, eps), Is.EqualTo(eq));
				}
			}
		}
		Assert.That(Double.PositiveInfinity.FastApprox(Double.PositiveInfinity));
		Assert.That(Double.PositiveInfinity.FastApprox(Double.NegativeInfinity), Is.False);
		Assert.That(Double.NegativeInfinity.FastApprox(Double.NegativeInfinity));
		Assert.That(Double.NaN.FastApprox(Double.NaN));
	}

	[Test]
	public void ApproxZero()
	{
		// Float
		foreach (var v in F32_ARR) {
			foreach (var eps in F32_EPS) {
				Assert.That(v.ApproxZero(eps), Is.EqualTo(Math.Abs(v) <= eps));
			}
		}
		Assert.That(Single.PositiveInfinity.ApproxZero(), Is.False);
		Assert.That(Single.NegativeInfinity.ApproxZero(), Is.False);
		Assert.That(Single.NaN.ApproxZero(), Is.False);
		
		// Double
		foreach (var v in F64_ARR) {
			foreach (var eps in F64_EPS) {
				Assert.That(v.ApproxZero(eps), Is.EqualTo(Math.Abs(v) <= eps));
			}
		}
		Assert.That(Double.PositiveInfinity.ApproxZero(), Is.False);
		Assert.That(Double.NegativeInfinity.ApproxZero(), Is.False);
		Assert.That(Double.NaN.ApproxZero(), Is.False);
	}
}
