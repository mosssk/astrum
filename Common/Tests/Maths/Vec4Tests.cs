﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
// ReSharper disable RedundantCast

#pragma warning disable NUnit2045

namespace Astrum.Maths;


// Tests for Vec4
public sealed class Vec4Tests
{

	[Test]
	public void Vec4_Ctor()
	{
		Assert.That((_,_,_,_) = new Vec4(), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = new Vec4(5.5f), Is.EqualTo((5.5, 5.5, 5.5, 5.5)));
		Assert.That((_,_,_,_) = new Vec4(-1.5f, 2.5f, -3.5f, 4.5f), Is.EqualTo((-1.5, 2.5, -3.5, 4.5)));
		Assert.That((_,_,_,_) = new Vec4(new(-1.5f, 2.5f, -3.5f), 4.5f), Is.EqualTo((-1.5, 2.5, -3.5, 4.5f)));
		Assert.That((_,_,_,_) = new Vec4(new Vec2(-1.5f, 2.5f), new(-3.5f, 4.5f)), Is.EqualTo((-1.5, 2.5, -3.5, 4.5f)));
	}

	[Test]
	public void Vec4_Fields()
	{
		Vec4 v = new(1.5f, 2.5f, 3.5f, 4.5f);
		Assert.That(v.X, Is.EqualTo(1.5));
		Assert.That(v.Y, Is.EqualTo(2.5));
		Assert.That(v.Z, Is.EqualTo(3.5));
		Assert.That(v.W, Is.EqualTo(4.5));
		Assert.That(v.Length, Is.EqualTo(Single.Sqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W)));
		Assert.That(v.LengthSq, Is.EqualTo(v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W));

		var ilen = 1 / Single.Sqrt(54);
		var norm = new Vec4(2, -3, 4, -5).Normalized;
		Assert.That(norm.X, Is.EqualTo(2 * ilen).Within(1e-5));
		Assert.That(norm.Y, Is.EqualTo(-3 * ilen).Within(1e-5));
		Assert.That(norm.Z, Is.EqualTo(4 * ilen).Within(1e-5));
		Assert.That(norm.W, Is.EqualTo(-5 * ilen).Within(1e-5));
	}

	[Test]
	public void Vec4_Constants()
	{
		Assert.That((_,_,_,_) = Vec4.Zero, Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = Vec4.One, Is.EqualTo((1, 1, 1, 1)));
		Assert.That((_,_,_,_) = Vec4.UnitX, Is.EqualTo((1, 0, 0, 0)));
		Assert.That((_,_,_,_) = Vec4.UnitY, Is.EqualTo((0, 1, 0, 0)));
		Assert.That((_,_,_,_) = Vec4.UnitZ, Is.EqualTo((0, 0, 1, 0)));
		Assert.That((_,_,_,_) = Vec4.UnitW, Is.EqualTo((0, 0, 0, 1)));
	}

	[Test]
	public void Vec4_Equality()
	{
		Vec4[] points = [ new(1, 0, 0, 0), new(0, 1, 0, 0), new(0, 0, 1, 0), new(0, 0, 0, 1), new(10, 10, 10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X.FastApprox(p2.X) && p1.Y.FastApprox(p2.Y) && p1.Z.FastApprox(p2.Z) && p1.W.FastApprox(p2.W);
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_,_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y, p1.Z <= p2.Z, p1.W <= p2.W)));
				Assert.That((_,_,_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y, p1.Z <  p2.Z, p1.W <  p2.W)));
				Assert.That((_,_,_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y, p1.Z >= p2.Z, p1.W >= p2.W)));
				Assert.That((_,_,_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y, p1.Z >  p2.Z, p1.W >  p2.W)));
			}
		}
	}

	[Test]
	public void Vec4_Operators()
	{
		Vec4 p1 = new(2, 8, 9, 7), p2 = new(6, 3, 1, 5);

		Assert.That((_,_,_,_) = p1 + p2, Is.EqualTo((8, 11, 10, 12)));
		Assert.That((_,_,_,_) = p1 + 10, Is.EqualTo((12, 18, 19, 17)));
		Assert.That((_,_,_,_) = p1 - p2, Is.EqualTo((-4, 5, 8, 2)));
		Assert.That((_,_,_,_) = p1 - 10, Is.EqualTo((-8, -2, -1, -3)));
		Assert.That((_,_,_,_) = p1 * p2, Is.EqualTo((12, 24, 9, 35)));
		Assert.That((_,_,_,_) = p1 * 10, Is.EqualTo((20, 80, 90, 70)));
		Assert.That((_,_,_,_) = p1 / p2, Is.EqualTo((p1.X / p2.X, p1.Y / p2.Y, p1.Z / p2.Z, p1.W / p2.W)));
		Assert.That((_,_,_,_) = p1 /  2, Is.EqualTo((p1.X / 2, p1.Y / 2, p1.Z / 2, p1.W / 2)));
		Assert.That((_,_,_,_) = p1 % p2, Is.EqualTo((2, 2, 0, 2)));
		Assert.That((_,_,_,_) = p1 %  5, Is.EqualTo((2, 3, 4, 2)));
		Assert.That((_,_,_,_) = -p1, Is.EqualTo((-2, -8, -9, -7)));
	}

	[Test]
	public void Vec4_PointOps()
	{
		Vec4 p1 = new(3, 5, 6, 8), p2 = new(-1, 7, -4, -2);
		Assert.That(p1.Dot(p2), Is.EqualTo(-8));
		Assert.That((_,_,_,_) = Vec4.Clamp(p1, new(-1, -1, -1, -1), new(0, 1, 2, 3)), Is.EqualTo((0, 1, 2, 3)));
		Assert.That((_,_,_,_) = Vec4.Clamp(p1, new(8, 9, 7, 10), new(10, 10, 10, 10)), Is.EqualTo((8, 9, 7, 10)));
		Assert.That((_,_,_,_) = Vec4.Clamp(p1, new(-1, -1, -1, -1), new(10, 10, 10, 10)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Vec4.Min(p1, new(-1, -2, -3, -4)), Is.EqualTo((-1, -2, -3, -4)));
		Assert.That((_,_,_,_) = Vec4.Min(p1, new(10, 10, 10, 10)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Vec4.Max(p1, new(-1, -2, -3, -4)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Vec4.Max(p1, new(10, 11, 12, 13)), Is.EqualTo((10, 11, 12, 13)));
	}

	[Test]
	public void Vec4_PointCasting()
	{
		Assert.That((Vec4)new Point4(-1, 2, -3, 4), Is.EqualTo(new Vec4(-1, 2, -3, 4)));
		Assert.That((Vec4)new Point4L(-1, 2, -3, 4), Is.EqualTo(new Vec4(-1, 2, -3, 4)));
	}


	[Test]
	public void Vec4D_Ctor()
	{
		Assert.That((_,_,_,_) = new Vec4D(), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = new Vec4D(5.5f), Is.EqualTo((5.5, 5.5, 5.5, 5.5)));
		Assert.That((_,_,_,_) = new Vec4D(-1.5f, 2.5f, -3.5f, 4.5f), Is.EqualTo((-1.5, 2.5, -3.5, 4.5)));
		Assert.That((_,_,_,_) = new Vec4D(new(-1.5f, 2.5f, -3.5f), 4.5f), Is.EqualTo((-1.5, 2.5, -3.5, 4.5f)));
		Assert.That((_,_,_,_) = new Vec4D(new Vec2D(-1.5f, 2.5f), new(-3.5f, 4.5f)), Is.EqualTo((-1.5, 2.5, -3.5, 4.5f)));
	}

	[Test]
	public void Vec4D_Fields()
	{
		Vec4D v = new(1.5f, 2.5f, 3.5f, 4.5f);
		Assert.That(v.X, Is.EqualTo(1.5));
		Assert.That(v.Y, Is.EqualTo(2.5));
		Assert.That(v.Z, Is.EqualTo(3.5));
		Assert.That(v.W, Is.EqualTo(4.5));
		Assert.That(v.Length, Is.EqualTo(Double.Sqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W)));
		Assert.That(v.LengthSq, Is.EqualTo(v.X*v.X + v.Y*v.Y + v.Z*v.Z + v.W*v.W));

		var ilen = 1 / Double.Sqrt(54);
		var norm = new Vec4D(2, -3, 4, -5).Normalized;
		Assert.That(norm.X, Is.EqualTo(2 * ilen).Within(1e-5));
		Assert.That(norm.Y, Is.EqualTo(-3 * ilen).Within(1e-5));
		Assert.That(norm.Z, Is.EqualTo(4 * ilen).Within(1e-5));
		Assert.That(norm.W, Is.EqualTo(-5 * ilen).Within(1e-5));
	}

	[Test]
	public void Vec4D_Constants()
	{
		Assert.That((_,_,_,_) = Vec4D.Zero, Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = Vec4D.One, Is.EqualTo((1, 1, 1, 1)));
		Assert.That((_,_,_,_) = Vec4D.UnitX, Is.EqualTo((1, 0, 0, 0)));
		Assert.That((_,_,_,_) = Vec4D.UnitY, Is.EqualTo((0, 1, 0, 0)));
		Assert.That((_,_,_,_) = Vec4D.UnitZ, Is.EqualTo((0, 0, 1, 0)));
		Assert.That((_,_,_,_) = Vec4D.UnitW, Is.EqualTo((0, 0, 0, 1)));
	}

	[Test]
	public void Vec4D_Equality()
	{
		Vec4D[] points = [ new(1, 0, 0, 0), new(0, 1, 0, 0), new(0, 0, 1, 0), new(0, 0, 0, 1), new(10, 10, 10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X.FastApprox(p2.X) && p1.Y.FastApprox(p2.Y) && p1.Z.FastApprox(p2.Z) && p1.W.FastApprox(p2.W);
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_,_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y, p1.Z <= p2.Z, p1.W <= p2.W)));
				Assert.That((_,_,_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y, p1.Z <  p2.Z, p1.W <  p2.W)));
				Assert.That((_,_,_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y, p1.Z >= p2.Z, p1.W >= p2.W)));
				Assert.That((_,_,_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y, p1.Z >  p2.Z, p1.W >  p2.W)));
			}
		}
	}

	[Test]
	public void Vec4D_Operators()
	{
		Vec4D p1 = new(2, 8, 9, 7), p2 = new(6, 3, 1, 5);

		Assert.That((_,_,_,_) = p1 + p2, Is.EqualTo((8, 11, 10, 12)));
		Assert.That((_,_,_,_) = p1 + 10, Is.EqualTo((12, 18, 19, 17)));
		Assert.That((_,_,_,_) = p1 - p2, Is.EqualTo((-4, 5, 8, 2)));
		Assert.That((_,_,_,_) = p1 - 10, Is.EqualTo((-8, -2, -1, -3)));
		Assert.That((_,_,_,_) = p1 * p2, Is.EqualTo((12, 24, 9, 35)));
		Assert.That((_,_,_,_) = p1 * 10, Is.EqualTo((20, 80, 90, 70)));
		Assert.That((_,_,_,_) = p1 / p2, Is.EqualTo((p1.X / p2.X, p1.Y / p2.Y, p1.Z / p2.Z, p1.W / p2.W)));
		Assert.That((_,_,_,_) = p1 /  2, Is.EqualTo((p1.X / 2, p1.Y / 2, p1.Z / 2, p1.W / 2)));
		Assert.That((_,_,_,_) = p1 % p2, Is.EqualTo((2, 2, 0, 2)));
		Assert.That((_,_,_,_) = p1 %  5, Is.EqualTo((2, 3, 4, 2)));
		Assert.That((_,_,_,_) = -p1, Is.EqualTo((-2, -8, -9, -7)));
	}

	[Test]
	public void Vec4D_PointOps()
	{
		Vec4D p1 = new(3, 5, 6, 8), p2 = new(-1, 7, -4, -2);
		Assert.That(p1.Dot(p2), Is.EqualTo(-8));
		Assert.That((_,_,_,_) = Vec4D.Clamp(p1, new(-1, -1, -1, -1), new(0, 1, 2, 3)), Is.EqualTo((0, 1, 2, 3)));
		Assert.That((_,_,_,_) = Vec4D.Clamp(p1, new(8, 9, 7, 10), new(10, 10, 10, 10)), Is.EqualTo((8, 9, 7, 10)));
		Assert.That((_,_,_,_) = Vec4D.Clamp(p1, new(-1, -1, -1, -1), new(10, 10, 10, 10)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Vec4D.Min(p1, new(-1, -2, -3, -4)), Is.EqualTo((-1, -2, -3, -4)));
		Assert.That((_,_,_,_) = Vec4D.Min(p1, new(10, 10, 10, 10)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Vec4D.Max(p1, new(-1, -2, -3, -4)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Vec4D.Max(p1, new(10, 11, 12, 13)), Is.EqualTo((10, 11, 12, 13)));
	}

	[Test]
	public void Vec4D_PointCasting()
	{
		Assert.That((Vec4D)new Point4(-1, 2, -3, 4), Is.EqualTo(new Vec4D(-1, 2, -3, 4)));
		Assert.That((Vec4D)new Point4L(-1, 2, -3, 4), Is.EqualTo(new Vec4D(-1, 2, -3, 4)));
	}


	[Test]
	public void Vec4_AsNumeric()
	{
		var v = new Vec4(1, 2, 3, 4);
		ref readonly var vRef = ref v.AsNumeric();
		ref var vRef2 = ref v.AsNumericMutable();

		Assert.That(vRef.X,  Is.EqualTo(1));
		Assert.That(vRef.Y,  Is.EqualTo(2));
		Assert.That(vRef.Z,  Is.EqualTo(3));
		Assert.That(vRef.W,  Is.EqualTo(4));
		Assert.That(vRef2.X, Is.EqualTo(1));
		Assert.That(vRef2.Y, Is.EqualTo(2));
		Assert.That(vRef2.Z, Is.EqualTo(3));
		Assert.That(vRef2.W, Is.EqualTo(4));
		v.X = 5;
		Assert.That(vRef.X,  Is.EqualTo(5));
		Assert.That(vRef2.X, Is.EqualTo(5));
		vRef2.X = 10;
		Assert.That(v.X,    Is.EqualTo(10));
		Assert.That(vRef.X, Is.EqualTo(10));
	}

	[Test]
	public void Vector_Casting()
	{
		Vec4 p1 = new(-10, 11, -12, 13);
		Vec4D p2 = new(-10, 11, -12, 13);
		Assert.That((Vec4D)p1, Is.EqualTo(p2));
		Assert.That((Vec4)p2, Is.EqualTo(p1));
		p2 = new(Double.MinValue, Double.MaxValue, Double.MinValue, Double.MaxValue);
		Assert.That((Vec4)p2, Is.EqualTo(new Vec4((float)p2.X, (float)p2.Y, (float)p2.Z, (float)p2.W)));
	}
}
