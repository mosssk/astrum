﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Maths;


// Tests for Point4
public sealed class Point4Tests
{

	[Test]
	public void Point4_Ctor()
	{
		Assert.That((_,_,_,_) = new Point4(), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = new Point4(5), Is.EqualTo((5, 5, 5, 5)));
		Assert.That((_,_,_,_) = new Point4(-1, 1, 0, 2), Is.EqualTo((-1, 1, 0, 2)));
		Assert.That((_,_,_,_) = new Point4(new(-2, -1, 1), 10), Is.EqualTo((-2, -1, 1, 10)));
		Assert.That((_,_,_,_) = new Point4(new Point2(-2, -1), new(5, 10)), Is.EqualTo((-2, -1, 5, 10)));
	}

	[Test]
	public void Point4_Fields()
	{
		var v = new Point4(10, 15, 20, 25);
		Assert.That(v.X, Is.EqualTo(10));
		Assert.That(v.Y, Is.EqualTo(15));
		Assert.That(v.Z, Is.EqualTo(20));
		Assert.That(v.W, Is.EqualTo(25));
		Assert.That(v.Length, Is.EqualTo(Single.Sqrt(10*10 + 15*15 + 20*20 + 25*25)));
		Assert.That(v.LengthSq, Is.EqualTo(10*10 + 15*15 + 20*20 + 25*25));
	}

	[Test]
	public void Point4_Constants()
	{
		Assert.That((_,_,_,_) = Point4.Zero, Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = Point4.One, Is.EqualTo((1, 1, 1, 1)));
		Assert.That((_,_,_,_) = Point4.UnitX, Is.EqualTo((1, 0, 0, 0)));
		Assert.That((_,_,_,_) = Point4.UnitY, Is.EqualTo((0, 1, 0, 0)));
		Assert.That((_,_,_,_) = Point4.UnitZ, Is.EqualTo((0, 0, 1, 0)));
		Assert.That((_,_,_,_) = Point4.UnitW, Is.EqualTo((0, 0, 0, 1)));
	}

	[Test]
	public void Point4_Equality()
	{
		Point4[] points = [ new(1, 0, 0, 0), new(0, 1, 0, 0), new(0, 0, 1, 0), new(0, 0, 0, 1), new(10, 10, 10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X == p2.X && p1.Y == p2.Y && p1.Z == p2.Z && p1.W == p2.W;
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_,_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y, p1.Z <= p2.Z, p1.W <= p2.W)));
				Assert.That((_,_,_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y, p1.Z <  p2.Z, p1.W <  p2.W)));
				Assert.That((_,_,_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y, p1.Z >= p2.Z, p1.W >= p2.W)));
				Assert.That((_,_,_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y, p1.Z >  p2.Z, p1.W >  p2.W)));
			}
		}
	}

	[Test]
	public void Point4_Operators()
	{
		Point4 p1 = new(2, 8, 9, 7), p2 = new(6, 3, 1, 5);

		Assert.That((_,_,_,_) = p1 + p2, Is.EqualTo((8, 11, 10, 12)));
		Assert.That((_,_,_,_) = p1 + 10, Is.EqualTo((12, 18, 19, 17)));
		Assert.That((_,_,_,_) = p1 - p2, Is.EqualTo((-4, 5, 8, 2)));
		Assert.That((_,_,_,_) = p1 - 10, Is.EqualTo((-8, -2, -1, -3)));
		Assert.That((_,_,_,_) = p1 * p2, Is.EqualTo((12, 24, 9, 35)));
		Assert.That((_,_,_,_) = p1 * 10, Is.EqualTo((20, 80, 90, 70)));
		Assert.That((_,_,_,_) = p1 / p2, Is.EqualTo((0, 2, 9, 1)));
		Assert.That((_,_,_,_) = p1 /  2, Is.EqualTo((1, 4, 4, 3)));
		Assert.That((_,_,_,_) = p1 % p2, Is.EqualTo((2, 2, 0, 2)));
		Assert.That((_,_,_,_) = p1 %  5, Is.EqualTo((2, 3, 4, 2)));

		(p1, p2) = (new(123, 567, 789, 159), new(426, 846, 258, 852));
		Assert.That((_,_,_,_) = p1 & p2, Is.EqualTo((123 & 426, 567 & 846, 789 & 258, 159 & 852)));
		Assert.That((_,_,_,_) = p1 & 99, Is.EqualTo((123 &  99, 567 &  99, 789 &  99, 159 &  99)));
		Assert.That((_,_,_,_) = p1 | p2, Is.EqualTo((123 | 426, 567 | 846, 789 | 258, 159 | 852)));
		Assert.That((_,_,_,_) = p1 | 99, Is.EqualTo((123 |  99, 567 |  99, 789 |  99, 159 |  99)));
		Assert.That((_,_,_,_) = p1 ^ p2, Is.EqualTo((123 ^ 426, 567 ^ 846, 789 ^ 258, 159 ^ 852)));
		Assert.That((_,_,_,_) = p1 ^ 99, Is.EqualTo((123 ^  99, 567 ^  99, 789 ^  99, 159 ^  99)));
		Assert.That((_,_,_,_) = ~p1, Is.EqualTo((~123, ~567, ~789, ~159)));
		Assert.That((_,_,_,_) = -p1, Is.EqualTo((-123, -567, -789, -159)));
	}

	[Test]
	public void Point4_PointOps()
	{
		Point4 p1 = new(3, 5, 6, 8), p2 = new(-1, 7, -4, -2);
		Assert.That(p1.Dot(p2), Is.EqualTo(-8));
		Assert.That((_,_,_,_) = Point4.Clamp(p1, new(-1, -1, -1, -1), new(0, 1, 2, 3)), Is.EqualTo((0, 1, 2, 3)));
		Assert.That((_,_,_,_) = Point4.Clamp(p1, new(8, 9, 7, 10), new(10, 10, 10, 10)), Is.EqualTo((8, 9, 7, 10)));
		Assert.That((_,_,_,_) = Point4.Clamp(p1, new(-1, -1, -1, -1), new(10, 10, 10, 10)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Point4.Min(p1, new(-1, -2, -3, -4)), Is.EqualTo((-1, -2, -3, -4)));
		Assert.That((_,_,_,_) = Point4.Min(p1, new(10, 10, 10, 10)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Point4.Max(p1, new(-1, -2, -3, -4)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Point4.Max(p1, new(10, 11, 12, 13)), Is.EqualTo((10, 11, 12, 13)));
	}

	[Test]
	public void Point4_VectorCasting()
	{
		Assert.That((Point4)new Vec4(-1.5f, 2.5f, -3.5f, 4.5f), Is.EqualTo(new Point4(-1, 2, -3, 4)));
		Assert.That((Point4)new Vec4D(-1.5, 2.5, -3.5, 4.5f), Is.EqualTo(new Point4(-1, 2, -3, 4)));
	}


	[Test]
	public void Point4L_Ctor()
	{
		Assert.That((_,_,_,_) = new Point4L(), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = new Point4L(5), Is.EqualTo((5, 5, 5, 5)));
		Assert.That((_,_,_,_) = new Point4L(-1, 1, 0, 2), Is.EqualTo((-1, 1, 0, 2)));
		Assert.That((_,_,_,_) = new Point4L(new(-2, -1, 1), 10), Is.EqualTo((-2, -1, 1, 10)));
		Assert.That((_,_,_,_) = new Point4L(new Point2L(-2, -1), new(5, 10)), Is.EqualTo((-2, -1, 5, 10)));
	}

	[Test]
	public void Point4L_Fields()
	{
		var v = new Point4L(10, 15, 20, 25);
		Assert.That(v.X, Is.EqualTo(10));
		Assert.That(v.Y, Is.EqualTo(15));
		Assert.That(v.Z, Is.EqualTo(20));
		Assert.That(v.W, Is.EqualTo(25));
		Assert.That(v.Length, Is.EqualTo(Double.Sqrt(10*10 + 15*15 + 20*20 + 25*25)));
		Assert.That(v.LengthSq, Is.EqualTo(10*10 + 15*15 + 20*20 + 25*25));
	}

	[Test]
	public void Point4L_Constants()
	{
		Assert.That((_,_,_,_) = Point4L.Zero, Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = Point4L.One, Is.EqualTo((1, 1, 1, 1)));
		Assert.That((_,_,_,_) = Point4L.UnitX, Is.EqualTo((1, 0, 0, 0)));
		Assert.That((_,_,_,_) = Point4L.UnitY, Is.EqualTo((0, 1, 0, 0)));
		Assert.That((_,_,_,_) = Point4L.UnitZ, Is.EqualTo((0, 0, 1, 0)));
		Assert.That((_,_,_,_) = Point4L.UnitW, Is.EqualTo((0, 0, 0, 1)));
	}

	[Test]
	public void Point4L_Equality()
	{
		Point4L[] points = [ new(1, 0, 0, 0), new(0, 1, 0, 0), new(0, 0, 1, 0), new(0, 0, 0, 1), new(10, 10, 10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X == p2.X && p1.Y == p2.Y && p1.Z == p2.Z && p1.W == p2.W;
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_,_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y, p1.Z <= p2.Z, p1.W <= p2.W)));
				Assert.That((_,_,_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y, p1.Z <  p2.Z, p1.W <  p2.W)));
				Assert.That((_,_,_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y, p1.Z >= p2.Z, p1.W >= p2.W)));
				Assert.That((_,_,_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y, p1.Z >  p2.Z, p1.W >  p2.W)));
			}
		}
	}

	[Test]
	public void Point4L_Operators()
	{
		Point4L p1 = new(2, 8, 9, 7), p2 = new(6, 3, 1, 5);

		Assert.That((_,_,_,_) = p1 + p2, Is.EqualTo((8, 11, 10, 12)));
		Assert.That((_,_,_,_) = p1 + 10, Is.EqualTo((12, 18, 19, 17)));
		Assert.That((_,_,_,_) = p1 - p2, Is.EqualTo((-4, 5, 8, 2)));
		Assert.That((_,_,_,_) = p1 - 10, Is.EqualTo((-8, -2, -1, -3)));
		Assert.That((_,_,_,_) = p1 * p2, Is.EqualTo((12, 24, 9, 35)));
		Assert.That((_,_,_,_) = p1 * 10, Is.EqualTo((20, 80, 90, 70)));
		Assert.That((_,_,_,_) = p1 / p2, Is.EqualTo((0, 2, 9, 1)));
		Assert.That((_,_,_,_) = p1 /  2, Is.EqualTo((1, 4, 4, 3)));
		Assert.That((_,_,_,_) = p1 % p2, Is.EqualTo((2, 2, 0, 2)));
		Assert.That((_,_,_,_) = p1 %  5, Is.EqualTo((2, 3, 4, 2)));

		(p1, p2) = (new(123, 567, 789, 159), new(426, 846, 258, 852));
		Assert.That((_,_,_,_) = p1 & p2, Is.EqualTo((123 & 426, 567 & 846, 789 & 258, 159 & 852)));
		Assert.That((_,_,_,_) = p1 & 99, Is.EqualTo((123 &  99, 567 &  99, 789 &  99, 159 &  99)));
		Assert.That((_,_,_,_) = p1 | p2, Is.EqualTo((123 | 426, 567 | 846, 789 | 258, 159 | 852)));
		Assert.That((_,_,_,_) = p1 | 99, Is.EqualTo((123 |  99, 567 |  99, 789 |  99, 159 |  99)));
		Assert.That((_,_,_,_) = p1 ^ p2, Is.EqualTo((123 ^ 426, 567 ^ 846, 789 ^ 258, 159 ^ 852)));
		Assert.That((_,_,_,_) = p1 ^ 99, Is.EqualTo((123 ^  99, 567 ^  99, 789 ^  99, 159 ^  99)));
		Assert.That((_,_,_,_) = ~p1, Is.EqualTo((~123, ~567, ~789, ~159)));
		Assert.That((_,_,_,_) = -p1, Is.EqualTo((-123, -567, -789, -159)));
	}

	[Test]
	public void Point4L_PointOps()
	{
		Point4L p1 = new(3, 5, 6, 8), p2 = new(-1, 7, -4, -2);
		Assert.That(p1.Dot(p2), Is.EqualTo(-8));
		Assert.That((_,_,_,_) = Point4L.Clamp(p1, new(-1, -1, -1, -1), new(0, 1, 2, 3)), Is.EqualTo((0, 1, 2, 3)));
		Assert.That((_,_,_,_) = Point4L.Clamp(p1, new(8, 9, 7, 10), new(10, 10, 10, 10)), Is.EqualTo((8, 9, 7, 10)));
		Assert.That((_,_,_,_) = Point4L.Clamp(p1, new(-1, -1, -1, -1), new(10, 10, 10, 10)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Point4L.Min(p1, new(-1, -2, -3, -4)), Is.EqualTo((-1, -2, -3, -4)));
		Assert.That((_,_,_,_) = Point4L.Min(p1, new(10, 10, 10, 10)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Point4L.Max(p1, new(-1, -2, -3, -4)), Is.EqualTo((3, 5, 6, 8)));
		Assert.That((_,_,_,_) = Point4L.Max(p1, new(10, 11, 12, 13)), Is.EqualTo((10, 11, 12, 13)));
	}

	[Test]
	public void Point4L_VectorCasting()
	{
		Assert.That((Point4L)new Vec4(-1.5f, 2.5f, -3.5f, 4.5f), Is.EqualTo(new Point4L(-1, 2, -3, 4)));
		Assert.That((Point4L)new Vec4D(-1.5, 2.5, -3.5, 4.5f), Is.EqualTo(new Point4L(-1, 2, -3, 4)));
	}


	[Test]
	public void Point_Casting()
	{
		Point4 p1 = new(-10, 11, -12, 13);
		Point4L p2 = new(-10, 11, -12, 13);
		Assert.That((Point4L)p1, Is.EqualTo(p2));
		Assert.That((Point4)p2, Is.EqualTo(p1));
		p2 = new(Int64.MinValue, Int64.MaxValue, Int64.MinValue, Int64.MaxValue);
		Assert.That((Point4)p2, Is.EqualTo(new Point4((int)p2.X, (int)p2.Y, (int)p2.Z, (int)p2.W)));
	}
}
