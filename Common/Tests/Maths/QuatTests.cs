﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Maths;


// Tests for Quat
public sealed class QuatTests
{
	[Test]
	public void Quat_Constants() => Assert.That((_,_,_,_) = Quat.Identity, Is.EqualTo((0, 0, 0, 1)));

	[Test]
	public void Quat_Ctor()
	{
		var q = new Quat(1, 2, 5, 11);
		Assert.That((_, _, _, _) = q, Is.EqualTo((1, 2, 5, 11)));
	}

	[Test]
	public void Quat_Fields()
	{
		var q = new Quat(1, 3, 5, 8);
		Assert.That(q.X, Is.EqualTo(1));
		Assert.That(q.Y, Is.EqualTo(3));
		Assert.That(q.Z, Is.EqualTo(5));
		Assert.That(q.W, Is.EqualTo(8));
		Assert.That(q.Length, Is.EqualTo(Single.Sqrt(1 + 9 + 25 + 64)));
		Assert.That(q.LengthSq, Is.EqualTo(1 + 9 + 25 + 64));
		Assert.That(q.Normalized, Is.EqualTo(new Quat(1 / q.Length, 3 / q.Length, 5 / q.Length, 8 / q.Length)));
		Assert.That(q.Inverted, Is.EqualTo(new Quat(-1 / q.Length, -3 / q.Length, -5 / q.Length, 8 / q.Length)));
	}

	[Test]
	public void Quat_Equality()
	{
		Quat[] quats = [ new(1, 0, 0, 0), new(0, 1, 0, 0), new(0, 0, 1, 0), new(0, 0, 0, 1), new(10, 10, 10, 10) ];
		foreach (var q1 in quats) {
			foreach (var q2 in quats) {
				var same = q1.X.FastApprox(q2.X) && q1.Y.FastApprox(q2.Y) && q1.Z.FastApprox(q2.Z) && q1.W.FastApprox(q2.W);
				Assert.That(q1 == q2, Is.EqualTo(same));
				Assert.That(q1 != q2, Is.EqualTo(!same));
				Assert.That(q1.Equals(q2), Is.EqualTo(same));
				Assert.That(q1.Equals((object)q2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void Quat_Ops()
	{
		var q1 = new Quat(1, 2, 3, 4);
		var q2 = new Quat(5, 7, 9, 11);
		Assert.That((_, _, _, _) = q1.Add(q2), Is.EqualTo((6, 9, 12, 15)));
		Assert.That((_, _, _, _) = q1.Subtract(q2), Is.EqualTo((-4, -5, -6, -7)));
		Assert.That((_, _, _, _) = q1.Concatenate(q2), Is.EqualTo((28, 56, 66, -2)));
		Assert.That(q1.Dot(q2), Is.EqualTo(5 + 14 + 27 + 44));
	}

	[Test]
	public void Quat_Create()
	{
#pragma warning disable CS9193 // Argument should be a variable because it is passed to a 'ref readonly' parameter
		var q = Quat.RotationAxis(Vec3.One, Angle.D60);
		Assert.That(q.ToMatrix().ApproxEqual(Mat4.RotationAxis(Vec3.One, Angle.D60), 1e-6f));

		q = Quat.YawPitchRoll(Angle.D10, Angle.D30, Angle.D60);
		Assert.That(q.ToMatrix().ApproxEqual(Mat4.YawPitchRoll(Angle.D10, Angle.D30, Angle.D60), 1e-6f));

		q = Quat.FromRotationMatrix(Mat4.RotationAxis(-Vec3.One, Angle.Pi));
		Assert.That(q.ToMatrix().ApproxEqual(Mat4.RotationAxis(-Vec3.One, Angle.Pi), 1e-6f));
#pragma warning restore CS9193 // Argument should be a variable because it is passed to a 'ref readonly' parameter
	}

	[Test]
	public void Quat_ToMatrix()
	{
		var q = Quat.RotationAxis(new(1, 2, 3), Angle.D60);
		var m = new Mat4(
			 0.53571433f,  0.76579356f, -0.35576713f, 0f,
			-0.62293639f,  0.64285719f,  0.44574070f, 0f,
			 0.57005286f, -0.01716929f,  0.82142860f, 0f,
			           0,            0,            0,  1
		);
		q.ToMatrix(out var o);
		Assert.That(q.ToMatrix().ApproxEqual(in m, 1e-6f));
		Assert.That(o.ApproxEqual(in m, 1e-6f));
	}

	[Test]
	public void Quat_Operators()
	{
		var q1 = new Quat(1, 2, 3, 4);
		var q2 = new Quat(5, 7, 9, 11);
		Assert.That(q1 + q2, Is.EqualTo(q1.Add(q2)));
		Assert.That(q1 - q2, Is.EqualTo(q1.Subtract(q2)));
		Assert.That(q1 * q2, Is.EqualTo(q1.Concatenate(q2)));
		Assert.That(q1 / q2, Is.EqualTo(q1.Concatenate(q2.Inverted)));
		Assert.That(-q1, Is.EqualTo(new Quat(-1, -2, -3, 4)));
	}
}
