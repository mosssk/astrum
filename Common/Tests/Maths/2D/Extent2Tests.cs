﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

namespace Astrum.Maths;


// Tests for Extent2
public sealed class Extent2Tests
{

	[Test]
	public void Extent2_Ctor()
	{
		Assert.That((_,_) = new Extent2(), Is.EqualTo((0, 0)));
		Assert.That((_,_) = new Extent2(1, 2), Is.EqualTo((1, 2)));
	}

	[Test]
	public void Extent2_Fields()
	{
		var e = new Extent2(3, 6);
		Assert.That(e.X, Is.EqualTo(3));
		Assert.That(e.Y, Is.EqualTo(6));
		Assert.That(e.Area, Is.EqualTo(18));
		Assert.That(e.IsEmpty, Is.False);
	}

	[Test]
	public void Extent2_Constants()
	{
		Assert.That(Extent2.Zero.X, Is.EqualTo(0));
		Assert.That(Extent2.Zero.Y, Is.EqualTo(0));
		Assert.That(Extent2.Zero.IsEmpty, Is.True);
	}

	[Test]
	public void Extent2_Equality()
	{
		Extent2[] exts = [ new(), new(1, 0), new(0, 1), new(1, 1) ];
		foreach (var e1 in exts) {
			foreach (var e2 in exts) {
				var same = e1.X == e2.X && e1.Y == e2.Y;
				Assert.That(e1.Equals(e2), Is.EqualTo(same));
				Assert.That(e1.Equals((object)e2), Is.EqualTo(same));
				Assert.That(e1 == e2, Is.EqualTo(same));
				Assert.That(e1 != e2, Is.EqualTo(!same));
			}
		}
	}


	[Test]
	public void Extent2L_Ctor()
	{
		Assert.That((_,_) = new Extent2L(), Is.EqualTo((0, 0)));
		Assert.That((_,_) = new Extent2L(1, 2), Is.EqualTo((1, 2)));
	}

	[Test]
	public void Extent2L_Fields()
	{
		var e = new Extent2L(3, 6);
		Assert.That(e.X, Is.EqualTo(3));
		Assert.That(e.Y, Is.EqualTo(6));
		Assert.That(e.Area, Is.EqualTo(18));
		Assert.That(e.IsEmpty, Is.False);
	}

	[Test]
	public void Extent2L_Constants()
	{
		Assert.That(Extent2L.Zero.X, Is.EqualTo(0));
		Assert.That(Extent2L.Zero.Y, Is.EqualTo(0));
		Assert.That(Extent2L.Zero.IsEmpty, Is.True);
	}

	[Test]
	public void Extent2L_Equality()
	{
		Extent2L[] exts = [ new(), new(1, 0), new(0, 1), new(1, 1) ];
		foreach (var e1 in exts) {
			foreach (var e2 in exts) {
				var same = e1.X == e2.X && e1.Y == e2.Y;
				Assert.That(e1.Equals(e2), Is.EqualTo(same));
				Assert.That(e1.Equals((object)e2), Is.EqualTo(same));
				Assert.That(e1 == e2, Is.EqualTo(same));
				Assert.That(e1 != e2, Is.EqualTo(!same));
			}
		}
	}


	[Test]
	public void Extent2_Casting()
	{
		Assert.That((_, _) = (Extent2L)new Extent2(1, 2), Is.EqualTo((1, 2)));
		Assert.That((_, _) = (Extent2)new Extent2L(3, 4), Is.EqualTo((3, 4)));
	}
}
