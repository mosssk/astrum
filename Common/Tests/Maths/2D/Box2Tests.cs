﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System.Linq;

namespace Astrum.Maths;


// Tests for Box2
public sealed class Box2Tests
{

	[Test]
	public void Box2_Ctor()
	{
		Assert.That((_,_,_,_) = new Box2(), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = new Box2(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2(3, 4, 1, 2), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2(new(1, 2), new(3, 4)), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2(new(3, 4), new(1, 2)), Is.EqualTo((1, 2, 3, 4)));
	}

	[Test]
	public void Box2_Constants() 
	{
		Assert.That((_,_,_,_) = Box2.Empty, Is.EqualTo((0,0,0,0)));
		Assert.That((_,_,_,_) = Box2.Unit, Is.EqualTo((0,0,1,1)));
	}

	[Test]
	public void Box2_Fields()
	{
		var b = new Box2(1, 3, 6, 9);
		Assert.That(b.Min, Is.EqualTo(new Point2(1, 3)));
		Assert.That(b.Max, Is.EqualTo(new Point2(6, 9)));
		Assert.That(b.Size, Is.EqualTo(new Extent2(5, 6)));
		Assert.That(b.Width, Is.EqualTo(5));
		Assert.That(b.Height, Is.EqualTo(6));
		Assert.That(b.Center, Is.EqualTo(new Vec2(3.5f, 6f)));
		Assert.That(b.Area, Is.EqualTo(30));
		Assert.That(b.IsEmpty, Is.False);
		Assert.That(Box2.Empty.IsEmpty, Is.True);
	}

	[Test]
	public void Box2_Deconstruct()
	{
		Assert.That((_,_,_,_) = new Box2(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_) = new Box2(1, 2, 3, 4), Is.EqualTo((new Point2(1, 2), new Point2(3, 4))));
	}

	[Test]
	public void Box2_Equality()
	{
		Box2[] boxes = [ new(1, 0, 0, 0), new(0, 1, 0, 0), new(0, 0, 1, 0), new(0, 0, 0, 1), new(3, 4, 5, 6) ];
		foreach (var b1 in boxes) {
			foreach (var b2 in boxes) {
				var same = b1.Min == b2.Min && b1.Max == b2.Max;
				Assert.That(b1 == b2, Is.EqualTo(same));
				Assert.That(b1 != b2, Is.EqualTo(!same));
				Assert.That(b1.Equals(b2), Is.EqualTo(same));
				Assert.That(b1.Equals((object)b2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void Box2_Transform()
	{
		var b = new Box2(2, 3, 5, 8);
		Assert.That((_,_,_,_) = b.Translated(new(-2, 4)), Is.EqualTo((0, 7, 3, 12)));
		Assert.That((_,_,_,_) = b.Inflated(new(-1, 3)), Is.EqualTo((3, 0, 4, 11)));

		b = new(1, 1, 3, 3);
		Assert.That((_,_,_,_) = b.Scaled(new(2, 3), new(0, 0)), Is.EqualTo((2, 3, 6, 9)));
		Assert.That((_,_,_,_) = b.Scaled(new(3, 2), new(3, 3)), Is.EqualTo((-3, -1, 3, 3)));
	}

	[Test]
	public void Box2_Union()
	{
		Box2 b1 = new(1, 1, 2, 2), b2 = new(3, 3, 5, 5);
		Assert.That((_,_,_,_) = Box2.Union(b1, b2), Is.EqualTo((1, 1, 5, 5)));
		Assert.That((_,_,_,_) = Box2.Union(b2, b1), Is.EqualTo((1, 1, 5, 5)));
		b1 = new(2, 2, 6, 6);
		Assert.That((_,_,_,_) = Box2.Union(b1, b2), Is.EqualTo((2, 2, 6, 6)));
		Assert.That((_,_,_,_) = Box2.Union(b2, b1), Is.EqualTo((2, 2, 6, 6)));
		Assert.That(Box2.Union(Box2.Empty, Box2.Empty), Is.EqualTo(Box2.Empty));
	}

	[Test]
	public void Box2_Intersect()
	{
		Box2 b1 = new(1, 1, 3, 3), b2 = new(2, 2, 4, 4);
		Assert.That((_,_,_,_) = Box2.Intersect(b1, b2), Is.EqualTo((2, 2, 3, 3)));
		Assert.That((_,_,_,_) = Box2.Intersect(b2, b1), Is.EqualTo((2, 2, 3, 3)));
		b1 = new(0, 0, 1, 1);
		Assert.That((_,_,_,_) = Box2.Intersect(b1, b2), Is.EqualTo((0, 0, 0, 0)));
	}

	[Test]
	public void Box2_Casting()
	{
		Assert.That((_,_,_,_) = (Box2)new Box2L(2, 4, 7, 9), 
			Is.EqualTo(((int)2, (int)4, (int)7, (int)9)));
		Assert.That((_,_,_,_) = (Box2)new Box2F(2, 4, 7, 9), 
			Is.EqualTo(((int)2, (int)4, (int)7, (int)9)));
		Assert.That((_,_,_,_) = (Box2)new Box2D(2, 4, 7, 9), 
			Is.EqualTo(((int)2, (int)4, (int)7, (int)9)));
	}


	[Test]
	public void Box2L_Ctor()
	{
		Assert.That((_,_,_,_) = new Box2L(), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = new Box2L(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2L(3, 4, 1, 2), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2L(new(1, 2), new(3, 4)), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2L(new(3, 4), new(1, 2)), Is.EqualTo((1, 2, 3, 4)));
	}

	[Test]
	public void Box2L_Constants() 
	{
		Assert.That((_,_,_,_) = Box2L.Empty, Is.EqualTo((0,0,0,0)));
		Assert.That((_,_,_,_) = Box2L.Unit, Is.EqualTo((0,0,1,1)));
	}

	[Test]
	public void Box2L_Fields()
	{
		var b = new Box2L(1, 3, 6, 9);
		Assert.That(b.Min, Is.EqualTo(new Point2L(1, 3)));
		Assert.That(b.Max, Is.EqualTo(new Point2L(6, 9)));
		Assert.That(b.Size, Is.EqualTo(new Extent2L(5, 6)));
		Assert.That(b.Width, Is.EqualTo(5));
		Assert.That(b.Height, Is.EqualTo(6));
		Assert.That(b.Center, Is.EqualTo(new Vec2D(3.5f, 6f)));
		Assert.That(b.Area, Is.EqualTo(30));
		Assert.That(b.IsEmpty, Is.False);
		Assert.That(Box2L.Empty.IsEmpty, Is.True);
	}

	[Test]
	public void Box2L_Deconstruct()
	{
		Assert.That((_,_,_,_) = new Box2L(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_) = new Box2L(1, 2, 3, 4), Is.EqualTo((new Point2L(1, 2), new Point2L(3, 4))));
	}

	[Test]
	public void Box2L_Equality()
	{
		Box2L[] boxes = [ new(1, 0, 0, 0), new(0, 1, 0, 0), new(0, 0, 1, 0), new(0, 0, 0, 1), new(3, 4, 5, 6) ];
		foreach (var b1 in boxes) {
			foreach (var b2 in boxes) {
				var same = b1.Min == b2.Min && b1.Max == b2.Max;
				Assert.That(b1 == b2, Is.EqualTo(same));
				Assert.That(b1 != b2, Is.EqualTo(!same));
				Assert.That(b1.Equals(b2), Is.EqualTo(same));
				Assert.That(b1.Equals((object)b2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void Box2L_Transform()
	{
		var b = new Box2L(2, 3, 5, 8);
		Assert.That((_,_,_,_) = b.Translated(new(-2, 4)), Is.EqualTo((0, 7, 3, 12)));
		Assert.That((_,_,_,_) = b.Inflated(new(-1, 3)), Is.EqualTo((3, 0, 4, 11)));

		b = new(1, 1, 3, 3);
		Assert.That((_,_,_,_) = b.Scaled(new(2, 3), new(0, 0)), Is.EqualTo((2, 3, 6, 9)));
		Assert.That((_,_,_,_) = b.Scaled(new(3, 2), new(3, 3)), Is.EqualTo((-3, -1, 3, 3)));
	}

	[Test]
	public void Box2L_Union()
	{
		Box2L b1 = new(1, 1, 2, 2), b2 = new(3, 3, 5, 5);
		Assert.That((_,_,_,_) = Box2L.Union(b1, b2), Is.EqualTo((1, 1, 5, 5)));
		Assert.That((_,_,_,_) = Box2L.Union(b2, b1), Is.EqualTo((1, 1, 5, 5)));
		b1 = new(2, 2, 6, 6);
		Assert.That((_,_,_,_) = Box2L.Union(b1, b2), Is.EqualTo((2, 2, 6, 6)));
		Assert.That((_,_,_,_) = Box2L.Union(b2, b1), Is.EqualTo((2, 2, 6, 6)));
		Assert.That(Box2L.Union(Box2L.Empty, Box2L.Empty), Is.EqualTo(Box2L.Empty));
	}

	[Test]
	public void Box2L_Intersect()
	{
		Box2L b1 = new(1, 1, 3, 3), b2 = new(2, 2, 4, 4);
		Assert.That((_,_,_,_) = Box2L.Intersect(b1, b2), Is.EqualTo((2, 2, 3, 3)));
		Assert.That((_,_,_,_) = Box2L.Intersect(b2, b1), Is.EqualTo((2, 2, 3, 3)));
		b1 = new(0, 0, 1, 1);
		Assert.That((_,_,_,_) = Box2L.Intersect(b1, b2), Is.EqualTo((0, 0, 0, 0)));
	}

	[Test]
	public void Box2L_Casting()
	{
		Assert.That((_,_,_,_) = (Box2L)new Box2(2, 4, 7, 9), 
			Is.EqualTo(((long)2, (long)4, (long)7, (long)9)));
		Assert.That((_,_,_,_) = (Box2L)new Box2F(2, 4, 7, 9), 
			Is.EqualTo(((long)2, (long)4, (long)7, (long)9)));
		Assert.That((_,_,_,_) = (Box2L)new Box2D(2, 4, 7, 9), 
			Is.EqualTo(((long)2, (long)4, (long)7, (long)9)));
	}


	[Test]
	public void Box2F_Ctor()
	{
		Assert.That((_,_,_,_) = new Box2F(), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = new Box2F(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2F(3, 4, 1, 2), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2F(new(1, 2), new(3, 4)), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2F(new(3, 4), new(1, 2)), Is.EqualTo((1, 2, 3, 4)));
	}

	[Test]
	public void Box2F_Constants() 
	{
		Assert.That((_,_,_,_) = Box2F.Empty, Is.EqualTo((0,0,0,0)));
		Assert.That((_,_,_,_) = Box2F.Unit, Is.EqualTo((0,0,1,1)));
	}

	[Test]
	public void Box2F_Fields()
	{
		var b = new Box2F(1, 3, 6, 9);
		Assert.That(b.Min, Is.EqualTo(new Vec2(1, 3)));
		Assert.That(b.Max, Is.EqualTo(new Vec2(6, 9)));
		Assert.That(b.Size, Is.EqualTo(new Vec2(5, 6)));
		Assert.That(b.Width, Is.EqualTo(5));
		Assert.That(b.Height, Is.EqualTo(6));
		Assert.That(b.Center, Is.EqualTo(new Vec2(3.5f, 6f)));
		Assert.That(b.Area, Is.EqualTo(30));
		Assert.That(b.IsEmpty, Is.False);
		Assert.That(Box2F.Empty.IsEmpty, Is.True);
	}

	[Test]
	public void Box2F_Deconstruct()
	{
		Assert.That((_,_,_,_) = new Box2F(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_) = new Box2F(1, 2, 3, 4), Is.EqualTo((new Vec2(1, 2), new Vec2(3, 4))));
	}

	[Test]
	public void Box2F_Equality()
	{
		Box2F[] boxes = [ new(1, 0, 0, 0), new(0, 1, 0, 0), new(0, 0, 1, 0), new(0, 0, 0, 1), new(3, 4, 5, 6) ];
		foreach (var b1 in boxes) {
			foreach (var b2 in boxes) {
				var same = b1.Min == b2.Min && b1.Max == b2.Max;
				Assert.That(b1 == b2, Is.EqualTo(same));
				Assert.That(b1 != b2, Is.EqualTo(!same));
				Assert.That(b1.Equals(b2), Is.EqualTo(same));
				Assert.That(b1.Equals((object)b2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void Box2F_Transform()
	{
		var b = new Box2F(2, 3, 5, 8);
		Assert.That((_,_,_,_) = b.Translated(new(-2, 4)), Is.EqualTo((0, 7, 3, 12)));
		Assert.That((_,_,_,_) = b.Inflated(new(-1, 3)), Is.EqualTo((3, 0, 4, 11)));

		b = new(1, 1, 3, 3);
		Assert.That((_,_,_,_) = b.Scaled(new(2, 3), new(0, 0)), Is.EqualTo((2, 3, 6, 9)));
		Assert.That((_,_,_,_) = b.Scaled(new(3, 2), new(3, 3)), Is.EqualTo((-3, -1, 3, 3)));
	}

	[Test]
	public void Box2F_Union()
	{
		Box2F b1 = new(1, 1, 2, 2), b2 = new(3, 3, 5, 5);
		Assert.That((_,_,_,_) = Box2F.Union(b1, b2), Is.EqualTo((1, 1, 5, 5)));
		Assert.That((_,_,_,_) = Box2F.Union(b2, b1), Is.EqualTo((1, 1, 5, 5)));
		b1 = new(2, 2, 6, 6);
		Assert.That((_,_,_,_) = Box2F.Union(b1, b2), Is.EqualTo((2, 2, 6, 6)));
		Assert.That((_,_,_,_) = Box2F.Union(b2, b1), Is.EqualTo((2, 2, 6, 6)));
		Assert.That(Box2F.Union(Box2F.Empty, Box2F.Empty), Is.EqualTo(Box2F.Empty));
	}

	[Test]
	public void Box2F_Intersect()
	{
		Box2F b1 = new(1, 1, 3, 3), b2 = new(2, 2, 4, 4);
		Assert.That((_,_,_,_) = Box2F.Intersect(b1, b2), Is.EqualTo((2, 2, 3, 3)));
		Assert.That((_,_,_,_) = Box2F.Intersect(b2, b1), Is.EqualTo((2, 2, 3, 3)));
		b1 = new(0, 0, 1, 1);
		Assert.That((_,_,_,_) = Box2F.Intersect(b1, b2), Is.EqualTo((0, 0, 0, 0)));
	}

	[Test]
	public void Box2F_Casting()
	{
		Assert.That((_,_,_,_) = (Box2F)new Box2(2, 4, 7, 9), 
			Is.EqualTo(((float)2, (float)4, (float)7, (float)9)));
		Assert.That((_,_,_,_) = (Box2F)new Box2L(2, 4, 7, 9), 
			Is.EqualTo(((float)2, (float)4, (float)7, (float)9)));
		Assert.That((_,_,_,_) = (Box2F)new Box2D(2, 4, 7, 9), 
			Is.EqualTo(((float)2, (float)4, (float)7, (float)9)));
	}


	[Test]
	public void Box2D_Ctor()
	{
		Assert.That((_,_,_,_) = new Box2D(), Is.EqualTo((0, 0, 0, 0)));
		Assert.That((_,_,_,_) = new Box2D(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2D(3, 4, 1, 2), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2D(new(1, 2), new(3, 4)), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_,_,_) = new Box2D(new(3, 4), new(1, 2)), Is.EqualTo((1, 2, 3, 4)));
	}

	[Test]
	public void Box2D_Constants() 
	{
		Assert.That((_,_,_,_) = Box2D.Empty, Is.EqualTo((0,0,0,0)));
		Assert.That((_,_,_,_) = Box2D.Unit, Is.EqualTo((0,0,1,1)));
	}

	[Test]
	public void Box2D_Fields()
	{
		var b = new Box2D(1, 3, 6, 9);
		Assert.That(b.Min, Is.EqualTo(new Vec2D(1, 3)));
		Assert.That(b.Max, Is.EqualTo(new Vec2D(6, 9)));
		Assert.That(b.Size, Is.EqualTo(new Vec2D(5, 6)));
		Assert.That(b.Width, Is.EqualTo(5));
		Assert.That(b.Height, Is.EqualTo(6));
		Assert.That(b.Center, Is.EqualTo(new Vec2D(3.5f, 6f)));
		Assert.That(b.Area, Is.EqualTo(30));
		Assert.That(b.IsEmpty, Is.False);
		Assert.That(Box2D.Empty.IsEmpty, Is.True);
	}

	[Test]
	public void Box2D_Deconstruct()
	{
		Assert.That((_,_,_,_) = new Box2D(1, 2, 3, 4), Is.EqualTo((1, 2, 3, 4)));
		Assert.That((_,_) = new Box2D(1, 2, 3, 4), Is.EqualTo((new Vec2D(1, 2), new Vec2D(3, 4))));
	}

	[Test]
	public void Box2D_Equality()
	{
		Box2D[] boxes = [ new(1, 0, 0, 0), new(0, 1, 0, 0), new(0, 0, 1, 0), new(0, 0, 0, 1), new(3, 4, 5, 6) ];
		foreach (var b1 in boxes) {
			foreach (var b2 in boxes) {
				var same = b1.Min == b2.Min && b1.Max == b2.Max;
				Assert.That(b1 == b2, Is.EqualTo(same));
				Assert.That(b1 != b2, Is.EqualTo(!same));
				Assert.That(b1.Equals(b2), Is.EqualTo(same));
				Assert.That(b1.Equals((object)b2), Is.EqualTo(same));
			}
		}
	}

	[Test]
	public void Box2D_Transform()
	{
		var b = new Box2D(2, 3, 5, 8);
		Assert.That((_,_,_,_) = b.Translated(new(-2, 4)), Is.EqualTo((0, 7, 3, 12)));
		Assert.That((_,_,_,_) = b.Inflated(new(-1, 3)), Is.EqualTo((3, 0, 4, 11)));

		b = new(1, 1, 3, 3);
		Assert.That((_,_,_,_) = b.Scaled(new(2, 3), new(0, 0)), Is.EqualTo((2, 3, 6, 9)));
		Assert.That((_,_,_,_) = b.Scaled(new(3, 2), new(3, 3)), Is.EqualTo((-3, -1, 3, 3)));
	}

	[Test]
	public void Box2D_Union()
	{
		Box2D b1 = new(1, 1, 2, 2), b2 = new(3, 3, 5, 5);
		Assert.That((_,_,_,_) = Box2D.Union(b1, b2), Is.EqualTo((1, 1, 5, 5)));
		Assert.That((_,_,_,_) = Box2D.Union(b2, b1), Is.EqualTo((1, 1, 5, 5)));
		b1 = new(2, 2, 6, 6);
		Assert.That((_,_,_,_) = Box2D.Union(b1, b2), Is.EqualTo((2, 2, 6, 6)));
		Assert.That((_,_,_,_) = Box2D.Union(b2, b1), Is.EqualTo((2, 2, 6, 6)));
		Assert.That(Box2D.Union(Box2D.Empty, Box2D.Empty), Is.EqualTo(Box2D.Empty));
	}

	[Test]
	public void Box2D_Intersect()
	{
		Box2D b1 = new(1, 1, 3, 3), b2 = new(2, 2, 4, 4);
		Assert.That((_,_,_,_) = Box2D.Intersect(b1, b2), Is.EqualTo((2, 2, 3, 3)));
		Assert.That((_,_,_,_) = Box2D.Intersect(b2, b1), Is.EqualTo((2, 2, 3, 3)));
		b1 = new(0, 0, 1, 1);
		Assert.That((_,_,_,_) = Box2D.Intersect(b1, b2), Is.EqualTo((0, 0, 0, 0)));
	}

	[Test]
	public void Box2D_Casting()
	{
		Assert.That((_,_,_,_) = (Box2D)new Box2(2, 4, 7, 9), 
			Is.EqualTo(((double)2, (double)4, (double)7, (double)9)));
		Assert.That((_,_,_,_) = (Box2D)new Box2L(2, 4, 7, 9), 
			Is.EqualTo(((double)2, (double)4, (double)7, (double)9)));
		Assert.That((_,_,_,_) = (Box2D)new Box2F(2, 4, 7, 9), 
			Is.EqualTo(((double)2, (double)4, (double)7, (double)9)));
	}

}


// Tests for Box2 in Space2D
public sealed partial class Space2DTests
{
	private static readonly (int X, int Y)[] _Coords = (
		from x in Enumerable.Range(0, 5)
		from y in Enumerable.Range(0, 5)
		select (x, y)
	).ToArray();

	
	[Test]
	public void Box2_Contains()
	{
		var box = new Box2(1, 1, 3, 3);
		foreach (var (x, y) in _Coords) {
			var contains = x >= box.Min.X && x <= box.Max.X && y >= box.Min.Y && y <= box.Max.Y;
			Assert.That(box.Contains(new Point2(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Point2L(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec2(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec2D(x, y)), Is.EqualTo(contains));
		}
	}

	
	[Test]
	public void Box2L_Contains()
	{
		var box = new Box2L(1, 1, 3, 3);
		foreach (var (x, y) in _Coords) {
			var contains = x >= box.Min.X && x <= box.Max.X && y >= box.Min.Y && y <= box.Max.Y;
			Assert.That(box.Contains(new Point2(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Point2L(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec2(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec2D(x, y)), Is.EqualTo(contains));
		}
	}

	
	[Test]
	public void Box2F_Contains()
	{
		var box = new Box2F(1, 1, 3, 3);
		foreach (var (x, y) in _Coords) {
			var contains = x >= box.Min.X && x <= box.Max.X && y >= box.Min.Y && y <= box.Max.Y;
			Assert.That(box.Contains(new Point2(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Point2L(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec2(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec2D(x, y)), Is.EqualTo(contains));
		}
	}

	
	[Test]
	public void Box2D_Contains()
	{
		var box = new Box2D(1, 1, 3, 3);
		foreach (var (x, y) in _Coords) {
			var contains = x >= box.Min.X && x <= box.Max.X && y >= box.Min.Y && y <= box.Max.Y;
			Assert.That(box.Contains(new Point2(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Point2L(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec2(x, y)), Is.EqualTo(contains));
			Assert.That(box.Contains(new Vec2D(x, y)), Is.EqualTo(contains));
		}
	}

}
