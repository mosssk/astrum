﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Maths;


// Tests for Point2
public sealed class Point2Tests
{

	[Test]
	public void Point2_Ctor()
	{
		Assert.That((_,_) = new Point2(), Is.EqualTo((0, 0)));
		Assert.That((_,_) = new Point2(5), Is.EqualTo((5, 5)));
		Assert.That((_,_) = new Point2(-1, 1), Is.EqualTo((-1, 1)));
	}

	[Test]
	public void Point2_Fields()
	{
		var v = new Point2(10, 15);
		Assert.That(v.X, Is.EqualTo(10));
		Assert.That(v.Y, Is.EqualTo(15));
		Assert.That(v.Length, Is.EqualTo(Single.Sqrt(10*10 + 15*15)));
		Assert.That(v.LengthSq, Is.EqualTo(10*10 + 15*15));
	}

	[Test]
	public void Point2_Constants()
	{
		Assert.That((_,_) = Point2.Zero, Is.EqualTo((0, 0)));
		Assert.That((_,_) = Point2.One, Is.EqualTo((1, 1)));
		Assert.That((_,_) = Point2.UnitX, Is.EqualTo((1, 0)));
		Assert.That((_,_) = Point2.UnitY, Is.EqualTo((0, 1)));
	}

	[Test]
	public void Point2_Equality()
	{
		Point2[] points = [ new(1, 0), new(0, 1), new(10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X == p2.X && p1.Y == p2.Y;
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y)));
				Assert.That((_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y)));
				Assert.That((_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y)));
				Assert.That((_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y)));
			}
		}
	}

	[Test]
	public void Point2_Operators()
	{
		Point2 p1 = new(2, 8), p2 = new(6, 3);

		Assert.That((_,_) = p1 + p2, Is.EqualTo((8, 11)));
		Assert.That((_,_) = p1 + 10, Is.EqualTo((12, 18)));
		Assert.That((_,_) = p1 - p2, Is.EqualTo((-4, 5)));
		Assert.That((_,_) = p1 - 10, Is.EqualTo((-8, -2)));
		Assert.That((_,_) = p1 * p2, Is.EqualTo((12, 24)));
		Assert.That((_,_) = p1 * 10, Is.EqualTo((20, 80)));
		Assert.That((_,_) = p1 / p2, Is.EqualTo((0, 2)));
		Assert.That((_,_) = p1 /  2, Is.EqualTo((1, 4)));
		Assert.That((_,_) = p1 % p2, Is.EqualTo((2, 2)));
		Assert.That((_,_) = p1 %  5, Is.EqualTo((2, 3)));

		(p1, p2) = (new(123, 567), new(426, 846));
		Assert.That((_,_) = p1 & p2, Is.EqualTo((123 & 426, 567 & 846)));
		Assert.That((_,_) = p1 & 99, Is.EqualTo((123 &  99, 567 &  99)));
		Assert.That((_,_) = p1 | p2, Is.EqualTo((123 | 426, 567 | 846)));
		Assert.That((_,_) = p1 | 99, Is.EqualTo((123 |  99, 567 |  99)));
		Assert.That((_,_) = p1 ^ p2, Is.EqualTo((123 ^ 426, 567 ^ 846)));
		Assert.That((_,_) = p1 ^ 99, Is.EqualTo((123 ^  99, 567 ^  99)));
		Assert.That((_,_) = ~p1, Is.EqualTo((~123, ~567)));
		Assert.That((_,_) = -p1, Is.EqualTo((-123, -567)));
	}

	[Test]
	public void Point2_PointOps()
	{
		Point2 p1 = new(3, 5), p2 = new(-1, 7);
		Assert.That(p1.Dot(p2), Is.EqualTo(32));
		Assert.That(new Point2(1, 0).AngleWith(new(1, 1)).ApproxEqual(Angle.D45, 1e-3f), Is.True);
		Assert.That(new Point2(1, 0).AngleWith(new(0, 1)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Point2(1, 0).AngleWith(new(-1, 1)).ApproxEqual(Angle.D45 + Angle.D90, 1e-3f), Is.True);
		Assert.That((_,_) = Point2.Clamp(p1, new(-1, -1), new(0, 1)), Is.EqualTo((0, 1))); 
		Assert.That((_,_) = Point2.Clamp(p1, new(8, 9), new(10, 10)), Is.EqualTo((8, 9))); 
		Assert.That((_,_) = Point2.Clamp(p1, new(-1, -1), new(10, 10)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Point2.Min(p1, new(-1, -2)), Is.EqualTo((-1, -2)));
		Assert.That((_,_) = Point2.Min(p1, new(10, 10)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Point2.Max(p1, new(-1, -2)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Point2.Max(p1, new(10, 11)), Is.EqualTo((10, 11)));
	}

	[Test]
	public void Point2_VectorCasting()
	{
		Assert.That((Point2)new Vec2(-1.5f, 2.5f), Is.EqualTo(new Point2(-1, 2)));
		Assert.That((Point2)new Vec2D(-1.5, 2.5), Is.EqualTo(new Point2(-1, 2)));
	}


	[Test]
	public void Point2L_Ctor()
	{
		Assert.That((_,_) = new Point2L(), Is.EqualTo((0, 0)));
		Assert.That((_,_) = new Point2L(5), Is.EqualTo((5, 5)));
		Assert.That((_,_) = new Point2L(-1, 1), Is.EqualTo((-1, 1)));
	}

	[Test]
	public void Point2L_Fields()
	{
		var v = new Point2L(10, 15);
		Assert.That(v.X, Is.EqualTo(10));
		Assert.That(v.Y, Is.EqualTo(15));
		Assert.That(v.Length, Is.EqualTo(Double.Sqrt(10*10 + 15*15)));
		Assert.That(v.LengthSq, Is.EqualTo(10*10 + 15*15));
	}

	[Test]
	public void Point2L_Constants()
	{
		Assert.That((_,_) = Point2L.Zero, Is.EqualTo((0, 0)));
		Assert.That((_,_) = Point2L.One, Is.EqualTo((1, 1)));
		Assert.That((_,_) = Point2L.UnitX, Is.EqualTo((1, 0)));
		Assert.That((_,_) = Point2L.UnitY, Is.EqualTo((0, 1)));
	}

	[Test]
	public void Point2L_Equality()
	{
		Point2L[] points = [ new(1, 0), new(0, 1), new(10, 10) ];
		foreach (var p1 in points) {
			foreach (var p2 in points) {
				var same = p1.X == p2.X && p1.Y == p2.Y;
				Assert.That(p1 == p2, Is.EqualTo(same));
				Assert.That(p1 != p2, Is.EqualTo(!same));
				Assert.That(p1.Equals(p2), Is.EqualTo(same));
				Assert.That(p1.Equals((object)p2), Is.EqualTo(same));

				Assert.That((_,_) = p1 <= p2, Is.EqualTo((p1.X <= p2.X, p1.Y <= p2.Y)));
				Assert.That((_,_) = p1 <  p2, Is.EqualTo((p1.X <  p2.X, p1.Y <  p2.Y)));
				Assert.That((_,_) = p1 >= p2, Is.EqualTo((p1.X >= p2.X, p1.Y >= p2.Y)));
				Assert.That((_,_) = p1 >  p2, Is.EqualTo((p1.X >  p2.X, p1.Y >  p2.Y)));
			}
		}
	}

	[Test]
	public void Point2L_Operators()
	{
		Point2L p1 = new(2, 8), p2 = new(6, 3);

		Assert.That((_,_) = p1 + p2, Is.EqualTo((8, 11)));
		Assert.That((_,_) = p1 + 10, Is.EqualTo((12, 18)));
		Assert.That((_,_) = p1 - p2, Is.EqualTo((-4, 5)));
		Assert.That((_,_) = p1 - 10, Is.EqualTo((-8, -2)));
		Assert.That((_,_) = p1 * p2, Is.EqualTo((12, 24)));
		Assert.That((_,_) = p1 * 10, Is.EqualTo((20, 80)));
		Assert.That((_,_) = p1 / p2, Is.EqualTo((0, 2)));
		Assert.That((_,_) = p1 /  2, Is.EqualTo((1, 4)));
		Assert.That((_,_) = p1 % p2, Is.EqualTo((2, 2)));
		Assert.That((_,_) = p1 %  5, Is.EqualTo((2, 3)));

		(p1, p2) = (new(123, 567), new(426, 846));
		Assert.That((_,_) = p1 & p2, Is.EqualTo((123 & 426, 567 & 846)));
		Assert.That((_,_) = p1 & 99, Is.EqualTo((123 &  99, 567 &  99)));
		Assert.That((_,_) = p1 | p2, Is.EqualTo((123 | 426, 567 | 846)));
		Assert.That((_,_) = p1 | 99, Is.EqualTo((123 |  99, 567 |  99)));
		Assert.That((_,_) = p1 ^ p2, Is.EqualTo((123 ^ 426, 567 ^ 846)));
		Assert.That((_,_) = p1 ^ 99, Is.EqualTo((123 ^  99, 567 ^  99)));
		Assert.That((_,_) = ~p1, Is.EqualTo((~123, ~567)));
		Assert.That((_,_) = -p1, Is.EqualTo((-123, -567)));
	}

	[Test]
	public void Point2L_PointOps()
	{
		Point2L p1 = new(3, 5), p2 = new(-1, 7);
		Assert.That(p1.Dot(p2), Is.EqualTo(32));
		Assert.That(new Point2L(1, 0).AngleWith(new(1, 1)).ApproxEqual(Angle.D45, 1e-3f), Is.True);
		Assert.That(new Point2L(1, 0).AngleWith(new(0, 1)).ApproxEqual(Angle.D90, 1e-3f), Is.True);
		Assert.That(new Point2L(1, 0).AngleWith(new(-1, 1)).ApproxEqual(Angle.D45 + Angle.D90, 1e-3f), Is.True);
		Assert.That((_,_) = Point2L.Clamp(p1, new(-1, -1), new(0, 1)), Is.EqualTo((0, 1))); 
		Assert.That((_,_) = Point2L.Clamp(p1, new(8, 9), new(10, 10)), Is.EqualTo((8, 9))); 
		Assert.That((_,_) = Point2L.Clamp(p1, new(-1, -1), new(10, 10)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Point2L.Min(p1, new(-1, -2)), Is.EqualTo((-1, -2)));
		Assert.That((_,_) = Point2L.Min(p1, new(10, 10)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Point2L.Max(p1, new(-1, -2)), Is.EqualTo((3, 5)));
		Assert.That((_,_) = Point2L.Max(p1, new(10, 11)), Is.EqualTo((10, 11)));
	}

	[Test]
	public void Point2L_VectorCasting()
	{
		Assert.That((Point2L)new Vec2(-1.5f, 2.5f), Is.EqualTo(new Point2L(-1, 2)));
		Assert.That((Point2L)new Vec2D(-1.5, 2.5), Is.EqualTo(new Point2L(-1, 2)));
	}


	[Test]
	public void Point_Casting()
	{
		Point2 p1 = new(-10, 11);
		Point2L p2 = new(-10, 11);
		Assert.That((Point2L)p1, Is.EqualTo(p2));
		Assert.That((Point2)p2, Is.EqualTo(p1));
		p2 = new(Int64.MinValue, Int64.MaxValue);
		Assert.That((Point2)p2, Is.EqualTo(new Point2((int)p2.X, (int)p2.Y)));
	}
}
