/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

namespace Astrum;


// Tests for NativeStringUtf8Handle
internal sealed unsafe class NativeStringUtf8HandleTests
{
	[Test]
	public void DefaultFields()
	{
		using var handle = new NativeStringUtf8Handle();
		Assert.That(handle.Length, Is.Zero);
		Assert.That((nuint)handle.Data, Is.EqualTo((nuint)0));
	}
	
	[Test]
	public void ZeroLengthString()
	{
		using var handle = NativeStringUtf8Handle.Create("");
		Assert.That(handle.Length, Is.Zero);
		Assert.That((nuint)handle.Data, Is.Not.EqualTo(0));
		Assert.That(handle.Data[0], Is.Zero);
	}

	[Test]
	public void CreateString()
	{
		using var handle = NativeStringUtf8Handle.Create("asDF");
		Assert.That(handle.Length, Is.EqualTo(4));
		Assert.That((nuint)handle.Data, Is.Not.Zero);
		Assert.That(handle.Data[0], Is.EqualTo((int)'a'));
		Assert.That(handle.Data[1], Is.EqualTo((int)'s'));
		Assert.That(handle.Data[2], Is.EqualTo((int)'D'));
		Assert.That(handle.Data[3], Is.EqualTo((int)'F'));
		Assert.That(handle.Data[4], Is.Zero);
	}

	[Test]
	public void GetString()
	{
		using var handle = NativeStringUtf8Handle.Create("qwerty");
		Assert.That(handle.GetString(), Is.EqualTo("qwerty"));
	}

	[Test]
	public void DisposeString()
	{
		var handle = NativeStringUtf8Handle.Create("test");
		handle.Dispose();
		Assert.That(handle.Length, Is.Zero);
		Assert.That((nuint)handle.Data, Is.EqualTo((nuint)0));
	}
}
