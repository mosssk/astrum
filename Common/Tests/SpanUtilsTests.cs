﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Text;

// ReSharper disable IdentifierTypo StringLiteralTypo

namespace Astrum;


// Tests for SpanUtils
internal sealed unsafe class SpanUtilsTests
{
	[Test]
	public void AsByteSpan()
	{
		Span<char> abcdefg = stackalloc char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g' };
		ReadOnlySpan<byte> abcdefgBytes = Encoding.Unicode.GetBytes("abcdefg");
		Assert.That(abcdefg.AsByteSpan().SequenceEqual(abcdefgBytes), Is.True);
		Assert.That("abcdefg".AsSpan().AsByteSpan().SequenceEqual(abcdefgBytes), Is.True);
		
		Span<int> intVal = stackalloc int[] { 0x1245789A };
		ReadOnlySpan<byte> intValBytes = stackalloc byte[] { 0x9A, 0x78, 0x45, 0x12 };
		Assert.That(intVal.AsByteSpan().SequenceEqual(intValBytes), Is.True);
		Assert.That(Span<char>.Empty.AsByteSpan().IsEmpty, Is.True);
	}

	[Test]
	public void AsByteSpan_EmptySpan()
	{
		Assert.That(Span<char>.Empty.AsByteSpan().IsEmpty, Is.True);
		Assert.That(ReadOnlySpan<char>.Empty.AsByteSpan().IsEmpty, Is.True);
	}

	[Test]
	public void WrapBytes()
	{
		byte val1 = 255;
		var span1 = SpanUtils.WrapBytes(in val1);
		Assert.That(span1.Length, Is.EqualTo(1));
		Assert.That(span1[0], Is.EqualTo(val1));

		uint val2 = 0x1324_5768;
		var span2 = SpanUtils.WrapBytes(in val2);
		Assert.That(span2.Length, Is.EqualTo(4));
		Assert.That(span2[0], Is.EqualTo(0x68));
		Assert.That(span2[1], Is.EqualTo(0x57));
		Assert.That(span2[2], Is.EqualTo(0x24));
		Assert.That(span2[3], Is.EqualTo(0x13));
	}

	[Test]
	public void CreateCStringSpan()
	{
		Assert.That(SpanUtils.CreateCStringSpan(null).IsEmpty, Is.True);

		var cstr = stackalloc byte[] { (byte)'a', (byte)'b', (byte)'c', 0 };
		var span = SpanUtils.CreateCStringSpan(cstr);
		Assert.That(span.Length, Is.EqualTo(3));
		Assert.That(span[0], Is.EqualTo((byte)'a'));
		Assert.That(span[1], Is.EqualTo((byte)'b'));
		Assert.That(span[2], Is.EqualTo((byte)'c'));
		cstr[0] = (byte)'d';
		Assert.That(span[0], Is.EqualTo((byte)'d'));
	}
}
