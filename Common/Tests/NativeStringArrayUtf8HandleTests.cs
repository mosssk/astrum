/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;

namespace Astrum;


// Tests for NativeStringArrayUtf8Handle
internal sealed unsafe class NativeStringArrayUtf8HandleTests
{
	[Test]
	public void DefaultFields()
	{
		using var handle = new NativeStringArrayUtf8Handle();
		Assert.That(handle.Count, Is.Zero);
		Assert.That((nuint)handle.Data, Is.EqualTo((nuint)0));
	}

	[Test]
	public void ZeroLengthArray()
	{
		using var handle = NativeStringArrayUtf8Handle.Create();
		Assert.That(handle.Count, Is.Zero);
		Assert.That((nuint)handle.Data, Is.EqualTo((nuint)0));
	}

	[Test]
	public void CreateArray()
	{
		using var handle = NativeStringArrayUtf8Handle.Create("test", "asDF");
		Assert.That(handle.Count, Is.EqualTo(2));
		Assert.That((nuint)handle.Data, Is.Not.EqualTo((nuint)0));
		Assert.That(Marshal.PtrToStringAnsi((nint)handle.Data[0]), Is.EqualTo("test"));
		Assert.That(Marshal.PtrToStringAnsi((nint)handle.Data[1]), Is.EqualTo("asDF"));
	}

	[Test]
	public void GetString()
	{
		using var handle = NativeStringArrayUtf8Handle.Create("qwerty", "asdf");
		Assert.That(handle.GetString(0), Is.EqualTo("qwerty"));
		Assert.That(handle.GetString(1), Is.EqualTo("asdf"));
		
		Assert.Throws<ArgumentOutOfRangeException>(static () => {
			using var handle = NativeStringArrayUtf8Handle.Create("test");
			handle.GetString(handle.Count);
		});
	}

	[Test]
	public void DisposeArray()
	{
		var handle = NativeStringArrayUtf8Handle.Create("test");
		handle.Dispose();
		Assert.That(handle.Count, Is.Zero);
		Assert.That((nuint)handle.Data, Is.EqualTo((nuint)0));
	}
}
