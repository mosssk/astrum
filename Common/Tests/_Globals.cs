/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

global using NUnit.Framework;

using System;
using Astrum;

[assembly: AstrumApplication("Astrum.Common.Tests")]


// Global setup and teardown
[SetUpFixture]
internal sealed class GlobalFixture
{
	[OneTimeSetUp]
	public void SetupTests() => Environment.SetEnvironmentVariable("_ASTRUM_TESTS", "1");
}
