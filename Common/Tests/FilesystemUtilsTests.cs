﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


// Tests for FilesystemUtils
internal sealed class FilesystemUtilsTests
{
	[Test]
	public void SanitizeName()
	{
		var alnums = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345678980_-.";
		Assert.That(FilesystemUtils.SanitizeName(alnums, true), Is.EqualTo(alnums));
		Assert.That(FilesystemUtils.SanitizeName(alnums, false), Is.EqualTo(alnums));
		var alternate = "a!b@c#d$e%f^g&h*i(j)k[l]m{n}o;p:q'r\"s<t,u>v.w?x|y=z+a`b~c";
		Assert.That(FilesystemUtils.SanitizeName(alternate, true), Is.EqualTo("abcdefghijklmnopqrstuv.wxyzabc"));
		Assert.That(FilesystemUtils.SanitizeName(alternate, false), Is.EqualTo("a_b_c_d_e_f_g_h_i_j_k_l_m_n_o_p_q_r_s_t_u_v.w_x_y_z_a_b_c"));
	}

	[Test]
	public void SanitizeName_Throws_InvalidArgs()
	{
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("", true));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("", false));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("      ", true));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("      ", false));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("\t\t\n", true));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("\t\t\n", false));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("\t\t\n    ", true));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("\t\t\n    ", false));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("_-", true));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("_-", false));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("!@#$%^&*()[]{};:'\"<,>?/\\|=+`~", true));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("!@#$%^&*()[]{};:'\"<,>?/\\|=+`~", false));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("/\\", true));
		Assert.Throws<ArgumentException>(() => FilesystemUtils.SanitizeName("/\\", false));
	}
}
