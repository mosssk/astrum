/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.IO;
using System.IO.Hashing;
using System.Reflection;
using Microsoft.Extensions.Logging;

namespace Astrum;


/// <summary>
/// Provides access to the highest-level common functionality and static configuration for Astrum applications,
/// including headless ones.
/// </summary>
public static class Astrum
{
	#region Fields
	/// <summary>The application assembly reference.</summary>
	public static readonly Assembly ApplicationAssembly;

	/// <inheritdoc cref="AstrumApplicationAttribute.Name"/>
	public static readonly string ApplicationName;
	
	/// <summary>The reported version of the application assembly.</summary>
	public static readonly Version ApplicationVersion;

	/// <summary>A unique hashed ID for the application and Astrum engine configuration.</summary>
	/// <remarks>This hash remains constant as long as the app name, version, and engine version are the same.</remarks>
	public static readonly Guid ApplicationHash;

	/// <summary>The version of the Astrum engine runtime.</summary>
	public static readonly Version EngineVersion;
	
	// Environment testing flag
	internal static readonly bool IsTesting;
	
	// Engine assembly
	internal static readonly Assembly EngineAssembly;
	
	// Application and engine cache locations
	internal static readonly string EngineCachePath;
	internal static readonly string DataRootPath;
	#endregion // Fields


	/// <summary>Sets the logger instance to use for internal library logging.</summary>
	/// <remarks>This method should be called as early as possible in the application to catch all logs.</remarks>
	/// <param name="logger">The logger instance to use, or <c>null</c> to disable library logging.</param>
	public static void UseLogger(ILogger? logger) => InternalLogging.UseInternalLogger(logger);


	/// <summary>Creates a path relative to the application data root.</summary>
	/// <param name="path">The relative path from the application data root.</param>
	/// <returns>The full path.</returns>
	/// <exception cref="ArgumentException">The path is empty, or absolute.</exception>
	public static string CreateDataPath(ReadOnlySpan<char> path)
	{
		if (path.IsEmpty) throw new ArgumentException("Path cannot be empty", nameof(path));
		if (Path.IsPathFullyQualified(path)) throw new ArgumentException("Path cannot be fully qualified", nameof(path));
		return Path.Join(DataRootPath, path);
	}
	
	
	//
	static Astrum()
	{
		// Note: logging is not available in this method
		
		ApplicationAssembly = Assembly.GetEntryAssembly()!;
		ApplicationVersion = ApplicationAssembly.GetName().Version ?? new(1, 0, 0, 0);
		EngineAssembly = typeof(Astrum).Assembly;
		EngineVersion = EngineAssembly.GetName().Version!;
		IsTesting = Environment.GetEnvironmentVariable("_ASTRUM_TESTS") is not (null or "0");

		// Load attribute
		var attr = ApplicationAssembly.GetCustomAttribute<AstrumApplicationAttribute>() ??
			throw new ApplicationException("Applications using Astrum must have an AstrumApplicationAttribute.");
		ApplicationName = attr.Name;
		
		// Calculate hash
		var hashData = $"{ApplicationName}{ApplicationAssembly.GetName().Version}{EngineAssembly.GetName().Version}";
		var hashValue = XxHash128.HashToUInt128(hashData.AsSpan().AsByteSpan());
		ApplicationHash = new(SpanUtils.WrapBytes(in hashValue));
		
		// Calculate paths
		var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
		var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
		EngineCachePath = Path.Join(appData, "Astrum", "cache");
		DataRootPath = attr.DataRoot switch {
			DataRoot.AppData            => Path.Join(appData, attr.FolderName),
			DataRoot.AppDataSubfolder   => Path.Join(appData, "Astrum", "appdata", attr.FolderName),
			DataRoot.Documents          => Path.Join(documents, attr.FolderName),
			DataRoot.DocumentsSubfolder => Path.Join(documents, "Astrum", attr.FolderName),
			DataRoot.InstallFolder      => Path.GetDirectoryName(Environment.ProcessPath) ?? Directory.GetCurrentDirectory(),
			_                           => throw new ArgumentOutOfRangeException()
		};
		Directory.CreateDirectory(EngineCachePath);
		Directory.CreateDirectory(DataRootPath);
	}
	
	
	/// <summary>Common root locations for application data files.</summary>
	public enum DataRoot
	{
		/// <summary>Personal local application data folder (files in <c>%APPDATA%/{App Folder Name}</c>).</summary>
		AppData,
		/// <summary>
		/// Personal local application data folder in the Astrum subdirectory (files in
		/// <c>%APPDATA%/Astrum/appdata/{App Folder Name}</c>).
		/// </summary>
		AppDataSubfolder,
		/// <summary>Personal documents folder (files in <c>%Documents%/{App Folder Name}</c>).</summary>
		Documents,
		/// <summary>
		/// Personal documents folder in the Astrum subdirectory (files in <c>%Documents%/Astrum/{App Folder Name}</c>).
		/// </summary>
		DocumentsSubfolder,
		/// <summary>Application executable install directory.</summary>
		InstallFolder
	}
}
