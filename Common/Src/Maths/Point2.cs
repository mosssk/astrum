﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>A two-component vector of <c>int</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 8)]
public struct Point2 :
	IEquatable<Point2>, IEqualityOperators<Point2, Point2, bool>,
	IAdditionOperators<Point2, Point2, Point2>, IAdditionOperators<Point2, int, Point2>,
	ISubtractionOperators<Point2, Point2, Point2>, ISubtractionOperators<Point2, int, Point2>,
	IMultiplyOperators<Point2, Point2, Point2>, IMultiplyOperators<Point2, int, Point2>,
	IDivisionOperators<Point2, Point2, Point2>, IDivisionOperators<Point2, int, Point2>,
	IModulusOperators<Point2, Point2, Point2>, IModulusOperators<Point2, int, Point2>,
	IBitwiseOperators<Point2, Point2, Point2>, IBitwiseOperators<Point2, int, Point2>,
	IUnaryNegationOperators<Point2, Point2>
{
	#region Constants
	/// <summary>Point with all-zero components.</summary>
	public static readonly Point2 Zero  = new();

	/// <summary>Point with all-one components.</summary>
	public static readonly Point2 One   = new(1);
	/// <summary>Point with unit-length along the X-axis.</summary>
	public static readonly Point2 UnitX = new(1, 0);
	/// <summary>Point with unit-length along the Y-axis.</summary>
	public static readonly Point2 UnitY = new(0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public int X;
	/// <summary>The Y-component.</summary>
	public int Y;

	/// <summary>The length of the point as a vector.</summary>
	public readonly float Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Single.Sqrt(X*X + Y*Y);
	}

	/// <summary>The squared length of the point as a vector.</summary>
	public readonly float LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y;
	}
	#endregion // Fields


	/// <summary>Construct a point with equal components.</summary>
	public Point2(int v) => X = Y = v;

	/// <summary>Construct a point from the components.</summary>
	public Point2(int x, int y) => (X, Y) = (x, y);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Point2 o) => X == o.X && Y == o.Y;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point2 p && X == p.X && Y == p.Y;

	public readonly override int GetHashCode() => HashCode.Combine(X, Y);

	public readonly override string ToString() => $"{{{X},{Y}}}";

	public readonly string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) => 
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)}}}";

	public readonly void Deconstruct(out int x, out int y) => (x, y) = (X, Y);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly int Dot(Point2 o) => X*o.X + Y*o.Y;

	/// <summary>Calculates angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point2 o) => Angle.Rad((float)Single.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Component-wise clamp of the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2 Clamp(Point2 v, Point2 min, Point2 max) =>
		new(Int32.Clamp(v.X, min.X, max.X), Int32.Clamp(v.Y, min.Y, max.Y)); 

	/// <summary>Component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2 Min(Point2 l, Point2 r) => new(Int32.Min(l.X, r.X), Int32.Min(l.Y, r.Y));

	/// <summary>Component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2 Max(Point2 l, Point2 r) => new(Int32.Max(l.X, r.X), Int32.Max(l.Y, r.Y)); 
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point2 l, Point2 r) => l.Equals(r);
	public static bool operator != (Point2 l, Point2 r) => !l.Equals(r);

	public static Bool2 operator <= (Point2 l, Point2 r) => new(l.X <= r.X, l.Y <= r.Y);
	public static Bool2 operator <  (Point2 l, Point2 r) => new(l.X <  r.X, l.Y <  r.Y);
	public static Bool2 operator >= (Point2 l, Point2 r) => new(l.X >= r.X, l.Y >= r.Y);
	public static Bool2 operator >  (Point2 l, Point2 r) => new(l.X >  r.X, l.Y >  r.Y);

	public static Point2 operator + (Point2 l, Point2 r) => new(l.X + r.X, l.Y + r.Y);
	public static Point2 operator + (Point2 l, int r) => new(l.X + r, l.Y + r);
	public static Point2 operator - (Point2 l, Point2 r) => new(l.X - r.X, l.Y - r.Y);
	public static Point2 operator - (Point2 l, int r) => new(l.X - r, l.Y - r);
	public static Point2 operator * (Point2 l, Point2 r) => new(l.X * r.X, l.Y * r.Y);
	public static Point2 operator * (Point2 l, int r) => new(l.X * r, l.Y * r);
	public static Point2 operator / (Point2 l, Point2 r) => new(l.X / r.X, l.Y / r.Y);
	public static Point2 operator / (Point2 l, int r) => new(l.X / r, l.Y / r);
	public static Point2 operator % (Point2 l, Point2 r) => new(l.X % r.X, l.Y % r.Y);
	public static Point2 operator % (Point2 l, int r) => new(l.X % r, l.Y % r);

	public static Point2 operator & (Point2 l, Point2 r) => new(l.X & r.X, l.Y & r.Y);
	public static Point2 operator & (Point2 l, int r) => new(l.X & r, l.Y & r);
	public static Point2 operator | (Point2 l, Point2 r) => new(l.X | r.X, l.Y | r.Y);
	public static Point2 operator | (Point2 l, int r) => new(l.X | r, l.Y | r);
	public static Point2 operator ^ (Point2 l, Point2 r) => new(l.X ^ r.X, l.Y ^ r.Y);
	public static Point2 operator ^ (Point2 l, int r) => new(l.X ^ r, l.Y ^ r);
	public static Point2 operator ~ (Point2 r) => new(~r.X, ~r.Y);

	public static Point2 operator - (Point2 r) => new(-r.X, -r.Y);

	public static explicit operator Point2 (Vec2 v) => new((int)v.X, (int)v.Y);
	public static explicit operator Point2 (Vec2D v) => new((int)v.X, (int)v.Y);

	public static explicit operator Point2 (Point2L o) => new((int)o.X, (int)o.Y);
	#endregion // Operators
}


/// <summary>A two-component vector of <c>long</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Point2L :
	IEquatable<Point2L>, IEqualityOperators<Point2L, Point2L, bool>,
	IAdditionOperators<Point2L, Point2L, Point2L>, IAdditionOperators<Point2L, long, Point2L>,
	ISubtractionOperators<Point2L, Point2L, Point2L>, ISubtractionOperators<Point2L, long, Point2L>,
	IMultiplyOperators<Point2L, Point2L, Point2L>, IMultiplyOperators<Point2L, long, Point2L>,
	IDivisionOperators<Point2L, Point2L, Point2L>, IDivisionOperators<Point2L, long, Point2L>,
	IModulusOperators<Point2L, Point2L, Point2L>, IModulusOperators<Point2L, long, Point2L>,
	IBitwiseOperators<Point2L, Point2L, Point2L>, IBitwiseOperators<Point2L, long, Point2L>,
	IUnaryNegationOperators<Point2L, Point2L>
{
	#region Constants
	/// <summary>Point with all-zero components.</summary>
	public static readonly Point2L Zero  = new();

	/// <summary>Point with all-one components.</summary>
	public static readonly Point2L One   = new(1);
	/// <summary>Point with unit-length along the X-axis.</summary>
	public static readonly Point2L UnitX = new(1, 0);
	/// <summary>Point with unit-length along the Y-axis.</summary>
	public static readonly Point2L UnitY = new(0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public long X;
	/// <summary>The Y-component.</summary>
	public long Y;

	/// <summary>The length of the point as a vector.</summary>
	public readonly double Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Double.Sqrt(X*X + Y*Y);
	}

	/// <summary>The squared length of the point as a vector.</summary>
	public readonly double LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y;
	}
	#endregion // Fields


	/// <summary>Construct a point with equal components.</summary>
	public Point2L(long v) => X = Y = v;

	/// <summary>Construct a point from the components.</summary>
	public Point2L(long x, long y) => (X, Y) = (x, y);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Point2L o) => X == o.X && Y == o.Y;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Point2L p && X == p.X && Y == p.Y;

	public readonly override int GetHashCode() => HashCode.Combine(X, Y);

	public readonly override string ToString() => $"{{{X},{Y}}}";

	public readonly string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) => 
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)}}}";

	public readonly void Deconstruct(out long x, out long y) => (x, y) = (X, Y);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly long Dot(Point2L o) => X*o.X + Y*o.Y;

	/// <summary>Calculates angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point2L o) => Angle.Rad((float)Double.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Component-wise clamp of the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2L Clamp(Point2L v, Point2L min, Point2L max) =>
		new(Int64.Clamp(v.X, min.X, max.X), Int64.Clamp(v.Y, min.Y, max.Y)); 

	/// <summary>Component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2L Min(Point2L l, Point2L r) => new(Int64.Min(l.X, r.X), Int64.Min(l.Y, r.Y));

	/// <summary>Component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point2L Max(Point2L l, Point2L r) => new(Int64.Max(l.X, r.X), Int64.Max(l.Y, r.Y)); 
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point2L l, Point2L r) => l.Equals(r);
	public static bool operator != (Point2L l, Point2L r) => !l.Equals(r);

	public static Bool2 operator <= (Point2L l, Point2L r) => new(l.X <= r.X, l.Y <= r.Y);
	public static Bool2 operator <  (Point2L l, Point2L r) => new(l.X <  r.X, l.Y <  r.Y);
	public static Bool2 operator >= (Point2L l, Point2L r) => new(l.X >= r.X, l.Y >= r.Y);
	public static Bool2 operator >  (Point2L l, Point2L r) => new(l.X >  r.X, l.Y >  r.Y);

	public static Point2L operator + (Point2L l, Point2L r) => new(l.X + r.X, l.Y + r.Y);
	public static Point2L operator + (Point2L l, long r) => new(l.X + r, l.Y + r);
	public static Point2L operator - (Point2L l, Point2L r) => new(l.X - r.X, l.Y - r.Y);
	public static Point2L operator - (Point2L l, long r) => new(l.X - r, l.Y - r);
	public static Point2L operator * (Point2L l, Point2L r) => new(l.X * r.X, l.Y * r.Y);
	public static Point2L operator * (Point2L l, long r) => new(l.X * r, l.Y * r);
	public static Point2L operator / (Point2L l, Point2L r) => new(l.X / r.X, l.Y / r.Y);
	public static Point2L operator / (Point2L l, long r) => new(l.X / r, l.Y / r);
	public static Point2L operator % (Point2L l, Point2L r) => new(l.X % r.X, l.Y % r.Y);
	public static Point2L operator % (Point2L l, long r) => new(l.X % r, l.Y % r);

	public static Point2L operator & (Point2L l, Point2L r) => new(l.X & r.X, l.Y & r.Y);
	public static Point2L operator & (Point2L l, long r) => new(l.X & r, l.Y & r);
	public static Point2L operator | (Point2L l, Point2L r) => new(l.X | r.X, l.Y | r.Y);
	public static Point2L operator | (Point2L l, long r) => new(l.X | r, l.Y | r);
	public static Point2L operator ^ (Point2L l, Point2L r) => new(l.X ^ r.X, l.Y ^ r.Y);
	public static Point2L operator ^ (Point2L l, long r) => new(l.X ^ r, l.Y ^ r);
	public static Point2L operator ~ (Point2L r) => new(~r.X, ~r.Y);

	public static Point2L operator - (Point2L r) => new(-r.X, -r.Y);

	public static explicit operator Point2L (Vec2 v) => new((long)v.X, (long)v.Y);
	public static explicit operator Point2L (Vec2D v) => new((long)v.X, (long)v.Y);

	public static implicit operator Point2L (Point2 o) => new(o.X, o.Y);
	#endregion // Operators
}

