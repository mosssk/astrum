﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>Three-component vector of booleans.</summary>
[StructLayout(LayoutKind.Sequential, Size = 3)]
public struct Bool3 :
	IEquatable<Bool3>,
	IEqualityOperators<Bool3, Bool3, bool>, IBitwiseOperators<Bool3, Bool3, Bool3>
{
	#region Fields
	/// <summary>The X component.</summary>
	public bool X;
	/// <summary>The Y component.</summary>
	public bool Y;
	/// <summary>The Z component.</summary>
	public bool Z;

	/// <summary>If any components are true.</summary>
	public readonly bool Any => X || Y || Z;
	/// <summary>If all components are true.</summary>
	public readonly bool All => X && Y && Z;
	/// <summary>If all components are false.</summary>
	public readonly bool None => !Any;

	/// <summary>The number of components that are true.</summary>
	public readonly uint Count => (X ? 1u : 0u) + (Y ? 1u : 0u) + (Z ? 1u : 0u);
	#endregion // Fields

	
	/// <summary>Construct a vector with equal components.</summary>
	public Bool3(bool b) => X = Y = Z = b;

	/// <summary>Construct a vector by appending a Z component.</summary>
	public Bool3(Bool2 xy, bool z) => (X, Y, Z) = (xy.X, xy.Y, z);

	/// <summary>Construct a vector with the components.</summary>
	public Bool3(bool x, bool y, bool z) => (X, Y, Z) = (x, y, z);


	#region Base
	public readonly bool Equals(Bool3 o) => o.X == X && o.Y == Y && o.Z == Z;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => 
		o is Bool3 b && b.X == X && b.Y == Y && b.Z == Z;

	public readonly override int GetHashCode() => (X, Y, Z) switch {
		(false, false, false) => 0, (false, false, true) => 1,
		(false, true, false)  => 2, (false, true, true)  => 3,
		(true, false, false)  => 4, (true, false, true)  => 5,
		(true, true, false)   => 6, (true, true, true)   => 8
};

	public readonly override string ToString() => (X, Y, Z) switch {
		(false, false, false) => "{F,F,F}", (false, false, true) => "{F,F,T}",
		(false, true, false)  => "{F,T,F}", (false, true, true)  => "{F,T,T}",
		(true, false, false)  => "{T,F,F}", (true, false, true)  => "{T,F,T}",
		(true, true, false)   => "{T,T,F}", (true, true, true)   => "{T,T,T}"
	};

	public readonly void Deconstruct(out bool x, out bool y, out bool z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	#region Operators
	public static bool operator == (Bool3 l, Bool3 r) => l.Equals(r);
	public static bool operator != (Bool3 l, Bool3 r) => !l.Equals(r);
	
	public static Bool3 operator & (Bool3 l, Bool3 r) => new(l.X && r.X, l.Y && r.Y, l.Z && r.Z);
	public static Bool3 operator | (Bool3 l, Bool3 r) => new(l.X || r.X, l.Y || r.Y, l.Z || r.Z);
	public static Bool3 operator ^ (Bool3 l, Bool3 r) => new(l.X != r.X, l.Y != r.Y, l.Z != r.Z);
	public static Bool3 operator ~ (Bool3 r) => new(!r.X, !r.Y, !r.Z);
	public static Bool3 operator ! (Bool3 r) => new(!r.X, !r.Y, !r.Z);
	#endregion // Operators
}
