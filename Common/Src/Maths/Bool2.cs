﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>Two-component vector of booleans.</summary>
[StructLayout(LayoutKind.Sequential, Size = 2)]
public struct Bool2 :
	IEquatable<Bool2>,
	IEqualityOperators<Bool2, Bool2, bool>, IBitwiseOperators<Bool2, Bool2, Bool2>
{
	#region Fields
	/// <summary>The X component.</summary>
	public bool X;
	/// <summary>The Y component.</summary>
	public bool Y;

	/// <summary>If any components are true.</summary>
	public readonly bool Any => X || Y;
	/// <summary>If all components are true.</summary>
	public readonly bool All => X && Y;
	/// <summary>If all components are false.</summary>
	public readonly bool None => !Any;

	/// <summary>The number of components that are true.</summary>
	public readonly uint Count => (X ? 1u : 0u) + (Y ? 1u : 0u);
	#endregion // Fields

	
	/// <summary>Construct a vector with equal components.</summary>
	public Bool2(bool b) => X = Y = b;

	/// <summary>Construct a vector with the components.</summary>
	public Bool2(bool x, bool y) => (X, Y) = (x, y);


	#region Base
	public readonly bool Equals(Bool2 o) => o.X == X && o.Y == Y;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Bool2 b && b.X == X && b.Y == Y;

	public readonly override int GetHashCode() => (X, Y) switch {
		(false, false) => 0, (false, true) => 1,
		(true, false)  => 2, (true, true)  => 3
	};

	public readonly override string ToString() => (X, Y) switch {
		(false, false) => "{F,F}", (false, true) => "{F,T}",
		(true, false)  => "{T,F}", (true, true)  => "{T,T}"
	};

	public readonly void Deconstruct(out bool x, out bool y) => (x, y) = (X, Y);
	#endregion // Base


	#region Operators
	public static bool operator == (Bool2 l, Bool2 r) => l.Equals(r);
	public static bool operator != (Bool2 l, Bool2 r) => !l.Equals(r);
	
	public static Bool2 operator & (Bool2 l, Bool2 r) => new(l.X && r.X, l.Y && r.Y);
	public static Bool2 operator | (Bool2 l, Bool2 r) => new(l.X || r.X, l.Y || r.Y);
	public static Bool2 operator ^ (Bool2 l, Bool2 r) => new(l.X != r.X, l.Y != r.Y);
	public static Bool2 operator ~ (Bool2 r) => new(!r.X, !r.Y);
	public static Bool2 operator ! (Bool2 r) => new(!r.X, !r.Y);
	#endregion // Operators
}
