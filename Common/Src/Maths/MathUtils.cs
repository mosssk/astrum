﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;

namespace Astrum.Maths;


/// <summary>Math utilities and numeric extensions.</summary>
public static class MathUtils
{
	// Method impl max optimization
	internal const MethodImplOptions MAX_OPT =
		MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization;
	
	// Default equality tolerances
	internal const float TOL_32 = 1e-8f;
	internal const double TOL_64 = 1e-10;
	
	
	#region Equality
	/// <summary>Checks if the values are approximately equal to each other.</summary>
	/// <remarks>The costliest, but most accurate equality approximator in the class. Handles special classes.</remarks>
	/// <param name="l">The first value to check.</param>
	/// <param name="r">The second value to check.</param>
	/// <param name="eps">The equality tolerance. Near zero, this is absolute, otherwise relative.</param>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxEqual(this float l, float r, float eps = TOL_32)
	{
		if (l.Equals(r)) return true; // Checks for infinities and NaN
		float absL = Math.Abs(l), absR = Math.Abs(r), diff = Math.Abs(r - l);
		return diff <= eps * Math.Max(1.0f, absL + absR);
	}
	
	/// <inheritdoc cref="ApproxEqual(float,float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxEqual(this double l, double r, double eps = TOL_64)
	{
		if (l.Equals(r)) return true; // Checks for infinities and NaN
		double absL = Math.Abs(l), absR = Math.Abs(r), diff = Math.Abs(r - l);
		return diff <= eps * Math.Max(1.0, absL + absR);
	}
	
	/// <summary>Checks if the values are approximately equal to each other.</summary>
	/// <remarks>Cheaper than <see cref="ApproxEqual(float,float,float)"/>, but less accurate for large values.</remarks>
	/// <param name="l">The first value to check.</param>
	/// <param name="r">The second value to check.</param>
	/// <param name="eps">The absolute equality tolerance.</param>
	[MethodImpl(MAX_OPT)]
	public static bool FastApprox(this float l, float r, float eps = TOL_32) => l.Equals(r) || Math.Abs(r - l) <= eps;
	
	/// <inheritdoc cref="FastApprox(float,float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool FastApprox(this double l, double r, double eps = TOL_64) => l.Equals(r) || Math.Abs(r - l) <= eps;
	
	/// <summary>Checks if the float value is arbitrarily close to zero within some tolerance.</summary>
	/// <param name="f">The value to check.</param>
	/// <param name="eps">The absolute tolerance value to check against.</param>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxZero(this float f, float eps = TOL_32) => !Single.IsNaN(f) && Math.Abs(f) <= eps;
	
	/// <inheritdoc cref="ApproxZero(float,float)"/>
	[MethodImpl(MAX_OPT)]
	public static bool ApproxZero(this double f, double eps = TOL_64) => !Double.IsNaN(f) && Math.Abs(f) <= eps;
	#endregion // Equality
}
