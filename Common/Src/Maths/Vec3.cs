﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Astrum.Maths;


/// <summary>A three-component vector of <c>float</c>.</summary>
[StructLayout(LayoutKind.Sequential, Size = 12)]
public struct Vec3 :
	IEquatable<Vec3>, IEqualityOperators<Vec3, Vec3, bool>,
	IAdditionOperators<Vec3, Vec3, Vec3>, IAdditionOperators<Vec3, float, Vec3>,
	ISubtractionOperators<Vec3, Vec3, Vec3>, ISubtractionOperators<Vec3, float, Vec3>,
	IMultiplyOperators<Vec3, Vec3, Vec3>, IMultiplyOperators<Vec3, float, Vec3>,
	IDivisionOperators<Vec3, Vec3, Vec3>, IDivisionOperators<Vec3, float, Vec3>,
	IModulusOperators<Vec3, Vec3, Vec3>, IModulusOperators<Vec3, float, Vec3>,
	IUnaryNegationOperators<Vec3, Vec3>
{
	#region Constants
	/// <summary>Vector with all-zero components.</summary>
	public static readonly Vec3 Zero  = new();
	/// <summary>Vector with all-one components.</summary>
	public static readonly Vec3 One   = new(1);

	/// <summary>Vector with unit-length along the X-axis.</summary>
	public static readonly Vec3 UnitX = new(1, 0, 0);
	/// <summary>Vector with unit-length along the Y-axis.</summary>
	public static readonly Vec3 UnitY = new(0, 1, 0);
	/// <summary>Vector with unit-length along the Z-axis.</summary>
	public static readonly Vec3 UnitZ = new(0, 0, 1);

	/// <summary>Unit-length right-pointing vector in right-handed space (+X).</summary>
	public static readonly Vec3 Right    = new(1, 0, 0);
	/// <summary>Unit-length up-pointing vector in right-handed space (+Y).</summary>
	public static readonly Vec3 Up       = new(0, 1, 0);
	/// <summary>Unit-length backward-pointing vector in right-handed space (+Z).</summary>
	public static readonly Vec3 Backward = new(0, 0, 1);
	/// <summary>Unit-length left-pointing vector in right-handed space (-X).</summary>
	public static readonly Vec3 Left     = new(-1, 0, 0);
	/// <summary>Unit-length down-pointing vector in right-handed space (-Y).</summary>
	public static readonly Vec3 Down     = new(0, -1, 0);
	/// <summary>Unit-length forward-pointing vector in right-handed space (-Z).</summary>
	public static readonly Vec3 Forward  = new(0, 0, -1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public float X;
	/// <summary>The Y-component.</summary>
	public float Y;
	/// <summary>The Z-component.</summary>
	public float Z;

	/// <summary>The length of the vector.</summary>
	public readonly float Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Single.Sqrt(X*X + Y*Y + Z*Z);
	}

	/// <summary>The squared length of the vector.</summary>
	public readonly float LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z;
	}

	/// <summary>The normalized vector (length 1).</summary>
	public readonly Vec3 Normalized {
		[MethodImpl(MathUtils.MAX_OPT)]
		get { var iLen = 1 / Length; return new(X * iLen, Y * iLen, Z * iLen); }
	}
	#endregion // Fields


	/// <summary>Construct a vector with equal components.</summary>
	public Vec3(float v) => X = Y = Z = v;

	/// <summary>Construct a vector from the components.</summary>
	public Vec3(float x, float y, float z) => (X, Y, Z) = (x, y, z);

	/// <summary>Construct a vector from a vector providing XY, with an additional Z component.</summary>
	public Vec3(Vec2 xy, float z) => (X, Y, Z) = (xy.X, xy.Y, z);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Vec3 o) => X.FastApprox(o.X) && Y.FastApprox(o.Y) && Z.FastApprox(o.Z);

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Vec3 v && Equals(v);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public readonly override string ToString() => $"{{{X:G},{Y:G},{Z:G}}}";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)}}}";

	public readonly void Deconstruct(out float x, out float y, out float z) => (x, y, z) = (X, Y, Z);

	/// <summary>Reinterprets the vector as a <see cref="Vector3"/> from <c>System.Numerics</c>.</summary>
	public readonly ref readonly Vector3 AsNumeric() => ref Unsafe.As<Vec3, Vector3>(ref Unsafe.AsRef(in this));

	/// <summary>Reinterprets the vector as a mutable <see cref="Vector3"/> from <c>System.Numerics</c>.</summary>
	public ref Vector3 AsNumericMutable() => ref Unsafe.As<Vec3, Vector3>(ref Unsafe.AsRef(in this));
	#endregion // Base


	#region Vector Ops
	/// <summary>Calculates the dot product of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly float Dot(Vec3 o) => X*o.X + Y*o.Y + Z*o.Z;

	/// <summary>Calculates the angle between the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Vec3 o) => Angle.Rad((float)Single.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Projects this vector onto the given axis.</summary>
	/// <param name="axis">The axis to project onto, must be normalized for accurate results.</param>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec3 Project(Vec3 axis)
	{
		var dot = Dot(axis);
		return new(dot * axis.X, dot * axis.Y, dot * axis.Z);
	}

	/// <summary>Reflects this vector around the given normal.</summary>
	/// <param name="norm">The normal to reflect around, must be normalized for accurate results.</param>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec3 Reflect(Vec3 norm)
	{
		var f = 2 * Dot(norm);
		return new(X - norm.X * f, Y - norm.Y * f, Z - norm.Z * f);
	}

	/// <summary>Calculates the cartesian cross-product of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec3 Cross(Vec3 o) => new(Y * o.Z - Z * o.Y, Z * o.X - X * o.Z, X * o.Y - Y * o.X);

	/// <summary>Clamps the vector components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec3 Clamp(Vec3 v, Vec3 min, Vec3 max) =>
		new(Single.Clamp(v.X, min.X, max.X), Single.Clamp(v.Y, min.Y, max.Y), Single.Clamp(v.Z, min.Z, max.Z));

	/// <summary>Takes the component-wise minimum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec3 Min(Vec3 l, Vec3 r) =>
		new(Single.Min(l.X, r.X), Single.Min(l.Y, r.Y), Single.Min(l.Z, r.Z));

	/// <summary>Takes the component-wise maximum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec3 Max(Vec3 l, Vec3 r) =>
		new(Single.Max(l.X, r.X), Single.Max(l.Y, r.Y), Single.Max(l.Z, r.Z));

	/// <inheritdoc cref="MathUtils.ApproxEqual(float, float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec3 o, float eps) =>
		X.ApproxEqual(o.X, eps) && Y.ApproxEqual(o.Y, eps) && Z.ApproxEqual(o.Z, eps);

	/// <inheritdoc cref="MathUtils.ApproxEqual(float, float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec3 o) => X.ApproxEqual(o.X) && Y.ApproxEqual(o.Y) && Z.ApproxEqual(o.Z);

	/// <inheritdoc cref="MathUtils.ApproxZero(float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero(float eps) => X.ApproxZero(eps) && Y.ApproxZero(eps) && Z.ApproxZero(eps);

	/// <inheritdoc cref="MathUtils.ApproxZero(float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero() => X.ApproxZero() && Y.ApproxZero() && Z.ApproxZero();
	#endregion // Vector Ops


	#region Operators
	public static bool operator == (Vec3 l, Vec3 r) => l.Equals(r);
	public static bool operator != (Vec3 l, Vec3 r) => !l.Equals(r);

	public static Bool3 operator <= (Vec3 l, Vec3 r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z);
	public static Bool3 operator <  (Vec3 l, Vec3 r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z);
	public static Bool3 operator >= (Vec3 l, Vec3 r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
	public static Bool3 operator >  (Vec3 l, Vec3 r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z);

	public static Vec3 operator + (Vec3 l, Vec3 r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z);
	public static Vec3 operator + (Vec3 l, float r) => new(l.X + r, l.Y + r, l.Z + r);
	public static Vec3 operator - (Vec3 l, Vec3 r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z);
	public static Vec3 operator - (Vec3 l, float r) => new(l.X - r, l.Y - r, l.Z - r);
	public static Vec3 operator * (Vec3 l, Vec3 r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z);
	public static Vec3 operator * (Vec3 l, float r) => new(l.X * r, l.Y * r, l.Z * r);
	public static Vec3 operator / (Vec3 l, Vec3 r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z);
	public static Vec3 operator / (Vec3 l, float r) => new(l.X / r, l.Y / r, l.Z / r);
	public static Vec3 operator % (Vec3 l, Vec3 r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z);
	public static Vec3 operator % (Vec3 l, float r) => new(l.X % r, l.Y % r, l.Z % r);

	public static Vec3 operator - (Vec3 r) => new(-r.X, -r.Y, -r.Z);

	public static explicit operator Vec3 (Point3 o) => new((float)o.X, (float)o.Y, (float)o.Z);
	public static explicit operator Vec3 (Point3L o) => new((float)o.X, (float)o.Y, (float)o.Z);

	public static explicit operator Vec3 (Vec3D o) => new((float)o.X, (float)o.Y, (float)o.Z);
	#endregion // Operators
}


/// <summary>A three-component vector of <c>double</c>.</summary>
[StructLayout(LayoutKind.Sequential, Size = 24)]
public struct Vec3D :
	IEquatable<Vec3D>, IEqualityOperators<Vec3D, Vec3D, bool>,
	IAdditionOperators<Vec3D, Vec3D, Vec3D>, IAdditionOperators<Vec3D, double, Vec3D>,
	ISubtractionOperators<Vec3D, Vec3D, Vec3D>, ISubtractionOperators<Vec3D, double, Vec3D>,
	IMultiplyOperators<Vec3D, Vec3D, Vec3D>, IMultiplyOperators<Vec3D, double, Vec3D>,
	IDivisionOperators<Vec3D, Vec3D, Vec3D>, IDivisionOperators<Vec3D, double, Vec3D>,
	IModulusOperators<Vec3D, Vec3D, Vec3D>, IModulusOperators<Vec3D, double, Vec3D>,
	IUnaryNegationOperators<Vec3D, Vec3D>
{
	#region Constants
	/// <summary>Vector with all-zero components.</summary>
	public static readonly Vec3D Zero  = new();
	/// <summary>Vector with all-one components.</summary>
	public static readonly Vec3D One   = new(1);

	/// <summary>Vector with unit-length along the X-axis.</summary>
	public static readonly Vec3D UnitX = new(1, 0, 0);
	/// <summary>Vector with unit-length along the Y-axis.</summary>
	public static readonly Vec3D UnitY = new(0, 1, 0);
	/// <summary>Vector with unit-length along the Z-axis.</summary>
	public static readonly Vec3D UnitZ = new(0, 0, 1);

	/// <summary>Unit-length right-pointing vector in right-handed space (+X).</summary>
	public static readonly Vec3D Right    = new(1, 0, 0);
	/// <summary>Unit-length up-pointing vector in right-handed space (+Y).</summary>
	public static readonly Vec3D Up       = new(0, 1, 0);
	/// <summary>Unit-length backward-pointing vector in right-handed space (+Z).</summary>
	public static readonly Vec3D Backward = new(0, 0, 1);
	/// <summary>Unit-length left-pointing vector in right-handed space (-X).</summary>
	public static readonly Vec3D Left     = new(-1, 0, 0);
	/// <summary>Unit-length down-pointing vector in right-handed space (-Y).</summary>
	public static readonly Vec3D Down     = new(0, -1, 0);
	/// <summary>Unit-length forward-pointing vector in right-handed space (-Z).</summary>
	public static readonly Vec3D Forward  = new(0, 0, -1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public double X;
	/// <summary>The Y-component.</summary>
	public double Y;
	/// <summary>The Z-component.</summary>
	public double Z;

	/// <summary>The length of the vector.</summary>
	public readonly double Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Double.Sqrt(X*X + Y*Y + Z*Z);
	}

	/// <summary>The squared length of the vector.</summary>
	public readonly double LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z;
	}

	/// <summary>The normalized vector (length 1).</summary>
	public readonly Vec3D Normalized {
		[MethodImpl(MathUtils.MAX_OPT)]
		get { var iLen = 1 / Length; return new(X * iLen, Y * iLen, Z * iLen); }
	}
	#endregion // Fields


	/// <summary>Construct a vector with equal components.</summary>
	public Vec3D(double v) => X = Y = Z = v;

	/// <summary>Construct a vector from the components.</summary>
	public Vec3D(double x, double y, double z) => (X, Y, Z) = (x, y, z);

	/// <summary>Construct a vector from a vector providing XY, with an additional Z component.</summary>
	public Vec3D(Vec2D xy, double z) => (X, Y, Z) = (xy.X, xy.Y, z);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Vec3D o) => X.FastApprox(o.X) && Y.FastApprox(o.Y) && Z.FastApprox(o.Z);

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Vec3D v && Equals(v);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public readonly override string ToString() => $"{{{X:G},{Y:G},{Z:G}}}";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)}}}";

	public readonly void Deconstruct(out double x, out double y, out double z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	#region Vector Ops
	/// <summary>Calculates the dot product of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly double Dot(Vec3D o) => X*o.X + Y*o.Y + Z*o.Z;

	/// <summary>Calculates the angle between the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Vec3D o) => Angle.Rad((float)Double.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Projects this vector onto the given axis.</summary>
	/// <param name="axis">The axis to project onto, must be normalized for accurate results.</param>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec3D Project(Vec3D axis)
	{
		var dot = Dot(axis);
		return new(dot * axis.X, dot * axis.Y, dot * axis.Z);
	}

	/// <summary>Reflects this vector around the given normal.</summary>
	/// <param name="norm">The normal to reflect around, must be normalized for accurate results.</param>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec3D Reflect(Vec3D norm)
	{
		var f = 2 * Dot(norm);
		return new(X - norm.X * f, Y - norm.Y * f, Z - norm.Z * f);
	}

	/// <summary>Calculates the cartesian cross-product of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec3D Cross(Vec3D o) => new(Y * o.Z - Z * o.Y, Z * o.X - X * o.Z, X * o.Y - Y * o.X);

	/// <summary>Clamps the vector components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec3D Clamp(Vec3D v, Vec3D min, Vec3D max) =>
		new(Double.Clamp(v.X, min.X, max.X), Double.Clamp(v.Y, min.Y, max.Y), Double.Clamp(v.Z, min.Z, max.Z));

	/// <summary>Takes the component-wise minimum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec3D Min(Vec3D l, Vec3D r) =>
		new(Double.Min(l.X, r.X), Double.Min(l.Y, r.Y), Double.Min(l.Z, r.Z));

	/// <summary>Takes the component-wise maximum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec3D Max(Vec3D l, Vec3D r) =>
		new(Double.Max(l.X, r.X), Double.Max(l.Y, r.Y), Double.Max(l.Z, r.Z));

	/// <inheritdoc cref="MathUtils.ApproxEqual(double, double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec3D o, double eps) =>
		X.ApproxEqual(o.X, eps) && Y.ApproxEqual(o.Y, eps) && Z.ApproxEqual(o.Z, eps);

	/// <inheritdoc cref="MathUtils.ApproxEqual(double, double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec3D o) => X.ApproxEqual(o.X) && Y.ApproxEqual(o.Y) && Z.ApproxEqual(o.Z);

	/// <inheritdoc cref="MathUtils.ApproxZero(double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero(double eps) => X.ApproxZero(eps) && Y.ApproxZero(eps) && Z.ApproxZero(eps);

	/// <inheritdoc cref="MathUtils.ApproxZero(double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero() => X.ApproxZero() && Y.ApproxZero() && Z.ApproxZero();
	#endregion // Vector Ops


	#region Operators
	public static bool operator == (Vec3D l, Vec3D r) => l.Equals(r);
	public static bool operator != (Vec3D l, Vec3D r) => !l.Equals(r);

	public static Bool3 operator <= (Vec3D l, Vec3D r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z);
	public static Bool3 operator <  (Vec3D l, Vec3D r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z);
	public static Bool3 operator >= (Vec3D l, Vec3D r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
	public static Bool3 operator >  (Vec3D l, Vec3D r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z);

	public static Vec3D operator + (Vec3D l, Vec3D r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z);
	public static Vec3D operator + (Vec3D l, double r) => new(l.X + r, l.Y + r, l.Z + r);
	public static Vec3D operator - (Vec3D l, Vec3D r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z);
	public static Vec3D operator - (Vec3D l, double r) => new(l.X - r, l.Y - r, l.Z - r);
	public static Vec3D operator * (Vec3D l, Vec3D r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z);
	public static Vec3D operator * (Vec3D l, double r) => new(l.X * r, l.Y * r, l.Z * r);
	public static Vec3D operator / (Vec3D l, Vec3D r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z);
	public static Vec3D operator / (Vec3D l, double r) => new(l.X / r, l.Y / r, l.Z / r);
	public static Vec3D operator % (Vec3D l, Vec3D r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z);
	public static Vec3D operator % (Vec3D l, double r) => new(l.X % r, l.Y % r, l.Z % r);

	public static Vec3D operator - (Vec3D r) => new(-r.X, -r.Y, -r.Z);

	public static explicit operator Vec3D (Point3 o) => new((double)o.X, (double)o.Y, (double)o.Z);
	public static explicit operator Vec3D (Point3L o) => new((double)o.X, (double)o.Y, (double)o.Z);

	public static implicit operator Vec3D (Vec3 o) => new(o.X, o.Y, o.Z);
	#endregion // Operators
}

