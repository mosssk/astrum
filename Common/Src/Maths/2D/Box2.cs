﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace Astrum.Maths;


/// <summary>An axis-aligned rectangular region in 2D space.</summary>
public readonly struct Box2 :
	IEquatable<Box2>, IEqualityOperators<Box2, Box2, bool>
{
	/// <summary>Represents a region at the origin with zero size.</summary>
	public static readonly Box2 Empty = new(0, 0, 0, 0);
	/// <summary>Represents a region with unit side-lengths, with <see cref="Min"/> at the origin.</summary>
	public static readonly Box2 Unit = new(0, 0, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public readonly Point2 Min;
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public readonly Point2 Max;

	/// <summary>The dimensions of the region.</summary>
	public Extent2 Size => new((uint)(Max.X - Min.X), (uint)(Max.Y - Min.Y));
	/// <summary>The width of the region.</summary>
	public uint Width => (uint)(Max.X - Min.X);
	/// <summary>The height of the region.</summary>
	public uint Height => (uint)(Max.Y - Min.Y);

	/// <summary>The center point of the region.</summary>
	public Vec2 Center => new(Min.X + (Max.X - Min.X) / 2f, Min.Y + (Max.Y - Min.Y) / 2f);

	/// <summary>The total interior area of the region.</summary>
	public int Area => (Max.X - Min.X) * (Max.Y - Min.Y);

	/// <summary>If the region is empty (zero internal area).</summary>
	public bool IsEmpty => Min.X == Max.X || Min.Y == Max.Y;
	#endregion // Fields

	
	/// <summary>Create a region from corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	public Box2(int x1, int y1, int x2, int y2)
	{
		bool sx = x2 < x1, sy = y2 < y1;
		Min = new(sx ? x2 : x1, sy ? y2 : y1);
		Max = new(sx ? x1 : x2, sy ? y1 : y2);
	}

	/// <summary>Create a region defined by two corner points.</summary>
	public Box2(Point2 c1, Point2 c2) : this(c1.X, c1.Y, c2.X, c2.Y) { }


	#region Base
	public bool Equals(Box2 o) => Min == o.Min && Max == o.Max;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Box2 b && Equals(b);

	public override int GetHashCode() => HashCode.Combine(Min.X, Min.Y, Max.X, Max.Y);

	public override string ToString() => $"{{{Min},{Max}}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) =>
		$"{{{Min.ToString(fmt)},{Max.ToString(fmt)}}}";

	public void Deconstruct(out Point2 min, out Point2 max) => (min, max) = (Min, Max);

	public void Deconstruct(out int minX, out int minY, out int maxX, out int maxY) =>
		(minX, minY, maxX, maxY) = (Min.X, Min.Y, Max.X, Max.Y);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets a linearly translated version of the region.</summary>
	public Box2 Translated(Point2 delta) => new(Min + delta, Max + delta);

	/// <summary>Scales the region around the center point.</summary>
	/// <remarks>Since all edges are grown by the same amount, the total dimensions will change by twice the delta.</remarks>
	/// <param name="delta">The absolute amount to scale each edge by relative to the center.</param>
	public Box2 Inflated(Point2 delta) => new(Min - delta, Max + delta);

	/// <summary>Scales the region around the global anchor point.</summary>
	/// <param name="scale">The relative amount to scale each dimension by.</param>
	/// <param name="anchor">The global point around which the scaling originates.</param>
	public Box2 Scaled(Point2 scale, Point2 anchor) =>
		new(scale * (Min - anchor) + anchor, scale * (Max - anchor) + anchor);

	/// <summary>Calculates the minimal region that contains both input regions entirely.</summary>
	public static Box2 Union(Box2 l, Box2 r) => new(Point2.Min(l.Min, r.Min), Point2.Max(l.Max, r.Max));

	/// <summary>Calculates the overlap between the regions, returns <see cref="Empty"/> when no overlap.</summary>
	public static Box2 Intersect(Box2 l, Box2 r)
	{
		var min = Point2.Max(l.Min, r.Min);
		var max = Point2.Min(l.Max, r.Max);
		return (min.X > max.X || min.Y > max.Y) ? Empty : new(min, max);
	}
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box2 l, Box2 r) => l.Equals(r);
	public static bool operator != (Box2 l, Box2 r) => !l.Equals(r);

	public static explicit operator Box2 (Box2L o) => new((int)o.Min.X, (int)o.Min.Y, (int)o.Max.X, (int)o.Max.Y);
	public static explicit operator Box2 (Box2F o) => new((int)o.Min.X, (int)o.Min.Y, (int)o.Max.X, (int)o.Max.Y);
	public static explicit operator Box2 (Box2D o) => new((int)o.Min.X, (int)o.Min.Y, (int)o.Max.X, (int)o.Max.Y);
	#endregion // Operators
}


// [Partial] Space2D operations for Box2
public static partial class Space2D
{
	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Point2 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Point2L point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Vec2 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2 box, Vec2D point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

}


/// <summary>An axis-aligned rectangular region in 2D space.</summary>
public readonly struct Box2L :
	IEquatable<Box2L>, IEqualityOperators<Box2L, Box2L, bool>
{
	/// <summary>Represents a region at the origin with zero size.</summary>
	public static readonly Box2L Empty = new(0, 0, 0, 0);
	/// <summary>Represents a region with unit side-lengths, with <see cref="Min"/> at the origin.</summary>
	public static readonly Box2L Unit = new(0, 0, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public readonly Point2L Min;
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public readonly Point2L Max;

	/// <summary>The dimensions of the region.</summary>
	public Extent2L Size => new((ulong)(Max.X - Min.X), (ulong)(Max.Y - Min.Y));
	/// <summary>The width of the region.</summary>
	public ulong Width => (ulong)(Max.X - Min.X);
	/// <summary>The height of the region.</summary>
	public ulong Height => (ulong)(Max.Y - Min.Y);

	/// <summary>The center point of the region.</summary>
	public Vec2D Center => new(Min.X + (Max.X - Min.X) / 2f, Min.Y + (Max.Y - Min.Y) / 2f);

	/// <summary>The total interior area of the region.</summary>
	public long Area => (Max.X - Min.X) * (Max.Y - Min.Y);

	/// <summary>If the region is empty (zero internal area).</summary>
	public bool IsEmpty => Min.X == Max.X || Min.Y == Max.Y;
	#endregion // Fields

	
	/// <summary>Create a region from corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	public Box2L(long x1, long y1, long x2, long y2)
	{
		bool sx = x2 < x1, sy = y2 < y1;
		Min = new(sx ? x2 : x1, sy ? y2 : y1);
		Max = new(sx ? x1 : x2, sy ? y1 : y2);
	}

	/// <summary>Create a region defined by two corner points.</summary>
	public Box2L(Point2L c1, Point2L c2) : this(c1.X, c1.Y, c2.X, c2.Y) { }


	#region Base
	public bool Equals(Box2L o) => Min == o.Min && Max == o.Max;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Box2L b && Equals(b);

	public override int GetHashCode() => HashCode.Combine(Min.X, Min.Y, Max.X, Max.Y);

	public override string ToString() => $"{{{Min},{Max}}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) =>
		$"{{{Min.ToString(fmt)},{Max.ToString(fmt)}}}";

	public void Deconstruct(out Point2L min, out Point2L max) => (min, max) = (Min, Max);

	public void Deconstruct(out long minX, out long minY, out long maxX, out long maxY) =>
		(minX, minY, maxX, maxY) = (Min.X, Min.Y, Max.X, Max.Y);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets a linearly translated version of the region.</summary>
	public Box2L Translated(Point2L delta) => new(Min + delta, Max + delta);

	/// <summary>Scales the region around the center point.</summary>
	/// <remarks>Since all edges are grown by the same amount, the total dimensions will change by twice the delta.</remarks>
	/// <param name="delta">The absolute amount to scale each edge by relative to the center.</param>
	public Box2L Inflated(Point2L delta) => new(Min - delta, Max + delta);

	/// <summary>Scales the region around the global anchor point.</summary>
	/// <param name="scale">The relative amount to scale each dimension by.</param>
	/// <param name="anchor">The global point around which the scaling originates.</param>
	public Box2L Scaled(Point2L scale, Point2L anchor) =>
		new(scale * (Min - anchor) + anchor, scale * (Max - anchor) + anchor);

	/// <summary>Calculates the minimal region that contains both input regions entirely.</summary>
	public static Box2L Union(Box2L l, Box2L r) => new(Point2L.Min(l.Min, r.Min), Point2L.Max(l.Max, r.Max));

	/// <summary>Calculates the overlap between the regions, returns <see cref="Empty"/> when no overlap.</summary>
	public static Box2L Intersect(Box2L l, Box2L r)
	{
		var min = Point2L.Max(l.Min, r.Min);
		var max = Point2L.Min(l.Max, r.Max);
		return (min.X > max.X || min.Y > max.Y) ? Empty : new(min, max);
	}
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box2L l, Box2L r) => l.Equals(r);
	public static bool operator != (Box2L l, Box2L r) => !l.Equals(r);

	public static implicit operator Box2L (Box2 o) => new((long)o.Min.X, (long)o.Min.Y, (long)o.Max.X, (long)o.Max.Y);
	public static explicit operator Box2L (Box2F o) => new((long)o.Min.X, (long)o.Min.Y, (long)o.Max.X, (long)o.Max.Y);
	public static explicit operator Box2L (Box2D o) => new((long)o.Min.X, (long)o.Min.Y, (long)o.Max.X, (long)o.Max.Y);
	#endregion // Operators
}


// [Partial] Space2D operations for Box2L
public static partial class Space2D
{
	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Point2 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Point2L point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Vec2 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2L box, Vec2D point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

}


/// <summary>An axis-aligned rectangular region in 2D space.</summary>
public readonly struct Box2F :
	IEquatable<Box2F>, IEqualityOperators<Box2F, Box2F, bool>
{
	/// <summary>Represents a region at the origin with zero size.</summary>
	public static readonly Box2F Empty = new(0, 0, 0, 0);
	/// <summary>Represents a region with unit side-lengths, with <see cref="Min"/> at the origin.</summary>
	public static readonly Box2F Unit = new(0, 0, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public readonly Vec2 Min;
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public readonly Vec2 Max;

	/// <summary>The dimensions of the region.</summary>
	public Vec2 Size => new((float)(Max.X - Min.X), (float)(Max.Y - Min.Y));
	/// <summary>The width of the region.</summary>
	public float Width => (float)(Max.X - Min.X);
	/// <summary>The height of the region.</summary>
	public float Height => (float)(Max.Y - Min.Y);

	/// <summary>The center point of the region.</summary>
	public Vec2 Center => new(Min.X + (Max.X - Min.X) / 2f, Min.Y + (Max.Y - Min.Y) / 2f);

	/// <summary>The total interior area of the region.</summary>
	public float Area => (Max.X - Min.X) * (Max.Y - Min.Y);

	/// <summary>If the region is empty (zero internal area).</summary>
	public bool IsEmpty => Min.X.FastApprox(Max.X) || Min.Y.FastApprox(Max.Y);
	#endregion // Fields

	
	/// <summary>Create a region from corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	public Box2F(float x1, float y1, float x2, float y2)
	{
		bool sx = x2 < x1, sy = y2 < y1;
		Min = new(sx ? x2 : x1, sy ? y2 : y1);
		Max = new(sx ? x1 : x2, sy ? y1 : y2);
	}

	/// <summary>Create a region defined by two corner points.</summary>
	public Box2F(Vec2 c1, Vec2 c2) : this(c1.X, c1.Y, c2.X, c2.Y) { }


	#region Base
	public bool Equals(Box2F o) => Min == o.Min && Max == o.Max;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Box2F b && Equals(b);

	public override int GetHashCode() => HashCode.Combine(Min.X, Min.Y, Max.X, Max.Y);

	public override string ToString() => $"{{{Min},{Max}}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) =>
		$"{{{Min.ToString(fmt)},{Max.ToString(fmt)}}}";

	public void Deconstruct(out Vec2 min, out Vec2 max) => (min, max) = (Min, Max);

	public void Deconstruct(out float minX, out float minY, out float maxX, out float maxY) =>
		(minX, minY, maxX, maxY) = (Min.X, Min.Y, Max.X, Max.Y);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets a linearly translated version of the region.</summary>
	public Box2F Translated(Vec2 delta) => new(Min + delta, Max + delta);

	/// <summary>Scales the region around the center point.</summary>
	/// <remarks>Since all edges are grown by the same amount, the total dimensions will change by twice the delta.</remarks>
	/// <param name="delta">The absolute amount to scale each edge by relative to the center.</param>
	public Box2F Inflated(Vec2 delta) => new(Min - delta, Max + delta);

	/// <summary>Scales the region around the global anchor point.</summary>
	/// <param name="scale">The relative amount to scale each dimension by.</param>
	/// <param name="anchor">The global point around which the scaling originates.</param>
	public Box2F Scaled(Vec2 scale, Vec2 anchor) =>
		new(scale * (Min - anchor) + anchor, scale * (Max - anchor) + anchor);

	/// <summary>Calculates the minimal region that contains both input regions entirely.</summary>
	public static Box2F Union(Box2F l, Box2F r) => new(Vec2.Min(l.Min, r.Min), Vec2.Max(l.Max, r.Max));

	/// <summary>Calculates the overlap between the regions, returns <see cref="Empty"/> when no overlap.</summary>
	public static Box2F Intersect(Box2F l, Box2F r)
	{
		var min = Vec2.Max(l.Min, r.Min);
		var max = Vec2.Min(l.Max, r.Max);
		return (min.X > max.X || min.Y > max.Y) ? Empty : new(min, max);
	}
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box2F l, Box2F r) => l.Equals(r);
	public static bool operator != (Box2F l, Box2F r) => !l.Equals(r);

	public static implicit operator Box2F (Box2 o) => new((float)o.Min.X, (float)o.Min.Y, (float)o.Max.X, (float)o.Max.Y);
	public static implicit operator Box2F (Box2L o) => new((float)o.Min.X, (float)o.Min.Y, (float)o.Max.X, (float)o.Max.Y);
	public static explicit operator Box2F (Box2D o) => new((float)o.Min.X, (float)o.Min.Y, (float)o.Max.X, (float)o.Max.Y);
	#endregion // Operators
}


// [Partial] Space2D operations for Box2F
public static partial class Space2D
{
	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Point2 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Point2L point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Vec2 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2F box, Vec2D point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

}


/// <summary>An axis-aligned rectangular region in 2D space.</summary>
public readonly struct Box2D :
	IEquatable<Box2D>, IEqualityOperators<Box2D, Box2D, bool>
{
	/// <summary>Represents a region at the origin with zero size.</summary>
	public static readonly Box2D Empty = new(0, 0, 0, 0);
	/// <summary>Represents a region with unit side-lengths, with <see cref="Min"/> at the origin.</summary>
	public static readonly Box2D Unit = new(0, 0, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public readonly Vec2D Min;
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public readonly Vec2D Max;

	/// <summary>The dimensions of the region.</summary>
	public Vec2D Size => new((double)(Max.X - Min.X), (double)(Max.Y - Min.Y));
	/// <summary>The width of the region.</summary>
	public double Width => (double)(Max.X - Min.X);
	/// <summary>The height of the region.</summary>
	public double Height => (double)(Max.Y - Min.Y);

	/// <summary>The center point of the region.</summary>
	public Vec2D Center => new(Min.X + (Max.X - Min.X) / 2f, Min.Y + (Max.Y - Min.Y) / 2f);

	/// <summary>The total interior area of the region.</summary>
	public double Area => (Max.X - Min.X) * (Max.Y - Min.Y);

	/// <summary>If the region is empty (zero internal area).</summary>
	public bool IsEmpty => Min.X.FastApprox(Max.X) || Min.Y.FastApprox(Max.Y);
	#endregion // Fields

	
	/// <summary>Create a region from corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	public Box2D(double x1, double y1, double x2, double y2)
	{
		bool sx = x2 < x1, sy = y2 < y1;
		Min = new(sx ? x2 : x1, sy ? y2 : y1);
		Max = new(sx ? x1 : x2, sy ? y1 : y2);
	}

	/// <summary>Create a region defined by two corner points.</summary>
	public Box2D(Vec2D c1, Vec2D c2) : this(c1.X, c1.Y, c2.X, c2.Y) { }


	#region Base
	public bool Equals(Box2D o) => Min == o.Min && Max == o.Max;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Box2D b && Equals(b);

	public override int GetHashCode() => HashCode.Combine(Min.X, Min.Y, Max.X, Max.Y);

	public override string ToString() => $"{{{Min},{Max}}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) =>
		$"{{{Min.ToString(fmt)},{Max.ToString(fmt)}}}";

	public void Deconstruct(out Vec2D min, out Vec2D max) => (min, max) = (Min, Max);

	public void Deconstruct(out double minX, out double minY, out double maxX, out double maxY) =>
		(minX, minY, maxX, maxY) = (Min.X, Min.Y, Max.X, Max.Y);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets a linearly translated version of the region.</summary>
	public Box2D Translated(Vec2D delta) => new(Min + delta, Max + delta);

	/// <summary>Scales the region around the center point.</summary>
	/// <remarks>Since all edges are grown by the same amount, the total dimensions will change by twice the delta.</remarks>
	/// <param name="delta">The absolute amount to scale each edge by relative to the center.</param>
	public Box2D Inflated(Vec2D delta) => new(Min - delta, Max + delta);

	/// <summary>Scales the region around the global anchor point.</summary>
	/// <param name="scale">The relative amount to scale each dimension by.</param>
	/// <param name="anchor">The global point around which the scaling originates.</param>
	public Box2D Scaled(Vec2D scale, Vec2D anchor) =>
		new(scale * (Min - anchor) + anchor, scale * (Max - anchor) + anchor);

	/// <summary>Calculates the minimal region that contains both input regions entirely.</summary>
	public static Box2D Union(Box2D l, Box2D r) => new(Vec2D.Min(l.Min, r.Min), Vec2D.Max(l.Max, r.Max));

	/// <summary>Calculates the overlap between the regions, returns <see cref="Empty"/> when no overlap.</summary>
	public static Box2D Intersect(Box2D l, Box2D r)
	{
		var min = Vec2D.Max(l.Min, r.Min);
		var max = Vec2D.Min(l.Max, r.Max);
		return (min.X > max.X || min.Y > max.Y) ? Empty : new(min, max);
	}
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box2D l, Box2D r) => l.Equals(r);
	public static bool operator != (Box2D l, Box2D r) => !l.Equals(r);

	public static implicit operator Box2D (Box2 o) => new((double)o.Min.X, (double)o.Min.Y, (double)o.Max.X, (double)o.Max.Y);
	public static implicit operator Box2D (Box2L o) => new((double)o.Min.X, (double)o.Min.Y, (double)o.Max.X, (double)o.Max.Y);
	public static implicit operator Box2D (Box2F o) => new((double)o.Min.X, (double)o.Min.Y, (double)o.Max.X, (double)o.Max.Y);
	#endregion // Operators
}


// [Partial] Space2D operations for Box2D
public static partial class Space2D
{
	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Point2 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Point2L point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Vec2 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box2D box, Vec2D point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y;

}

