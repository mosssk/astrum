﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>A positive-valued axis extent or size in 2D space.</summary>
[StructLayout(LayoutKind.Sequential, Size = 8)]
public readonly struct Extent2 :
	IEquatable<Extent2>, IEqualityOperators<Extent2, Extent2, bool>
{
	/// <summary>Zero-size extent constant.</summary>
	public static readonly Extent2 Zero = new();


	#region Fields
	/// <summary>The width of the extent (size in X-axis).</summary>
	public readonly uint X;
	/// <summary>The height of the extent (size in Y-axis).</summary>
	public readonly uint Y;

	/// <summary>The total internal area of the extent.</summary>
	public uint Area => X * Y;

	/// <summary>If the extent has zero internal area.</summary>
	public bool IsEmpty => X == 0 || Y == 0;
	#endregion // Fields

	
	/// <summary>Construct an extent from axis sizes.</summary>
	/// <param name="x">The region width (x-axis).</param>
	/// <param name="y">The region height (y-axis).</param>
	public Extent2(uint x, uint y) => (X, Y) = (x, y);


	#region Base
	public bool Equals(Extent2 o) => X == o.X && Y == o.Y;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Extent2 e && X == e.X && Y == e.Y;

	public override int GetHashCode() => HashCode.Combine(X, Y);

	public override string ToString() => $"{{{X},{Y}}}";

	public void Deconstruct(out uint x, out uint y) => (x, y) = (X, Y);
	#endregion // Base


	public static bool operator == (Extent2 l, Extent2 r) => l.Equals(r);
	public static bool operator != (Extent2 l, Extent2 r) => !l.Equals(r);

	public static explicit operator Extent2 (Extent2L r) => new((uint)r.X, (uint)r.Y);
}


/// <summary>A positive-valued axis extent or size in 2D space.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public readonly struct Extent2L :
	IEquatable<Extent2L>, IEqualityOperators<Extent2L, Extent2L, bool>
{
	/// <summary>Zero-size extent constant.</summary>
	public static readonly Extent2L Zero = new();


	#region Fields
	/// <summary>The width of the extent (size in X-axis).</summary>
	public readonly ulong X;
	/// <summary>The height of the extent (size in Y-axis).</summary>
	public readonly ulong Y;

	/// <summary>The total internal area of the extent.</summary>
	public ulong Area => X * Y;

	/// <summary>If the extent has zero internal area.</summary>
	public bool IsEmpty => X == 0 || Y == 0;
	#endregion // Fields

	
	/// <summary>Construct an extent from axis sizes.</summary>
	/// <param name="x">The region width (x-axis).</param>
	/// <param name="y">The region height (y-axis).</param>
	public Extent2L(ulong x, ulong y) => (X, Y) = (x, y);


	#region Base
	public bool Equals(Extent2L o) => X == o.X && Y == o.Y;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Extent2L e && X == e.X && Y == e.Y;

	public override int GetHashCode() => HashCode.Combine(X, Y);

	public override string ToString() => $"{{{X},{Y}}}";

	public void Deconstruct(out ulong x, out ulong y) => (x, y) = (X, Y);
	#endregion // Base


	public static bool operator == (Extent2L l, Extent2L r) => l.Equals(r);
	public static bool operator != (Extent2L l, Extent2L r) => !l.Equals(r);

	public static implicit operator Extent2L (Extent2 r) => new(r.X, r.Y);
}

