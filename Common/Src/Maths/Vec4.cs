﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Astrum.Maths;


/// <summary>A four-component vector of <c>float</c>.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Vec4 :
	IEquatable<Vec4>, IEqualityOperators<Vec4, Vec4, bool>,
	IAdditionOperators<Vec4, Vec4, Vec4>, IAdditionOperators<Vec4, float, Vec4>,
	ISubtractionOperators<Vec4, Vec4, Vec4>, ISubtractionOperators<Vec4, float, Vec4>,
	IMultiplyOperators<Vec4, Vec4, Vec4>, IMultiplyOperators<Vec4, float, Vec4>,
	IDivisionOperators<Vec4, Vec4, Vec4>, IDivisionOperators<Vec4, float, Vec4>,
	IModulusOperators<Vec4, Vec4, Vec4>, IModulusOperators<Vec4, float, Vec4>,
	IUnaryNegationOperators<Vec4, Vec4>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Vec4 Zero = new();
	/// <summary>Point with all-one components.</summary>
	public static readonly Vec4 One = new(1);

	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Vec4 UnitX = new(1, 0, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Vec4 UnitY = new(0, 1, 0, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Vec4 UnitZ = new(0, 0, 1, 0);
	/// <summary>Point with unit length along W-axis.</summary>
	public static readonly Vec4 UnitW = new(0, 0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public float X;
	/// <summary>The Y-component.</summary>
	public float Y;
	/// <summary>The Z-component.</summary>
	public float Z;
	/// <summary>The W-component.</summary>
	public float W;

	/// <summary>The length of the vector.</summary>
	public readonly float Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Single.Sqrt(X*X + Y*Y + Z*Z + W*W);
	}

	/// <summary>The squared length of the vector.</summary>
	public readonly float LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z + W*W;
	}

	/// <summary>The normalized vector (length 1).</summary>
	public readonly Vec4 Normalized {
		[MethodImpl(MathUtils.MAX_OPT)]
		get { var iLen = 1 / Length; return new(X * iLen, Y * iLen, Z * iLen, W * iLen); }
	}
	#endregion // Fields


	/// <summary>Construct the vector with the given value for all components.</summary>
	public Vec4(float f) => X = Y = Z = W = f;

	/// <summary>Construct the vector from XY and ZW vectors.</summary>
	public Vec4(Vec2 xy, Vec2 zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);

	/// <summary>Construct the vector from an XYZ vector and an explicit W component.</summary>
	public Vec4(Vec3 xyz, float w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct the vector from explicit components.</summary>
	public Vec4(float x, float y, float z, float w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Vec4 o) =>
		X.FastApprox(o.X) && Y.FastApprox(o.Y) && Z.FastApprox(o.Z) && W.FastApprox(o.W);

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Vec4 v && Equals(v);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"{{{X:G},{Y:G},{Z:G},{W:G}}}";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)},{W.ToString(fmt)}}}";

	public readonly void Deconstruct(out float x, out float y, out float z, out float w) =>
		(x, y, z, w) = (X, Y, Z, W);

	/// <summary>Reinterprets the vector as a <see cref="Vector4"/> from <c>System.Numerics</c>.</summary>
	public readonly ref readonly Vector4 AsNumeric() => ref Unsafe.As<Vec4, Vector4>(ref Unsafe.AsRef(in this));

	/// <summary>Reinterprets the vector as a mutable <see cref="Vector4"/> from <c>System.Numerics</c>.</summary>
	public ref Vector4 AsNumericMutable() => ref Unsafe.As<Vec4, Vector4>(ref Unsafe.AsRef(in this));
	#endregion // Base


	#region Vector Ops
	/// <summary>Calculates the dot product of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly float Dot(Vec4 o) => X*o.X + Y*o.Y + Z*o.Z + W*o.W;

	/// <summary>Clamps the vector components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec4 Clamp(Vec4 v, Vec4 min, Vec4 max) => new(
		Single.Clamp(v.X, min.X, max.X), Single.Clamp(v.Y, min.Y, max.Y),
		Single.Clamp(v.Z, min.Z, max.Z), Single.Clamp(v.W, min.W, max.W)
	);

	/// <summary>Takes the component-wise minimum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec4 Min(Vec4 l, Vec4 r) =>
		new(Single.Min(l.X, r.X), Single.Min(l.Y, r.Y), Single.Min(l.Z, r.Z), Single.Min(l.W, r.W));

	/// <summary>Takes the component-wise maximum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec4 Max(Vec4 l, Vec4 r) =>
		new(Single.Max(l.X, r.X), Single.Max(l.Y, r.Y), Single.Max(l.Z, r.Z), Single.Max(l.W, r.W));

	/// <inheritdoc cref="MathUtils.ApproxEqual(float, float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec4 o, float eps) =>
		X.ApproxEqual(o.X, eps) && Y.ApproxEqual(o.Y, eps) && Z.ApproxEqual(o.Z, eps) && W.ApproxEqual(o.W, eps);

	/// <inheritdoc cref="MathUtils.ApproxEqual(float, float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec4 o) =>
		X.ApproxEqual(o.X) && Y.ApproxEqual(o.Y) && Z.ApproxEqual(o.Z) && W.ApproxEqual(o.W);

	/// <inheritdoc cref="MathUtils.ApproxZero(float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero(float eps) =>
		X.ApproxZero(eps) && Y.ApproxZero(eps) && Z.ApproxZero(eps) && W.ApproxZero(eps);

	/// <inheritdoc cref="MathUtils.ApproxZero(float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero() => X.ApproxZero() && Y.ApproxZero() && Z.ApproxZero() && W.ApproxZero();
	#endregion // Vector Ops


	#region Operators
	public static bool operator == (Vec4 l, Vec4 r) => l.Equals(r);
	public static bool operator != (Vec4 l, Vec4 r) => !l.Equals(r);

	public static Bool4 operator <= (Vec4 l, Vec4 r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W);
	public static Bool4 operator <  (Vec4 l, Vec4 r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z, l.W < r.W);
	public static Bool4 operator >= (Vec4 l, Vec4 r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
	public static Bool4 operator >  (Vec4 l, Vec4 r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z, l.W > r.W);

	public static Vec4 operator + (Vec4 l, Vec4 r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W);
	public static Vec4 operator + (Vec4 l, float r) => new(l.X + r, l.Y + r, l.Z + r, l.W + r);
	public static Vec4 operator - (Vec4 l, Vec4 r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W);
	public static Vec4 operator - (Vec4 l, float r) => new(l.X - r, l.Y - r, l.Z - r, l.W - r);
	public static Vec4 operator * (Vec4 l, Vec4 r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W);
	public static Vec4 operator * (Vec4 l, float r) => new(l.X * r, l.Y * r, l.Z * r, l.W * r);
	public static Vec4 operator / (Vec4 l, Vec4 r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W);
	public static Vec4 operator / (Vec4 l, float r) => new(l.X / r, l.Y / r, l.Z / r, l.W / r);
	public static Vec4 operator % (Vec4 l, Vec4 r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z, l.W % r.W);
	public static Vec4 operator % (Vec4 l, float r) => new(l.X % r, l.Y % r, l.Z % r, l.W % r);

	public static Vec4 operator - (Vec4 r) => new(-r.X, -r.Y, -r.Z, -r.W);

	public static explicit operator Vec4 (Point4 o) => new((float)o.X, (float)o.Y, (float)o.Z, (float)o.W);
	public static explicit operator Vec4 (Point4L o) => new((float)o.X, (float)o.Y, (float)o.Z, (float)o.W);

	public static explicit operator Vec4 (Vec4D o) => new((float)o.X, (float)o.Y, (float)o.Z, (float)o.W);
	#endregion // Operators
}


/// <summary>A four-component vector of <c>double</c>.</summary>
[StructLayout(LayoutKind.Sequential, Size = 32)]
public struct Vec4D :
	IEquatable<Vec4D>, IEqualityOperators<Vec4D, Vec4D, bool>,
	IAdditionOperators<Vec4D, Vec4D, Vec4D>, IAdditionOperators<Vec4D, double, Vec4D>,
	ISubtractionOperators<Vec4D, Vec4D, Vec4D>, ISubtractionOperators<Vec4D, double, Vec4D>,
	IMultiplyOperators<Vec4D, Vec4D, Vec4D>, IMultiplyOperators<Vec4D, double, Vec4D>,
	IDivisionOperators<Vec4D, Vec4D, Vec4D>, IDivisionOperators<Vec4D, double, Vec4D>,
	IModulusOperators<Vec4D, Vec4D, Vec4D>, IModulusOperators<Vec4D, double, Vec4D>,
	IUnaryNegationOperators<Vec4D, Vec4D>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Vec4D Zero = new();
	/// <summary>Point with all-one components.</summary>
	public static readonly Vec4D One = new(1);

	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Vec4D UnitX = new(1, 0, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Vec4D UnitY = new(0, 1, 0, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Vec4D UnitZ = new(0, 0, 1, 0);
	/// <summary>Point with unit length along W-axis.</summary>
	public static readonly Vec4D UnitW = new(0, 0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public double X;
	/// <summary>The Y-component.</summary>
	public double Y;
	/// <summary>The Z-component.</summary>
	public double Z;
	/// <summary>The W-component.</summary>
	public double W;

	/// <summary>The length of the vector.</summary>
	public readonly double Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Double.Sqrt(X*X + Y*Y + Z*Z + W*W);
	}

	/// <summary>The squared length of the vector.</summary>
	public readonly double LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z + W*W;
	}

	/// <summary>The normalized vector (length 1).</summary>
	public readonly Vec4D Normalized {
		[MethodImpl(MathUtils.MAX_OPT)]
		get { var iLen = 1 / Length; return new(X * iLen, Y * iLen, Z * iLen, W * iLen); }
	}
	#endregion // Fields


	/// <summary>Construct the vector with the given value for all components.</summary>
	public Vec4D(double f) => X = Y = Z = W = f;

	/// <summary>Construct the vector from XY and ZW vectors.</summary>
	public Vec4D(Vec2D xy, Vec2D zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);

	/// <summary>Construct the vector from an XYZ vector and an explicit W component.</summary>
	public Vec4D(Vec3D xyz, double w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct the vector from explicit components.</summary>
	public Vec4D(double x, double y, double z, double w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Vec4D o) =>
		X.FastApprox(o.X) && Y.FastApprox(o.Y) && Z.FastApprox(o.Z) && W.FastApprox(o.W);

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Vec4D v && Equals(v);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"{{{X:G},{Y:G},{Z:G},{W:G}}}";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)},{W.ToString(fmt)}}}";

	public readonly void Deconstruct(out double x, out double y, out double z, out double w) =>
		(x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Vector Ops
	/// <summary>Calculates the dot product of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly double Dot(Vec4D o) => X*o.X + Y*o.Y + Z*o.Z + W*o.W;

	/// <summary>Clamps the vector components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec4D Clamp(Vec4D v, Vec4D min, Vec4D max) => new(
		Double.Clamp(v.X, min.X, max.X), Double.Clamp(v.Y, min.Y, max.Y),
		Double.Clamp(v.Z, min.Z, max.Z), Double.Clamp(v.W, min.W, max.W)
	);

	/// <summary>Takes the component-wise minimum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec4D Min(Vec4D l, Vec4D r) =>
		new(Double.Min(l.X, r.X), Double.Min(l.Y, r.Y), Double.Min(l.Z, r.Z), Double.Min(l.W, r.W));

	/// <summary>Takes the component-wise maximum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec4D Max(Vec4D l, Vec4D r) =>
		new(Double.Max(l.X, r.X), Double.Max(l.Y, r.Y), Double.Max(l.Z, r.Z), Double.Max(l.W, r.W));

	/// <inheritdoc cref="MathUtils.ApproxEqual(double, double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec4D o, double eps) =>
		X.ApproxEqual(o.X, eps) && Y.ApproxEqual(o.Y, eps) && Z.ApproxEqual(o.Z, eps) && W.ApproxEqual(o.W, eps);

	/// <inheritdoc cref="MathUtils.ApproxEqual(double, double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec4D o) =>
		X.ApproxEqual(o.X) && Y.ApproxEqual(o.Y) && Z.ApproxEqual(o.Z) && W.ApproxEqual(o.W);

	/// <inheritdoc cref="MathUtils.ApproxZero(double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero(double eps) =>
		X.ApproxZero(eps) && Y.ApproxZero(eps) && Z.ApproxZero(eps) && W.ApproxZero(eps);

	/// <inheritdoc cref="MathUtils.ApproxZero(double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero() => X.ApproxZero() && Y.ApproxZero() && Z.ApproxZero() && W.ApproxZero();
	#endregion // Vector Ops


	#region Operators
	public static bool operator == (Vec4D l, Vec4D r) => l.Equals(r);
	public static bool operator != (Vec4D l, Vec4D r) => !l.Equals(r);

	public static Bool4 operator <= (Vec4D l, Vec4D r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W);
	public static Bool4 operator <  (Vec4D l, Vec4D r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z, l.W < r.W);
	public static Bool4 operator >= (Vec4D l, Vec4D r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
	public static Bool4 operator >  (Vec4D l, Vec4D r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z, l.W > r.W);

	public static Vec4D operator + (Vec4D l, Vec4D r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W);
	public static Vec4D operator + (Vec4D l, double r) => new(l.X + r, l.Y + r, l.Z + r, l.W + r);
	public static Vec4D operator - (Vec4D l, Vec4D r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W);
	public static Vec4D operator - (Vec4D l, double r) => new(l.X - r, l.Y - r, l.Z - r, l.W - r);
	public static Vec4D operator * (Vec4D l, Vec4D r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W);
	public static Vec4D operator * (Vec4D l, double r) => new(l.X * r, l.Y * r, l.Z * r, l.W * r);
	public static Vec4D operator / (Vec4D l, Vec4D r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W);
	public static Vec4D operator / (Vec4D l, double r) => new(l.X / r, l.Y / r, l.Z / r, l.W / r);
	public static Vec4D operator % (Vec4D l, Vec4D r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z, l.W % r.W);
	public static Vec4D operator % (Vec4D l, double r) => new(l.X % r, l.Y % r, l.Z % r, l.W % r);

	public static Vec4D operator - (Vec4D r) => new(-r.X, -r.Y, -r.Z, -r.W);

	public static explicit operator Vec4D (Point4 o) => new((double)o.X, (double)o.Y, (double)o.Z, (double)o.W);
	public static explicit operator Vec4D (Point4L o) => new((double)o.X, (double)o.Y, (double)o.Z, (double)o.W);

	public static implicit operator Vec4D (Vec4 o) => new(o.X, o.Y, o.Z, o.W);
	#endregion // Operators
}

