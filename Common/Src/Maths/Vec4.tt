﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */
<#@ template language="C#" #>
<#
	(string Name, string Field, string FieldClass, uint Size, string Cast, string Eps, string Vec2, string Vec3)[] Types_ = [
		("Vec4", "float", "Single", 16, "", "float", "Vec2", "Vec3"),
		("Vec4D", "double", "Double", 32, "", "double", "Vec2D", "Vec3D")
	];
#>

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Astrum.Maths;

<# foreach (var (N_, F_, FC_, SZ_, C_, E_, V2_, V3_) in Types_) { #>

/// <summary>A four-component vector of <c><#=F_#></c>.</summary>
[StructLayout(LayoutKind.Sequential, Size = <#=SZ_#>)]
public struct <#=N_#> :
	IEquatable<<#=N_#>>, IEqualityOperators<<#=N_#>, <#=N_#>, bool>,
	IAdditionOperators<<#=N_#>, <#=N_#>, <#=N_#>>, IAdditionOperators<<#=N_#>, <#=F_#>, <#=N_#>>,
	ISubtractionOperators<<#=N_#>, <#=N_#>, <#=N_#>>, ISubtractionOperators<<#=N_#>, <#=F_#>, <#=N_#>>,
	IMultiplyOperators<<#=N_#>, <#=N_#>, <#=N_#>>, IMultiplyOperators<<#=N_#>, <#=F_#>, <#=N_#>>,
	IDivisionOperators<<#=N_#>, <#=N_#>, <#=N_#>>, IDivisionOperators<<#=N_#>, <#=F_#>, <#=N_#>>,
	IModulusOperators<<#=N_#>, <#=N_#>, <#=N_#>>, IModulusOperators<<#=N_#>, <#=F_#>, <#=N_#>>,
	IUnaryNegationOperators<<#=N_#>, <#=N_#>>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly <#=N_#> Zero = new();
	/// <summary>Point with all-one components.</summary>
	public static readonly <#=N_#> One = new(<#=C_#>1);

	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly <#=N_#> UnitX = new(<#=C_#>1, <#=C_#>0, <#=C_#>0, <#=C_#>0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly <#=N_#> UnitY = new(<#=C_#>0, <#=C_#>1, <#=C_#>0, <#=C_#>0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly <#=N_#> UnitZ = new(<#=C_#>0, <#=C_#>0, <#=C_#>1, <#=C_#>0);
	/// <summary>Point with unit length along W-axis.</summary>
	public static readonly <#=N_#> UnitW = new(<#=C_#>0, <#=C_#>0, <#=C_#>0, <#=C_#>1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public <#=F_#> X;
	/// <summary>The Y-component.</summary>
	public <#=F_#> Y;
	/// <summary>The Z-component.</summary>
	public <#=F_#> Z;
	/// <summary>The W-component.</summary>
	public <#=F_#> W;

	/// <summary>The length of the vector.</summary>
	public readonly <#=F_#> Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => <#=FC_#>.Sqrt(X*X + Y*Y + Z*Z + W*W);
	}

	/// <summary>The squared length of the vector.</summary>
	public readonly <#=F_#> LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z + W*W;
	}

	/// <summary>The normalized vector (length 1).</summary>
	public readonly <#=N_#> Normalized {
		[MethodImpl(MathUtils.MAX_OPT)]
		get { var iLen = <#=C_#>1 / Length; return new(X * iLen, Y * iLen, Z * iLen, W * iLen); }
	}
	#endregion // Fields


	/// <summary>Construct the vector with the given value for all components.</summary>
	public <#=N_#>(<#=F_#> f) => X = Y = Z = W = f;

	/// <summary>Construct the vector from XY and ZW vectors.</summary>
	public <#=N_#>(<#=V2_#> xy, <#=V2_#> zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);

	/// <summary>Construct the vector from an XYZ vector and an explicit W component.</summary>
	public <#=N_#>(<#=V3_#> xyz, <#=F_#> w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct the vector from explicit components.</summary>
	public <#=N_#>(<#=F_#> x, <#=F_#> y, <#=F_#> z, <#=F_#> w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(<#=N_#> o) =>
		X.FastApprox(o.X) && Y.FastApprox(o.Y) && Z.FastApprox(o.Z) && W.FastApprox(o.W);

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is <#=N_#> v && Equals(v);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"{{{X:G},{Y:G},{Z:G},{W:G}}}";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)},{W.ToString(fmt)}}}";

	public readonly void Deconstruct(out <#=F_#> x, out <#=F_#> y, out <#=F_#> z, out <#=F_#> w) =>
		(x, y, z, w) = (X, Y, Z, W);
<# if (N_ == "Vec4") { #>

	/// <summary>Reinterprets the vector as a <see cref="Vector4"/> from <c>System.Numerics</c>.</summary>
	public readonly ref readonly Vector4 AsNumeric() => ref Unsafe.As<Vec4, Vector4>(ref Unsafe.AsRef(in this));

	/// <summary>Reinterprets the vector as a mutable <see cref="Vector4"/> from <c>System.Numerics</c>.</summary>
	public ref Vector4 AsNumericMutable() => ref Unsafe.As<Vec4, Vector4>(ref Unsafe.AsRef(in this));
<# } #>
	#endregion // Base


	#region Vector Ops
	/// <summary>Calculates the dot product of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly <#=F_#> Dot(<#=N_#> o) => X*o.X + Y*o.Y + Z*o.Z + W*o.W;

	/// <summary>Clamps the vector components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static <#=N_#> Clamp(<#=N_#> v, <#=N_#> min, <#=N_#> max) => new(
		<#=FC_#>.Clamp(v.X, min.X, max.X), <#=FC_#>.Clamp(v.Y, min.Y, max.Y),
		<#=FC_#>.Clamp(v.Z, min.Z, max.Z), <#=FC_#>.Clamp(v.W, min.W, max.W)
	);

	/// <summary>Takes the component-wise minimum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static <#=N_#> Min(<#=N_#> l, <#=N_#> r) =>
		new(<#=FC_#>.Min(l.X, r.X), <#=FC_#>.Min(l.Y, r.Y), <#=FC_#>.Min(l.Z, r.Z), <#=FC_#>.Min(l.W, r.W));

	/// <summary>Takes the component-wise maximum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static <#=N_#> Max(<#=N_#> l, <#=N_#> r) =>
		new(<#=FC_#>.Max(l.X, r.X), <#=FC_#>.Max(l.Y, r.Y), <#=FC_#>.Max(l.Z, r.Z), <#=FC_#>.Max(l.W, r.W));

	/// <inheritdoc cref="MathUtils.ApproxEqual(<#=F_#>, <#=F_#>, <#=E_#>)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(<#=N_#> o, <#=E_#> eps) =>
		X.ApproxEqual(o.X, eps) && Y.ApproxEqual(o.Y, eps) && Z.ApproxEqual(o.Z, eps) && W.ApproxEqual(o.W, eps);

	/// <inheritdoc cref="MathUtils.ApproxEqual(<#=F_#>, <#=F_#>, <#=E_#>)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(<#=N_#> o) =>
		X.ApproxEqual(o.X) && Y.ApproxEqual(o.Y) && Z.ApproxEqual(o.Z) && W.ApproxEqual(o.W);

	/// <inheritdoc cref="MathUtils.ApproxZero(<#=F_#>, <#=E_#>)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero(<#=E_#> eps) =>
		X.ApproxZero(eps) && Y.ApproxZero(eps) && Z.ApproxZero(eps) && W.ApproxZero(eps);

	/// <inheritdoc cref="MathUtils.ApproxZero(<#=F_#>, <#=E_#>)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero() => X.ApproxZero() && Y.ApproxZero() && Z.ApproxZero() && W.ApproxZero();
	#endregion // Vector Ops


	#region Operators
	public static bool operator == (<#=N_#> l, <#=N_#> r) => l.Equals(r);
	public static bool operator != (<#=N_#> l, <#=N_#> r) => !l.Equals(r);

	public static Bool4 operator <= (<#=N_#> l, <#=N_#> r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W);
	public static Bool4 operator <  (<#=N_#> l, <#=N_#> r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z, l.W < r.W);
	public static Bool4 operator >= (<#=N_#> l, <#=N_#> r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
	public static Bool4 operator >  (<#=N_#> l, <#=N_#> r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z, l.W > r.W);

	public static <#=N_#> operator + (<#=N_#> l, <#=N_#> r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W);
	public static <#=N_#> operator + (<#=N_#> l, <#=F_#> r) => new(l.X + r, l.Y + r, l.Z + r, l.W + r);
	public static <#=N_#> operator - (<#=N_#> l, <#=N_#> r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W);
	public static <#=N_#> operator - (<#=N_#> l, <#=F_#> r) => new(l.X - r, l.Y - r, l.Z - r, l.W - r);
	public static <#=N_#> operator * (<#=N_#> l, <#=N_#> r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W);
	public static <#=N_#> operator * (<#=N_#> l, <#=F_#> r) => new(l.X * r, l.Y * r, l.Z * r, l.W * r);
	public static <#=N_#> operator / (<#=N_#> l, <#=N_#> r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W);
	public static <#=N_#> operator / (<#=N_#> l, <#=F_#> r) => new(l.X / r, l.Y / r, l.Z / r, l.W / r);
	public static <#=N_#> operator % (<#=N_#> l, <#=N_#> r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z, l.W % r.W);
	public static <#=N_#> operator % (<#=N_#> l, <#=F_#> r) => new(l.X % r, l.Y % r, l.Z % r, l.W % r);

	public static <#=N_#> operator - (<#=N_#> r) => new(-r.X, -r.Y, -r.Z, -r.W);

	public static explicit operator <#=N_#> (Point4 o) => new((<#=F_#>)o.X, (<#=F_#>)o.Y, (<#=F_#>)o.Z, (<#=F_#>)o.W);
	public static explicit operator <#=N_#> (Point4L o) => new((<#=F_#>)o.X, (<#=F_#>)o.Y, (<#=F_#>)o.Z, (<#=F_#>)o.W);

<# if (N_ == "Vec4") { #>
	public static explicit operator Vec4 (Vec4D o) => new((float)o.X, (float)o.Y, (float)o.Z, (float)o.W);
<# } else { #>
	public static implicit operator Vec4D (Vec4 o) => new(o.X, o.Y, o.Z, o.W);
<# } #>
	#endregion // Operators
}

<# } #>
