﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>An angle measurement with <c>float</c> precision.</summary>
/// <remarks>Angle equality is based on the sign and normalized representation.</remarks>
[StructLayout(LayoutKind.Sequential, Size = sizeof(float))]
public readonly struct Angle :
	IEquatable<Angle>, IComparable<Angle>,
	IComparisonOperators<Angle, Angle, bool>,
	IAdditionOperators<Angle, Angle, Angle>,
	ISubtractionOperators<Angle, Angle, Angle>,
	IMultiplyOperators<Angle, float, Angle>,
	IDivisionOperators<Angle, float, Angle>,
	IModulusOperators<Angle, Angle, Angle>,
	IUnaryNegationOperators<Angle, Angle>
{
	// Conversion constants
	private const float R_2_D = 180 / MathF.PI;
	private const float D_2_R = MathF.PI / 180;

	
	#region Constants
	/// <summary>Zero angle.</summary>
	public static readonly Angle Zero = new(0);

	/// <summary>Angle of 10 degrees.</summary>
	public static readonly Angle D10 = new(10 * D_2_R);
	/// <summary>Angle of 15 degrees.</summary>
	public static readonly Angle D15 = new(15 * D_2_R);
	/// <summary>Angle of 20 degrees.</summary>
	public static readonly Angle D20 = new(20 * D_2_R);
	/// <summary>Angle of 30 degrees.</summary>
	public static readonly Angle D30 = new(30 * D_2_R);
	/// <summary>Angle of 45 degrees.</summary>
	public static readonly Angle D45 = new(45 * D_2_R);
	/// <summary>Angle of 60 degrees.</summary>
	public static readonly Angle D60 = new(60 * D_2_R);
	/// <summary>Angle of 90 degrees.</summary>
	public static readonly Angle D90 = new(90 * D_2_R);
	/// <summary>Angle of 180 degrees.</summary>
	public static readonly Angle D180 = new(180 * D_2_R);
	/// <summary>Angle of 360 degrees.</summary>
	public static readonly Angle D360 = new(360 * D_2_R);

	/// <summary>Angle of π/6 degrees.</summary>
	public static readonly Angle PiO6 = new(MathF.PI / 6);
	/// <summary>Angle of π/4 degrees.</summary>
	public static readonly Angle PiO4 = new(MathF.PI / 4);
	/// <summary>Angle of π/3 degrees.</summary>
	public static readonly Angle PiO3 = new(MathF.PI / 3);
	/// <summary>Angle of π/2 degrees.</summary>
	public static readonly Angle PiO2 = new(MathF.PI / 2);
	/// <summary>Angle of π degrees.</summary>
	public static readonly Angle Pi = new(MathF.PI);
	/// <summary>Angle of τ (2π) degrees.</summary>
	public static readonly Angle Tau = new(MathF.Tau);
	#endregion // Constants


	#region Fields
	/// <summary>The angle measurement in radians.</summary>
	public readonly float Radians;

	/// <summary>The angle measurement in degrees.</summary>
	public float Degrees => Radians * R_2_D;

	/// <summary>The sine of the angle.</summary>
	public float Sin => MathF.Sin(Radians);
	/// <summary>The cosine of the angle.</summary>
	public float Cos => MathF.Cos(Radians);
	/// <summary>The tangent of the angle.</summary>
	public float Tan => MathF.Tan(Radians);
	/// <summary>The combined sine and cosine of the angle.</summary>
	public (float Sin, float Cos) SinCos => MathF.SinCos(Radians);

	/// <summary>Normalizes the angle to the range <c>(-2π,2π)</c>, preserving the sign.</summary>
	public Angle Normalized => new(Radians % MathF.Tau);
	#endregion // Fields


	private Angle(float rad) => Radians = rad;

	/// <summary>Create an angle from radians.</summary>	
	public static Angle Rad(float radians) => new(radians);

	/// <summary>Create an angle from degrees.</summary>	
	public static Angle Deg(float degrees) => new(degrees * D_2_R);


	#region Base
	public override string ToString() => $"{{{Radians:G} rad}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) => 
		$"{{{Radians.ToString(fmt)} rad}}";

	//// <summary>Equality is performed using normalized angles.</summary>
	public override bool Equals([NotNullWhen(true)] object? o) => 
		o is Angle a && (Radians % MathF.Tau).FastApprox(a.Radians % MathF.Tau);

	//// <summary>Equality is performed using normalized angles.</summary>
	public bool Equals(Angle a) => (Radians % MathF.Tau).FastApprox(a.Radians % MathF.Tau);

	public override int GetHashCode() => Radians.GetHashCode();

	public int CompareTo(Angle o) => Radians.CompareTo(o.Radians);
	#endregion // Base


	/// <summary>Clamps the angle between min and max values.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Angle Clamp(Angle v, Angle min, Angle max) => new(Math.Clamp(v.Radians, min.Radians, max.Radians));

	/// <summary>Clamps the angle between min and max values.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Angle Min(Angle l, Angle r) => new(Math.Min(l.Radians, r.Radians));

	/// <summary>Clamps the angle between min and max values.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Angle Max(Angle l, Angle r) => new(Math.Max(l.Radians, r.Radians));

	/// <summary>Checks equality using <see cref="MathUtils.ApproxEqual(float,float,float)"/>.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxEqual(Angle o, float eps = MathUtils.TOL_32) => 
		(Radians % MathF.Tau).ApproxEqual(o.Radians % MathF.Tau, eps);

	/// <summary>Checks for approximate zero using <see cref="MathUtils.ApproxZero(float,float)"/>.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxZero(float eps = MathUtils.TOL_32) => (Radians % MathF.Tau).ApproxZero(eps);


	#region Operators
	public static bool operator == (Angle l, Angle r) => l.Equals(r);
	public static bool operator != (Angle l, Angle r) => !l.Equals(r);
	public static bool operator <= (Angle l, Angle r) => l.Radians <= r.Radians;
	public static bool operator <  (Angle l, Angle r) => l.Radians <  r.Radians;
	public static bool operator >= (Angle l, Angle r) => l.Radians >= r.Radians;
	public static bool operator >  (Angle l, Angle r) => l.Radians >  r.Radians;

	public static Angle operator + (Angle l, Angle r) => new(l.Radians + r.Radians);
	public static Angle operator - (Angle l, Angle r) => new(l.Radians - r.Radians);
	public static Angle operator * (Angle l, float r) => new(l.Radians * r);
	public static Angle operator / (Angle l, float r) => new(l.Radians / r);
	public static Angle operator % (Angle l, Angle r) => new(l.Radians % r.Radians);
	public static Angle operator - (Angle r) => new(-r.Radians);

	public static explicit operator Angle (AngleD a) => new((float)a.Radians);
	#endregion // Operators
}


/// <summary>An angle measurement with <c>double</c> precision.</summary>
/// <remarks>Angle equality is based on the sign and normalized representation.</remarks>
[StructLayout(LayoutKind.Sequential, Size = sizeof(double))]
public readonly struct AngleD :
	IEquatable<AngleD>, IComparable<AngleD>,
	IComparisonOperators<AngleD, AngleD, bool>,
	IAdditionOperators<AngleD, AngleD, AngleD>,
	ISubtractionOperators<AngleD, AngleD, AngleD>,
	IMultiplyOperators<AngleD, double, AngleD>,
	IDivisionOperators<AngleD, double, AngleD>,
	IModulusOperators<AngleD, AngleD, AngleD>,
	IUnaryNegationOperators<AngleD, AngleD>
{
	// Conversion constants
	private const double R_2_D = 180 / Math.PI;
	private const double D_2_R = Math.PI / 180;

	
	#region Constants
	/// <summary>Zero angle.</summary>
	public static readonly AngleD Zero = new(0);

	/// <summary>Angle of 10 degrees.</summary>
	public static readonly AngleD D10 = new(10 * D_2_R);
	/// <summary>Angle of 15 degrees.</summary>
	public static readonly AngleD D15 = new(15 * D_2_R);
	/// <summary>Angle of 20 degrees.</summary>
	public static readonly AngleD D20 = new(20 * D_2_R);
	/// <summary>Angle of 30 degrees.</summary>
	public static readonly AngleD D30 = new(30 * D_2_R);
	/// <summary>Angle of 45 degrees.</summary>
	public static readonly AngleD D45 = new(45 * D_2_R);
	/// <summary>Angle of 60 degrees.</summary>
	public static readonly AngleD D60 = new(60 * D_2_R);
	/// <summary>Angle of 90 degrees.</summary>
	public static readonly AngleD D90 = new(90 * D_2_R);
	/// <summary>Angle of 180 degrees.</summary>
	public static readonly AngleD D180 = new(180 * D_2_R);
	/// <summary>Angle of 360 degrees.</summary>
	public static readonly AngleD D360 = new(360 * D_2_R);

	/// <summary>Angle of π/6 degrees.</summary>
	public static readonly AngleD PiO6 = new(Math.PI / 6);
	/// <summary>Angle of π/4 degrees.</summary>
	public static readonly AngleD PiO4 = new(Math.PI / 4);
	/// <summary>Angle of π/3 degrees.</summary>
	public static readonly AngleD PiO3 = new(Math.PI / 3);
	/// <summary>Angle of π/2 degrees.</summary>
	public static readonly AngleD PiO2 = new(Math.PI / 2);
	/// <summary>Angle of π degrees.</summary>
	public static readonly AngleD Pi = new(Math.PI);
	/// <summary>Angle of τ (2π) degrees.</summary>
	public static readonly AngleD Tau = new(Math.Tau);
	#endregion // Constants


	#region Fields
	/// <summary>The angle measurement in radians.</summary>
	public readonly double Radians;

	/// <summary>The angle measurement in degrees.</summary>
	public double Degrees => Radians * R_2_D;

	/// <summary>The sine of the angle.</summary>
	public double Sin => Math.Sin(Radians);
	/// <summary>The cosine of the angle.</summary>
	public double Cos => Math.Cos(Radians);
	/// <summary>The tangent of the angle.</summary>
	public double Tan => Math.Tan(Radians);
	/// <summary>The combined sine and cosine of the angle.</summary>
	public (double Sin, double Cos) SinCos => Math.SinCos(Radians);

	/// <summary>Normalizes the angle to the range <c>(-2π,2π)</c>, preserving the sign.</summary>
	public AngleD Normalized => new(Radians % Math.Tau);
	#endregion // Fields


	private AngleD(double rad) => Radians = rad;

	/// <summary>Create an angle from radians.</summary>	
	public static AngleD Rad(double radians) => new(radians);

	/// <summary>Create an angle from degrees.</summary>	
	public static AngleD Deg(double degrees) => new(degrees * D_2_R);


	#region Base
	public override string ToString() => $"{{{Radians:G} rad}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) => 
		$"{{{Radians.ToString(fmt)} rad}}";

	//// <summary>Equality is performed using normalized angles.</summary>
	public override bool Equals([NotNullWhen(true)] object? o) => 
		o is AngleD a && (Radians % Math.Tau).FastApprox(a.Radians % Math.Tau);

	//// <summary>Equality is performed using normalized angles.</summary>
	public bool Equals(AngleD a) => (Radians % Math.Tau).FastApprox(a.Radians % Math.Tau);

	public override int GetHashCode() => Radians.GetHashCode();

	public int CompareTo(AngleD o) => Radians.CompareTo(o.Radians);
	#endregion // Base


	/// <summary>Clamps the angle between min and max values.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static AngleD Clamp(AngleD v, AngleD min, AngleD max) => new(Math.Clamp(v.Radians, min.Radians, max.Radians));

	/// <summary>Clamps the angle between min and max values.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static AngleD Min(AngleD l, AngleD r) => new(Math.Min(l.Radians, r.Radians));

	/// <summary>Clamps the angle between min and max values.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static AngleD Max(AngleD l, AngleD r) => new(Math.Max(l.Radians, r.Radians));

	/// <summary>Checks equality using <see cref="MathUtils.ApproxEqual(double,double,double)"/>.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxEqual(AngleD o, double eps = MathUtils.TOL_64) => 
		(Radians % Math.Tau).ApproxEqual(o.Radians % Math.Tau, eps);

	/// <summary>Checks for approximate zero using <see cref="MathUtils.ApproxZero(double,double)"/>.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public bool ApproxZero(double eps = MathUtils.TOL_64) => (Radians % Math.Tau).ApproxZero(eps);


	#region Operators
	public static bool operator == (AngleD l, AngleD r) => l.Equals(r);
	public static bool operator != (AngleD l, AngleD r) => !l.Equals(r);
	public static bool operator <= (AngleD l, AngleD r) => l.Radians <= r.Radians;
	public static bool operator <  (AngleD l, AngleD r) => l.Radians <  r.Radians;
	public static bool operator >= (AngleD l, AngleD r) => l.Radians >= r.Radians;
	public static bool operator >  (AngleD l, AngleD r) => l.Radians >  r.Radians;

	public static AngleD operator + (AngleD l, AngleD r) => new(l.Radians + r.Radians);
	public static AngleD operator - (AngleD l, AngleD r) => new(l.Radians - r.Radians);
	public static AngleD operator * (AngleD l, double r) => new(l.Radians * r);
	public static AngleD operator / (AngleD l, double r) => new(l.Radians / r);
	public static AngleD operator % (AngleD l, AngleD r) => new(l.Radians % r.Radians);
	public static AngleD operator - (AngleD r) => new(-r.Radians);

	public static implicit operator AngleD (Angle a) => new(a.Radians);
	#endregion // Operators
}

