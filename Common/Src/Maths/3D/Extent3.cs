﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>A positive-valued axis extent or size in 3D space.</summary>
[StructLayout(LayoutKind.Sequential, Size = 12)]
public readonly struct Extent3 :
	IEquatable<Extent3>, IEqualityOperators<Extent3, Extent3, bool>
{
	/// <summary>Zero-size extent constant.</summary>
	public static readonly Extent3 Zero = new();


	#region Fields
	/// <summary>The width of the extent (size in X-axis).</summary>
	public readonly uint X;
	/// <summary>The height of the extent (size in Y-axis).</summary>
	public readonly uint Y;
	/// <summary>The depth of the extent (size in Z-axis).</summary>
	public readonly uint Z;

	/// <summary>The total internal volume of the extent.</summary>
	public uint Volume => X * Y * Z;

	/// <summary>If the extent has zero internal volume.</summary>
	public bool IsEmpty => X == 0 || Y == 0 || Z == 0;
	#endregion // Fields

	
	/// <summary>Construct an extent from axis sizes.</summary>
	/// <param name="x">The region width (x-axis).</param>
	/// <param name="y">The region height (y-axis).</param>
	/// <param name="z">The region depth (z-axis).</param>
	public Extent3(uint x, uint y, uint z) => (X, Y, Z) = (x, y, z);


	#region Base
	public bool Equals(Extent3 o) => X == o.X && Y == o.Y && Z == o.Z;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Extent3 e && X == e.X && Y == e.Y && Z == e.Z;

	public override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public override string ToString() => $"{{{X},{Y},{Z}}}";

	public void Deconstruct(out uint x, out uint y, out uint z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	public static bool operator == (Extent3 l, Extent3 r) => l.Equals(r);
	public static bool operator != (Extent3 l, Extent3 r) => !l.Equals(r);

	public static explicit operator Extent3 (Extent3L r) => new((uint)r.X, (uint)r.Y, (uint)r.Z);
}


/// <summary>A positive-valued axis extent or size in 3D space.</summary>
[StructLayout(LayoutKind.Sequential, Size = 24)]
public readonly struct Extent3L :
	IEquatable<Extent3L>, IEqualityOperators<Extent3L, Extent3L, bool>
{
	/// <summary>Zero-size extent constant.</summary>
	public static readonly Extent3L Zero = new();


	#region Fields
	/// <summary>The width of the extent (size in X-axis).</summary>
	public readonly ulong X;
	/// <summary>The height of the extent (size in Y-axis).</summary>
	public readonly ulong Y;
	/// <summary>The depth of the extent (size in Z-axis).</summary>
	public readonly ulong Z;

	/// <summary>The total internal volume of the extent.</summary>
	public ulong Volume => X * Y * Z;

	/// <summary>If the extent has zero internal volume.</summary>
	public bool IsEmpty => X == 0 || Y == 0 || Z == 0;
	#endregion // Fields

	
	/// <summary>Construct an extent from axis sizes.</summary>
	/// <param name="x">The region width (x-axis).</param>
	/// <param name="y">The region height (y-axis).</param>
	/// <param name="z">The region depth (z-axis).</param>
	public Extent3L(ulong x, ulong y, ulong z) => (X, Y, Z) = (x, y, z);


	#region Base
	public bool Equals(Extent3L o) => X == o.X && Y == o.Y && Z == o.Z;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Extent3L e && X == e.X && Y == e.Y && Z == e.Z;

	public override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public override string ToString() => $"{{{X},{Y},{Z}}}";

	public void Deconstruct(out ulong x, out ulong y, out ulong z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	public static bool operator == (Extent3L l, Extent3L r) => l.Equals(r);
	public static bool operator != (Extent3L l, Extent3L r) => !l.Equals(r);

	public static implicit operator Extent3L (Extent3 r) => new(r.X, r.Y, r.Z);
}

