﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace Astrum.Maths;


/// <summary>An axis-aligned rectangular region in 3D space.</summary>
public readonly struct Box3 :
	IEquatable<Box3>, IEqualityOperators<Box3, Box3, bool>
{
	/// <summary>Represents a region at the origin with zero size.</summary>
	public static readonly Box3 Empty = new(0, 0, 0, 0, 0, 0);
	/// <summary>Represents a region with unit side-lengths, with <see cref="Min"/> at the origin.</summary>
	public static readonly Box3 Unit = new(0, 0, 0, 1, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public readonly Point3 Min;
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public readonly Point3 Max;

	/// <summary>The dimensions of the region.</summary>
	public Extent3 Size => new((uint)(Max.X - Min.X), (uint)(Max.Y - Min.Y), (uint)(Max.Z - Min.Z));
	/// <summary>The width of the region.</summary>
	public uint Width => (uint)(Max.X - Min.X);
	/// <summary>The height of the region.</summary>
	public uint Height => (uint)(Max.Y - Min.Y);
	/// <summary>The depth of the region.</summary>
	public uint Depth => (uint)(Max.Z - Min.Z);

	/// <summary>The center point of the region.</summary>
	public Vec3 Center => new(Min.X + (Max.X - Min.X) / 2f, Min.Y + (Max.Y - Min.Y) / 2f, Min.Z + (Max.Z - Min.Z) / 2f);

	/// <summary>The total interior volume of the region.</summary>
	public int Volume => (Max.X - Min.X) * (Max.Y - Min.Y) * (Max.Z - Min.Z);

	/// <summary>If the region is empty (zero internal volume).</summary>
	public bool IsEmpty => Min.X == Max.X || Min.Y == Max.Y || Min.Z == Max.Z;
	#endregion // Fields


	/// <summary>Create a region from corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="z1">The z-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	/// <param name="z2">The z-component of the second corner.</param>
	public Box3(int x1, int y1, int z1, int x2, int y2, int z2)
	{
		bool sx = x2 < x1, sy = y2 < y1, sz = z2 < z1;
		Min = new(sx ? x2 : x1, sy ? y2 : y1, sz ? z2 : z1);
		Max = new(sx ? x1 : x2, sy ? y1 : y2, sz ? z1 : z2);
	}

	/// <summary>Create a region defined by two corner points.</summary>
	public Box3(Point3 c1, Point3 c2) : this(c1.X, c1.Y, c1.Z, c2.X, c2.Y, c2.Z) { }


	#region Base
	public bool Equals(Box3 o) => Min == o.Min && Max == o.Max;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Box3 b && Equals(b);

	public override int GetHashCode() => HashCode.Combine(Min.X, Min.Y, Min.Z, Max.X, Max.Y, Max.Z);

	public override string ToString() => $"{{{Min},{Max}}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) =>
		$"{{{Min.ToString(fmt)},{Max.ToString(fmt)}}}";

	public void Deconstruct(out Point3 min, out Point3 max) => (min, max) = (Min, Max);

	public void Deconstruct(out int minX, out int minY, out int minZ, out int maxX, out int maxY, out int maxZ) =>
		(minX, minY, minZ, maxX, maxY, maxZ) = (Min.X, Min.Y, Min.Z, Max.X, Max.Y, Max.Z);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets a linearly translated version of the region.</summary>
	public Box3 Translated(Point3 delta) => new(Min + delta, Max + delta);

	/// <summary>Scales the region around the center point.</summary>
	/// <remarks>Since all edges are grown by the same amount, the total dimensions will change by twice the delta.</remarks>
	/// <param name="delta">The absolute amount to scale each edge by relative to the center.</param>
	public Box3 Inflated(Point3 delta) => new(Min - delta, Max + delta);

	/// <summary>Scales the region around the global anchor point.</summary>
	/// <param name="scale">The relative amount to scale each dimension by.</param>
	/// <param name="anchor">The global point around which the scaling originates.</param>
	public Box3 Scaled(Point3 scale, Point3 anchor) =>
		new(scale * (Min - anchor) + anchor, scale * (Max - anchor) + anchor);

	/// <summary>Calculates the minimal region that contains both input regions entirely.</summary>
	public static Box3 Union(Box3 l, Box3 r) => new(Point3.Min(l.Min, r.Min), Point3.Max(l.Max, r.Max));

	/// <summary>Calculates the overlap between the regions, returns <see cref="Empty"/> when no overlap.</summary>
	public static Box3 Intersect(Box3 l, Box3 r)
	{
		var min = Point3.Max(l.Min, r.Min);
		var max = Point3.Min(l.Max, r.Max);
		return (min.X > max.X || min.Y > max.Y || min.Z > max.Z) ? Empty : new(min, max);
	}
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box3 l, Box3 r) => l.Equals(r);
	public static bool operator != (Box3 l, Box3 r) => !l.Equals(r);

	public static explicit operator Box3 (Box3L o) => 
		new((int)o.Min.X, (int)o.Min.Y, (int)o.Min.Z, (int)o.Max.X, (int)o.Max.Y, (int)o.Max.Z);
	public static explicit operator Box3 (Box3F o) => 
		new((int)o.Min.X, (int)o.Min.Y, (int)o.Min.Z, (int)o.Max.X, (int)o.Max.Y, (int)o.Max.Z);
	public static explicit operator Box3 (Box3D o) => 
		new((int)o.Min.X, (int)o.Min.Y, (int)o.Min.Z, (int)o.Max.X, (int)o.Max.Y, (int)o.Max.Z);
	#endregion // Operators
}


// [Partial] Space3D operations for Box3
public static partial class Space3D
{
	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Point3 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Point3L point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Vec3 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3 box, Vec3D point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

}


/// <summary>An axis-aligned rectangular region in 3D space.</summary>
public readonly struct Box3L :
	IEquatable<Box3L>, IEqualityOperators<Box3L, Box3L, bool>
{
	/// <summary>Represents a region at the origin with zero size.</summary>
	public static readonly Box3L Empty = new(0, 0, 0, 0, 0, 0);
	/// <summary>Represents a region with unit side-lengths, with <see cref="Min"/> at the origin.</summary>
	public static readonly Box3L Unit = new(0, 0, 0, 1, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public readonly Point3L Min;
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public readonly Point3L Max;

	/// <summary>The dimensions of the region.</summary>
	public Extent3L Size => new((ulong)(Max.X - Min.X), (ulong)(Max.Y - Min.Y), (ulong)(Max.Z - Min.Z));
	/// <summary>The width of the region.</summary>
	public ulong Width => (ulong)(Max.X - Min.X);
	/// <summary>The height of the region.</summary>
	public ulong Height => (ulong)(Max.Y - Min.Y);
	/// <summary>The depth of the region.</summary>
	public ulong Depth => (ulong)(Max.Z - Min.Z);

	/// <summary>The center point of the region.</summary>
	public Vec3D Center => new(Min.X + (Max.X - Min.X) / 2f, Min.Y + (Max.Y - Min.Y) / 2f, Min.Z + (Max.Z - Min.Z) / 2f);

	/// <summary>The total interior volume of the region.</summary>
	public long Volume => (Max.X - Min.X) * (Max.Y - Min.Y) * (Max.Z - Min.Z);

	/// <summary>If the region is empty (zero internal volume).</summary>
	public bool IsEmpty => Min.X == Max.X || Min.Y == Max.Y || Min.Z == Max.Z;
	#endregion // Fields


	/// <summary>Create a region from corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="z1">The z-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	/// <param name="z2">The z-component of the second corner.</param>
	public Box3L(long x1, long y1, long z1, long x2, long y2, long z2)
	{
		bool sx = x2 < x1, sy = y2 < y1, sz = z2 < z1;
		Min = new(sx ? x2 : x1, sy ? y2 : y1, sz ? z2 : z1);
		Max = new(sx ? x1 : x2, sy ? y1 : y2, sz ? z1 : z2);
	}

	/// <summary>Create a region defined by two corner points.</summary>
	public Box3L(Point3L c1, Point3L c2) : this(c1.X, c1.Y, c1.Z, c2.X, c2.Y, c2.Z) { }


	#region Base
	public bool Equals(Box3L o) => Min == o.Min && Max == o.Max;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Box3L b && Equals(b);

	public override int GetHashCode() => HashCode.Combine(Min.X, Min.Y, Min.Z, Max.X, Max.Y, Max.Z);

	public override string ToString() => $"{{{Min},{Max}}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) =>
		$"{{{Min.ToString(fmt)},{Max.ToString(fmt)}}}";

	public void Deconstruct(out Point3L min, out Point3L max) => (min, max) = (Min, Max);

	public void Deconstruct(out long minX, out long minY, out long minZ, out long maxX, out long maxY, out long maxZ) =>
		(minX, minY, minZ, maxX, maxY, maxZ) = (Min.X, Min.Y, Min.Z, Max.X, Max.Y, Max.Z);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets a linearly translated version of the region.</summary>
	public Box3L Translated(Point3L delta) => new(Min + delta, Max + delta);

	/// <summary>Scales the region around the center point.</summary>
	/// <remarks>Since all edges are grown by the same amount, the total dimensions will change by twice the delta.</remarks>
	/// <param name="delta">The absolute amount to scale each edge by relative to the center.</param>
	public Box3L Inflated(Point3L delta) => new(Min - delta, Max + delta);

	/// <summary>Scales the region around the global anchor point.</summary>
	/// <param name="scale">The relative amount to scale each dimension by.</param>
	/// <param name="anchor">The global point around which the scaling originates.</param>
	public Box3L Scaled(Point3L scale, Point3L anchor) =>
		new(scale * (Min - anchor) + anchor, scale * (Max - anchor) + anchor);

	/// <summary>Calculates the minimal region that contains both input regions entirely.</summary>
	public static Box3L Union(Box3L l, Box3L r) => new(Point3L.Min(l.Min, r.Min), Point3L.Max(l.Max, r.Max));

	/// <summary>Calculates the overlap between the regions, returns <see cref="Empty"/> when no overlap.</summary>
	public static Box3L Intersect(Box3L l, Box3L r)
	{
		var min = Point3L.Max(l.Min, r.Min);
		var max = Point3L.Min(l.Max, r.Max);
		return (min.X > max.X || min.Y > max.Y || min.Z > max.Z) ? Empty : new(min, max);
	}
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box3L l, Box3L r) => l.Equals(r);
	public static bool operator != (Box3L l, Box3L r) => !l.Equals(r);

	public static implicit operator Box3L (Box3 o) => 
		new((long)o.Min.X, (long)o.Min.Y, (long)o.Min.Z, (long)o.Max.X, (long)o.Max.Y, (long)o.Max.Z);
	public static explicit operator Box3L (Box3F o) => 
		new((long)o.Min.X, (long)o.Min.Y, (long)o.Min.Z, (long)o.Max.X, (long)o.Max.Y, (long)o.Max.Z);
	public static explicit operator Box3L (Box3D o) => 
		new((long)o.Min.X, (long)o.Min.Y, (long)o.Min.Z, (long)o.Max.X, (long)o.Max.Y, (long)o.Max.Z);
	#endregion // Operators
}


// [Partial] Space3D operations for Box3L
public static partial class Space3D
{
	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Point3 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Point3L point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Vec3 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3L box, Vec3D point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

}


/// <summary>An axis-aligned rectangular region in 3D space.</summary>
public readonly struct Box3F :
	IEquatable<Box3F>, IEqualityOperators<Box3F, Box3F, bool>
{
	/// <summary>Represents a region at the origin with zero size.</summary>
	public static readonly Box3F Empty = new(0, 0, 0, 0, 0, 0);
	/// <summary>Represents a region with unit side-lengths, with <see cref="Min"/> at the origin.</summary>
	public static readonly Box3F Unit = new(0, 0, 0, 1, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public readonly Vec3 Min;
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public readonly Vec3 Max;

	/// <summary>The dimensions of the region.</summary>
	public Vec3 Size => new((float)(Max.X - Min.X), (float)(Max.Y - Min.Y), (float)(Max.Z - Min.Z));
	/// <summary>The width of the region.</summary>
	public float Width => (float)(Max.X - Min.X);
	/// <summary>The height of the region.</summary>
	public float Height => (float)(Max.Y - Min.Y);
	/// <summary>The depth of the region.</summary>
	public float Depth => (float)(Max.Z - Min.Z);

	/// <summary>The center point of the region.</summary>
	public Vec3 Center => new(Min.X + (Max.X - Min.X) / 2f, Min.Y + (Max.Y - Min.Y) / 2f, Min.Z + (Max.Z - Min.Z) / 2f);

	/// <summary>The total interior volume of the region.</summary>
	public float Volume => (Max.X - Min.X) * (Max.Y - Min.Y) * (Max.Z - Min.Z);

	/// <summary>If the region is empty (zero internal volume).</summary>
	public bool IsEmpty => Min.X.FastApprox(Max.X) || Min.Y.FastApprox(Max.Y) || Min.Z.FastApprox(Max.Z);
	#endregion // Fields


	/// <summary>Create a region from corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="z1">The z-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	/// <param name="z2">The z-component of the second corner.</param>
	public Box3F(float x1, float y1, float z1, float x2, float y2, float z2)
	{
		bool sx = x2 < x1, sy = y2 < y1, sz = z2 < z1;
		Min = new(sx ? x2 : x1, sy ? y2 : y1, sz ? z2 : z1);
		Max = new(sx ? x1 : x2, sy ? y1 : y2, sz ? z1 : z2);
	}

	/// <summary>Create a region defined by two corner points.</summary>
	public Box3F(Vec3 c1, Vec3 c2) : this(c1.X, c1.Y, c1.Z, c2.X, c2.Y, c2.Z) { }


	#region Base
	public bool Equals(Box3F o) => Min == o.Min && Max == o.Max;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Box3F b && Equals(b);

	public override int GetHashCode() => HashCode.Combine(Min.X, Min.Y, Min.Z, Max.X, Max.Y, Max.Z);

	public override string ToString() => $"{{{Min},{Max}}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) =>
		$"{{{Min.ToString(fmt)},{Max.ToString(fmt)}}}";

	public void Deconstruct(out Vec3 min, out Vec3 max) => (min, max) = (Min, Max);

	public void Deconstruct(out float minX, out float minY, out float minZ, out float maxX, out float maxY, out float maxZ) =>
		(minX, minY, minZ, maxX, maxY, maxZ) = (Min.X, Min.Y, Min.Z, Max.X, Max.Y, Max.Z);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets a linearly translated version of the region.</summary>
	public Box3F Translated(Vec3 delta) => new(Min + delta, Max + delta);

	/// <summary>Scales the region around the center point.</summary>
	/// <remarks>Since all edges are grown by the same amount, the total dimensions will change by twice the delta.</remarks>
	/// <param name="delta">The absolute amount to scale each edge by relative to the center.</param>
	public Box3F Inflated(Vec3 delta) => new(Min - delta, Max + delta);

	/// <summary>Scales the region around the global anchor point.</summary>
	/// <param name="scale">The relative amount to scale each dimension by.</param>
	/// <param name="anchor">The global point around which the scaling originates.</param>
	public Box3F Scaled(Vec3 scale, Vec3 anchor) =>
		new(scale * (Min - anchor) + anchor, scale * (Max - anchor) + anchor);

	/// <summary>Calculates the minimal region that contains both input regions entirely.</summary>
	public static Box3F Union(Box3F l, Box3F r) => new(Vec3.Min(l.Min, r.Min), Vec3.Max(l.Max, r.Max));

	/// <summary>Calculates the overlap between the regions, returns <see cref="Empty"/> when no overlap.</summary>
	public static Box3F Intersect(Box3F l, Box3F r)
	{
		var min = Vec3.Max(l.Min, r.Min);
		var max = Vec3.Min(l.Max, r.Max);
		return (min.X > max.X || min.Y > max.Y || min.Z > max.Z) ? Empty : new(min, max);
	}
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box3F l, Box3F r) => l.Equals(r);
	public static bool operator != (Box3F l, Box3F r) => !l.Equals(r);

	public static implicit operator Box3F (Box3 o) => 
		new((float)o.Min.X, (float)o.Min.Y, (float)o.Min.Z, (float)o.Max.X, (float)o.Max.Y, (float)o.Max.Z);
	public static implicit operator Box3F (Box3L o) => 
		new((float)o.Min.X, (float)o.Min.Y, (float)o.Min.Z, (float)o.Max.X, (float)o.Max.Y, (float)o.Max.Z);
	public static explicit operator Box3F (Box3D o) => 
		new((float)o.Min.X, (float)o.Min.Y, (float)o.Min.Z, (float)o.Max.X, (float)o.Max.Y, (float)o.Max.Z);
	#endregion // Operators
}


// [Partial] Space3D operations for Box3F
public static partial class Space3D
{
	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Point3 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Point3L point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Vec3 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3F box, Vec3D point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

}


/// <summary>An axis-aligned rectangular region in 3D space.</summary>
public readonly struct Box3D :
	IEquatable<Box3D>, IEqualityOperators<Box3D, Box3D, bool>
{
	/// <summary>Represents a region at the origin with zero size.</summary>
	public static readonly Box3D Empty = new(0, 0, 0, 0, 0, 0);
	/// <summary>Represents a region with unit side-lengths, with <see cref="Min"/> at the origin.</summary>
	public static readonly Box3D Unit = new(0, 0, 0, 1, 1, 1);


	#region Fields
	/// <summary>The minimum (most negative) corner of the region.</summary>
	public readonly Vec3D Min;
	/// <summary>The maximum (most positive) corner of the region.</summary>
	public readonly Vec3D Max;

	/// <summary>The dimensions of the region.</summary>
	public Vec3D Size => new((double)(Max.X - Min.X), (double)(Max.Y - Min.Y), (double)(Max.Z - Min.Z));
	/// <summary>The width of the region.</summary>
	public double Width => (double)(Max.X - Min.X);
	/// <summary>The height of the region.</summary>
	public double Height => (double)(Max.Y - Min.Y);
	/// <summary>The depth of the region.</summary>
	public double Depth => (double)(Max.Z - Min.Z);

	/// <summary>The center point of the region.</summary>
	public Vec3D Center => new(Min.X + (Max.X - Min.X) / 2f, Min.Y + (Max.Y - Min.Y) / 2f, Min.Z + (Max.Z - Min.Z) / 2f);

	/// <summary>The total interior volume of the region.</summary>
	public double Volume => (Max.X - Min.X) * (Max.Y - Min.Y) * (Max.Z - Min.Z);

	/// <summary>If the region is empty (zero internal volume).</summary>
	public bool IsEmpty => Min.X.FastApprox(Max.X) || Min.Y.FastApprox(Max.Y) || Min.Z.FastApprox(Max.Z);
	#endregion // Fields


	/// <summary>Create a region from corner components.</summary>
	/// <param name="x1">The x-component of the first corner.</param>
	/// <param name="y1">The y-component of the first corner.</param>
	/// <param name="z1">The z-component of the first corner.</param>
	/// <param name="x2">The x-component of the second corner.</param>
	/// <param name="y2">The y-component of the second corner.</param>
	/// <param name="z2">The z-component of the second corner.</param>
	public Box3D(double x1, double y1, double z1, double x2, double y2, double z2)
	{
		bool sx = x2 < x1, sy = y2 < y1, sz = z2 < z1;
		Min = new(sx ? x2 : x1, sy ? y2 : y1, sz ? z2 : z1);
		Max = new(sx ? x1 : x2, sy ? y1 : y2, sz ? z1 : z2);
	}

	/// <summary>Create a region defined by two corner points.</summary>
	public Box3D(Vec3D c1, Vec3D c2) : this(c1.X, c1.Y, c1.Z, c2.X, c2.Y, c2.Z) { }


	#region Base
	public bool Equals(Box3D o) => Min == o.Min && Max == o.Max;

	public override bool Equals([NotNullWhen(true)] object? o) => o is Box3D b && Equals(b);

	public override int GetHashCode() => HashCode.Combine(Min.X, Min.Y, Min.Z, Max.X, Max.Y, Max.Z);

	public override string ToString() => $"{{{Min},{Max}}}";

	public string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) =>
		$"{{{Min.ToString(fmt)},{Max.ToString(fmt)}}}";

	public void Deconstruct(out Vec3D min, out Vec3D max) => (min, max) = (Min, Max);

	public void Deconstruct(out double minX, out double minY, out double minZ, out double maxX, out double maxY, out double maxZ) =>
		(minX, minY, minZ, maxX, maxY, maxZ) = (Min.X, Min.Y, Min.Z, Max.X, Max.Y, Max.Z);
	#endregion // Base


	#region Rect Ops
	/// <summary>Gets a linearly translated version of the region.</summary>
	public Box3D Translated(Vec3D delta) => new(Min + delta, Max + delta);

	/// <summary>Scales the region around the center point.</summary>
	/// <remarks>Since all edges are grown by the same amount, the total dimensions will change by twice the delta.</remarks>
	/// <param name="delta">The absolute amount to scale each edge by relative to the center.</param>
	public Box3D Inflated(Vec3D delta) => new(Min - delta, Max + delta);

	/// <summary>Scales the region around the global anchor point.</summary>
	/// <param name="scale">The relative amount to scale each dimension by.</param>
	/// <param name="anchor">The global point around which the scaling originates.</param>
	public Box3D Scaled(Vec3D scale, Vec3D anchor) =>
		new(scale * (Min - anchor) + anchor, scale * (Max - anchor) + anchor);

	/// <summary>Calculates the minimal region that contains both input regions entirely.</summary>
	public static Box3D Union(Box3D l, Box3D r) => new(Vec3D.Min(l.Min, r.Min), Vec3D.Max(l.Max, r.Max));

	/// <summary>Calculates the overlap between the regions, returns <see cref="Empty"/> when no overlap.</summary>
	public static Box3D Intersect(Box3D l, Box3D r)
	{
		var min = Vec3D.Max(l.Min, r.Min);
		var max = Vec3D.Min(l.Max, r.Max);
		return (min.X > max.X || min.Y > max.Y || min.Z > max.Z) ? Empty : new(min, max);
	}
	#endregion // Rect Ops


	#region Operators
	public static bool operator == (Box3D l, Box3D r) => l.Equals(r);
	public static bool operator != (Box3D l, Box3D r) => !l.Equals(r);

	public static implicit operator Box3D (Box3 o) => 
		new((double)o.Min.X, (double)o.Min.Y, (double)o.Min.Z, (double)o.Max.X, (double)o.Max.Y, (double)o.Max.Z);
	public static implicit operator Box3D (Box3L o) => 
		new((double)o.Min.X, (double)o.Min.Y, (double)o.Min.Z, (double)o.Max.X, (double)o.Max.Y, (double)o.Max.Z);
	public static implicit operator Box3D (Box3F o) => 
		new((double)o.Min.X, (double)o.Min.Y, (double)o.Min.Z, (double)o.Max.X, (double)o.Max.Y, (double)o.Max.Z);
	#endregion // Operators
}


// [Partial] Space3D operations for Box3D
public static partial class Space3D
{
	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Point3 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Point3L point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Vec3 point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

	/// <summary>Checks if the given point is contained within the given rectangular region.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static bool Contains(this Box3D box, Vec3D point) =>
		point.X >= box.Min.X && point.X <= box.Max.X && 
		point.Y >= box.Min.Y && point.Y <= box.Max.Y &&
		point.Z >= box.Min.Z && point.Z <= box.Max.Z;

}

