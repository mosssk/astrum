﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>A three-component vector of <c>int</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 12)]
public struct Point3 :
	IEquatable<Point3>, IEqualityOperators<Point3, Point3, bool>,
	IAdditionOperators<Point3, Point3, Point3>, IAdditionOperators<Point3, int, Point3>,
	ISubtractionOperators<Point3, Point3, Point3>, ISubtractionOperators<Point3, int, Point3>,
	IMultiplyOperators<Point3, Point3, Point3>, IMultiplyOperators<Point3, int, Point3>,
	IDivisionOperators<Point3, Point3, Point3>, IDivisionOperators<Point3, int, Point3>,
	IModulusOperators<Point3, Point3, Point3>, IModulusOperators<Point3, int, Point3>,
	IBitwiseOperators<Point3, Point3, Point3>, IBitwiseOperators<Point3, int, Point3>,
	IUnaryNegationOperators<Point3, Point3>
{
	#region Constants
	/// <summary>Point with all-zero components.</summary>
	public static readonly Point3 Zero  = new();

	/// <summary>Point with all-one components.</summary>
	public static readonly Point3 One   = new(1);
	/// <summary>Point with unit-length along the X-axis.</summary>
	public static readonly Point3 UnitX = new(1, 0, 0);
	/// <summary>Point with unit-length along the Y-axis.</summary>
	public static readonly Point3 UnitY = new(0, 1, 0);
	/// <summary>Point with unit-length along the Z-axis.</summary>
	public static readonly Point3 UnitZ = new(0, 0, 1);

	/// <summary>Unit-length right-pointing vector in right-handed space (+X).</summary>
	public static readonly Point3 Right    = new(1, 0, 0);
	/// <summary>Unit-length up-pointing vector in right-handed space (+Y).</summary>
	public static readonly Point3 Up       = new(0, 1, 0);
	/// <summary>Unit-length backward-pointing vector in right-handed space (+Z).</summary>
	public static readonly Point3 Backward = new(0, 0, 1);
	/// <summary>Unit-length left-pointing vector in right-handed space (-X).</summary>
	public static readonly Point3 Left     = new(-1, 0, 0);
	/// <summary>Unit-length down-pointing vector in right-handed space (-Y).</summary>
	public static readonly Point3 Down     = new(0, -1, 0);
	/// <summary>Unit-length forward-pointing vector in right-handed space (-Z).</summary>
	public static readonly Point3 Forward  = new(0, 0, -1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public int X;
	/// <summary>The Y-component.</summary>
	public int Y;
	/// <summary>The Z-component.</summary>
	public int Z;

	/// <summary>The length of the point as a vector.</summary>
	public readonly float Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Single.Sqrt(X*X + Y*Y + Z*Z);
	}

	/// <summary>The squared length of the point as a vector.</summary>
	public readonly float LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z;
	}
	#endregion // Fields


	/// <summary>Construct a point with equal components.</summary>
	public Point3(int v) => X = Y = Z = v;

	/// <summary>Construct a point from the components.</summary>
	public Point3(int x, int y, int z) => (X, Y, Z) = (x, y, z);

	/// <summary>Construct a point from a point providing XY, with an additional Z component.</summary>
	public Point3(Point2 xy, int z) => (X, Y, Z) = (xy.X, xy.Y, z);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Point3 o) => X == o.X && Y == o.Y && Z == o.Z;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => 
		o is Point3 p && X == p.X && Y == p.Y && Z == p.Z;

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public readonly override string ToString() => $"{{{X},{Y},{Z}}}";

	public readonly string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) => 
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)}}}";

	public readonly void Deconstruct(out int x, out int y, out int z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly int Dot(Point3 o) => X*o.X + Y*o.Y + Z*o.Z;

	/// <summary>Calculates the right-handed cross product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Point3 Cross(Point3 o) => new(Y * o.Z - Z * o.Y, Z * o.X - X * o.Z, X * o.Y - Y * o.X);

	/// <summary>Calculates angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point3 o) => Angle.Rad((float)Single.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Component-wise clamp of the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3 Clamp(Point3 v, Point3 min, Point3 max) =>
		new(Int32.Clamp(v.X, min.X, max.X), Int32.Clamp(v.Y, min.Y, max.Y), Int32.Clamp(v.Z, min.Z, max.Z)); 

	/// <summary>Component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3 Min(Point3 l, Point3 r) => 
		new(Int32.Min(l.X, r.X), Int32.Min(l.Y, r.Y), Int32.Min(l.Z, r.Z));

	/// <summary>Component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3 Max(Point3 l, Point3 r) => 
		new(Int32.Max(l.X, r.X), Int32.Max(l.Y, r.Y), Int32.Max(l.Z, r.Z)); 
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point3 l, Point3 r) => l.Equals(r);
	public static bool operator != (Point3 l, Point3 r) => !l.Equals(r);

	public static Bool3 operator <= (Point3 l, Point3 r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z);
	public static Bool3 operator <  (Point3 l, Point3 r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z);
	public static Bool3 operator >= (Point3 l, Point3 r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
	public static Bool3 operator >  (Point3 l, Point3 r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z);

	public static Point3 operator + (Point3 l, Point3 r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z);
	public static Point3 operator + (Point3 l, int r) => new(l.X + r, l.Y + r, l.Z + r);
	public static Point3 operator - (Point3 l, Point3 r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z);
	public static Point3 operator - (Point3 l, int r) => new(l.X - r, l.Y - r, l.Z - r);
	public static Point3 operator * (Point3 l, Point3 r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z);
	public static Point3 operator * (Point3 l, int r) => new(l.X * r, l.Y * r, l.Z * r);
	public static Point3 operator / (Point3 l, Point3 r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z);
	public static Point3 operator / (Point3 l, int r) => new(l.X / r, l.Y / r, l.Z / r);
	public static Point3 operator % (Point3 l, Point3 r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z);
	public static Point3 operator % (Point3 l, int r) => new(l.X % r, l.Y % r, l.Z % r);

	public static Point3 operator & (Point3 l, Point3 r) => new(l.X & r.X, l.Y & r.Y, l.Z & r.Z);
	public static Point3 operator & (Point3 l, int r) => new(l.X & r, l.Y & r, l.Z & r);
	public static Point3 operator | (Point3 l, Point3 r) => new(l.X | r.X, l.Y | r.Y, l.Z | r.Z);
	public static Point3 operator | (Point3 l, int r) => new(l.X | r, l.Y | r, l.Z | r);
	public static Point3 operator ^ (Point3 l, Point3 r) => new(l.X ^ r.X, l.Y ^ r.Y, l.Z ^ r.Z);
	public static Point3 operator ^ (Point3 l, int r) => new(l.X ^ r, l.Y ^ r, l.Z ^ r);
	public static Point3 operator ~ (Point3 r) => new(~r.X, ~r.Y, ~r.Z);

	public static Point3 operator - (Point3 r) => new(-r.X, -r.Y, -r.Z);

	public static explicit operator Point3 (Vec3 v) => new((int)v.X, (int)v.Y, (int)v.Z);
	public static explicit operator Point3 (Vec3D v) => new((int)v.X, (int)v.Y, (int)v.Z);

	public static explicit operator Point3 (Point3L o) => new((int)o.X, (int)o.Y, (int)o.Z);
	#endregion // Operators
}


/// <summary>A three-component vector of <c>long</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 24)]
public struct Point3L :
	IEquatable<Point3L>, IEqualityOperators<Point3L, Point3L, bool>,
	IAdditionOperators<Point3L, Point3L, Point3L>, IAdditionOperators<Point3L, long, Point3L>,
	ISubtractionOperators<Point3L, Point3L, Point3L>, ISubtractionOperators<Point3L, long, Point3L>,
	IMultiplyOperators<Point3L, Point3L, Point3L>, IMultiplyOperators<Point3L, long, Point3L>,
	IDivisionOperators<Point3L, Point3L, Point3L>, IDivisionOperators<Point3L, long, Point3L>,
	IModulusOperators<Point3L, Point3L, Point3L>, IModulusOperators<Point3L, long, Point3L>,
	IBitwiseOperators<Point3L, Point3L, Point3L>, IBitwiseOperators<Point3L, long, Point3L>,
	IUnaryNegationOperators<Point3L, Point3L>
{
	#region Constants
	/// <summary>Point with all-zero components.</summary>
	public static readonly Point3L Zero  = new();

	/// <summary>Point with all-one components.</summary>
	public static readonly Point3L One   = new(1);
	/// <summary>Point with unit-length along the X-axis.</summary>
	public static readonly Point3L UnitX = new(1, 0, 0);
	/// <summary>Point with unit-length along the Y-axis.</summary>
	public static readonly Point3L UnitY = new(0, 1, 0);
	/// <summary>Point with unit-length along the Z-axis.</summary>
	public static readonly Point3L UnitZ = new(0, 0, 1);

	/// <summary>Unit-length right-pointing vector in right-handed space (+X).</summary>
	public static readonly Point3L Right    = new(1, 0, 0);
	/// <summary>Unit-length up-pointing vector in right-handed space (+Y).</summary>
	public static readonly Point3L Up       = new(0, 1, 0);
	/// <summary>Unit-length backward-pointing vector in right-handed space (+Z).</summary>
	public static readonly Point3L Backward = new(0, 0, 1);
	/// <summary>Unit-length left-pointing vector in right-handed space (-X).</summary>
	public static readonly Point3L Left     = new(-1, 0, 0);
	/// <summary>Unit-length down-pointing vector in right-handed space (-Y).</summary>
	public static readonly Point3L Down     = new(0, -1, 0);
	/// <summary>Unit-length forward-pointing vector in right-handed space (-Z).</summary>
	public static readonly Point3L Forward  = new(0, 0, -1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public long X;
	/// <summary>The Y-component.</summary>
	public long Y;
	/// <summary>The Z-component.</summary>
	public long Z;

	/// <summary>The length of the point as a vector.</summary>
	public readonly double Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Double.Sqrt(X*X + Y*Y + Z*Z);
	}

	/// <summary>The squared length of the point as a vector.</summary>
	public readonly double LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z;
	}
	#endregion // Fields


	/// <summary>Construct a point with equal components.</summary>
	public Point3L(long v) => X = Y = Z = v;

	/// <summary>Construct a point from the components.</summary>
	public Point3L(long x, long y, long z) => (X, Y, Z) = (x, y, z);

	/// <summary>Construct a point from a point providing XY, with an additional Z component.</summary>
	public Point3L(Point2L xy, long z) => (X, Y, Z) = (xy.X, xy.Y, z);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Point3L o) => X == o.X && Y == o.Y && Z == o.Z;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => 
		o is Point3L p && X == p.X && Y == p.Y && Z == p.Z;

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z);

	public readonly override string ToString() => $"{{{X},{Y},{Z}}}";

	public readonly string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) => 
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)}}}";

	public readonly void Deconstruct(out long x, out long y, out long z) => (x, y, z) = (X, Y, Z);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly long Dot(Point3L o) => X*o.X + Y*o.Y + Z*o.Z;

	/// <summary>Calculates the right-handed cross product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Point3L Cross(Point3L o) => new(Y * o.Z - Z * o.Y, Z * o.X - X * o.Z, X * o.Y - Y * o.X);

	/// <summary>Calculates angle between the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Point3L o) => Angle.Rad((float)Double.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Component-wise clamp of the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3L Clamp(Point3L v, Point3L min, Point3L max) =>
		new(Int64.Clamp(v.X, min.X, max.X), Int64.Clamp(v.Y, min.Y, max.Y), Int64.Clamp(v.Z, min.Z, max.Z)); 

	/// <summary>Component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3L Min(Point3L l, Point3L r) => 
		new(Int64.Min(l.X, r.X), Int64.Min(l.Y, r.Y), Int64.Min(l.Z, r.Z));

	/// <summary>Component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point3L Max(Point3L l, Point3L r) => 
		new(Int64.Max(l.X, r.X), Int64.Max(l.Y, r.Y), Int64.Max(l.Z, r.Z)); 
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point3L l, Point3L r) => l.Equals(r);
	public static bool operator != (Point3L l, Point3L r) => !l.Equals(r);

	public static Bool3 operator <= (Point3L l, Point3L r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z);
	public static Bool3 operator <  (Point3L l, Point3L r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z);
	public static Bool3 operator >= (Point3L l, Point3L r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z);
	public static Bool3 operator >  (Point3L l, Point3L r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z);

	public static Point3L operator + (Point3L l, Point3L r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z);
	public static Point3L operator + (Point3L l, long r) => new(l.X + r, l.Y + r, l.Z + r);
	public static Point3L operator - (Point3L l, Point3L r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z);
	public static Point3L operator - (Point3L l, long r) => new(l.X - r, l.Y - r, l.Z - r);
	public static Point3L operator * (Point3L l, Point3L r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z);
	public static Point3L operator * (Point3L l, long r) => new(l.X * r, l.Y * r, l.Z * r);
	public static Point3L operator / (Point3L l, Point3L r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z);
	public static Point3L operator / (Point3L l, long r) => new(l.X / r, l.Y / r, l.Z / r);
	public static Point3L operator % (Point3L l, Point3L r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z);
	public static Point3L operator % (Point3L l, long r) => new(l.X % r, l.Y % r, l.Z % r);

	public static Point3L operator & (Point3L l, Point3L r) => new(l.X & r.X, l.Y & r.Y, l.Z & r.Z);
	public static Point3L operator & (Point3L l, long r) => new(l.X & r, l.Y & r, l.Z & r);
	public static Point3L operator | (Point3L l, Point3L r) => new(l.X | r.X, l.Y | r.Y, l.Z | r.Z);
	public static Point3L operator | (Point3L l, long r) => new(l.X | r, l.Y | r, l.Z | r);
	public static Point3L operator ^ (Point3L l, Point3L r) => new(l.X ^ r.X, l.Y ^ r.Y, l.Z ^ r.Z);
	public static Point3L operator ^ (Point3L l, long r) => new(l.X ^ r, l.Y ^ r, l.Z ^ r);
	public static Point3L operator ~ (Point3L r) => new(~r.X, ~r.Y, ~r.Z);

	public static Point3L operator - (Point3L r) => new(-r.X, -r.Y, -r.Z);

	public static explicit operator Point3L (Vec3 v) => new((long)v.X, (long)v.Y, (long)v.Z);
	public static explicit operator Point3L (Vec3D v) => new((long)v.X, (long)v.Y, (long)v.Z);

	public static implicit operator Point3L (Point3 o) => new(o.X, o.Y, o.Z);
	#endregion // Operators
}

