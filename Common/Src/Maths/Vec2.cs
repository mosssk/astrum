﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
// ReSharper disable CompareOfFloatsByEqualityOperator

namespace Astrum.Maths;


/// <summary>A two-component vector of <c>float</c>.</summary>
[StructLayout(LayoutKind.Sequential, Size = 8)]
public struct Vec2 :
	IEquatable<Vec2>, IEqualityOperators<Vec2, Vec2, bool>,
	IAdditionOperators<Vec2, Vec2, Vec2>, IAdditionOperators<Vec2, float, Vec2>,
	ISubtractionOperators<Vec2, Vec2, Vec2>, ISubtractionOperators<Vec2, float, Vec2>,
	IMultiplyOperators<Vec2, Vec2, Vec2>, IMultiplyOperators<Vec2, float, Vec2>,
	IDivisionOperators<Vec2, Vec2, Vec2>, IDivisionOperators<Vec2, float, Vec2>,
	IModulusOperators<Vec2, Vec2, Vec2>, IModulusOperators<Vec2, float, Vec2>,
	IUnaryNegationOperators<Vec2, Vec2>
{
	#region Constants
	/// <summary>Vector with all-zero components.</summary>
	public static readonly Vec2 Zero = new();
	/// <summary>Vector with all-one components.</summary>
	public static readonly Vec2 One = new(1);

	/// <summary>Vector with unit length along X-axis.</summary>
	public static readonly Vec2 UnitX = new(1, 0);
	/// <summary>Vector with unit length along Y-axis.</summary>
	public static readonly Vec2 UnitY = new(0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public float X;
	/// <summary>The Y-component.</summary>
	public float Y;

	/// <summary>The length of the vector.</summary>
	public readonly float Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Single.Sqrt(X*X + Y*Y);
	}

	/// <summary>The squared length of the vector.</summary>
	public readonly float LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y;
	}

	/// <summary>The normalized vector (length 1).</summary>
	public readonly Vec2 Normalized {
		[MethodImpl(MathUtils.MAX_OPT)]
		get { var iLen = 1 / Length; return new(X * iLen, Y * iLen); }
	}
	#endregion // Fields


	/// <summary>Construct a vector with equal components.</summary>
	public Vec2(float v) => X = Y = v;

	/// <summary>Construct a vector from the components.</summary>
	public Vec2(float x, float y) => (X, Y) = (x, y);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Vec2 o) => X.FastApprox(o.X) && Y.FastApprox(o.Y);

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Vec2 v && Equals(v);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y);

	public readonly override string ToString() => $"{{{X:G},{Y:G}}}";

	public readonly string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) => 
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)}}}";

	public readonly void Deconstruct(out float x, out float y) => (x, y) = (X, Y);

	/// <summary>Reinterprets the vector as a <see cref="Vector2"/> from <c>System.Numerics</c>.</summary>
	public readonly ref readonly Vector2 AsNumeric() => ref Unsafe.As<Vec2, Vector2>(ref Unsafe.AsRef(in this));

	/// <summary>Reinterprets the vector as a mutable <see cref="Vector2"/> from <c>System.Numerics</c>.</summary>
	public ref Vector2 AsNumericMutable() => ref Unsafe.As<Vec2, Vector2>(ref Unsafe.AsRef(in this));
	#endregion // Base


	#region Vector Ops
	/// <summary>Calculates the dot product of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly float Dot(Vec2 o) => X*o.X + Y*o.Y;

	/// <summary>Calculates the angle between the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Vec2 o) => Angle.Rad((float)Single.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Projects this vector onto the given axis.</summary>
	/// <param name="axis">The axis to project onto, must be normalized for accurate results.</param>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec2 Project(Vec2 axis)
	{
		var dot = Dot(axis);
		return new(dot * axis.X, dot * axis.Y);
	}

	/// <summary>Reflects this vector around the given normal.</summary>
	/// <param name="norm">The normal to reflect around, must be normalized for accurate results.</param>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec2 Reflect(Vec2 norm)
	{
		var f = 2 * Dot(norm);
		return new(X - norm.X * f, Y - norm.Y * f);
	}

	/// <summary>Clamps the vector components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec2 Clamp(Vec2 v, Vec2 min, Vec2 max) =>
		new(Single.Clamp(v.X, min.X, max.X), Single.Clamp(v.Y, min.Y, max.Y));

	/// <summary>Takes the component-wise minimum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec2 Min(Vec2 l, Vec2 r) => new(Single.Min(l.X, r.X), Single.Min(l.Y, r.Y));

	/// <summary>Takes the component-wise maximum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec2 Max(Vec2 l, Vec2 r) => new(Single.Max(l.X, r.X), Single.Max(l.Y, r.Y));

	/// <inheritdoc cref="MathUtils.ApproxEqual(float, float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec2 o, float eps) => X.ApproxEqual(o.X, eps) && Y.ApproxEqual(o.Y, eps);

	/// <inheritdoc cref="MathUtils.ApproxEqual(float, float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec2 o) => X.ApproxEqual(o.X) && Y.ApproxEqual(o.Y);

	/// <inheritdoc cref="MathUtils.ApproxZero(float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero(float eps) => X.ApproxZero(eps) && Y.ApproxZero(eps);

	/// <inheritdoc cref="MathUtils.ApproxZero(float, float)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero() => X.ApproxZero() && Y.ApproxZero();
	#endregion // Vector Ops


	#region Operators
	public static bool operator == (Vec2 l, Vec2 r) => l.Equals(r);
	public static bool operator != (Vec2 l, Vec2 r) => !l.Equals(r);

	public static Bool2 operator <= (Vec2 l, Vec2 r) => new(l.X <= r.X, l.Y <= r.Y);
	public static Bool2 operator <  (Vec2 l, Vec2 r) => new(l.X < r.X, l.Y < r.Y);
	public static Bool2 operator >= (Vec2 l, Vec2 r) => new(l.X >= r.X, l.Y >= r.Y);
	public static Bool2 operator >  (Vec2 l, Vec2 r) => new(l.X > r.X, l.Y > r.Y);

	public static Vec2 operator + (Vec2 l, Vec2 r) => new(l.X + r.X, l.Y + r.Y);
	public static Vec2 operator + (Vec2 l, float r) => new(l.X + r, l.Y + r);
	public static Vec2 operator - (Vec2 l, Vec2 r) => new(l.X - r.X, l.Y - r.Y);
	public static Vec2 operator - (Vec2 l, float r) => new(l.X - r, l.Y - r);
	public static Vec2 operator * (Vec2 l, Vec2 r) => new(l.X * r.X, l.Y * r.Y);
	public static Vec2 operator * (Vec2 l, float r) => new(l.X * r, l.Y * r);
	public static Vec2 operator / (Vec2 l, Vec2 r) => new(l.X / r.X, l.Y / r.Y);
	public static Vec2 operator / (Vec2 l, float r) => new(l.X / r, l.Y / r);
	public static Vec2 operator % (Vec2 l, Vec2 r) => new(l.X % r.X, l.Y % r.Y);
	public static Vec2 operator % (Vec2 l, float r) => new(l.X % r, l.Y % r);

	public static Vec2 operator - (Vec2 r) => new(-r.X, -r.Y);

	public static explicit operator Vec2 (Point2 o) => new((float)o.X, (float)o.Y);
	public static explicit operator Vec2 (Point2L o) => new((float)o.X, (float)o.Y);

	public static explicit operator Vec2 (Vec2D o) => new((float)o.X, (float)o.Y);
	#endregion // Operators
}


/// <summary>A two-component vector of <c>double</c>.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Vec2D :
	IEquatable<Vec2D>, IEqualityOperators<Vec2D, Vec2D, bool>,
	IAdditionOperators<Vec2D, Vec2D, Vec2D>, IAdditionOperators<Vec2D, double, Vec2D>,
	ISubtractionOperators<Vec2D, Vec2D, Vec2D>, ISubtractionOperators<Vec2D, double, Vec2D>,
	IMultiplyOperators<Vec2D, Vec2D, Vec2D>, IMultiplyOperators<Vec2D, double, Vec2D>,
	IDivisionOperators<Vec2D, Vec2D, Vec2D>, IDivisionOperators<Vec2D, double, Vec2D>,
	IModulusOperators<Vec2D, Vec2D, Vec2D>, IModulusOperators<Vec2D, double, Vec2D>,
	IUnaryNegationOperators<Vec2D, Vec2D>
{
	#region Constants
	/// <summary>Vector with all-zero components.</summary>
	public static readonly Vec2D Zero = new();
	/// <summary>Vector with all-one components.</summary>
	public static readonly Vec2D One = new(1);

	/// <summary>Vector with unit length along X-axis.</summary>
	public static readonly Vec2D UnitX = new(1, 0);
	/// <summary>Vector with unit length along Y-axis.</summary>
	public static readonly Vec2D UnitY = new(0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public double X;
	/// <summary>The Y-component.</summary>
	public double Y;

	/// <summary>The length of the vector.</summary>
	public readonly double Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Double.Sqrt(X*X + Y*Y);
	}

	/// <summary>The squared length of the vector.</summary>
	public readonly double LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y;
	}

	/// <summary>The normalized vector (length 1).</summary>
	public readonly Vec2D Normalized {
		[MethodImpl(MathUtils.MAX_OPT)]
		get { var iLen = 1 / Length; return new(X * iLen, Y * iLen); }
	}
	#endregion // Fields


	/// <summary>Construct a vector with equal components.</summary>
	public Vec2D(double v) => X = Y = v;

	/// <summary>Construct a vector from the components.</summary>
	public Vec2D(double x, double y) => (X, Y) = (x, y);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Vec2D o) => X.FastApprox(o.X) && Y.FastApprox(o.Y);

	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Vec2D v && Equals(v);

	public readonly override int GetHashCode() => HashCode.Combine(X, Y);

	public readonly override string ToString() => $"{{{X:G},{Y:G}}}";

	public readonly string ToString([StringSyntax(StringSyntaxAttribute.NumericFormat)] string? fmt) => 
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)}}}";

	public readonly void Deconstruct(out double x, out double y) => (x, y) = (X, Y);
	#endregion // Base


	#region Vector Ops
	/// <summary>Calculates the dot product of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly double Dot(Vec2D o) => X*o.X + Y*o.Y;

	/// <summary>Calculates the angle between the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Angle AngleWith(Vec2D o) => Angle.Rad((float)Double.Acos(Dot(o) / (Length * o.Length)));

	/// <summary>Projects this vector onto the given axis.</summary>
	/// <param name="axis">The axis to project onto, must be normalized for accurate results.</param>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec2D Project(Vec2D axis)
	{
		var dot = Dot(axis);
		return new(dot * axis.X, dot * axis.Y);
	}

	/// <summary>Reflects this vector around the given normal.</summary>
	/// <param name="norm">The normal to reflect around, must be normalized for accurate results.</param>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Vec2D Reflect(Vec2D norm)
	{
		var f = 2 * Dot(norm);
		return new(X - norm.X * f, Y - norm.Y * f);
	}

	/// <summary>Clamps the vector components between the given min and max components.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec2D Clamp(Vec2D v, Vec2D min, Vec2D max) =>
		new(Double.Clamp(v.X, min.X, max.X), Double.Clamp(v.Y, min.Y, max.Y));

	/// <summary>Takes the component-wise minimum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec2D Min(Vec2D l, Vec2D r) => new(Double.Min(l.X, r.X), Double.Min(l.Y, r.Y));

	/// <summary>Takes the component-wise maximum of the vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Vec2D Max(Vec2D l, Vec2D r) => new(Double.Max(l.X, r.X), Double.Max(l.Y, r.Y));

	/// <inheritdoc cref="MathUtils.ApproxEqual(double, double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec2D o, double eps) => X.ApproxEqual(o.X, eps) && Y.ApproxEqual(o.Y, eps);

	/// <inheritdoc cref="MathUtils.ApproxEqual(double, double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxEqual(Vec2D o) => X.ApproxEqual(o.X) && Y.ApproxEqual(o.Y);

	/// <inheritdoc cref="MathUtils.ApproxZero(double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero(double eps) => X.ApproxZero(eps) && Y.ApproxZero(eps);

	/// <inheritdoc cref="MathUtils.ApproxZero(double, double)"/>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool ApproxZero() => X.ApproxZero() && Y.ApproxZero();
	#endregion // Vector Ops


	#region Operators
	public static bool operator == (Vec2D l, Vec2D r) => l.Equals(r);
	public static bool operator != (Vec2D l, Vec2D r) => !l.Equals(r);

	public static Bool2 operator <= (Vec2D l, Vec2D r) => new(l.X <= r.X, l.Y <= r.Y);
	public static Bool2 operator <  (Vec2D l, Vec2D r) => new(l.X < r.X, l.Y < r.Y);
	public static Bool2 operator >= (Vec2D l, Vec2D r) => new(l.X >= r.X, l.Y >= r.Y);
	public static Bool2 operator >  (Vec2D l, Vec2D r) => new(l.X > r.X, l.Y > r.Y);

	public static Vec2D operator + (Vec2D l, Vec2D r) => new(l.X + r.X, l.Y + r.Y);
	public static Vec2D operator + (Vec2D l, double r) => new(l.X + r, l.Y + r);
	public static Vec2D operator - (Vec2D l, Vec2D r) => new(l.X - r.X, l.Y - r.Y);
	public static Vec2D operator - (Vec2D l, double r) => new(l.X - r, l.Y - r);
	public static Vec2D operator * (Vec2D l, Vec2D r) => new(l.X * r.X, l.Y * r.Y);
	public static Vec2D operator * (Vec2D l, double r) => new(l.X * r, l.Y * r);
	public static Vec2D operator / (Vec2D l, Vec2D r) => new(l.X / r.X, l.Y / r.Y);
	public static Vec2D operator / (Vec2D l, double r) => new(l.X / r, l.Y / r);
	public static Vec2D operator % (Vec2D l, Vec2D r) => new(l.X % r.X, l.Y % r.Y);
	public static Vec2D operator % (Vec2D l, double r) => new(l.X % r, l.Y % r);

	public static Vec2D operator - (Vec2D r) => new(-r.X, -r.Y);

	public static explicit operator Vec2D (Point2 o) => new((double)o.X, (double)o.Y);
	public static explicit operator Vec2D (Point2L o) => new((double)o.X, (double)o.Y);

	public static implicit operator Vec2D (Vec2 o) => new(o.X, o.Y);
	#endregion // Operators
}

