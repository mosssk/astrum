﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>Quaternion - a special vector type that efficiently encodes a rotation in 3D space.</summary>
/// <remarks>
/// The fields of this type have a special format to properly encode rotations, and should not be manipulated directly
/// without understanding this format.
/// </remarks>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Quat :
	IEquatable<Quat>, IEqualityOperators<Quat, Quat, bool>,
	IAdditionOperators<Quat, Quat, Quat>, ISubtractionOperators<Quat, Quat, Quat>,
	IMultiplyOperators<Quat, Quat, Quat>, IDivisionOperators<Quat, Quat, Quat>,
	IUnaryNegationOperators<Quat, Quat>
{
	/// <summary>The identity quaternion, which encodes no rotation.</summary>
	public static readonly Quat Identity = new(0, 0, 0, 1);
	
	
	#region Fields
	/// <summary>The x-component of the quaternion vector.</summary>
	public float X;
	/// <summary>The y-component of the quaternion vector.</summary>
	public float Y;
	/// <summary>The z-component of the quaternion vector.</summary>
	public float Z;
	/// <summary>The rotation component of the quaternion.</summary>
	public float W;

	/// <summary>The length of the quaternion.</summary>
	public readonly float Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Single.Sqrt(X*X + Y*Y + Z*Z + W*W);
	}

	/// <summary>The squared length of the quaternion.</summary>
	public readonly float LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z + W*W;
	}

	/// <summary>Normalizes the quaternion into a unit-length equivalent rotation.</summary>
	public readonly Quat Normalized {
		[MethodImpl(MathUtils.MAX_OPT)]
		get { var iLen = 1 / Length; return new(X * iLen, Y * iLen, Z * iLen, W * iLen); }
	}

	/// <summary>Represents the inverse rotation. Always produces a normalized quaternion.</summary>
	public readonly Quat Inverted {
		[MethodImpl(MathUtils.MAX_OPT)]
		get { var iLen = 1 / Length; return new(-X * iLen, -Y * iLen, -Z * iLen, W * iLen); }
	}
	#endregion // Fields

	
	/// <summary>Construct a quaternion from explicit components.</summary>
	/// <remarks>No validation is done - the components are assumed to encode a valid rotation.</remarks>
	public Quat(float x, float y, float z, float w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Quat o) =>
		X.FastApprox(o.X) && Y.FastApprox(o.Y) && Z.FastApprox(o.Z) && W.FastApprox(o.W);
	
	public readonly override bool Equals([NotNullWhen(true)] object? o) => o is Quat q && Equals(q);
	
	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"{{{{{X:G},{Y:G},{Z:G}}},{W:G}}}";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"{{{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)}}},{W.ToString(fmt)}}}";

	public readonly void Deconstruct(out double x, out double y, out double z, out double w) =>
		(x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Arithmetic
	/// <summary>Component-wise addition of quaternions.</summary>
	public readonly Quat Add(Quat q) => new(X + q.X, Y + q.Y, Z + q.Z, W + q.W);
	
	/// <summary>Component-wise subtraction of quaternions.</summary>
	public readonly Quat Subtract(Quat q) => new(X - q.X, Y - q.Y, Z - q.Z, W - q.W);

	/// <summary>Produces a new quaternion which represents applying two rotations sequentially.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly Quat Concatenate(Quat q) => new(
		(X * q.W) + (W * q.X) + (Y * q.Z) - (Z * q.Y),
		(Y * q.W) + (W * q.Y) + (Z * q.X) - (X * q.Z),
		(Z * q.W) + (W * q.Z) + (X * q.Y) - (Y * q.X),
		(W * q.W) - (X * q.X) - (Y * q.Y) - (Z * q.Z)
	);
	
	/// <summary>The dot product of the quaternions.</summary>
	public readonly float Dot(Quat q) => X * q.X + Y * q.Y + Z * q.Z + W * q.W;
	#endregion // Arithmetic


	#region Create
	/// <summary>Describes a rotation around a given axis.</summary>
	/// <param name="axis">The axis to rotate around.</param>
	/// <param name="angle">The angle to rotate.</param>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public static Quat RotationAxis(Vec3 axis, Angle angle)
	{
		axis = axis.Normalized;
		var (s, c) = (angle / 2).SinCos;
		return new(axis.X * s, axis.Y * s, axis.Z * s, c);
	}

	/// <summary>Describes a combined yaw, pitch, and roll rotation.</summary>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public static Quat YawPitchRoll(Angle yaw, Angle pitch, Angle roll)
	{
		var (sy, cy) = (yaw / 2).SinCos;
		var (sp, cp) = (pitch / 2).SinCos;
		var (sr, cr) = (roll / 2).SinCos;
		return new(
			(sy * cp * sr) + (cy * sp * cr),
			(sy * cp * cr) - (cy * sp * sr),
			(cy * cp * sr) - (sy * sp * cr),
			(cy * cp * cr) + (sy * sp * sr)
		);
	}

	/// <summary>Creates a quaternion equivalent to an existing rotation matrix.</summary>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public static Quat FromRotationMatrix(ref readonly Mat4 m)
	{
		var trace = m.M00 + m.M11 + m.M22;
		if (trace > 0) {
			var s = Single.Sqrt(trace + 1);
			var v = 1 / (2 * s);
			return new(v * (m.M12 - m.M21), v * (m.M20 - m.M02), v * (m.M01 - m.M10), s / 2);
		}
		if (m.M00 >= m.M11 && m.M00 >= m.M22) {
			var s = Single.Sqrt(1 + m.M00 - m.M11 - m.M22);
			var v = 1 / (2 * s);
			return new(s / 2, v * (m.M01 + m.M10), v * (m.M02 + m.M20), v * (m.M12 - m.M21));
		}
		if (m.M11 > m.M22) {
			var s = Single.Sqrt(1 + m.M11 - m.M00 - m.M22);
			var v = 1 / (2 * s);
			return new(v * (m.M10 + m.M01), s / 2, v * (m.M21 + m.M12), v * (m.M20 - m.M02));
		}
		else {
			var s = Single.Sqrt(1 + m.M22 - m.M00 - m.M11);
			var v = 1 / (2 * s);
			return new(v * (m.M20 + m.M02), v * (m.M21 + m.M12), s / 2, v * (m.M01 - m.M10));
		}
	}
	
	/// <summary>Converts the quaternion into the equivalent rotation matrix.</summary>
	public readonly Mat4 ToMatrix() { ToMatrix(out var o); return o; }

	/// <inheritdoc cref="ToMatrix()"/>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public readonly void ToMatrix(out Mat4 o)
	{
		float
			xx = X * X, yy = Y * Y, zz = Z * Z,
			wx = W * X, wy = W * Y, wz = W * Z,  
			xy = X * Y, xz = X * Z, yz = Y * Z;
		Unsafe.SkipInit(out o);
		o.M00 = 1 - 2 * (yy + zz); o.M01 =     2 * (xy + wz); o.M02 =     2 * (xz - wy); o.M03 = 0;
		o.M10 =     2 * (xy - wz); o.M11 = 1 - 2 * (zz + xx); o.M12 =     2 * (yz + wx); o.M13 = 0;
		o.M20 =     2 * (xz + wy); o.M21 =     2 * (yz - wx); o.M22 = 1 - 2 * (xx + yy); o.M23 = 0;
		o.Row3 = Vec4.UnitW;
	}
	#endregion // Create


	#region Operators
	public static bool operator == (Quat l, Quat r) => l.Equals(r);
	public static bool operator != (Quat l, Quat r) => !l.Equals(r);
	
	public static Quat operator + (Quat l, Quat r) => l.Add(r);
	public static Quat operator - (Quat l, Quat r) => l.Subtract(r);
	/// <summary>Equivalent to <see cref="Concatenate"/>.</summary>
	public static Quat operator * (Quat l, Quat r) => l.Concatenate(r);
	/// <summary>Equivalent to <see cref="Concatenate"/> with the <see cref="Inverted"/> <c>r</c>.</summary>
	public static Quat operator / (Quat l, Quat r) => l.Concatenate(r.Inverted);
	
	/// <summary>Equivalent to <see cref="Inverted"/>, but without explicit normalization.</summary>
	public static Quat operator - (Quat r) => new(-r.X, -r.Y, -r.Z, r.W);
	#endregion // Operators
}
