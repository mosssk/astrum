﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics;

using NumVec = System.Runtime.Intrinsics.Vector128<float>;
// ReSharper disable PropertyCanBeMadeInitOnly.Global

namespace Astrum.Maths;


/// <summary>
/// A 4x4 matrix of single-precision floats. Uses row-major order for fields:<br/>
/// <c>M00 M01 M02 M03</c><br/>
///	<c>M10 M11 M12 M13</c><br/>
///	<c>M20 M21 M22 M23</c><br/>
///	<c>M30 M31 M32 M33</c><br/>
/// The contiguous field order is: <c>M00 M01 M02 M03 M10 ...</c> The translation components are <c>M30 M31 M32</c>.
/// </summary>
/// <remarks>This struct is large, so care should be taken to avoid copies when possible.</remarks>
[StructLayout(LayoutKind.Explicit, Size = 64)]
public struct Mat4 :
 	IEquatable<Mat4>, IEqualityOperators<Mat4, Mat4, bool>,
 	IAdditionOperators<Mat4, Mat4, Mat4>, IAdditionOperators<Mat4, float, Mat4>, 
 	ISubtractionOperators<Mat4, Mat4, Mat4>, ISubtractionOperators<Mat4, float, Mat4>,
 	IMultiplyOperators<Mat4, Mat4, Mat4>, IMultiplyOperators<Mat4, float, Mat4>,
 	IMultiplyOperators<Mat4, Vec4, Vec4>, IMultiplyOperators<Mat4, Vec3, Vec3>,
 	IMultiplyOperators<Mat4, Vec4D, Vec4D>, IMultiplyOperators<Mat4, Vec3D, Vec3D>,
 	IDivisionOperators<Mat4, float, Mat4>,
 	IUnaryNegationOperators<Mat4, Mat4>
{
	/// <summary>The identity matrix.</summary>
	public static readonly Mat4 Identity = new(1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  0, 0, 0, 1);
	/// <summary>The zero matrix.</summary>
	public static readonly Mat4 Zero = new();
	
	
	#region Fields
	/// <summary>Row 0, column 0. Index 0.</summary>
	[FieldOffset(0)]  public float M00;
	/// <summary>Row 0, column 1. Index 1.</summary>
	[FieldOffset(4)]  public float M01;
	/// <summary>Row 0, column 2. Index 2.</summary>
	[FieldOffset(8)]  public float M02;
	/// <summary>Row 0, column 3. Index 3.</summary>
	[FieldOffset(12)] public float M03;
	/// <summary>Row 1, column 0. Index 4.</summary>
	[FieldOffset(16)] public float M10;
	/// <summary>Row 1, column 1. Index 5.</summary>
	[FieldOffset(20)] public float M11;
	/// <summary>Row 1, column 2. Index 6.</summary>
	[FieldOffset(24)] public float M12;
	/// <summary>Row 1, column 3. Index 7.</summary>
	[FieldOffset(28)] public float M13;
	/// <summary>Row 2, column 0. Index 8.</summary>
	[FieldOffset(32)] public float M20;
	/// <summary>Row 2, column 1. Index 9.</summary>
	[FieldOffset(36)] public float M21;
	/// <summary>Row 2, column 2. Index 10.</summary>
	[FieldOffset(40)] public float M22;
	/// <summary>Row 2, column 3. Index 11.</summary>
	[FieldOffset(44)] public float M23;
	/// <summary>Row 3, column 0. Index 12.</summary>
	[FieldOffset(48)] public float M30;
	/// <summary>Row 3, column 1. Index 13.</summary>
	[FieldOffset(52)] public float M31;
	/// <summary>Row 3, column 2. Index 14.</summary>
	[FieldOffset(56)] public float M32;
	/// <summary>Row 3, column 3. Index 15.</summary>
	[FieldOffset(60)] public float M33;
	
	// SIMD vector row fields
	[FieldOffset(0)]  private NumVec _simd0;
	[FieldOffset(16)] private NumVec _simd1;
	[FieldOffset(32)] private NumVec _simd2;
	[FieldOffset(48)] private NumVec _simd3;
	

	/// <summary>Row 0 (<see cref="M00"/>, <see cref="M01"/>, <see cref="M02"/>, <see cref="M03"/>).</summary>
	public Vec4 Row0 {
		readonly get => Unsafe.As<NumVec, Vec4>(ref Unsafe.AsRef(in _simd0));
		set => _simd0 = Unsafe.As<Vec4, NumVec>(ref value);
	}
	/// <summary>Row 1 (<see cref="M10"/>, <see cref="M11"/>, <see cref="M12"/>, <see cref="M13"/>).</summary>
	public Vec4 Row1 {
		readonly get => Unsafe.As<NumVec, Vec4>(ref Unsafe.AsRef(in _simd1));
		set => _simd1 = Unsafe.As<Vec4, NumVec>(ref value);
	}
	/// <summary>Row 2 (<see cref="M20"/>, <see cref="M21"/>, <see cref="M22"/>, <see cref="M23"/>).</summary>
	public Vec4 Row2 {
		readonly get => Unsafe.As<NumVec, Vec4>(ref Unsafe.AsRef(in _simd2));
		set => _simd2 = Unsafe.As<Vec4, NumVec>(ref value);
	}
	/// <summary>Row 3 (<see cref="M30"/>, <see cref="M31"/>, <see cref="M32"/>, <see cref="M33"/>).</summary>
	public Vec4 Row3 {
		readonly get => Unsafe.As<NumVec, Vec4>(ref Unsafe.AsRef(in _simd3));
		set => _simd3 = Unsafe.As<Vec4, NumVec>(ref value);
	}

	/// <summary>Column 0 (<see cref="M00"/>, <see cref="M10"/>, <see cref="M20"/>, <see cref="M30"/>).</summary>
	public Vec4 Col0 {
		readonly get => new(M00, M10, M20, M30);
		set => (M00, M10, M20, M30) = value;
	}
	/// <summary>Column 1 (<see cref="M01"/>, <see cref="M11"/>, <see cref="M21"/>, <see cref="M31"/>).</summary>
	public Vec4 Col1 {
		readonly get => new(M01, M11, M21, M31);
		set => (M01, M11, M21, M31) = value;
	}
	/// <summary>Column 2 (<see cref="M02"/>, <see cref="M12"/>, <see cref="M22"/>, <see cref="M32"/>).</summary>
	public Vec4 Col2 {
		readonly get => new(M02, M12, M22, M32);
		set => (M02, M12, M22, M32) = value;
	}
	/// <summary>Column 3 (<see cref="M03"/>, <see cref="M13"/>, <see cref="M23"/>, <see cref="M33"/>).</summary>
	public Vec4 Col3 {
		readonly get => new(M03, M13, M23, M33);
		set => (M03, M13, M23, M33) = value;
	}
	
	/// <summary>Gets or sets the field at the given index.</summary>
	public float this[int i] {
		readonly get => 
			Unsafe.Add(ref Unsafe.AsRef(in M00), ((uint)i < 16) ? i : throw new ArgumentOutOfRangeException(nameof(i)));
		set => Unsafe.Add(ref M00, ((uint)i < 16) ? i : throw new ArgumentOutOfRangeException(nameof(i))) = value;
	}

	/// <summary>Gets or sets the field at the given row and column.</summary>
	/// <param name="r">The row index to access.</param>
	/// <param name="c">The column index to access.</param>
	public float this[int r, int c] {
		readonly get => Unsafe.Add(ref Unsafe.AsRef(in M00),
			((uint)r) > 3 ? throw new ArgumentOutOfRangeException(nameof(r)) :
			((uint)c) > 3 ? throw new ArgumentOutOfRangeException(nameof(c)) :
			r * 4 + c);
		set => Unsafe.Add(ref M00,
			((uint)r) > 3 ? throw new ArgumentOutOfRangeException(nameof(r)) :
			((uint)c) > 3 ? throw new ArgumentOutOfRangeException(nameof(c)) :
			r * 4 + c) = value;
	}

	/// <summary>The matrix trace (sum of diagonals).</summary>
	public readonly float Trace => M00 + M11 + M22 + M33;
	
	/// <summary>For world and view matrices, the right-pointing (+x) basis vector.</summary>
	/// <remarks>The vector is not explicitly normalized.</remarks>
	public readonly Vec3 Right => new(M00, M01, M02);
	/// <summary>For world and view matrices, the left-pointing (-x) basis vector.</summary>
	/// <remarks>The vector is not explicitly normalized.</remarks>
	public readonly Vec3 Left => new(-M00, -M01, -M02);
	/// <summary>For world and view matrices, the up-pointing (+y) basis vector.</summary>
	/// <remarks>The vector is not explicitly normalized.</remarks>
	public readonly Vec3 Up => new(M10, M11, M12);
	/// <summary>For world and view matrices, the down-pointing (-y) basis vector.</summary>
	/// <remarks>The vector is not explicitly normalized.</remarks>
	public readonly Vec3 Down => new(-M10, -M11, -M12);
	/// <summary>For world and view matrices, the forward-pointing (-z) basis vector.</summary>
	/// <remarks>The vector is not explicitly normalized.</remarks>
	public readonly Vec3 Forward => new(-M20, -M21, -M22);
	/// <summary>For world and view matrices, the backward-pointing (+z) basis vector.</summary>
	/// <remarks>The vector is not explicitly normalized.</remarks>
	public readonly Vec3 Backward => new(M20, M21, M22);

	/// <summary>The world and view matrices, the translation component.</summary>
	public Vec3 Translation {
		readonly get => new(M30, M31, M32);
		set => (M30, M31, M32) = value;
	}
	#endregion // Fields


	/// <summary>Initializes all matrix fields to zero.</summary>
	public Mat4()
	{
		Unsafe.SkipInit(out this);
		var zero = NumVec.Zero;
		_simd0 = zero;
		_simd1 = zero;
		_simd2 = zero;
		_simd3 = zero;
	}

	/// <summary>Initializes the matrix using explicit rows.</summary>
	public Mat4(Vec4 r0, Vec4 r1, Vec4 r2, Vec4 r3)
	{
		Unsafe.SkipInit(out this);
		Row0 = r0;
		Row1 = r1;
		Row2 = r2;
		Row3 = r3;
	}
	
	/// <summary>Initializes the matrix using explicit row-major ordered fields.</summary>
	public Mat4(
		float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23, float m30, float m31, float m32, float m33)
	{
		Unsafe.SkipInit(out this);
		M00 = m00; M01 = m01; M02 = m02; M03 = m03;
		M10 = m10; M11 = m11; M12 = m12; M13 = m13;
		M20 = m20; M21 = m21; M22 = m22; M23 = m23;
		M30 = m30; M31 = m31; M32 = m32; M33 = m33;
	}


	#region Base
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public readonly bool Equals(ref readonly Mat4 o) => 
		M00.FastApprox(o.M00) && M01.FastApprox(o.M01) && M02.FastApprox(o.M02) && M03.FastApprox(o.M03) &&
		M10.FastApprox(o.M10) && M11.FastApprox(o.M11) && M12.FastApprox(o.M12) && M13.FastApprox(o.M13) &&
		M20.FastApprox(o.M20) && M21.FastApprox(o.M21) && M22.FastApprox(o.M22) && M23.FastApprox(o.M23) &&
		M30.FastApprox(o.M30) && M31.FastApprox(o.M31) && M32.FastApprox(o.M32) && M33.FastApprox(o.M33);

	public readonly bool Equals(Mat4 o) => Equals(in o);

	public readonly override bool Equals(object? obj) => obj is Mat4 m && Equals(m);

	public readonly override int GetHashCode() => HashCode.Combine(Row0, Row1, Row2, Row3);

	public readonly override string ToString() => $"{{{Row0}{Row1}{Row2}{Row3}}}";
	
	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"{{{Row0.ToString(fmt)}{Row1.ToString(fmt)}{Row2.ToString(fmt)}{Row3.ToString(fmt)}}}";

	public readonly void Deconstruct(out Vec4 r0, out Vec4 r1, out Vec4 r2, out Vec4 r3) =>
		(r0, r1, r2, r3) = (Row0, Row1, Row2, Row3);

	/// <summary>Reinterprets the matrix as a 4x4 matrix from <c>System.Numerics</c>.</summary>
	public readonly ref readonly Matrix4x4 AsNumeric() => ref Unsafe.As<Mat4, Matrix4x4>(ref Unsafe.AsRef(in this));

	/// <summary>Reinterprets the matrix as a mutable 4x4 matrix from <c>System.Numerics</c>.</summary>
	public ref Matrix4x4 AsNumericMutable() => ref Unsafe.As<Mat4, Matrix4x4>(ref Unsafe.AsRef(in this));
	#endregion // Base


	#region Arithmetic
	/// <summary>Component-wise addition of the matrices.</summary>
	public readonly Mat4 Add(ref readonly Mat4 m) { Add(in m, out var o); return o; }

	/// <summary>Component-wise addition of the matrices.</summary>
	public readonly void Add(ref readonly Mat4 m, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		o._simd0 = _simd0 + m._simd0;
		o._simd1 = _simd1 + m._simd1;
		o._simd2 = _simd2 + m._simd2;
		o._simd3 = _simd3 + m._simd3;
	}
	
	/// <summary>Constant addition to the matrix components.</summary>
	public readonly Mat4 Add(float f) { Add(f, out var o); return o; }

	/// <summary>Constant addition to the matrix components.</summary>
	public readonly void Add(float f, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		var vec = Vector128.Create(f, f, f, f);
		o._simd0 = _simd0 + vec;
		o._simd1 = _simd1 + vec;
		o._simd2 = _simd2 + vec;
		o._simd3 = _simd3 + vec;
	}
	
	/// <summary>Component-wise subtraction of the matrices.</summary>
	public readonly Mat4 Subtract(ref readonly Mat4 m) { Subtract(in m, out var o); return o; }

	/// <summary>Component-wise subtraction of the matrices.</summary>
	public readonly void Subtract(ref readonly Mat4 m, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		o._simd0 = _simd0 - m._simd0;
		o._simd1 = _simd1 - m._simd1;
		o._simd2 = _simd2 - m._simd2;
		o._simd3 = _simd3 - m._simd3;
	}
	
	/// <summary>Constant subtraction from the matrix components.</summary>
	public readonly Mat4 Subtract(float f) { Subtract(f, out var o); return o; }

	/// <summary>Constant subtraction from the matrix components.</summary>
	public readonly void Subtract(float f, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		var vec = Vector128.Create(f, f, f, f);
		o._simd0 = _simd0 - vec;
		o._simd1 = _simd1 - vec;
		o._simd2 = _simd2 - vec;
		o._simd3 = _simd3 - vec;
	}

	/// <summary>Component-wise multiplication of the matrices.</summary>
	/// <remarks><em>Note: this is not the standard matrix/matrix multiplication.</em></remarks>
	public readonly Mat4 MatCompMul(ref readonly Mat4 m) { MatCompMul(in m, out var o); return o; }

	/// <summary>Component-wise multiplication of the matrices.</summary>
	/// <remarks><em>Note: this is not the standard matrix/matrix multiplication.</em></remarks>
	public readonly void MatCompMul(ref readonly Mat4 m, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		o._simd0 = _simd0 * m._simd0;
		o._simd1 = _simd1 * m._simd1;
		o._simd2 = _simd2 * m._simd2;
		o._simd3 = _simd3 * m._simd3;
	}
	
	/// <summary>Component-wise division of the matrices.</summary>
	public readonly Mat4 MatCompDiv(ref readonly Mat4 m) { MatCompDiv(in m, out var o); return o; }

	/// <summary>Component-wise division of the matrices.</summary>
	public readonly void MatCompDiv(ref readonly Mat4 m, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		o._simd0 = _simd0 / m._simd0;
		o._simd1 = _simd1 / m._simd1;
		o._simd2 = _simd2 / m._simd2;
		o._simd3 = _simd3 / m._simd3;
	}
	
	/// <summary>Standard matrix/matrix multiplication.</summary>
	public readonly Mat4 Multiply(ref readonly Mat4 m) { Multiply(in m, out var o); return o; }

	/// <summary>Standard matrix/matrix multiplication.</summary>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public readonly void Multiply(ref readonly Mat4 m, out Mat4 o)
	{
		// Note: this pattern is recognized by Roslyn and heavily optimized (see the benchmarks in the test project)
		Unsafe.SkipInit(out o);
		o.M00 = M00 * m.M00 + M01 * m.M10 + M02 * m.M20 + M03 * m.M30;
		o.M01 = M00 * m.M01 + M01 * m.M11 + M02 * m.M21 + M03 * m.M31;
		o.M02 = M00 * m.M02 + M01 * m.M12 + M02 * m.M22 + M03 * m.M32;
		o.M03 = M00 * m.M03 + M01 * m.M13 + M02 * m.M23 + M03 * m.M33;
		o.M10 = M10 * m.M00 + M11 * m.M10 + M12 * m.M20 + M13 * m.M30;
		o.M11 = M10 * m.M01 + M11 * m.M11 + M12 * m.M21 + M13 * m.M31;
		o.M12 = M10 * m.M02 + M11 * m.M12 + M12 * m.M22 + M13 * m.M32;
		o.M13 = M10 * m.M03 + M11 * m.M13 + M12 * m.M23 + M13 * m.M33;
		o.M20 = M20 * m.M00 + M21 * m.M10 + M22 * m.M20 + M23 * m.M30;
		o.M21 = M20 * m.M01 + M21 * m.M11 + M22 * m.M21 + M23 * m.M31;
		o.M22 = M20 * m.M02 + M21 * m.M12 + M22 * m.M22 + M23 * m.M32;
		o.M23 = M20 * m.M03 + M21 * m.M13 + M22 * m.M23 + M23 * m.M33;
		o.M30 = M30 * m.M00 + M31 * m.M10 + M32 * m.M20 + M33 * m.M30;
		o.M31 = M30 * m.M01 + M31 * m.M11 + M32 * m.M21 + M33 * m.M31;
		o.M32 = M30 * m.M02 + M31 * m.M12 + M32 * m.M22 + M33 * m.M32;
		o.M33 = M30 * m.M03 + M31 * m.M13 + M32 * m.M23 + M33 * m.M33;
	}

	/// <summary>Constant multiplication of the matrix components.</summary>
	public readonly Mat4 Multiply(float f) { Multiply(f, out var o); return o; }

	/// <summary>Constant multiplication of the matrix components.</summary>
	public readonly void Multiply(float f, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		var vec = Vector128.Create(f, f, f, f);
		o._simd0 = _simd0 * vec;
		o._simd1 = _simd1 * vec;
		o._simd2 = _simd2 * vec;
		o._simd3 = _simd3 * vec;
	}
	
	/// <summary>Transforms the vector using standard matrix/vector multiplication.</summary>
	public readonly Vec4 Transform(Vec4 v) { Transform(v, out var o); return o; }

	/// <summary>Transforms the vector using standard matrix/vector multiplication.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly void Transform(Vec4 v, out Vec4 o) => o = new(
		M00 * v.X + M10 * v.Y + M20 * v.Z + M30 * v.W,
		M01 * v.X + M11 * v.Y + M21 * v.Z + M31 * v.W,
		M02 * v.X + M12 * v.Y + M22 * v.Z + M32 * v.W,
		M03 * v.X + M13 * v.Y + M23 * v.Z + M33 * v.W
	);
	
	/// <summary>Transforms the vector using standard matrix/vector multiplication.</summary>
	public readonly Vec4D Transform(Vec4D v) { Transform(v, out var o); return o; }

	/// <summary>Transforms the vector using standard matrix/vector multiplication.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly void Transform(Vec4D v, out Vec4D o) => o = new(
		M00 * v.X + M10 * v.Y + M20 * v.Z + M30 * v.W,
		M01 * v.X + M11 * v.Y + M21 * v.Z + M31 * v.W,
		M02 * v.X + M12 * v.Y + M22 * v.Z + M32 * v.W,
		M03 * v.X + M13 * v.Y + M23 * v.Z + M33 * v.W
	);
	
	/// <summary>Transforms the vector using standard matrix/vector multiplication.</summary>
	public readonly Vec3 Transform(Vec3 v) { Transform(v, out var o); return o; }

	/// <summary>Transforms the vector using standard matrix/vector multiplication.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly void Transform(Vec3 v, out Vec3 o) => o = new(
		M00 * v.X + M10 * v.Y + M20 * v.Z + M30,
		M01 * v.X + M11 * v.Y + M21 * v.Z + M31,
		M02 * v.X + M12 * v.Y + M22 * v.Z + M32
	);
	
	/// <summary>Transforms the vector using standard matrix/vector multiplication.</summary>
	public readonly Vec3D Transform(Vec3D v) { Transform(v, out var o); return o; }

	/// <summary>Transforms the vector using standard matrix/vector multiplication.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly void Transform(Vec3D v, out Vec3D o) => o = new(
		M00 * v.X + M10 * v.Y + M20 * v.Z + M30,
		M01 * v.X + M11 * v.Y + M21 * v.Z + M31,
		M02 * v.X + M12 * v.Y + M22 * v.Z + M32
	);
	
	/// <summary>Transforms the normal vector, ignoring the matrix translation component.</summary>
	public readonly Vec3 TransformNormal(Vec3 v) { TransformNormal(v, out var o); return o; }

	/// <summary>Transforms the normal vector, ignoring the matrix translation component.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly void TransformNormal(Vec3 v, out Vec3 o) => o = new(
		M00 * v.X + M10 * v.Y + M20 * v.Z,
		M01 * v.X + M11 * v.Y + M21 * v.Z,
		M02 * v.X + M12 * v.Y + M22 * v.Z
	);
	
	/// <summary>Constant division of the matrix components.</summary>
	public readonly Mat4 Divide(float f) { Divide(f, out var o); return o; }

	/// <summary>Constant division of the matrix components.</summary>
	public readonly void Divide(float f, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		var vec = Vector128.Create(f, f, f, f);
		o._simd0 = _simd0 / vec;
		o._simd1 = _simd1 / vec;
		o._simd2 = _simd2 / vec;
		o._simd3 = _simd3 / vec;
	}
	#endregion // Arithmetic


	#region Matrix Ops
	/// <summary>Gets the transposed value of the matrix.</summary>
	public readonly Mat4 Transpose() { Transpose(out var o); return o; }
	
	/// <summary>Gets the transposed value of the matrix.</summary>
	public readonly void Transpose(out Mat4 o) 
	{
		Unsafe.SkipInit(out o);
		o._simd0 = Vector128.Create(M00, M10, M20, M30);
		o._simd1 = Vector128.Create(M01, M11, M21, M31);
		o._simd2 = Vector128.Create(M02, M12, M22, M32);
		o._simd3 = Vector128.Create(M03, M13, M23, M33);
	}
	
	/// <summary>Gets the value of the matrix with all components negated.</summary>
	public readonly Mat4 Negate() { Negate(out var o); return o; }
	
	/// <summary>Gets the value of the matrix with all component negated.</summary>
	public readonly void Negate(out Mat4 o) 
	{
		Unsafe.SkipInit(out o);
		o._simd0 = -Vector128.Create(M00, M01, M02, M03);
		o._simd1 = -Vector128.Create(M10, M11, M12, M13);
		o._simd2 = -Vector128.Create(M20, M21, M22, M23);
		o._simd3 = -Vector128.Create(M30, M31, M32, M33);
	}

	/// <summary>Calculates the inverse of the matrix. Returns identity if the matrix cannot be inverted.</summary>
	public readonly Mat4 Invert() { Invert(out var o); return o; }
	
	/// <summary>Calculates the inverse of the matrix. Returns identity if the matrix cannot be inverted.</summary>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public readonly void Invert(out Mat4 o)
	{
		const double DET_TOLERANCE = 1e-6;

		// Solution based on Laplace Expansion, found at https://stackoverflow.com/a/9614511
		// Use doubles to minimize rounding errors, which can have a large effect in this calculation
		// The Roslyn compiler also seems to recognize this pattern and makes it extremely optimized (see benchmarks)
		double
			s0 = (double)M00 * M11 - (double)M10 * M01, s1 = (double)M00 * M12 - (double)M10 * M02,
			s2 = (double)M00 * M13 - (double)M10 * M03, s3 = (double)M01 * M12 - (double)M11 * M02,
			s4 = (double)M01 * M13 - (double)M11 * M03, s5 = (double)M02 * M13 - (double)M12 * M03,
			c5 = (double)M22 * M33 - (double)M32 * M23, c4 = (double)M21 * M33 - (double)M31 * M23,
			c3 = (double)M21 * M32 - (double)M31 * M22, c2 = (double)M20 * M33 - (double)M30 * M23,
			c1 = (double)M20 * M32 - (double)M30 * M22, c0 = (double)M20 * M31 - (double)M30 * M21;

		var det = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;
		if (det.ApproxZero(DET_TOLERANCE)) {
			o = Identity;
			return;
		}
		var iDet = 1 / det;

		Unsafe.SkipInit(out o);
		o.M00 = (float)(( M11 * c5 - M12 * c4 + M13 * c3) * iDet);
		o.M01 = (float)((-M01 * c5 + M02 * c4 - M03 * c3) * iDet);
		o.M02 = (float)(( M31 * s5 - M32 * s4 + M33 * s3) * iDet);
		o.M03 = (float)((-M21 * s5 + M22 * s4 - M23 * s3) * iDet);
		o.M10 = (float)((-M10 * c5 + M12 * c2 - M13 * c1) * iDet);
		o.M11 = (float)(( M00 * c5 - M02 * c2 + M03 * c1) * iDet);
		o.M12 = (float)((-M30 * s5 + M32 * s2 - M33 * s1) * iDet);
		o.M13 = (float)(( M20 * s5 - M22 * s2 + M23 * s1) * iDet);
		o.M20 = (float)(( M10 * c4 - M11 * c2 + M13 * c0) * iDet);
		o.M21 = (float)((-M00 * c4 + M01 * c2 - M03 * c0) * iDet);
		o.M22 = (float)(( M30 * s4 - M31 * s2 + M33 * s0) * iDet);
		o.M23 = (float)((-M20 * s4 + M21 * s2 - M23 * s0) * iDet);
		o.M30 = (float)((-M10 * c3 + M11 * c1 - M12 * c0) * iDet);
		o.M31 = (float)(( M00 * c3 - M01 * c1 + M02 * c0) * iDet);
		o.M32 = (float)((-M30 * s3 + M31 * s1 - M32 * s0) * iDet);
		o.M33 = (float)(( M20 * s3 - M21 * s1 + M22 * s0) * iDet);
	}

	/// <summary>Gets the determinant value of the matrix.</summary>
	public readonly float Determinant()
	{
		float
			s0 = M00 * M11 - M10 * M01, s1 = M00 * M12 - M10 * M02,
			s2 = M00 * M13 - M10 * M03, s3 = M01 * M12 - M11 * M02,
			s4 = M01 * M13 - M11 * M03, s5 = M02 * M13 - M12 * M03,
			c5 = M22 * M33 - M32 * M23, c4 = M21 * M33 - M31 * M23,
			c3 = M21 * M32 - M31 * M22, c2 = M20 * M33 - M30 * M23,
			c1 = M20 * M32 - M30 * M22, c0 = M20 * M31 - M30 * M21;
		return s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;
	}

	/// <inheritdoc cref="MathUtils.ApproxEqual(float,float,float)"/>
	public readonly bool ApproxEqual(ref readonly Mat4 o, float eps = MathUtils.TOL_32) => 
		Row0.ApproxEqual(o.Row0, eps) && Row1.ApproxEqual(o.Row1, eps) &&
		Row2.ApproxEqual(o.Row2, eps) && Row3.ApproxEqual(o.Row3, eps);

	/// <summary>Checks if the matrix is approximately equal to the zero matrix within the given tolerance.</summary>
	public readonly bool ApproxZero(float eps = MathUtils.TOL_32) =>
		Row0.ApproxZero(eps) && Row1.ApproxZero(eps) && Row2.ApproxZero(eps) && Row3.ApproxZero(eps);
	
	/// <summary>Checks if the matrix is approximately equal to the identity matrix within the given tolerance.</summary>
	public readonly bool ApproxIdentity(float eps = MathUtils.TOL_32) =>
		M00.FastApprox(1, eps) && M01.FastApprox(0, eps) && M02.FastApprox(0, eps) && M03.FastApprox(0, eps) &&
		M10.FastApprox(0, eps) && M11.FastApprox(1, eps) && M12.FastApprox(0, eps) && M13.FastApprox(0, eps) &&
		M20.FastApprox(0, eps) && M21.FastApprox(0, eps) && M22.FastApprox(1, eps) && M23.FastApprox(0, eps) &&
		M30.FastApprox(0, eps) && M31.FastApprox(0, eps) && M32.FastApprox(0, eps) && M33.FastApprox(1, eps);
	#endregion // Matrix Ops


	#region World Matrices
	/// <summary>Creates a matrix describing a right-hand rotation around the X-axis.</summary>
	public static Mat4 RotationX(Angle angle) { RotationX(angle, out var o); return o; }

	/// <summary>Creates a matrix describing a right-hand rotation around the X-axis.</summary>
	public static void RotationX(Angle angle, out Mat4 o)
	{
		var (s, c) = angle.SinCos;
		Unsafe.SkipInit(out o);
		o.Row0 = Vec4.UnitX;
		o.M10 = 0; o.M11 =  c; o.M12 = s; o.M13 = 0;
		o.M20 = 0; o.M21 = -s; o.M22 = c; o.M23 = 0;
		o.Row3 = Vec4.UnitW;
	}
	
	/// <summary>Creates a matrix describing a right-hand rotation around the Y-axis.</summary>
	public static Mat4 RotationY(Angle angle) { RotationY(angle, out var o); return o; }

	/// <summary>Creates a matrix describing a right-hand rotation around the Y-axis.</summary>
	public static void RotationY(Angle angle, out Mat4 o)
	{
		var (s, c) = angle.SinCos;
		Unsafe.SkipInit(out o);
		o.M00 = c; o.M01 = 0; o.M02 = -s; o.M03 = 0;
		o.Row1 = Vec4.UnitY;
		o.M20 = s; o.M21 = 0; o.M22 =  c; o.M23 = 0;
		o.Row3 = Vec4.UnitW;
	}
	
	/// <summary>Creates a matrix describing a right-hand rotation around the Z-axis.</summary>
	public static Mat4 RotationZ(Angle angle) { RotationZ(angle, out var o); return o; }

	/// <summary>Creates a matrix describing a right-hand rotation around the Z-axis.</summary>
	public static void RotationZ(Angle angle, out Mat4 o)
	{
		var (s, c) = angle.SinCos;
		Unsafe.SkipInit(out o);
		o.M00 =  c; o.M01 = s; o.M02 = 0; o.M03 = 0;
		o.M10 = -s; o.M11 = c; o.M12 = 0; o.M13 = 0;
		o.Row2 = Vec4.UnitZ;
		o.Row3 = Vec4.UnitW;
	}

	/// <summary>Creates a matrix describing a right-hand rotation around the given axis.</summary>
	public static Mat4 RotationAxis(Vec3 axis, Angle angle) { RotationAxis(axis, angle, out var o); return o; }

	/// <summary>Creates a matrix describing a right-hand rotation around the given axis.</summary>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public static void RotationAxis(Vec3 axis, Angle angle, out Mat4 o)
	{
		axis = axis.Normalized;
		float
			xx = axis.X * axis.X, yy = axis.Y * axis.Y, zz = axis.Z * axis.Z,
			xy = axis.X * axis.Y, xz = axis.X * axis.Z, yz = axis.Y * axis.Z;
		var (s, c) = angle.SinCos;
		var t = 1 - c;
		Unsafe.SkipInit(out o);
		o.M00 = t * xx + c;
		o.M01 = t * xy + s * axis.Z;
		o.M02 = t * xz - s * axis.Y;
		o.M03 = 0;
		o.M10 = t * xy - s * axis.Z;
		o.M11 = t * yy + c;
		o.M12 = t * yz + s * axis.X;
		o.M13 = 0;
		o.M20 = t * xz + s * axis.Y;
		o.M21 = t * yz - s * axis.X;
		o.M22 = t * zz + c;
		o.M23 = 0;
		o.Row3 = Vec4.UnitW;
	}

	/// <summary>Creates a matrix describing a combined yaw, pitch, and roll.</summary>
	public static Mat4 YawPitchRoll(Angle yaw, Angle pitch, Angle roll) { 
		YawPitchRoll(yaw, pitch, roll, out var o); return o;
	}
	
	/// <summary>Creates a matrix describing a combined yaw, pitch, and roll.</summary>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]
	public static void YawPitchRoll(Angle yaw, Angle pitch, Angle roll, out Mat4 o)
	{
		var (sy, cy) = (yaw / 2).SinCos;
		var (sp, cp) = (pitch / 2).SinCos;
		var (sr, cr) = (roll / 2).SinCos;
		float
			x = (cy * sp * cr) + (sy * cp * sr), y = (sy * cp * cr) - (cy * sp * sr),
			z = (cy * cp * sr) - (sy * sp * cr), w = (cy * cp * cr) + (sy * sp * sr),
			xx = x * x, yy = y * y, zz = z * z, xy = x * y, xz = x * z, xw = x * w, yz = y * z, yw = y * w, zw = z * w;
		Unsafe.SkipInit(out o);
		o.M00 = 1 - (2 * (yy + zz));
		o.M01 = 2 * (xy + zw);
		o.M02 = 2 * (xz - yw);
		o.M03 = 0;
		o.M10 = 2 * (xy - zw);
		o.M11 = 1 - (2 * (xx + zz));
		o.M12 = 2 * (yz + xw);
		o.M13 = 0;
		o.M20 = 2 * (xz + yw);
		o.M21 = 2 * (yz - xw);
		o.M22 = 1 - (2 * (xx + yy));
		o.M23 = 0;
		o.Row3 = Vec4.UnitW;
	}

	/// <summary>Creates a matrix describing a uniform scale along all axes.</summary>
	public static Mat4 Scale(float s) { Scale(s, out var o); return o; }

	/// <summary>Creates a matrix describing a uniform scale along all axes.</summary>
	public static void Scale(float s, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		o.M00 = s; o.M01 = 0; o.M02 = 0; o.M03 = 0;
		o.M10 = 0; o.M11 = s; o.M12 = 0; o.M13 = 0;
		o.M20 = 0; o.M21 = 0; o.M22 = s; o.M23 = 0;
		o.Row3 = Vec4.UnitW;
	}

	/// <summary>Creates a matrix describing separate scales along all axes.</summary>
	public static Mat4 Scale(float x, float y, float z) { Scale(x, y, z, out var o); return o; }
	
	/// <summary>Creates a matrix describing separate scales along all axes.</summary>
	public static void Scale(float x, float y, float z, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		o.M00 = x; o.M01 = 0; o.M02 = 0; o.M03 = 0;
		o.M10 = 0; o.M11 = y; o.M12 = 0; o.M13 = 0;
		o.M20 = 0; o.M21 = 0; o.M22 = z; o.M23 = 0;
		o.Row3 = Vec4.UnitW;
	}

	/// <summary>Creates a matrix describing a translation.</summary>
	public static Mat4 Translate(float x, float y, float z) { Translate(x, y, z, out var o); return o; }

	/// <summary>Creates a matrix describing a translation.</summary>
	public static void Translate(float x, float y, float z, out Mat4 o)
	{
		Unsafe.SkipInit(out o);
		o.Row0 = Vec4.UnitX;
		o.Row1 = Vec4.UnitY;
		o.Row2 = Vec4.UnitZ;
		o.M30 = x; o.M31 = y; o.M32 = z; o.M33 = 1;
	}
	
	/// <summary>Creates a matrix describing a translation.</summary>
	public static Mat4 Translate(Vec3 v) { Translate(v.X, v.Y, v.Z, out var o); return o; }
	
	/// <summary>Creates a matrix describing a translation.</summary>
	public static void Translate(Vec3 v, out Mat4 o) => Translate(v.X, v.Y, v.Z, out o);

	/// <summary>Creates a world matrix from a position, look direction, and up direction.</summary>
	/// <param name="pos">The world position.</param>
	/// <param name="forward">The forward look direction.</param>
	/// <param name="up">The up direction.</param>
	public static Mat4 World(Vec3 pos, Vec3 forward, Vec3 up) { World(pos, forward, up, out var o); return o; }
	
	/// <inheritdoc cref="World(Vec3,Vec3,Vec3)"/>
	public static void World(Vec3 pos, Vec3 forward, Vec3 up, out Mat4 o)
	{
		var z = -forward.Normalized;
		var x = up.Cross(z).Normalized;
		var y = z.Cross(x);
		Unsafe.SkipInit(out o);
		o.M00 =   x.X; o.M01 =   x.Y; o.M02 =   x.Z; o.M03 = 0;
		o.M10 =   y.X; o.M11 =   y.Y; o.M12 =   y.Z; o.M13 = 0;
		o.M20 =   z.X; o.M21 =   z.Y; o.M22 =   z.Z; o.M23 = 0;
		o.M30 = pos.X; o.M31 = pos.Y; o.M32 = pos.Z; o.M33 = 1;
	}

	/// <summary>Creates a world matrix describing an oriented billboard.</summary>
	/// <param name="pos">The object position.</param>
	/// <param name="viewPos">The view position.</param>
	/// <param name="viewUp">The up direction of the view.</param>
	public static Mat4 Billboard(Vec3 pos, Vec3 viewPos, Vec3 viewUp) {
		World(pos, viewPos - pos, viewUp, out var o); return o;
	}

	/// <inheritdoc cref="Billboard(Vec3,Vec3,Vec3)"/>
	public static void Billboard(Vec3 pos, Vec3 viewPos, Vec3 viewUp, out Mat4 o) =>
		World(pos, viewPos - pos, viewUp, out o);
	#endregion // World Matrices


	#region Camera Matrices
	/// <summary>Creates a view matrix describing a camera look orientation.</summary>
	/// <param name="viewPos">The position of the camera.</param>
	/// <param name="target">The position of the target.</param>
	/// <param name="viewUp">The camera up direction.</param>
	public static Mat4 LookAt(Vec3 viewPos, Vec3 target, Vec3 viewUp) {
		LookAt(viewPos, target, viewUp, out var o); return o;
	}

	/// <inheritdoc cref="LookAt(Vec3,Vec3,Vec3)"/>
	[MethodImpl(MethodImplOptions.AggressiveOptimization)]	
	public static void LookAt(Vec3 viewPos, Vec3 target, Vec3 viewUp, out Mat4 o)
	{
		var z = (viewPos - target).Normalized;
		var x = viewUp.Cross(z).Normalized;
		var y = z.Cross(x);
		float d1 = x.Dot(viewPos), d2 = y.Dot(viewPos), d3 = z.Dot(viewPos);
		Unsafe.SkipInit(out o);
		o.M00 = x.X; o.M01 = y.X; o.M02 = z.X; o.M03 = 0;
		o.M10 = x.Y; o.M11 = y.Y; o.M12 = z.Y; o.M13 = 0;
		o.M20 = x.Z; o.M21 = y.Z; o.M22 = z.Z; o.M23 = 0;
		o.M30 = -d1; o.M31 = -d2; o.M32 = -d3; o.M33 = 1;
	}

	/// <summary>Creates a matrix describing a perspective projection.</summary>
	/// <param name="fov">The horizontal camera field of view.</param>
	/// <param name="aspect">The camera aspect ratio.</param>
	/// <param name="near">The near plane distance.</param>
	/// <param name="far">The far plane distance.</param>
	public static Mat4 Perspective(Angle fov, float aspect, float near, float far) {
		Perspective(fov, aspect, near, far, out var o); return o;
	}
	
	/// <inheritdoc cref="Perspective(Angle,float,float,float)"/>
	public static void Perspective(Angle fov, float aspect, float near, float far, out Mat4 o)
	{
		float f = 1 / (fov / 2).Tan, d = near - far;
		Unsafe.SkipInit(out o);
		o.M00 = f / aspect; o.M01 = 0; o.M02 =              0; o.M03 =  0;
		o.M10 =          0; o.M11 = f; o.M12 =              0; o.M13 =  0;
		o.M20 =          0; o.M21 = 0; o.M22 =        far / d; o.M23 = -1;
		o.M30 =          0; o.M31 = 0; o.M32 = near * far / d; o.M33 =  0;
	}

	/// <summary>Creates a matrix describing a centered orthographic projection.</summary>
	/// <param name="width">The width of the projection.</param>
	/// <param name="height">The height of the projection.</param>
	/// <param name="near">The near plane distance.</param>
	/// <param name="far">The far plane distance.</param>
	public static Mat4 Ortho(float width, float height, float near, float far) {
		OrthoOffCenter(-width / 2, width / 2, -height / 2, height / 2, near, far, out var o); return o;
	}

	/// <inheritdoc cref="Ortho(float,float,float,float)"/>
	public static void Ortho(float width, float height, float near, float far, out Mat4 o) =>
		OrthoOffCenter(-width / 2, width / 2, height / 2, -height / 2, near, far, out o);

	/// <summary>Creates a matrix describing an off-center orthographic projection.</summary>
	/// <param name="left">The left plane coordinate.</param>
	/// <param name="right">The right plane coordinate.</param>
	/// <param name="top">The top plane coordinate.</param>
	/// <param name="bottom">The bottom plane coordinate.</param>
	/// <param name="near">The near plane distance.</param>
	/// <param name="far">The far plane distance.</param>
	public static Mat4 OrthoOffCenter(float left, float right, float top, float bottom, float near, float far) {
		OrthoOffCenter(left, right, top, bottom, near, far, out var o); return o;
	}

	/// <inheritdoc cref="OrthoOffCenter(float,float,float,float,float,float)"/>
	public static void OrthoOffCenter(float left, float right, float top, float bottom, float near, float far, out Mat4 o)
	{
		float w = right - left, h = top - bottom, d = near - far;
		Unsafe.SkipInit(out o);
		o.M00 =               2 / w; o.M01 =                   0; o.M02 =        0; o.M03 = 0;
		o.M10 =                   0; o.M11 =               2 / h; o.M12 =        0; o.M13 = 0;
		o.M20 =                   0; o.M21 =                   0; o.M22 =    1 / d; o.M23 = 0;
		o.M30 = -(left + right) / w; o.M31 = -(top + bottom) / h; o.M32 = near / d; o.M33 = 1;
	}
	#endregion // Camera Matrices


	#region Operators
	public static bool operator == (Mat4 l, Mat4 r) => l.Equals(in r);
	public static bool operator != (Mat4 l, Mat4 r) => !l.Equals(in r);
	
	public static Mat4  operator + (Mat4 l, Mat4 r) => l.Add(in r);
	public static Mat4  operator + (Mat4 l, float r) => l.Add(r);
	public static Mat4  operator - (Mat4 l, Mat4 r) => l.Subtract(in r);
	public static Mat4  operator - (Mat4 l, float r) => l.Subtract(r);
	public static Mat4  operator * (Mat4 l, Mat4 r) => l.Multiply(in r);
	public static Mat4  operator * (Mat4 l, float r) => l.Multiply(r);
	public static Vec4  operator * (Mat4 l, Vec4 r) => l.Transform(r);
	public static Vec3  operator * (Mat4 l, Vec3 r) => l.Transform(r);
	public static Vec4D operator * (Mat4 l, Vec4D r) => l.Transform(r);
	public static Vec3D operator * (Mat4 l, Vec3D r) => l.Transform(r);
	public static Mat4  operator / (Mat4 l, float r) => l.Divide(r);

	public static Mat4 operator - (Mat4 r) => r.Negate();
	#endregion // Operators
}
