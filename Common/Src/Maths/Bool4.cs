﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>Four-component vector of booleans.</summary>
[StructLayout(LayoutKind.Sequential, Size = 4)]
public struct Bool4 :
	IEquatable<Bool4>,
	IEqualityOperators<Bool4, Bool4, bool>, IBitwiseOperators<Bool4, Bool4, Bool4>
{
	#region Fields
	/// <summary>The X component.</summary>
	public bool X;
	/// <summary>The Y component.</summary>
	public bool Y;
	/// <summary>The Z component.</summary>
	public bool Z;
	/// <summary>The W component.</summary>
	public bool W;

	/// <summary>If any components are true.</summary>
	public readonly bool Any => X || Y || Z || W;
	/// <summary>If all components are true.</summary>
	public readonly bool All => X && Y && Z && W;
	/// <summary>If all components are false.</summary>
	public readonly bool None => !Any;

	/// <summary>The number of components that are true.</summary>
	public readonly uint Count => (X ? 1u : 0u) + (Y ? 1u : 0u) + (Z ? 1u : 0u) + (W ? 1u : 0u);
	#endregion // Fields

	
	/// <summary>Construct a vector with equal components.</summary>
	public Bool4(bool b) => X = Y = Z = W = b;

	/// <summary>Construct a vector by appending a W component.</summary>
	public Bool4(Bool3 xyz, bool w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct a vector by concatenating two vectors.</summary>
	public Bool4(Bool2 xy, Bool2 zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);

	/// <summary>Construct a vector with the components.</summary>
	public Bool4(bool x, bool y, bool z, bool w) => (X, Y, Z, W) = (x, y, z, w);


	#region Base
	public readonly bool Equals(Bool4 o) => o.X == X && o.Y == Y && o.Z == Z && o.W == W;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => 
		o is Bool4 b && b.X == X && b.Y == Y && b.Z == Z && b.W == W;

	public readonly override int GetHashCode() => (X, Y, Z, W) switch {
		(false, false, false, false) => 0,  (false, false, false, true) => 1,
		(false, false, true, false)  => 2,  (false, false, true, true)  => 3,
		(false, true, false, false)  => 4,  (false, true, false, true)  => 5,
		(false, true, true, false)   => 6,  (false, true, true, true)   => 7,
		(true, false, false, false)  => 8,  (true, false, false, true)  => 9,
		(true, false, true, false)   => 10, (true, false, true, true)   => 11,
		(true, true, false, false)   => 12, (true, true, false, true)   => 13,
		(true, true, true, false)    => 14, (true, true, true, true)    => 15
	};

	public readonly override string ToString() => (X, Y, Z, W) switch {
		(false, false, false, false) => "{F,F,F,F}", (false, false, false, true) => "{F,F,F,T}",
		(false, false, true, false)  => "{F,F,T,F}", (false, false, true, true)  => "{F,F,T,T}",
		(false, true, false, false)  => "{F,T,F,F}", (false, true, false, true)  => "{F,T,F,T}",
		(false, true, true, false)   => "{F,T,T,F}", (false, true, true, true)   => "{F,T,T,T}",
		(true, false, false, false)  => "{T,F,F,F}", (true, false, false, true)  => "{T,F,F,T}",
		(true, false, true, false)   => "{T,F,T,F}", (true, false, true, true)   => "{T,F,T,T}",
		(true, true, false, false)   => "{T,T,F,F}", (true, true, false, true)   => "{T,T,F,T}",
		(true, true, true, false)    => "{T,T,T,F}", (true, true, true, true)    => "{T,T,T,T}"
	};

	public readonly void Deconstruct(out bool x, out bool y, out bool z, out bool w) => (x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Operators
	public static bool operator == (Bool4 l, Bool4 r) => l.Equals(r);
	public static bool operator != (Bool4 l, Bool4 r) => !l.Equals(r);
	
	public static Bool4 operator & (Bool4 l, Bool4 r) => new(l.X && r.X, l.Y && r.Y, l.Z && r.Z, l.W && r.W);
	public static Bool4 operator | (Bool4 l, Bool4 r) => new(l.X || r.X, l.Y || r.Y, l.Z || r.Z, l.W || r.W);
	public static Bool4 operator ^ (Bool4 l, Bool4 r) => new(l.X != r.X, l.Y != r.Y, l.Z != r.Z, l.W != r.W);
	public static Bool4 operator ~ (Bool4 r) => new(!r.X, !r.Y, !r.Z, !r.W);
	public static Bool4 operator ! (Bool4 r) => new(!r.X, !r.Y, !r.Z, !r.W);
	#endregion // Operators
}
