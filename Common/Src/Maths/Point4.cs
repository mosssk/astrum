﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Astrum.Maths;


/// <summary>A four-component vector of <c>int</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 16)]
public struct Point4 :
	IEquatable<Point4>, IEqualityOperators<Point4, Point4, bool>,
	IAdditionOperators<Point4, Point4, Point4>, IAdditionOperators<Point4, int, Point4>,
	ISubtractionOperators<Point4, Point4, Point4>, ISubtractionOperators<Point4, int, Point4>,
	IMultiplyOperators<Point4, Point4, Point4>, IMultiplyOperators<Point4, int, Point4>,
	IDivisionOperators<Point4, Point4, Point4>, IDivisionOperators<Point4, int, Point4>,
	IModulusOperators<Point4, Point4, Point4>, IModulusOperators<Point4, int, Point4>,
	IBitwiseOperators<Point4, Point4, Point4>, IBitwiseOperators<Point4, int, Point4>,
	IUnaryNegationOperators<Point4, Point4>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point4 Zero = new();

	/// <summary>Point with all-one components.</summary>
	public static readonly Point4 One = new(1);
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point4 UnitX = new(1, 0, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point4 UnitY = new(0, 1, 0, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point4 UnitZ = new(0, 0, 1, 0);
	/// <summary>Point with unit length along W-axis.</summary>
	public static readonly Point4 UnitW = new(0, 0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public int X;
	/// <summary>The Y-component.</summary>
	public int Y;
	/// <summary>The Z-component.</summary>
	public int Z;
	/// <summary>The W-component.</summary>
	public int W;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly float Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Single.Sqrt(X*X + Y*Y + Z*Z + W*W);
	}

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly float LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z + W*W;
	}
	#endregion // Fields


	/// <summary>Construct a point with equal components.</summary>
	public Point4(int v) => X = Y = Z = W = v;

	/// <summary>Construct a point from the components.</summary>
	public Point4(int x, int y, int z, int w) => (X, Y, Z, W) = (x, y, z, w);

	/// <summary>Construct a point from a point providing XYZ, with an additional W component.</summary>
	public Point4(Point3 xyz, int w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct a point from points providing XY and ZW.</summary>
	public Point4(Point2 xy, Point2 zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Point4 o) => X == o.X && Y == o.Y && Z == o.Z && W == o.W;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => 
		o is Point4 p && X == p.X && Y == p.Y && Z == p.Z && W == p.W;

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"{{{X},{Y},{Z},{W}}}";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)},{W.ToString(fmt)}}}";

	public readonly void Deconstruct(out int x, out int y, out int z, out int w) => 
		(x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly int Dot(Point4 o) => X*o.X + Y*o.Y + Z*o.Z + W*o.W;

	/// <summary>Component-wise clamp of the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4 Clamp(Point4 v, Point4 min, Point4 max) => new(
		Int32.Clamp(v.X, min.X, max.X), Int32.Clamp(v.Y, min.Y, max.Y), 
		Int32.Clamp(v.Z, min.Z, max.Z), Int32.Clamp(v.W, min.W, max.W)
	); 

	/// <summary>Component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4 Min(Point4 l, Point4 r) => 
		new(Int32.Min(l.X, r.X), Int32.Min(l.Y, r.Y), Int32.Min(l.Z, r.Z), Int32.Min(l.W, r.W));

	/// <summary>Component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4 Max(Point4 l, Point4 r) => 
		new(Int32.Max(l.X, r.X), Int32.Max(l.Y, r.Y), Int32.Max(l.Z, r.Z), Int32.Max(l.W, r.W)); 
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point4 l, Point4 r) => l.Equals(r);
	public static bool operator != (Point4 l, Point4 r) => !l.Equals(r);

	public static Bool4 operator <= (Point4 l, Point4 r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W);
	public static Bool4 operator <  (Point4 l, Point4 r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z, l.W < r.W);
	public static Bool4 operator >= (Point4 l, Point4 r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
	public static Bool4 operator >  (Point4 l, Point4 r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z, l.W > r.W);

	public static Point4 operator + (Point4 l, Point4 r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W);
	public static Point4 operator + (Point4 l, int r) => new(l.X + r, l.Y + r, l.Z + r, l.W + r);
	public static Point4 operator - (Point4 l, Point4 r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W);
	public static Point4 operator - (Point4 l, int r) => new(l.X - r, l.Y - r, l.Z - r, l.W - r);
	public static Point4 operator * (Point4 l, Point4 r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W);
	public static Point4 operator * (Point4 l, int r) => new(l.X * r, l.Y * r, l.Z * r, l.W * r);
	public static Point4 operator / (Point4 l, Point4 r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W);
	public static Point4 operator / (Point4 l, int r) => new(l.X / r, l.Y / r, l.Z / r, l.W / r);
	public static Point4 operator % (Point4 l, Point4 r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z, l.W % r.W);
	public static Point4 operator % (Point4 l, int r) => new(l.X % r, l.Y % r, l.Z % r, l.W % r);

	public static Point4 operator & (Point4 l, Point4 r) => new(l.X & r.X, l.Y & r.Y, l.Z & r.Z, l.W & r.W);
	public static Point4 operator & (Point4 l, int r) => new(l.X & r, l.Y & r, l.Z & r, l.W & r);
	public static Point4 operator | (Point4 l, Point4 r) => new(l.X | r.X, l.Y | r.Y, l.Z | r.Z, l.W | r.W);
	public static Point4 operator | (Point4 l, int r) => new(l.X | r, l.Y | r, l.Z | r, l.W | r);
	public static Point4 operator ^ (Point4 l, Point4 r) => new(l.X ^ r.X, l.Y ^ r.Y, l.Z ^ r.Z, l.W ^ r.W);
	public static Point4 operator ^ (Point4 l, int r) => new(l.X ^ r, l.Y ^ r, l.Z ^ r, l.W ^ r);
	public static Point4 operator ~ (Point4 r) => new(~r.X, ~r.Y, ~r.Z, ~r.W);

	public static Point4 operator - (Point4 r) => new(-r.X, -r.Y, -r.Z, -r.W);

	public static explicit operator Point4 (Vec4 v) => new((int)v.X, (int)v.Y, (int)v.Z, (int)v.W);
	public static explicit operator Point4 (Vec4D v) => new((int)v.X, (int)v.Y, (int)v.Z, (int)v.W);

	public static explicit operator Point4 (Point4L o) => new((int)o.X, (int)o.Y, (int)o.Z, (int)o.W);
	#endregion // Operators
}


/// <summary>A four-component vector of <c>long</c>s.</summary>
[StructLayout(LayoutKind.Sequential, Size = 32)]
public struct Point4L :
	IEquatable<Point4L>, IEqualityOperators<Point4L, Point4L, bool>,
	IAdditionOperators<Point4L, Point4L, Point4L>, IAdditionOperators<Point4L, long, Point4L>,
	ISubtractionOperators<Point4L, Point4L, Point4L>, ISubtractionOperators<Point4L, long, Point4L>,
	IMultiplyOperators<Point4L, Point4L, Point4L>, IMultiplyOperators<Point4L, long, Point4L>,
	IDivisionOperators<Point4L, Point4L, Point4L>, IDivisionOperators<Point4L, long, Point4L>,
	IModulusOperators<Point4L, Point4L, Point4L>, IModulusOperators<Point4L, long, Point4L>,
	IBitwiseOperators<Point4L, Point4L, Point4L>, IBitwiseOperators<Point4L, long, Point4L>,
	IUnaryNegationOperators<Point4L, Point4L>
{
	#region Constants
	/// <summary>Point with all zero components.</summary>
	public static readonly Point4L Zero = new();

	/// <summary>Point with all-one components.</summary>
	public static readonly Point4L One = new(1);
	/// <summary>Point with unit length along X-axis.</summary>
	public static readonly Point4L UnitX = new(1, 0, 0, 0);
	/// <summary>Point with unit length along Y-axis.</summary>
	public static readonly Point4L UnitY = new(0, 1, 0, 0);
	/// <summary>Point with unit length along Z-axis.</summary>
	public static readonly Point4L UnitZ = new(0, 0, 1, 0);
	/// <summary>Point with unit length along W-axis.</summary>
	public static readonly Point4L UnitW = new(0, 0, 0, 1);
	#endregion // Constants


	#region Fields
	/// <summary>The X-component.</summary>
	public long X;
	/// <summary>The Y-component.</summary>
	public long Y;
	/// <summary>The Z-component.</summary>
	public long Z;
	/// <summary>The W-component.</summary>
	public long W;

	/// <summary>The cartesian length of the point as a vector.</summary>
	public readonly double Length {
		[MethodImpl(MathUtils.MAX_OPT)] get => Double.Sqrt(X*X + Y*Y + Z*Z + W*W);
	}

	/// <summary>The squared cartesian length of the point as a vector.</summary>
	public readonly double LengthSq {
		[MethodImpl(MathUtils.MAX_OPT)] get => X*X + Y*Y + Z*Z + W*W;
	}
	#endregion // Fields


	/// <summary>Construct a point with equal components.</summary>
	public Point4L(long v) => X = Y = Z = W = v;

	/// <summary>Construct a point from the components.</summary>
	public Point4L(long x, long y, long z, long w) => (X, Y, Z, W) = (x, y, z, w);

	/// <summary>Construct a point from a point providing XYZ, with an additional W component.</summary>
	public Point4L(Point3L xyz, long w) => (X, Y, Z, W) = (xyz.X, xyz.Y, xyz.Z, w);

	/// <summary>Construct a point from points providing XY and ZW.</summary>
	public Point4L(Point2L xy, Point2L zw) => (X, Y, Z, W) = (xy.X, xy.Y, zw.X, zw.Y);


	#region Base
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly bool Equals(Point4L o) => X == o.X && Y == o.Y && Z == o.Z && W == o.W;

	public readonly override bool Equals([NotNullWhen(true)] object? o) => 
		o is Point4L p && X == p.X && Y == p.Y && Z == p.Z && W == p.W;

	public readonly override int GetHashCode() => HashCode.Combine(X, Y, Z, W);

	public readonly override string ToString() => $"{{{X},{Y},{Z},{W}}}";

	public readonly string ToString([StringSyntax("NumericFormat")] string? fmt) =>
		$"{{{X.ToString(fmt)},{Y.ToString(fmt)},{Z.ToString(fmt)},{W.ToString(fmt)}}}";

	public readonly void Deconstruct(out long x, out long y, out long z, out long w) => 
		(x, y, z, w) = (X, Y, Z, W);
	#endregion // Base


	#region Point Ops
	/// <summary>Calculates the dot product of the points as vectors.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public readonly long Dot(Point4L o) => X*o.X + Y*o.Y + Z*o.Z + W*o.W;

	/// <summary>Component-wise clamp of the point.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4L Clamp(Point4L v, Point4L min, Point4L max) => new(
		Int64.Clamp(v.X, min.X, max.X), Int64.Clamp(v.Y, min.Y, max.Y), 
		Int64.Clamp(v.Z, min.Z, max.Z), Int64.Clamp(v.W, min.W, max.W)
	); 

	/// <summary>Component-wise minimum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4L Min(Point4L l, Point4L r) => 
		new(Int64.Min(l.X, r.X), Int64.Min(l.Y, r.Y), Int64.Min(l.Z, r.Z), Int64.Min(l.W, r.W));

	/// <summary>Component-wise maximum of the points.</summary>
	[MethodImpl(MathUtils.MAX_OPT)]
	public static Point4L Max(Point4L l, Point4L r) => 
		new(Int64.Max(l.X, r.X), Int64.Max(l.Y, r.Y), Int64.Max(l.Z, r.Z), Int64.Max(l.W, r.W)); 
	#endregion // Point Ops


	#region Operators
	public static bool operator == (Point4L l, Point4L r) => l.Equals(r);
	public static bool operator != (Point4L l, Point4L r) => !l.Equals(r);

	public static Bool4 operator <= (Point4L l, Point4L r) => new(l.X <= r.X, l.Y <= r.Y, l.Z <= r.Z, l.W <= r.W);
	public static Bool4 operator <  (Point4L l, Point4L r) => new(l.X < r.X, l.Y < r.Y, l.Z < r.Z, l.W < r.W);
	public static Bool4 operator >= (Point4L l, Point4L r) => new(l.X >= r.X, l.Y >= r.Y, l.Z >= r.Z, l.W >= r.W);
	public static Bool4 operator >  (Point4L l, Point4L r) => new(l.X > r.X, l.Y > r.Y, l.Z > r.Z, l.W > r.W);

	public static Point4L operator + (Point4L l, Point4L r) => new(l.X + r.X, l.Y + r.Y, l.Z + r.Z, l.W + r.W);
	public static Point4L operator + (Point4L l, long r) => new(l.X + r, l.Y + r, l.Z + r, l.W + r);
	public static Point4L operator - (Point4L l, Point4L r) => new(l.X - r.X, l.Y - r.Y, l.Z - r.Z, l.W - r.W);
	public static Point4L operator - (Point4L l, long r) => new(l.X - r, l.Y - r, l.Z - r, l.W - r);
	public static Point4L operator * (Point4L l, Point4L r) => new(l.X * r.X, l.Y * r.Y, l.Z * r.Z, l.W * r.W);
	public static Point4L operator * (Point4L l, long r) => new(l.X * r, l.Y * r, l.Z * r, l.W * r);
	public static Point4L operator / (Point4L l, Point4L r) => new(l.X / r.X, l.Y / r.Y, l.Z / r.Z, l.W / r.W);
	public static Point4L operator / (Point4L l, long r) => new(l.X / r, l.Y / r, l.Z / r, l.W / r);
	public static Point4L operator % (Point4L l, Point4L r) => new(l.X % r.X, l.Y % r.Y, l.Z % r.Z, l.W % r.W);
	public static Point4L operator % (Point4L l, long r) => new(l.X % r, l.Y % r, l.Z % r, l.W % r);

	public static Point4L operator & (Point4L l, Point4L r) => new(l.X & r.X, l.Y & r.Y, l.Z & r.Z, l.W & r.W);
	public static Point4L operator & (Point4L l, long r) => new(l.X & r, l.Y & r, l.Z & r, l.W & r);
	public static Point4L operator | (Point4L l, Point4L r) => new(l.X | r.X, l.Y | r.Y, l.Z | r.Z, l.W | r.W);
	public static Point4L operator | (Point4L l, long r) => new(l.X | r, l.Y | r, l.Z | r, l.W | r);
	public static Point4L operator ^ (Point4L l, Point4L r) => new(l.X ^ r.X, l.Y ^ r.Y, l.Z ^ r.Z, l.W ^ r.W);
	public static Point4L operator ^ (Point4L l, long r) => new(l.X ^ r, l.Y ^ r, l.Z ^ r, l.W ^ r);
	public static Point4L operator ~ (Point4L r) => new(~r.X, ~r.Y, ~r.Z, ~r.W);

	public static Point4L operator - (Point4L r) => new(-r.X, -r.Y, -r.Z, -r.W);

	public static explicit operator Point4L (Vec4 v) => new((long)v.X, (long)v.Y, (long)v.Z, (long)v.W);
	public static explicit operator Point4L (Vec4D v) => new((long)v.X, (long)v.Y, (long)v.Z, (long)v.W);

	public static implicit operator Point4L (Point4 o) => new(o.X, o.Y, o.Z, o.W);
	#endregion // Operators
}

