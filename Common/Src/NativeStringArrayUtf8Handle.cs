/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Astrum;


/// <summary>Contains a stack-only handle to an array of allocated, null-terminated native UTF-8 strings.</summary>
/// <remarks>This struct should <em>always</em> be disposed after use, otherwise memory leaks will occur.</remarks>
public unsafe ref struct NativeStringArrayUtf8Handle : IDisposable
{
	#region Fields
	/// <summary>The number of strings in the array.</summary>
	public uint Count { get; private set; }

	/// <summary>The native string array, or <c>null</c> if uninitialized or disposed.</summary>
	public byte** Data { get; private set; } // Block of array data, length data, and string data
	#endregion // Fields
	
	
	// Private ctor with validated data
	private NativeStringArrayUtf8Handle(uint count, byte** data)
	{
		Count = count;
		Data = data;
	}
	
	/// <summary>Creates a new array of native strings with UTF-8 encoding.</summary>
	/// <param name="strings">The strings to include in the array.</param>
	/// <returns>The native string array handle. Must be disposed by the caller.</returns>
	public static NativeStringArrayUtf8Handle Create(params ReadOnlySpan<string> strings)
	{
		if (strings.Length == 0) return new();

		byte** dataPtr = null;
		try {
			// Allocate the native memory
			var nativeSize = (nuint)(strings.Length * (sizeof(nuint) + sizeof(uint))); // Array + length data
			foreach (var str in strings) 
				nativeSize += (nuint)(Encoding.UTF8.GetByteCount(str) + 1);
			dataPtr = (byte**)NativeMemory.Alloc(nativeSize);
			
			// Populate the string data
			var strLenPtr = (uint*)(dataPtr + strings.Length);
			var strDataPtr = (byte*)(strLenPtr + strings.Length);
			for (var si = 0; si < strings.Length; ++si) {
				var len = Encoding.UTF8.GetBytes(strings[si], new(strDataPtr, (int)nativeSize));
				strDataPtr[len] = 0;
				dataPtr[si] = strDataPtr;
				strLenPtr[si] = (uint)len;
				strDataPtr += len + 1;
			}

			return new((uint)strings.Length, dataPtr);
		}
		catch {
			if (dataPtr != null) NativeMemory.Free(dataPtr);
			throw;
		}
	}
	
	
	#region Base
	public readonly override string ToString() => $"NativeStringArray[{Count},0x{(nuint)Data:X}]";

	public readonly override int GetHashCode() => ((nuint)Data).GetHashCode();
	
	/// <summary>Gets a managed string representation of the native string data at the given array index.</summary>
	/// <param name="index">The index of the native string to get.</param>
	public readonly string GetString(uint index)
	{
		ArgumentOutOfRangeException.ThrowIfGreaterThanOrEqual(index, Count);
		var strLenPtr = (uint*)(Data + Count);
		return Encoding.UTF8.GetString(Data[index], (int)strLenPtr[index]);
	}
	#endregion // Base


	public void Dispose()
	{
		if (Data == null) return;
		NativeMemory.Free(Data);
		Count = 0;
		Data = null;
	}
}
