/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;

#pragma warning disable CA2254

namespace Astrum;


/// <summary>Provides convenient internal logging operations to the Astrum library.</summary>
internal static class InternalLogging
{
	// The logger instance
	private static ILogger? _Logger;

	/// <summary>Sets the internal logger to use.</summary>
	public static void UseInternalLogger(ILogger? logger) => _Logger = logger;


	/// <summary>Logs an unformatted <c>Trace</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogTrace(string msg, Exception? ex = null) => _Logger?.LogTrace(ex, msg);
	
	/// <summary>Logs an unformatted <c>Debug</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogDebug(string msg, Exception? ex = null) => _Logger?.LogDebug(ex, msg);
	
	/// <summary>Logs an unformatted <c>Information</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogInfo(string msg, Exception? ex = null) => _Logger?.LogInformation(ex, msg);
	
	/// <summary>Logs an unformatted <c>Warning</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogWarn(string msg, Exception? ex = null) => _Logger?.LogWarning(ex, msg);
	
	/// <summary>Logs an unformatted <c>Error</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogError(string msg, Exception? ex = null) => _Logger?.LogError(ex, msg);
	
	/// <summary>Logs an unformatted <c>Critical</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogCritical(string msg, Exception? ex = null) => _Logger?.LogCritical(ex, msg);
	
	
	/// <summary>Logs a formatted <c>Trace</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogTrace(ref LogStringHandlers.Trace msg, Exception? ex = null) => 
		_Logger?.LogTrace(ex, msg.ToStringAndClear());
	
	/// <summary>Logs a formatted <c>Debug</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogDebug(ref LogStringHandlers.Debug msg, Exception? ex = null) => 
		_Logger?.LogDebug(ex, msg.ToStringAndClear());
	
	/// <summary>Logs a formatted <c>Information</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogInfo(ref LogStringHandlers.Information msg, Exception? ex = null) => 
		_Logger?.LogInformation(ex, msg.ToStringAndClear());
	
	/// <summary>Logs a formatted <c>Warning</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogWarn(ref LogStringHandlers.Warning msg, Exception? ex = null) => 
		_Logger?.LogWarning(ex, msg.ToStringAndClear());
	
	/// <summary>Logs a formatted <c>Error</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogError(ref LogStringHandlers.Error msg, Exception? ex = null) => 
		_Logger?.LogError(ex, msg.ToStringAndClear());
	
	/// <summary>Logs a formatted <c>Critical</c>-level message with an optional exception.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void LogCritical(ref LogStringHandlers.Critical msg, Exception? ex = null) => 
		_Logger?.LogCritical(ex, msg.ToStringAndClear());
	
	
	/// <summary>Interpolated string handlers for logging levels.</summary>
	public static class LogStringHandlers
	{
		[InterpolatedStringHandler]
		public ref struct Trace
		{
			private DefaultInterpolatedStringHandler _handler;

			public Trace(int literalLength, int formattedCount, out bool enabled)
			{
				enabled = _Logger is { } l && l.IsEnabled(LogLevel.Trace);
				_handler = enabled ? new DefaultInterpolatedStringHandler(literalLength, formattedCount) : default;
			}
			
			public string ToStringAndClear() => _handler.ToStringAndClear();
			public void AppendLiteral(string s) => _handler.AppendLiteral(s);
			public void AppendFormatted<T>(T t) => _handler.AppendFormatted(t);
			public void AppendFormatted<T>(T t, string format) => _handler.AppendFormatted(t, format);
		}
		
		[InterpolatedStringHandler]
		public ref struct Debug
		{
			private DefaultInterpolatedStringHandler _handler;

			public Debug(int literalLength, int formattedCount, out bool enabled)
			{
				enabled = _Logger is { } l && l.IsEnabled(LogLevel.Debug);
				_handler = enabled ? new DefaultInterpolatedStringHandler(literalLength, formattedCount) : default;
			}
			
			public string ToStringAndClear() => _handler.ToStringAndClear();
			public void AppendLiteral(string s) => _handler.AppendLiteral(s);
			public void AppendFormatted<T>(T t) => _handler.AppendFormatted(t);
			public void AppendFormatted<T>(T t, string format) => _handler.AppendFormatted(t, format);
		}
		
		[InterpolatedStringHandler]
		public ref struct Information
		{
			private DefaultInterpolatedStringHandler _handler;

			public Information(int literalLength, int formattedCount, out bool enabled)
			{
				enabled = _Logger is { } l && l.IsEnabled(LogLevel.Information);
				_handler = enabled ? new DefaultInterpolatedStringHandler(literalLength, formattedCount) : default;
			}
			
			public string ToStringAndClear() => _handler.ToStringAndClear();
			public void AppendLiteral(string s) => _handler.AppendLiteral(s);
			public void AppendFormatted<T>(T t) => _handler.AppendFormatted(t);
			public void AppendFormatted<T>(T t, string format) => _handler.AppendFormatted(t, format);
		}
		
		[InterpolatedStringHandler]
		public ref struct Warning
		{
			private DefaultInterpolatedStringHandler _handler;

			public Warning(int literalLength, int formattedCount, out bool enabled)
			{
				enabled = _Logger is { } l && l.IsEnabled(LogLevel.Warning);
				_handler = enabled ? new DefaultInterpolatedStringHandler(literalLength, formattedCount) : default;
			}
			
			public string ToStringAndClear() => _handler.ToStringAndClear();
			public void AppendLiteral(string s) => _handler.AppendLiteral(s);
			public void AppendFormatted<T>(T t) => _handler.AppendFormatted(t);
			public void AppendFormatted<T>(T t, string format) => _handler.AppendFormatted(t, format);
		}
		
		[InterpolatedStringHandler]
		public ref struct Error
		{
			private DefaultInterpolatedStringHandler _handler;

			public Error(int literalLength, int formattedCount, out bool enabled)
			{
				enabled = _Logger is { } l && l.IsEnabled(LogLevel.Error);
				_handler = enabled ? new DefaultInterpolatedStringHandler(literalLength, formattedCount) : default;
			}
			
			public string ToStringAndClear() => _handler.ToStringAndClear();
			public void AppendLiteral(string s) => _handler.AppendLiteral(s);
			public void AppendFormatted<T>(T t) => _handler.AppendFormatted(t);
			public void AppendFormatted<T>(T t, string format) => _handler.AppendFormatted(t, format);
		}
		
		[InterpolatedStringHandler]
		public ref struct Critical
		{
			private DefaultInterpolatedStringHandler _handler;

			public Critical(int literalLength, int formattedCount, out bool enabled)
			{
				enabled = _Logger is { } l && l.IsEnabled(LogLevel.Critical);
				_handler = enabled ? new DefaultInterpolatedStringHandler(literalLength, formattedCount) : default;
			}
			
			public string ToStringAndClear() => _handler.ToStringAndClear();
			public void AppendLiteral(string s) => _handler.AppendLiteral(s);
			public void AppendFormatted<T>(T t) => _handler.AppendFormatted(t);
			public void AppendFormatted<T>(T t, string format) => _handler.AppendFormatted(t, format);
		}
	}
}
