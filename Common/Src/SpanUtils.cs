﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Astrum;


/// <summary>Utilities related to values and operations for <see cref="Span{T}"/> and <see cref="ReadOnlySpan{T}"/>.</summary>
public static unsafe class SpanUtils
{
	/// <summary>Get the <c>unmanaged</c> span data as a span of bytes.</summary>
	/// <param name="span">The span to convert to a span of bytes.</param>
	/// <typeparam name="T">The original span type - must be unmanaged.</typeparam>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static Span<byte> AsByteSpan<T>(this Span<T> span) where T : unmanaged => span.IsEmpty
		? Span<byte>.Empty
		: MemoryMarshal.CreateSpan(ref Unsafe.As<T, byte>(ref span[0]), span.Length * sizeof(T));
	
	/// <inheritdoc cref="AsByteSpan{T}(System.Span{T})"/>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static ReadOnlySpan<byte> AsByteSpan<T>(this ReadOnlySpan<T> span) where T : unmanaged => span.IsEmpty
		? ReadOnlySpan<byte>.Empty 
		: MemoryMarshal.CreateSpan(ref Unsafe.As<T, byte>(ref Unsafe.AsRef(in span[0])), span.Length * sizeof(T));
	
	/// <summary>Returns a read-only span of the bytes occupying the same memory as the given value reference.</summary>
	/// <param name="value">The value reference to wrap in the byte span.</param>
	/// <typeparam name="T">The unmanaged value type.</typeparam>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static ReadOnlySpan<byte> WrapBytes<T>(scoped ref readonly T value) where T : unmanaged =>
		MemoryMarshal.CreateReadOnlySpan(ref Unsafe.As<T, byte>(ref Unsafe.AsRef(in value)), sizeof(T));
	
	
	/// <summary>Creates a span over the given null-terminated c-string.</summary>
	/// <returns>The c-string memory span, or an empty span if the pointer was null.</returns>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static ReadOnlySpan<byte> CreateCStringSpan(byte* str) =>
		str != null ? new(str, strlen(null!, str)) : ReadOnlySpan<byte>.Empty;
	
	
	
	// Unsafe access to System.String.strlen
	[UnsafeAccessor(UnsafeAccessorKind.StaticMethod, Name = "strlen")] 
	private static extern int strlen(string _, byte* str);
}
