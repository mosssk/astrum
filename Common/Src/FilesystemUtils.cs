﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


/// <summary>Utilities related to the filesystem operations.</summary>
public static class FilesystemUtils
{
	/// <summary>Sanitizes the given folder or file name to only include valid filesystem characters.</summary>
	/// <remarks>Invalid characters are removed.</remarks>
	/// <param name="name">The filesystem name to sanitize.</param>
	/// <param name="remove">If <c>true</c>, invalid characters are removed, otherwise they are replaced with <c>_</c>.</param>
	/// <returns>The sanitized name.</returns>
	/// <exception cref="ArgumentException">
	/// <paramref name="name"/> contains directory separators, or the sanitized name is empty or all non-alphanumeric
	/// characters.
	/// </exception>
	public static string SanitizeName(ReadOnlySpan<char> name, bool remove)
	{
		if (name.IsEmpty) throw new ArgumentException("Sanitized name cannot be blank");

		// Scan through the name and validate
		var sanitized = (name.Length >= 128) ? new char[name.Length] : stackalloc char[name.Length];
		var len = 0;
		var alnumCount = 0;
		foreach (var ch in name) {
			if (ch is '/' or '\\') {
				throw new ArgumentException("Sanitized names cannot contain directory separators");
			}
			
			if (Char.IsLetterOrDigit(ch)) {
				sanitized[len++] = ch;
				alnumCount += 1;
			}
			else if (ch is '_' or '-' or '.') {
				sanitized[len++] = ch;
			}
			else if (!remove) {
				sanitized[len++] = '_';
			}
		}

		// Validate and return
		return (alnumCount == 0)
			? throw new ArgumentException("Validated name is only non-alphanumeric characters")
			: (len > 0) 
				? new(sanitized[..len]) 
				: throw new ArgumentException("Validated name is zero length");
	}
}
