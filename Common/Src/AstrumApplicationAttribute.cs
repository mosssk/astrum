/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


/// <summary>Assembly-targeted attribute providing static configuration for the application.</summary>
[AttributeUsage(AttributeTargets.Assembly)]
public sealed class AstrumApplicationAttribute : Attribute
{
	#region Fields
	/// <summary>Human-readable name for the application.</summary>
	public readonly string Name;
	/// <summary>Name for the application data folder on the filesystem.</summary>
	public readonly string FolderName;

	/// <summary>The root location for application data files.</summary>
	public Astrum.DataRoot DataRoot { get; init; } = Astrum.DataRoot.AppData;
	#endregion // Fields

	
	/// <summary>Create a new application config attribute.</summary>
	/// <param name="name">Human-readable name for the application.</param>
	/// <param name="folderName">
	/// Name for the application data folder on the filesystem, uses <paramref name="name"/> if <c>null</c>.
	/// </param>
	public AstrumApplicationAttribute(string name, string? folderName = null)
	{
		ArgumentException.ThrowIfNullOrWhiteSpace(name);
		if (folderName is not null) ArgumentException.ThrowIfNullOrWhiteSpace(folderName);
		
		Name = name;
		FolderName = FilesystemUtils.SanitizeName(folderName ?? name, false);
	}
}
