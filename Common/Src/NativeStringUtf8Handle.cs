/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Astrum;


/// <summary>Contains a stack-only handle to an allocated, null-terminated native UTF-8 string.</summary>
/// <remarks>This struct should <em>always</em> be disposed after use, otherwise memory leaks will occur.</remarks>
public unsafe ref struct NativeStringUtf8Handle : IDisposable
{
	#region Fields
	/// <summary>The length of the native string (in bytes), without the null terminator.</summary>
	public uint Length { get; private set; }

	/// <summary>The native string data, or <c>null</c> if uninitialized or disposed.</summary>
	public byte* Data { get; private set; }
	#endregion // Fields

	
	// Private ctor with validated data
	private NativeStringUtf8Handle(uint length, byte* data)
	{
		Length = length;
		Data = data;
	}

	/// <summary>Creates a new native string with UTF-8 encoding from the given character sequence.</summary>
	/// <returns>The native string handle. Must be disposed by the caller.</returns>
	public static NativeStringUtf8Handle Create(in ReadOnlySpan<char> str)
	{
		if (str.IsEmpty) return CreateEmpty();
		
		var len = Encoding.UTF8.GetByteCount(str);
		var data = NativeMemory.Alloc((uint)len + 1);
		try {
			Encoding.UTF8.GetBytes(str, new(data, len));
			((byte*)data)[len] = 0;
			return new((uint)len, (byte*)data);
		}
		catch {
			NativeMemory.Free(data);
			throw;
		}
	}

	/// <summary>Creates a valid but zero-length native string.</summary>
	public static NativeStringUtf8Handle CreateEmpty()
	{
		var data = (byte*)NativeMemory.Alloc(1);
		data[0] = 0;
		return new(0, data);
	}


	#region Base
	public readonly override string ToString() => $"NativeStringUtf8[0x{(nuint)Data:X}]";

	public readonly override int GetHashCode() => ((nuint)Data).GetHashCode();

	/// <summary>Gets a managed string representation of the native string data.</summary>
	public readonly string GetString() => Data != null ? Encoding.UTF8.GetString(Data, (int)Length) : "";
	#endregion
	

	public void Dispose()
	{
		if (Data == null) return;
		NativeMemory.Free(Data);
		Length = 0;
		Data = null;
	}
}
