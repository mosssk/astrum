/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using static Astrum.InternalLogging;

namespace Astrum;


// [Partial] Application time operations
public static partial class Core
{
	/// <summary>Access and control over application timing.</summary>
	public static class Time
	{
		/// <summary>Handler for changes to <see cref="Time.DeltaScale"/>.</summary>
		/// <param name="oldScale">The old global timescale.</param>
		/// <param name="newScale">The new global timescale.</param>
		public delegate void ScaleChangedHandler(float oldScale, float newScale);
		
		
		#region Fields
		/// <summary>The timestamp of when the application started.</summary>
		public static DateTime Start { get; private set; } = DateTime.MinValue;
		
		/// <summary>The current application frame number.</summary>
		public static ulong FrameCount { get; private set; }
		
		/// <summary>The elapsed time since the application start. Updates at the start of each frame.</summary>
		public static TimeSpan Elapsed { get; private set; }
		/// <summary>The elapsed seconds since the application start. Updates at the start of each frame.</summary>
		public static double ElapsedSeconds { get; private set; }
		
		/// <summary>The value of <see cref="Elapsed"/> in the last frame.</summary>
		public static TimeSpan LastElapsed { get; private set; }
		/// <summary>The value of <see cref="ElapsedSeconds"/> in the last frame.</summary>
		public static double LastElapsedSeconds { get; private set; }

		/// <summary>The raw elapsed time since the application start. Updates each time it is queried.</summary>
		public static TimeSpan RawElapsed => _Timer.Elapsed;
		/// <summary>The raw elapsed seconds since the application start. Updates each time it is queried.</summary>
		public static double RawElapsedSeconds => _Timer.Elapsed.TotalSeconds;
		
		/// <summary>The time difference from the last frame. Affected by <see cref="DeltaScale"/>.</summary>
		public static TimeSpan Delta { get; private set; }
		/// <summary>The seconds difference from the last frame. Affected by <see cref="DeltaScale"/>.</summary>
		public static double DeltaSeconds { get; private set; }
		
		/// <summary>The time difference from the last frame. Unaffected by <see cref="DeltaScale"/>.</summary>
		public static TimeSpan RealDelta { get; private set; }
		/// <summary>The seconds difference from the last frame. Unaffected by <see cref="DeltaScale"/>.</summary>
		public static double RealDeltaSeconds { get; private set; }

		/// <summary>The global scaling factor to apply to <see cref="Delta"/> and <see cref="DeltaSeconds"/>.</summary>
		/// <remarks>Update with <see cref="SetDeltaScale"/>.</remarks>
		public static float DeltaScale => _DeltaScale;
		private static float _DeltaScale = 1;
		private static float? _NewDeltaScale;

		/// <summary>Event that is raised on the start of any frame where <see cref="DeltaScale"/> changes.</summary>
		public static event ScaleChangedHandler? OnDeltaScaleChanged;
		
		/// <summary>The FPS of the application, averaged over the last few frames.</summary>
		public static float Fps { get; private set; }
		private static FpsHistory _FpsHistory;
		private static uint _FpsIndex;
		
		// Time tracking stopwatch
		private static readonly Stopwatch _Timer = new();
		#endregion // Fields


		// Initializes the time fields
		internal static void Initialize()
		{
			_Timer.Restart();
			
			Start = DateTime.Now;
			FrameCount = 0;
			Elapsed = LastElapsed = TimeSpan.Zero;
			ElapsedSeconds = LastElapsedSeconds = 0;
			Delta = RealDelta = TimeSpan.Zero;
			DeltaSeconds = RealDeltaSeconds = 0;
			_DeltaScale = 1;
			_NewDeltaScale = null;
			Fps = 0;
			_FpsHistory.AsSpan().Clear();
			_FpsIndex = 0;
		}

		// Updates the time fields at the start of a frame
		internal static void Update()
		{
			FrameCount += 1;

			// Update scale
			if (_NewDeltaScale is { } newScale) {
				var old = _DeltaScale;
				_DeltaScale = newScale;
				_NewDeltaScale = null;
				try {
					OnDeltaScaleChanged?.Invoke(old, newScale);
				}
				catch (Exception ex) {
					LogWarn("Unhandled exception while handling time scale change", ex);
				}
			}
			
			// Update time
			LastElapsed = Elapsed;
			LastElapsedSeconds = ElapsedSeconds;
			Elapsed = _Timer.Elapsed;
			ElapsedSeconds = Elapsed.TotalSeconds;
			RealDelta = Elapsed - LastElapsed;
			RealDeltaSeconds = RealDelta.TotalSeconds;
			Delta = RealDelta * _DeltaScale;
			DeltaSeconds = RealDeltaSeconds * _DeltaScale;
			
			// Update fps
			var history = _FpsHistory.AsSpan();
			history[(int)_FpsIndex] = (float)(1 / RealDeltaSeconds);
			_FpsIndex = (_FpsIndex + 1) % FpsHistory.SIZE;
			var fps = 0f;
			for (var i = 0; i < FpsHistory.SIZE; ++i)
				fps += history[i];
			Fps = fps / UInt64.Min(FpsHistory.SIZE, FrameCount);
		}


		/// <summary>Sets the global scaling factor to apply to <see cref="Delta"/> and <see cref="DeltaSeconds"/>.</summary>
		/// <remarks>Changes to the global scaling factor do not take effect until the next frame.</remarks>
		/// <param name="scale">The new scaling factor, will be clamped to <c>[0.00001, ∞)</c>.</param>
		public static void SetDeltaScale(float scale) => _NewDeltaScale = Single.Max(0.00001f, scale);


		/// <summary>Returns true on the first frame (only) after the given application time has elapsed.</summary>
		/// <param name="span">The elapsed time to check for.</param>
		public static bool IsElapsed(TimeSpan span) => LastElapsed < span && Elapsed >= span;

		/// <summary>Returns true on each first frame after any multiple of the given elapsed time.</summary>
		/// <param name="span">The elapsed time to check multiples for.</param>
		public static bool IsElapsedMultiple(TimeSpan span) =>
			Elapsed.Ticks % span.Ticks < LastElapsed.Ticks % span.Ticks;

		/// <summary>Returns true on each first frame after any multiple of the given elapsed time, with an offset.</summary>
		/// <param name="span">The elapsed time to check multiples for.</param>
		/// <param name="offset">The offset for each multiple in elapsed time to check for.</param>
		public static bool IsElapsedMultiple(TimeSpan span, TimeSpan offset) =>
			(Elapsed.Ticks + offset.Ticks) % span.Ticks < (LastElapsed.Ticks + offset.Ticks) % span.Ticks;


		// Keeps a history of FPS values
		[InlineArray((int)SIZE)]
		private struct FpsHistory
		{
			public const uint SIZE = 10;
			private float _e0;
			
			public Span<float> AsSpan() => MemoryMarshal.CreateSpan(ref this[0], (int)SIZE);
		}
	}
}
