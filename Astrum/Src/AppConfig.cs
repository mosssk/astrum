/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System.Runtime.CompilerServices;
using Astrum.Render;

namespace Astrum;


/// <summary>Contains the full set of configuration options as used by <see cref="ApplicationBuilder"/>.</summary>
internal sealed class AppConfig
{
	/// <summary>Freezes the config (prevent further changes), then returns the config.</summary>
	public AppConfig Freeze()
	{
		_isFrozen = true;
		return this;
	}
	private bool _isFrozen;

	// Updates the given field, only if not frozen
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	private void checkedAssign<T>(ref T field, T value) => field = _isFrozen ? field : value;


	#region Core
	/// <summary>Top-level event handler.</summary>
	public IAppEventMonitor? EventMonitor { get; set => checkedAssign(ref field, value); }
	#endregion // Core


	#region Render
	/// <summary>The graphics device to use (<c>null</c> to use the default).</summary>
	public GraphicsDevice? GraphicsDevice { get; set => checkedAssign(ref field, value); }
	#endregion // Render
}
