/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using Astrum.Render;

namespace Astrum;


/// <summary>Used to configure the core Astrum application before it starts and launches the main loop.</summary>
public sealed class ApplicationBuilder
{
	/// <summary>A callback for accepting a builder object to configure an application with.</summary>
	/// <param name="app">The application config builder.</param>
	public delegate void ConfigureCallback(ApplicationBuilder app);
	
	
	#region Fields
	// The actual configuration object
	private readonly AppConfig _config = new();
	#endregion // Fields


	internal ApplicationBuilder() { }

	/// <summary>Validates the current builder configuration, then returns said configuration.</summary>
	internal AppConfig Build() => _config.Freeze();


	#region Core
	/// <summary>Sets the monitor instance to use for global application events.</summary>
	public ApplicationBuilder UseEventMonitor(IAppEventMonitor monitor)
	{
		_config.EventMonitor = monitor;
		return this;
	}
	#endregion // Core


	#region Render
	/// <summary>Sets the graphics device to use (or <c>null</c> to use the default).</summary>
	public ApplicationBuilder UseDevice(GraphicsDevice? device)
	{
		_config.GraphicsDevice = device;
		return this;
	}
	#endregion // Render
}
