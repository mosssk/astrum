﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System.Collections.Generic;
using Astrum.Render.Vulkan;

namespace Astrum.Render;


/// <summary>Contains the implementation of render/compute operations used by <see cref="RenderCore"/>.</summary>
internal sealed unsafe partial class RenderDriver
{
	#region Fields
	/// <summary>The list of all valid and supported graphics devices on the system.</summary>
	public static IReadOnlyList<GraphicsDevice> AllDevices => _AllDevices ??= CreateDeviceList();
	private static GraphicsDevice[]? _AllDevices;


	/// <summary>The device used by the driver.</summary>
	public readonly GraphicsDevice Device;

	// Cached device info
	private readonly Vma.MemoryTypeIndices _memoryTypes;
	
	// Cached API handles
	private readonly VkApi _vk;
	private readonly Vma _vma;
	private readonly DeviceQueues _queues;
	private readonly Vk.Device _device;
	private readonly Vma.Allocator _allocator;
	#endregion // Fields


	/// <summary>Initializes the rendering backend for the given device.</summary>
	public RenderDriver(GraphicsDevice device)
	{
		var ctx = VkCtx.Get();
		
		Device = device;
		ctx.OpenDevice(device, out _memoryTypes, out _queues);

		_vk = ctx.F;
		_vma = ctx.Vma;
		_device = ctx.Device;
		_allocator = ctx.Allocator;
	}

	/// <summary>Destroys the render driver during application shutdown.</summary>
	public void Destroy()
	{
		VkCtx.Get().CloseDevice();
	}
	

	#region Device
	/// <summary>Creates and populates an array of all support system <see cref="GraphicsDevice"/>s.</summary>
	private static GraphicsDevice[] CreateDeviceList()
	{
		// Get list
		var vk = VkCtx.GetApi();
		uint deviceCount;
		vk.EnumeratePhysicalDevices(VkCtx.Get().Instance, &deviceCount, null);
		var handles = stackalloc Vk.PhysicalDevice[(int)deviceCount];
		vk.EnumeratePhysicalDevices(VkCtx.Get().Instance, &deviceCount, handles);
		
		// Initialize the valid devices
		var devices = new List<GraphicsDevice>((int)deviceCount);
		for (uint di = 0; di < deviceCount; ++di) {
			if (!GraphicsDevice.TryCreate(handles[di], out var device)) continue;
			devices.Add(device);
		}
		devices.Sort(GraphicsDevice.DefaultComparer);
		return devices.ToArray();
	}
	#endregion // Device


	/// <summary>Contains the full set of device queue handles.</summary>
	/// <param name="Main">The main device queue. Always present.</param>
	/// <param name="Dma">The async transfer queue using the DMA engine. May not be present.</param>
	/// <param name="AsyncCompute">The async compute queue using the dedicated compute engine. May not be present.</param>
	public sealed record DeviceQueues(Vk.Queue Main, Vk.Queue Dma, Vk.Queue AsyncCompute)
	{
		/// <summary>If the DMA queue is present.</summary>
		public bool HasDma => Dma.Handle != 0;
		/// <summary>If the async compute queue is present.</summary>
		public bool HasAsyncCompute => AsyncCompute.Handle != 0;
	}
}
