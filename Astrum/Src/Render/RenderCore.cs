﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Astrum.Render;


/// <summary>Primary API surface for render and compute operations.</summary>
/// <remarks>Most API calls to this type will throw <see cref="UninitializedException"/> before initialization.</remarks>
public static class RenderCore
{
	#region Fields
	/// <summary>If the core rendering operations have been initialized and are ready for use.</summary>
	public static bool IsInitialized => _Driver is not null;

	/// <summary>A list of all supported graphics devices on the system.</summary>
	/// <remarks>The first device in the list will always be the "best" one as determined by a simple heuristic.</remarks>
	public static IReadOnlyList<GraphicsDevice> Devices => RenderDriver.AllDevices;
	/// <summary>The default device on the system as determined by a simple heuristic.</summary>
	public static GraphicsDevice DefaultDevice => RenderDriver.AllDevices.FirstOrDefault() 
		?? throw new PlatformNotSupportedException("No supported devices found on the system");

	/// <summary>The device currently in-use for rendering. Throws if uninitialized.</summary>
	public static GraphicsDevice Device => UninitializedException.ReturnOrThrow(_Driver?.Device)!;

	/// <summary>The rendering driver instance. Throws if not initialized.</summary>
	internal static RenderDriver Driver => UninitializedException.ReturnOrThrow(_Driver)!;
	private static RenderDriver? _Driver;
	#endregion // Fields


	#region Lifecycle
	/// <summary>Called to initialize the core rendering functionality during application initialization.</summary>
	internal static void Initialize(AppConfig config)
	{
		// Create the driver
		_Driver = new(config.GraphicsDevice ?? DefaultDevice);
	}

	/// <summary>Called to clean up the core rendering functionality during application shutdown.</summary>
	internal static void Shutdown()
	{
		_Driver?.Destroy();
	}
	#endregion // Lifecycle


	/// <summary>An exception that indicates <see cref="RenderCore"/> was accessed before it was initialized.</summary>
	public sealed class UninitializedException()
		: InvalidOperationException("RenderCore cannot be accessed until it has been initialized")
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		internal static T ReturnOrThrow<T>(T value) => IsInitialized ? value : throw new UninitializedException();
	}
}
