﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System.Runtime.InteropServices;
using Astrum.Native;
using Silk.NET.Core;
#pragma warning disable CS0649 // Field is never assigned to, and will always have its default value

namespace Astrum.Render.Vulkan;


/// <summary>NativeApi instance for the Vulkan Memory Allocator (VMA) functions in the native runtime.</summary>
internal sealed unsafe class Vma() : NativeApi("astrum-runtime", typeof(NativeApi).Assembly)
{
	#region Fields
	[NativeSymbol("VmaCreateAllocator")]
	public readonly delegate* unmanaged[Cdecl]<
		Vk.Instance, Vk.PhysicalDevice, Vk.Device, PfnVoidFunction, PfnVoidFunction, Allocator*, Vk.Result> CreateAllocator;
	
	[NativeSymbol("VmaDestroyAllocator")]
	public readonly delegate* unmanaged[Cdecl]<Allocator, void> DestroyAllocator;

	[NativeSymbol("VmaFindMemoryTypes")]
	public readonly delegate* unmanaged[Cdecl]<Allocator, MemoryTypeIndices*, void> FindMemoryTypes;
	
	[NativeSymbol("VmaCreateImage")]
	public readonly delegate* unmanaged[Cdecl]<
		Allocator, Vk.ImageCreateInfo*, uint, Bool32, Vk.Image*, Allocation*, Vk.Result> CreateImage;
	
	[NativeSymbol("VmaDestroyImage")]
	public readonly delegate* unmanaged[Cdecl]<Allocator, Vk.Image, Allocation, void> DestroyImage;

	[NativeSymbol("VmaCreateBuffer")]
	public readonly delegate* unmanaged[Cdecl]<
		Allocator, Vk.BufferCreateInfo*, uint, Bool32, Bool32, Vk.Buffer*, Allocation*, void**, Vk.Result> CreateBuffer;
	
	[NativeSymbol("VmaDestroyBuffer")]
	public readonly delegate* unmanaged[Cdecl]<Allocator, Vk.Buffer, Allocation, void> DestroyBuffer;
	#endregion // Fields
	
	
	/// <summary>Contains the discovered memory type indices.</summary>
	[StructLayout(LayoutKind.Explicit, Size = 20)]
	public struct MemoryTypeIndices
	{
		[FieldOffset(0)]  public uint Device;
		[FieldOffset(4)]  public uint Upload;
		[FieldOffset(8)]  public uint Readback;
		[FieldOffset(12)] public uint Dynamic;
		[FieldOffset(16)] public uint Lazy;
	}

	/// <summary>VmaAllocator instance handle.</summary>
	[StructLayout(LayoutKind.Explicit, Size = 8)]
	public readonly record struct Allocator
	{
		[FieldOffset(0)] public readonly nint Handle;
		
		public static implicit operator bool (Allocator allocator) => allocator.Handle != 0;
	}

	/// <summary>VmaAllocation instance handle.</summary>
	[StructLayout(LayoutKind.Explicit, Size = 8)]
	public readonly record struct Allocation
	{
		[FieldOffset(0)] public readonly nint Handle;
		
		public static implicit operator bool (Allocation allocation) => allocation.Handle != 0;
	}
}
