/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Runtime.CompilerServices;

namespace Astrum.Render.Vulkan;


/// <summary>Extension functionality for Silk.NET Vulkan types.</summary>
internal static class VulkanExtensions
{
	/// <summary>Gets if the result is equal to <see cref="Vk.Result.Success"/>.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool IsSuccess(this Vk.Result result) => result == Vk.Result.Success;
	
	/// <summary>Gets if the result is equal to <see cref="Vk.Result.Success"/>, also providing the result.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool IsSuccess(this Vk.Result result, out Vk.Result outResult) => (outResult = result) != Vk.Result.Success;

	/// <summary>Throws if the given result is not <see cref="Vk.Result.Success"/>.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void ThrowIfNotSuccess(this Vk.Result result, string? op = null)
	{
		if (result == Vk.Result.Success) return;
		throw op is not null ? throw new VulkanException(result, op) : new VulkanException(result);
	}

	/// <summary>Throws if the given result is not <see cref="Vk.Result.Success"/>, after invoking an error handler.</summary>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void ThrowIfNotSuccess(this Vk.Result result, Action<Vk.Result> handler, string? op = null)
	{
		if (result == Vk.Result.Success) return;
		try {
			handler(result);
		}
		catch { }
		throw op is not null ? throw new VulkanException(result, op) : new VulkanException(result);
	}
}
