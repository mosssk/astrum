﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */


global using Vk = Silk.NET.Vulkan;
global using VkApi = Silk.NET.Vulkan.Vk;
global using VkKhr = Silk.NET.Vulkan.Extensions.KHR;
global using VkExt = Silk.NET.Vulkan.Extensions.EXT;
global using VkMvk = Silk.NET.Vulkan.Extensions.MVK;

using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Silk.NET.Core;
using static Astrum.InternalLogging;

namespace Astrum.Render.Vulkan;


/// <summary>Provides the instance-level and API objects for the Vulkan runtime.</summary>
internal sealed unsafe class VkCtx
{
	// Maximum object name length for debugging
	private const uint MAX_NAME_LENGTH = 64;
	
	
	#region Fields
	/// <summary>Gets the global context, initializing it on first access (if Vulkan is available).</summary>
	/// <exception cref="PlatformNotSupportedException">The platform does not support the Vulkan backend.</exception>
	[MethodImpl(MethodImplOptions.Synchronized)]
	public static VkCtx Get() => _Instance ??= CreateInstanceOrThrow();
	private static VkCtx? _Instance;

	/// <summary>Gets the global Vulkan function vtable. Shorthand for <c>Get().F</c>.</summary>
	/// <exception cref="PlatformNotSupportedException">The platform does not support the Vulkan backend.</exception>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static VkApi GetApi() => Get().F;
	
	// Thread-specific string construction
	private static readonly ThreadLocal<StringBuilder> _StringBuilders = new(() => new(1_024), false);


	/// <summary>The core Vulkan native API object.</summary>
	public readonly VkApi F;

	/// <summary>The VMA native API object.</summary>
	public readonly Vma Vma;

	/// <summary>The Vulkan instance object.</summary>
	public readonly Vk.Instance Instance;
	
	/// <summary>The logical device handle (invalid until opened).</summary>
	public Vk.Device Device { get; private set; }

	/// <summary>The swapchain extension API object (invalid until opened).</summary>
	public VkKhr.KhrSwapchain? Swapchain { get; private set; }
	
	/// <summary>The VMA allocator instance (invalid until opened).</summary>
	public Vma.Allocator Allocator { get; private set; }
	
	// Debugging objects
	private readonly VkExt.ExtDebugUtils? _debug;
	private readonly Vk.DebugUtilsMessengerEXT _debugMsg;
	private readonly Vk.PfnDebugUtilsMessengerCallbackEXT _debugCallback;
	private readonly StreamWriter? _debugFile;
	private readonly DateTime _startTime;
	#endregion // Fields


	private VkCtx(VkApi f, VkExt.ExtDebugUtils? debug)
	{
		F = f;
		Instance = f.CurrentInstance!.Value;
		Vma = new();
		_debug = debug;
		_startTime = DateTime.UtcNow;
		
		// Initialize the debugging if present
		if (debug is not null) {
			// Open the debug file
			try {
				_debugFile = new(File.Open("./vulkan-debug.log", FileMode.Create, FileAccess.Write, FileShare.Read));
			}
			catch (Exception ex) {
				LogError("Could not open Vulkan debug file, validation logging is disabled", ex);
			}
			
			// Create messenger
			_debugCallback = new(debugMessengerCallback);
			Vk.DebugUtilsMessengerCreateInfoEXT debugMsgInfo = new() {
				SType = Vk.StructureType.DebugUtilsMessengerCreateInfoExt,
				MessageSeverity = 
					Vk.DebugUtilsMessageSeverityFlagsEXT.InfoBitExt |
					Vk.DebugUtilsMessageSeverityFlagsEXT.WarningBitExt |
					Vk.DebugUtilsMessageSeverityFlagsEXT.ErrorBitExt,
				MessageType = 
					Vk.DebugUtilsMessageTypeFlagsEXT.GeneralBitExt |
					Vk.DebugUtilsMessageTypeFlagsEXT.PerformanceBitExt |
					Vk.DebugUtilsMessageTypeFlagsEXT.ValidationBitExt,
				PfnUserCallback = _debugCallback
			};
			if (!debug.CreateDebugUtilsMessenger(Instance, &debugMsgInfo, null, out _debugMsg).IsSuccess(out var res)) {
				LogError($"Failed to create Vulkan validation messenger (res={res})");
				_debugMsg = default;
			}
		}
		
		// Register self for cleanup
		AppDomain.CurrentDomain.ProcessExit += (_, _) => destroy();
	}
	
	// Destroys the context objects
	private void destroy()
	{
		CloseDevice();
		if (_debug is not null) {
			if (_debugMsg.Handle != 0) 
				_debug.DestroyDebugUtilsMessenger(Instance, _debugMsg, null);
			_debugCallback.Dispose();
			_debugFile?.Dispose();
			_debug.Dispose();
		}
		Vma.Dispose();
		F.DestroyInstance(Instance, null);
		F.Dispose();
	}


	#region Device
	/// <summary>Creates and initializes the logical device context.</summary>
	[SkipLocalsInit]
	public void OpenDevice(GraphicsDevice device, out Vma.MemoryTypeIndices memoryTypes, out RenderDriver.DeviceQueues queues)
	{
		// Setup extensions
		using var extListNative = NativeStringArrayUtf8Handle.Create(VkKhr.KhrSwapchain.ExtensionName);

		// Setup features
		Vk.PhysicalDeviceVulkan12Features f12 = new() {
			SType = Vk.StructureType.PhysicalDeviceVulkan12Features,
			TimelineSemaphore = true,
			ScalarBlockLayout = true,
			BufferDeviceAddress = true,
			ImagelessFramebuffer = true,
			RuntimeDescriptorArray = true,
			DescriptorBindingPartiallyBound = true,
			DescriptorBindingUpdateUnusedWhilePending = true,
			DescriptorBindingSampledImageUpdateAfterBind = true,
			DescriptorBindingStorageImageUpdateAfterBind = true,
			DescriptorBindingStorageBufferUpdateAfterBind = true,
			ShaderSampledImageArrayNonUniformIndexing = true,
			ShaderStorageImageArrayNonUniformIndexing = true,
			ShaderStorageBufferArrayNonUniformIndexing = true
		};
		Vk.PhysicalDeviceVulkan11Features f11 = new() {
			SType = Vk.StructureType.PhysicalDeviceVulkan11Features,
			PNext = &f12,
			ShaderDrawParameters = true
		};
		Vk.PhysicalDeviceFeatures2 f10 = new() {
			SType = Vk.StructureType.PhysicalDeviceFeatures2,
			PNext = &f11,
			Features = new() {
				IndependentBlend = true,
				FillModeNonSolid = device.Features.FillModeNonSolid,
				WideLines = device.Features.WideLines,
				LargePoints = device.Features.LargePoints
			}
		};
		
		// Setup queues
		float queuePriority = 1;
		uint queueCount = 0;
		var queuePtr = stackalloc Vk.DeviceQueueCreateInfo[3];
		{
			queuePtr[queueCount++] = new() {
				SType = Vk.StructureType.DeviceQueueCreateInfo,
				PQueuePriorities = &queuePriority, QueueCount = 1, QueueFamilyIndex = device.Vulkan.MainQueue
			};
			if (device.Vulkan.DmaQueue is { } dmaQueue) {
				queuePtr[queueCount++] = new() {
					SType = Vk.StructureType.DeviceQueueCreateInfo,
					PQueuePriorities = &queuePriority, QueueCount = 1, QueueFamilyIndex = dmaQueue
				};
			}
			if (device.Vulkan.AsyncComputeQueue is { } acQueue) {
				queuePtr[queueCount++] = new() {
					SType = Vk.StructureType.DeviceQueueCreateInfo,
					PQueuePriorities = &queuePriority, QueueCount = 1, QueueFamilyIndex = acQueue
				};
			}
		}
		
		// Create the device
		Vk.DeviceCreateInfo createInfo = new() {
			SType = Vk.StructureType.DeviceCreateInfo,
			PNext = &f10,
			EnabledExtensionCount = extListNative.Count,
			PpEnabledExtensionNames = extListNative.Data,
			QueueCreateInfoCount = queueCount,
			PQueueCreateInfos = queuePtr
		};
		F.CreateDevice(device.Vulkan.Handle, &createInfo, null, out var deviceHandle).ThrowIfNotSuccess("Create device");
		F.CurrentDevice = Device = deviceHandle;
		
		// Load the extensions
		_ = F.TryGetDeviceExtension(Instance, Device, out VkKhr.KhrSwapchain swapchainApi);
		Swapchain = swapchainApi;
		
		// Create the memory allocator
		Vma.Allocator allocatorHandle;
		var vkGetInstanceProcAddr = F.GetInstanceProcAddr(Instance, "vkGetInstanceProcAddr");
		var vkGetDeviceProcAddr = F.GetInstanceProcAddr(Instance, "vkGetDeviceProcAddr");
		Vma.CreateAllocator(Instance, device.Vulkan.Handle, Device, vkGetInstanceProcAddr, vkGetDeviceProcAddr, 
			&allocatorHandle).ThrowIfNotSuccess(_ => {
				CloseDevice();
			}, "Create VMA allocator");
		Allocator = allocatorHandle;
		
		// Load the device memory types
		fixed (Vma.MemoryTypeIndices* memPtr = &memoryTypes)
			Vma.FindMemoryTypes(Allocator, memPtr);
		
		// Get the queue handles
		Vk.Queue mainQueueHandle, dmaQueueHandle = default, acQueueHandle = default;
		{
			F.GetDeviceQueue(Device, device.Vulkan.MainQueue, 0, &mainQueueHandle);
			if (device.Vulkan.DmaQueue is { } dmaQueue) 
				F.GetDeviceQueue(Device, dmaQueue, 0, &dmaQueueHandle);
			if (device.Vulkan.AsyncComputeQueue is { } acQueue)
				F.GetDeviceQueue(Device, acQueue, 0, &acQueueHandle);
		}
		queues = new(mainQueueHandle, dmaQueueHandle, acQueueHandle);
	}

	/// <summary>Destroys the logical device context.</summary>
	public void CloseDevice()
	{
		if (Allocator) Vma.DestroyAllocator(Allocator);
		Swapchain?.Dispose();
		if (Device.Handle != 0) F.DestroyDevice(Device, null);
	}
	#endregion // Device
	
	
	#region Debugging
	// Callback for the debug utils messenger
	[SkipLocalsInit]
	private uint debugMessengerCallback(
		Vk.DebugUtilsMessageSeverityFlagsEXT severity,
		Vk.DebugUtilsMessageTypeFlagsEXT types,
		Vk.DebugUtilsMessengerCallbackDataEXT* pData,
		void* pUserData
	) {
		if (_debugFile is null) return VkApi.True;
		var sb = _StringBuilders.Value!;
		
		// Write the elapsed time
		var time = DateTime.UtcNow - _startTime;
		sb.Append($"[{time:hh:mm:ss.fff}]");
		
		// Write the message metadata
		sb.Append('[');
		sb.Append(severity switch {
			Vk.DebugUtilsMessageSeverityFlagsEXT.InfoBitExt    => 'I',
			Vk.DebugUtilsMessageSeverityFlagsEXT.WarningBitExt => 'W',
			Vk.DebugUtilsMessageSeverityFlagsEXT.ErrorBitExt   => 'E',
			_                                                  => '?'
		});
		sb.Append('|');
		sb.Append(types switch {
			Vk.DebugUtilsMessageTypeFlagsEXT.GeneralBitExt     => 'G',
			Vk.DebugUtilsMessageTypeFlagsEXT.PerformanceBitExt => 'P',
			Vk.DebugUtilsMessageTypeFlagsEXT.ValidationBitExt  => 'V',
			_                                                  => '?'
		});
		sb.Append("] ");
		
		// Append the primary object name and type, if available
		var objPtr = pData->PObjects;
		if (pData->ObjectCount > 0 && objPtr->PObjectName != null) {
			Span<char> nameChars = stackalloc char[(int)MAX_NAME_LENGTH];
			var len = Encoding.UTF8.GetChars(SpanUtils.CreateCStringSpan(objPtr->PObjectName), nameChars);
			sb.Append('(').Append(nameChars[..len]).Append(':').Append(objPtr->ObjectType).Append(") ");
		}
		
		// Append message
		var msgSpan = SpanUtils.CreateCStringSpan(pData->PMessage);
		var msgLen = Encoding.UTF8.GetCharCount(msgSpan);
		var msgChars = ArrayPool<char>.Shared.Rent(msgLen);
		try {
			var len = Encoding.UTF8.GetChars(msgSpan, msgChars);
			sb.Append(msgChars.AsSpan(0, len));
		}
		finally {
			ArrayPool<char>.Shared.Return(msgChars);
		}
		
		// Write the message to the debug file
		lock (_debugFile) _debugFile.WriteLine(sb);

		return VkApi.True;
	}
	#endregion // Debugging
	
	
	// Performs instance-level initialization into a new context object
	private static VkCtx CreateInstanceOrThrow()
	{
		const string VALIDATION_LAYER_NAME = "VK_LAYER_KHRONOS_validation";
		const string VALIDATION_DISABLE_ENV = "PRYSM_DISABLE_VALIDATION";
		
		// Get the global API and check the runtime version
		var vk = VkApi.GetApi();
		uint instVer;
		if (!vk.EnumerateInstanceVersion(&instVer).IsSuccess() || instVer < VkApi.Version12)
			throw new PlatformNotSupportedException("The Vulkan 1.2 API is not supported on this platform");
		
		// Check instance extensions
		var surfaceExtName = getPlatformSurfaceNameOrThrow();
		if (!vk.IsInstanceExtensionPresent(VkKhr.KhrSurface.ExtensionName) ||
			!vk.IsInstanceExtensionPresent(surfaceExtName))
			throw new PlatformNotSupportedException("No surface support detected (are you running headless?)");
		var hasDebug = vk.IsInstanceExtensionPresent(VkExt.ExtDebugUtils.ExtensionName);
		
		// Build instance extensions list
		List<string> extList = [VkKhr.KhrSurface.ExtensionName, surfaceExtName];

		// Build layer list
		var hasValidationLayer = false;
#if DEVELOP
		if (hasDebug) {
			extList.Add(VkExt.ExtDebugUtils.ExtensionName);
			uint layerCount;
			if (Environment.GetEnvironmentVariable(VALIDATION_DISABLE_ENV) is null or "0" &&
				vk.EnumerateInstanceLayerProperties(&layerCount, null).IsSuccess()) 
			{
				var layers = stackalloc Vk.LayerProperties[(int)layerCount];
				vk.EnumerateInstanceLayerProperties(&layerCount, layers);
				for (var li = 0; li < layerCount; ++li) {
					if (Marshal.PtrToStringUTF8((nint)layers[li].LayerName) != VALIDATION_LAYER_NAME) continue;
					hasValidationLayer = true;
					break;
				}
			}
		}
#else
		hasDebug = false;
#endif
		
		// Describe application
		using var engNameNative = NativeStringUtf8Handle.Create("Astrum");
		using var appNameNative = NativeStringUtf8Handle.Create(Astrum.ApplicationName);
		var appInfo = new Vk.ApplicationInfo {
			SType = Vk.StructureType.ApplicationInfo,
			ApiVersion = VkApi.Version12,
			PApplicationName = appNameNative.Data,
			ApplicationVersion = (Version32)Astrum.ApplicationVersion,
			PEngineName = engNameNative.Data,
			EngineVersion = (Version32)Astrum.EngineVersion
		};
		
		// Create instance
		using var extListNative = NativeStringArrayUtf8Handle.Create(CollectionsMarshal.AsSpan(extList));
		using var layerListNative = hasValidationLayer 
			? NativeStringArrayUtf8Handle.Create(VALIDATION_LAYER_NAME) : default;
		var instInfo = new Vk.InstanceCreateInfo {
			SType = Vk.StructureType.InstanceCreateInfo,
			PApplicationInfo = &appInfo,
			PpEnabledExtensionNames = extListNative.Data,
			EnabledExtensionCount = extListNative.Count,
			PpEnabledLayerNames = layerListNative.Data,
			EnabledLayerCount = layerListNative.Count
		};
		Vk.Instance instance;
		vk.CreateInstance(&instInfo, null, &instance).ThrowIfNotSuccess("Create global instance");
		vk.CurrentInstance = instance;

		// Load the debug utils if requested
		VkExt.ExtDebugUtils? debug = null;
		if (hasDebug && !vk.TryGetInstanceExtension(instance, out debug)) 
			LogError("Failed to load Vulkan debugging extension, validation will not be available");

		// Return the created context
		return new(vk, debug);

		// Gets the platform surface extension name, or throws if not supported
		static string getPlatformSurfaceNameOrThrow()
		{
			if (OperatingSystem.IsWindows()) return VkKhr.KhrWin32Surface.ExtensionName;
			if (OperatingSystem.IsAndroid()) return VkKhr.KhrAndroidSurface.ExtensionName;
			if (OperatingSystem.IsLinux()) return VkKhr.KhrXcbSurface.ExtensionName; // TODO: Wayland maybe 
			if (OperatingSystem.IsMacOS()) return VkMvk.MvkMacosSurface.ExtensionName;
			if (OperatingSystem.IsIOS()) return VkMvk.MvkIosSurface.ExtensionName;
			throw new PlatformNotSupportedException("The current platform is not supported by Astrum");
		}
	}
}
