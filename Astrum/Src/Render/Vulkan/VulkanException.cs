/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Render.Vulkan;


/// <summary>Exception generated when a Vulkan call fails.</summary>
internal sealed class VulkanException : Exception
{
	#region Fields
	/// <summary>The result provided by the failed Vulkan call.</summary>
	public readonly Vk.Result Result;
	#endregion // Fields
	
	
	/// <summary>Create an exception from a result code only.</summary>
	public VulkanException(Vk.Result result) : base($"Vulkan call failed with error '{result}'") => Result = result;

	/// <summary>Create an exception from a result code and failed operation description.</summary>
	public VulkanException(Vk.Result result, string operation) : base(
		$"Vulkan call failed with error '{result}' during: {operation}") => Result = result;
}
