﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Astrum.Render.Vulkan;
using Silk.NET.Core;
using static Astrum.InternalLogging;

namespace Astrum.Render;


/// <summary>Represents a graphics and compute device on the system.</summary>
public sealed unsafe class GraphicsDevice
{
	/// <summary>The default comparer for ordering graphics devices from best to worst.</summary>
	/// <remarks>The ordering is simple, putting all discrete devices higher than others, then sorting by VRAM.</remarks>
	public static readonly IComparer<GraphicsDevice> DefaultComparer = new DefaultOrdering();
	
	
	#region Fields
	/// <summary>The reported human-readable device name.</summary>
	public readonly string Name;
	
	/// <summary>The hardware implementation kind for the device.</summary>
	public readonly DeviceKind Kind;

	/// <summary>The device vendor.</summary>
	public readonly VendorId Vendor;
	
	/// <summary>The version of the device driver.</summary>
	public Version DriverVersion => Vulkan.DriverVersion;

	/// <summary>Provides a constant UUID that will always uniquely identify the device, even across reboots.</summary>
	public Guid Uuid {
		get { var uuid = Vulkan.DeviceUuid; return Unsafe.As<UInt128, Guid>(ref uuid); }
	}
	
	/// <summary>Optional feature set supported by the device.</summary>
	public readonly FeatureSet Features;

	/// <summary>Set of device resource limits.</summary>
	public readonly LimitSet Limits;
	
	/// <summary>Internal Vulkan-specific device info.</summary>
	internal readonly VulkanInfo Vulkan;
	#endregion // Fields


	private GraphicsDevice(string name, DeviceKind kind, VendorId vendor, FeatureSet features, LimitSet limits, 
		VulkanInfo vulkan)
	{
		Name = name;
		Kind = kind;
		Vendor = vendor;
		Features = features;
		Limits = limits;
		Vulkan = vulkan;
	}

	/// <summary>Attempts to initialize the device from a handle, returning <c>false</c> if not supported.</summary>
	internal static bool TryCreate(Vk.PhysicalDevice handle, out GraphicsDevice device)
	{
		// Get the device properties
		getProperties(handle, out var name, out var kind, out var vendor, out var limits, out var vulkan);
		
		// Check the features
		if (checkFeatures(handle, vulkan, out var features) is { } missingFeatures) {
			LogInfo($"Skipping unsupported device '{name}', missing features: {String.Join(", ", missingFeatures)}");
			device = null!;
			return false;
		}
		
		// Create device
		device = new(name, kind, vendor, features, limits, vulkan);
		return true;

		//
		[SkipLocalsInit]
		static void getProperties(Vk.PhysicalDevice handle, out string name, out DeviceKind kind, out VendorId vendor, 
			out LimitSet limits, out VulkanInfo vulkan)
		{
			var vk = VkCtx.GetApi();
			
			// Load the properties
			Vk.PhysicalDeviceVulkan12Properties p12 = new() {
				SType = Vk.StructureType.PhysicalDeviceVulkan12Properties
			};
			Vk.PhysicalDeviceVulkan11Properties p11 = new() {
				SType = Vk.StructureType.PhysicalDeviceVulkan11Properties, PNext = &p12
			};
			Vk.PhysicalDeviceProperties2 p10 = new() {
				SType = Vk.StructureType.PhysicalDeviceProperties2, PNext = &p11
			};
			vk.GetPhysicalDeviceProperties2(handle, &p10);
			ref readonly var l10 = ref p10.Properties.Limits;
			
			// Load the queues
			uint queueCount;
			vk.GetPhysicalDeviceQueueFamilyProperties(handle, &queueCount, null);
			var queuePtr = stackalloc Vk.QueueFamilyProperties[(int)queueCount];
			vk.GetPhysicalDeviceQueueFamilyProperties(handle, &queueCount, queuePtr);
			uint? mainQueueFamily = null, dmaQueueFamily = null, acQueueFamily = null;
			for (uint qi = 0; qi < queueCount; ++qi) {
				var flags = queuePtr[qi].QueueFlags;
				if      (flags.HasFlag(Vk.QueueFlags.GraphicsBit)) mainQueueFamily ??= qi;
				else if (flags.HasFlag(Vk.QueueFlags.ComputeBit))  acQueueFamily ??= qi;
				else if (flags.HasFlag(Vk.QueueFlags.TransferBit)) dmaQueueFamily ??= qi;
			}
			
			// Load the memory 
			ulong deviceMemory = 0;
			Vk.PhysicalDeviceMemoryProperties memory;
			vk.GetPhysicalDeviceMemoryProperties(handle, &memory);
			for (var mi = 0; mi < memory.MemoryHeapCount; ++mi) {
				var heap = memory.MemoryHeaps[mi];
				if (heap.Flags.HasFlag(Vk.MemoryHeapFlags.DeviceLocalBit) && heap.Size > deviceMemory)
					deviceMemory = heap.Size;
			}
			
			// Read the properties
			name = Marshal.PtrToStringUTF8((nint)p10.Properties.DeviceName)!;
			kind = p10.Properties.DeviceType switch {
				Vk.PhysicalDeviceType.IntegratedGpu => DeviceKind.Integrated,
				Vk.PhysicalDeviceType.DiscreteGpu   => DeviceKind.Discrete,
				Vk.PhysicalDeviceType.VirtualGpu    => DeviceKind.Virtual,
				Vk.PhysicalDeviceType.Cpu           => DeviceKind.Cpu,
				_                                   => DeviceKind.Unknown
			};
			vendor = (VendorId)p10.Properties.VendorID;
			limits = new() {
				RenderTargetSize = (l10.MaxFramebufferWidth, l10.MaxFramebufferHeight),
				Texture1DSize = l10.MaxImageDimension1D,
				Texture2DSize = l10.MaxImageDimension2D,
				Texture3DSize = l10.MaxImageDimension3D,
				TextureLayers = l10.MaxImageArrayLayers,
				TextureCubeSize = l10.MaxImageDimensionCube,
				DeviceLocalMemory = deviceMemory
			};
			vulkan = new() {
				Handle = handle,
				DriverVersion = (Version32)p10.Properties.DriverVersion,
				ApiVersion = (Version32)p10.Properties.ApiVersion,
				PipelineCacheUuid = Unsafe.As<byte, UInt128>(ref p10.Properties.PipelineCacheUuid[0]),
				DeviceUuid = Unsafe.As<byte, UInt128>(ref p11.DeviceUuid[0]),
				DriverUuid = Unsafe.As<byte, UInt128>(ref p11.DriverUuid[0]),
				UniformAlignment = (uint)p10.Properties.Limits.MinUniformBufferOffsetAlignment,
				StorageAlignment = (uint)p10.Properties.Limits.MinStorageBufferOffsetAlignment,
				MainQueue = mainQueueFamily!.Value,
				DmaQueue = dmaQueueFamily,
				AsyncComputeQueue = acQueueFamily
			};
		}

		//
		[SkipLocalsInit]
		static IReadOnlyList<string>? checkFeatures(Vk.PhysicalDevice handle, VulkanInfo vulkan, out FeatureSet features)
		{
			var vk = VkCtx.GetApi();
			
			// Load the device features
			Vk.PhysicalDeviceVulkan12Features f12 = new() { SType = Vk.StructureType.PhysicalDeviceVulkan12Features };
			Vk.PhysicalDeviceVulkan11Features f11 = new() {
				SType = Vk.StructureType.PhysicalDeviceVulkan11Features, PNext = &f12
			};
			Vk.PhysicalDeviceFeatures2 f10 = new() { SType = Vk.StructureType.PhysicalDeviceFeatures2, PNext = &f11 };
			vk.GetPhysicalDeviceFeatures2(handle, &f10);
		
			// Validate the device features
			List<string>? missing = null;
			checkFeature(f10.Features.IndependentBlend, "IndependentBlend", ref missing);
			checkFeature(f11.ShaderDrawParameters, "DrawParameters", ref missing);
			checkFeature(f12.TimelineSemaphore, "TimelineSemaphore", ref missing);
			checkFeature(f12.ScalarBlockLayout, "ScalarBlockLayout", ref missing);
			checkFeature(f12.BufferDeviceAddress, "BufferDeviceAddress", ref missing);
			checkFeature(f12.ImagelessFramebuffer, "ImagelessFramebuffer", ref missing);
			checkFeature(f12.RuntimeDescriptorArray, "RuntimeDescriptorArray", ref missing);
			checkFeature(f12.DescriptorBindingPartiallyBound, "DescriptorPartiallyBound", ref missing);
			checkFeature(f12.DescriptorBindingUpdateUnusedWhilePending, "DescriptorUpdateWhilePending", ref missing);
			checkFeature(f12.DescriptorBindingSampledImageUpdateAfterBind, "SampledImageUAB", ref missing);
			checkFeature(f12.DescriptorBindingStorageImageUpdateAfterBind, "StorageImageUAB", ref missing);
			checkFeature(f12.DescriptorBindingStorageBufferUpdateAfterBind, "StorageBufferUAB", ref missing);
			checkFeature(f12.ShaderSampledImageArrayNonUniformIndexing, "SampledImageNUI", ref missing);
			checkFeature(f12.ShaderStorageImageArrayNonUniformIndexing, "StorageImageNUI", ref missing);
			checkFeature(f12.ShaderStorageBufferArrayNonUniformIndexing, "StorageBufferNUI", ref missing);
			
			// Validate required extensions
			checkFeature(vk.IsDeviceExtensionPresent(handle, VkKhr.KhrSwapchain.ExtensionName), "Swapchain", ref missing);

			// Setup device features
			features = new() {
				WideLines = f10.Features.WideLines,
				LargePoints = f10.Features.LargePoints,
				FillModeNonSolid = f10.Features.FillModeNonSolid,
				AsyncTransferQueue = vulkan.DmaQueue.HasValue,
				AsyncComputeQueue = vulkan.AsyncComputeQueue.HasValue
			};
			return missing;
			
			//
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			static void checkFeature(bool feature, string featureName, ref List<string>? missing)
			{
				if (!feature) (missing ??= []).Add(featureName);
			}
		}
	}


	#region Base
	public override string ToString() => Name;

	public override int GetHashCode() => Vulkan.DeviceUuid.GetHashCode();

	public override bool Equals(object? obj) => ReferenceEquals(this, obj);
	#endregion // Base
	
	
	/// <summary>Device implementation kinds.</summary>
	public enum DeviceKind
	{
		/// <summary>The device has an unknown or uncategorized implementation.</summary>
		Unknown,
		/// <summary>Dedicated hardware processor tightly coupled with the host processor.</summary>
		Integrated,
		/// <summary>Dedicated hardware processor uncoupled from the host processor.</summary>
		Discrete,
		/// <summary>Device is a virtual or otherwise abstracted from a physical device.</summary>
		Virtual,
		/// <summary>The host processor is acting as the GPU.</summary>
		Cpu
	}
	
	
	/// <summary>Known device vendor IDs.</summary>
	/// <remarks>
	/// This enum uses the vendor ID registered with the PCI SIG. The most common vendors are defined in the enum, but
	/// the <c>uint</c> numeric value of the vendor can be checked against the official database for any vendor. 
	/// </remarks>
	public enum VendorId : uint
	{
		/// <summary>Unknown or unrecognized vendor.</summary>
		Unknown  = 0,
		/// <summary>AMD - Advanced Micro Devices</summary>
		Amd      = 0x1022,
		/// <summary>Apple</summary>
		Apple    = 0x106B,
		/// <summary>ARM</summary>
		Arm      = 0x13B5,
		/// <summary>Broadcom Inc.</summary>
		Broadcom = 0x14E4,
		/// <summary>Google</summary>
		Google   = 0x1AE0,
		/// <summary>Imagination Technologies</summary>
		ImgTec   = 0x1010,
		/// <summary>Intel</summary>
		Intel    = 0x8086,
		/// <summary>Mesa drivers</summary>
		Mesa     = 0x10005,
		/// <summary>Nvidia</summary>
		Nvidia   = 0x10DE,
		/// <summary>Qualcomm</summary>
		Qualcomm = 0x17CB,
		/// <summary>Samsung</summary>
		Samsung  = 0x144D
	}


	/// <summary>Graphics device resource limit values.</summary>
	public sealed record LimitSet
	{
		/// <summary>The maximum dimensions for render targets.</summary>
		public required (uint Width, uint Height) RenderTargetSize { get; init; }

		/// <summary>The maximum dimensions for 1D textures.</summary>
		public required uint Texture1DSize { get; init; }
		/// <summary>The maximum dimensions for 2D textures.</summary>
		public required uint Texture2DSize { get; init; }
		/// <summary>The maximum dimensions for 3D textures.</summary>
		public required uint Texture3DSize { get; init; }
		/// <summary>The maximum number of layers in array textures.</summary>
		public required uint TextureLayers { get; init; }
		/// <summary>The maximum dimensions for cube textures.</summary>
		public required uint TextureCubeSize { get; init; }
		
		/// <summary>The total memory (in bytes) available locally to the device (as VRAM).</summary>
		/// <remarks>Note that for integrated GPUs, this will likely be the system RAM shared with the CPU.</remarks>
		public required ulong DeviceLocalMemory { get; init; }

		internal LimitSet() { }
	}
	
	
	/// <summary>Collection of optional features supported by graphics devices.</summary>
	public readonly record struct FeatureSet
	{
		#region Fields
		// Raw packed 
		private readonly uint _packed;

		/// <summary>If line widths other than 1.0 are supported.</summary>
		public bool WideLines {
			get => (_packed & 0x1) != 0;
			init => _packed = value ? (_packed | 0x1) : (_packed & ~0x1u);
		}

		/// <summary>If point sizes other than 1.0 are supported.</summary>
		public bool LargePoints {
			get => (_packed & 0x2) != 0;
			init => _packed = value ? (_packed | 0x2) : (_packed & ~0x2u);
		}

		/// <summary>If point and wireframe polygon fill modes are supported.</summary>
		public bool FillModeNonSolid {
			get => (_packed & 0x4) != 0;
			init => _packed = value ? (_packed | 0x4) : (_packed & ~0x4u);
		}

		/// <summary>If the device supports a dedicated command queue for asynchronous data transfers (DMA).</summary>
		/// <remarks>The device will use the queue if present, even if the flag is not set for device selection.</remarks>
		public bool AsyncTransferQueue {
			get => (_packed & 0x8) != 0;
			init => _packed = value ? (_packed | 0x8) : (_packed & ~0x8u);
		}

		/// <summary>If the device should use a dedicated command queue for asynchronous compute operations.</summary>
		/// <remarks>The device will use the queue if present, even if the flag is not set for device selection.</remarks>
		public bool AsyncComputeQueue {
			get => (_packed & 0x10) != 0;
			init => _packed = value ? (_packed | 0x10) : (_packed & ~0x10u);
		}
		#endregion // Fields

		public override int GetHashCode() => _packed.GetHashCode();
		public bool Equals(FeatureSet? other) => _packed == other?._packed;
	}
	
	
	/// <summary>Internal Vulkan-specific values for a device.</summary>
	internal sealed record VulkanInfo
	{
		/// <summary>The handle to the device.</summary>
		public required Vk.PhysicalDevice Handle { get; init; }
		
		/// <summary>The reported driver version.</summary>
		public required Version DriverVersion { get; init; }
		/// <summary>The highest supported Vulkan API version.</summary>
		public required Version ApiVersion { get; init; }
		
		/// <summary>UUID for the driver pipeline cache version.</summary>
		public required UInt128 PipelineCacheUuid { get; init; }
		/// <summary>UUID for the device.</summary>
		public required UInt128 DeviceUuid { get; init; }
		/// <summary>UUID for the device driver version.</summary>
		public required UInt128 DriverUuid { get; init; }
		
		/// <summary>Uniform buffer minimum alignment.</summary>
		public required uint UniformAlignment { get; init; }
		/// <summary>Storage buffer minimum alignment.</summary>
		public required uint StorageAlignment { get; init; }
		
		/// <summary>The queue family for the main graphics queue.</summary>
		public required uint MainQueue { get; init; }
		/// <summary>The queue family for the DMA async transfer queue, if present.</summary>
		public required uint? DmaQueue { get; init; }
		/// <summary>The queue family for the async compute queue, if present.</summary>
		public required uint? AsyncComputeQueue { get; init; }
	}


	/// <summary>Default graphics device ordering heuristic.</summary>
	private sealed class DefaultOrdering : IComparer<GraphicsDevice>
	{
		public int Compare(GraphicsDevice? x, GraphicsDevice? y)
		{
			if (ReferenceEquals(x, y)) return 0;
			return (x!.Kind, y!.Kind) switch {
				(DeviceKind.Discrete, not DeviceKind.Discrete) => -1,
				(not DeviceKind.Discrete, DeviceKind.Discrete) => 1,
				_ => y.Limits.DeviceLocalMemory.CompareTo(x.Limits.DeviceLocalMemory)
			};
		}
	}
}
