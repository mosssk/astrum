/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum;


/// <summary>Interface for types that handle global application events in <see cref="Core"/>.</summary>
public interface IAppEventMonitor
{
	/// <summary>Called when framework initialization is complete, but before user initialization occurs.</summary>
	void OnFrameworkInitialized() { }
	
	/// <summary>Called when the application is starting a new frame.</summary>
	void OnFrameStarting() { }
	
	/// <summary>Called when the current application frame is ending.</summary>
	void OnFrameEnding() { }
	
	/// <summary>Called when the application is starting update logic.</summary>
	void OnUpdateStarting() { }
	
	/// <summary>Called when the application has finished update logic.</summary>
	void OnUpdateEnding() { }
	
	/// <summary>Called when the application is starting render graph execution.</summary>
	void OnRenderStarting() { }
	
	/// <summary>Called when the application has finished render graph execution.</summary>
	void OnRenderEnding() { }

	/// <summary>Called when the application is requested to quit.</summary>
	/// <param name="system">
	/// <c>true</c> if the system requested the quit (such as from an OS signal), <c>false</c> if
	/// <see cref="Core.RequestQuit"/> was called.
	/// </param>
	void OnApplicationQuitRequested(bool system) { }
	
	/// <summary>Called when the application has started the shutdown process.</summary>
	void OnApplicationQuitting() { }
	
	/// <summary>Called when the application encounters a top-level propagated exception and must exit.</summary>
	/// <param name="ex">The top-level propagated exception.</param>
	void OnTopLevelException(Exception ex) { }
}
