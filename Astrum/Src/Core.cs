/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Threading;
using Astrum.Render;
using static Astrum.InternalLogging;

namespace Astrum;


/// <summary>Primary API surface for global configuration, core functionality, and runtime operations.</summary>
public static partial class Core
{
	#region Fields
	/// <summary>If the core application is currently running.</summary>
	public static bool IsRunning => Config is not null;
	
	/// <summary>The finalized application configuration (<c>null</c> if not started).</summary>
	internal static AppConfig? Config { get; private set; }
	
	// Run state lock
	private static readonly Lock _RunLock = new();
	
	// Exit requested flag
	private static bool _ShouldKeepRunning;
	#endregion // Fields


	/// <summary>Runs the application. Should <em>always</em> be called from the main application thread.</summary>
	/// <remarks>Does not return until the application main loop exits and shutdown is complete.</remarks>
	/// <param name="configure">The callback accepting an application configuration object.</param>
	public static void Run(ApplicationBuilder.ConfigureCallback configure)
	{
		// Check and set start configuration
		lock (_RunLock) {
			if (IsRunning) throw new InvalidOperationException("Cannot call Core.Run more than once.");
			var builder = new ApplicationBuilder();
			configure(builder);
			Config = builder.Build();
		}
		
		// Startup the framework
		_ShouldKeepRunning = true;
		Time.Initialize();
		RenderCore.Initialize(Config);

		try {
			var events = Config.EventMonitor;
			events?.OnFrameworkInitialized();
			
			// Enter main loop
			LogInfo("Entering main application loop");
			while (_ShouldKeepRunning) {
				// Framework new frame
				Time.Update();
				events?.OnFrameStarting();
				
				// Perform logic updates
				events?.OnUpdateStarting();
				events?.OnUpdateEnding();
				
				// Execute the render graph
				events?.OnRenderStarting();
				events?.OnRenderEnding();
				
				// Framework end frame
				events?.OnFrameEnding();
			}
			LogInfo("Exited main application loop normally");
		}
		catch (Exception ex) {
			LogCritical($"An exception has propagated to the top level: ({ex.GetType()}) {ex.Message}", ex);
			try {
				Config.EventMonitor?.OnTopLevelException(ex);
			}
			catch { }
			// TODO: Display popup
		}
		finally {
			// Perform user shutdown
			try {
				Config.EventMonitor?.OnApplicationQuitting();
			}
			catch (Exception ex) {
				LogError($"An exception occured during application shutdown: ({ex.GetType()}) {ex.Message}", ex);
			}
			
			// Shut down the framework
			RenderCore.Shutdown();
			
			// Cleanup state
			_ShouldKeepRunning = false;
			Config = null;
		}
	}

	/// <summary>Requests that the application be exited at the end of the current frame.</summary>
	public static void RequestQuit()
	{
		LogInfo("Explicit exit has been requested");
		_ShouldKeepRunning = false;
		Config?.EventMonitor?.OnApplicationQuitRequested(false);
	}
}
