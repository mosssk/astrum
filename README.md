# Astrum


## Acknowledgements

The following third party libraries are used in one or more of the Astrum projects:
- [VMA](https://gpuopen.com/vulkan-memory-allocator/) - Vulkan Memory Allocator
- [Vulkan-Headers](https://github.com/KhronosGroup/Vulkan-Headers)
- [Silk.NET](https://dotnet.github.io/Silk.NET/)

All of these third-party projects are used (and re-hosted where applicable) within the bounds of their original licenses.
