/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#pragma once

///// Platform detection
#if defined(_MSC_VER)
#   define ASTRUM_MSVC
#	define ASTRUM_EXPORT __declspec(dllexport)
#	define WIN32_LEAN_AND_MEAN
#elif defined(__clang__)
#   define ASTRUM_CLANG
#	define ASTRUM_EXPORT __attribute__((visibility("default")))
#elif defined(__GNUC__)
#   define ASTRUM_GCC
#	define ASTRUM_EXPORT __attribute__((visibility("default")))
#else
#   error "Unknown build platform"
#endif // defined(_MSC_VER)


///// Platform checks
static_assert(sizeof(void*) == 8, "Must be compiled on a 64-bit platform");


///// Common includes
#include <cstdint>
