/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./astrum_common.h"


extern "C"
{

/////
ASTRUM_EXPORT uint32_t _Test0(uint32_t l, uint32_t r) {
    return l + r;
}

/////
ASTRUM_EXPORT void _Test1(uint32_t l, uint32_t* r) {
    *r = l;
}

/////
ASTRUM_EXPORT void _Test2(uint32_t l, uint32_t r, uint32_t* o) {
    *o = l + r;
}

}
