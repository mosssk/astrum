/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

#include "./astrum_common.h"

#define VMA_IMPLEMENTATION
#define VMA_VULKAN_VERSION 1002000 // Vulkan 1.2
#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1

#define VMA_DEDICATED_ALLOCATION 1
#define VMA_BIND_MEMORY2 1
#define VMA_MEMORY_BUDGET 0
#define VMA_BUFFER_DEVICE_ADDRESS 1
#define VMA_MEMORY_PRIORITY 0
#define VMA_EXTERNAL_MEMORY 0
#define VMA_EXTERNAL_MEMORY_WIN32 0
#define VMA_KHR_MAINTENANCE4 0
#define VMA_KHR_MAINTENANCE5 0

#if defined(ASTRUM_GCC) || defined(ASTRUM_CLANG)
#	define VMA_CALL_PRE __attribute__((visiblity("hidden")))
#endif

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>


///// Core set of memory heap type indices
struct VmaMemoryTypeIndices
{
	uint32_t device;
	uint32_t upload;
	uint32_t readback;
	uint32_t dynamic;
	uint32_t lazy; // UINT32_MAX if not present
};


extern "C"
{

/////
ASTRUM_EXPORT VkResult VmaCreateAllocator(
	VkInstance instance, VkPhysicalDevice physicalDevice, VkDevice device,
	PFN_vkGetInstanceProcAddr _vkGetInstanceProcAddr, PFN_vkGetDeviceProcAddr _vkGetDeviceProcAddr,
	VmaAllocator* allocator
) {
	// Create Info
	VmaAllocatorCreateInfo info {};
	info.flags = 
		VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT |
		VMA_ALLOCATOR_CREATE_KHR_BIND_MEMORY2_BIT |
		VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT;
	info.instance = instance;
	info.physicalDevice = physicalDevice;
	info.device = device;
	info.vulkanApiVersion = VK_API_VERSION_1_2;

	// Function pointers
	VmaVulkanFunctions functions {};
	functions.vkGetInstanceProcAddr = _vkGetInstanceProcAddr;
	functions.vkGetDeviceProcAddr = _vkGetDeviceProcAddr;
	info.pVulkanFunctions = &functions;

	// Create
	return vmaCreateAllocator(&info, allocator);
}

/////
ASTRUM_EXPORT void VmaDestroyAllocator(VmaAllocator allocator)
{
	if (!allocator) return;
	vmaDestroyAllocator(allocator);
}

/////
ASTRUM_EXPORT void VmaFindMemoryTypes(VmaAllocator allocator, VmaMemoryTypeIndices* types)
{
	if (!types) return;
	
	VmaAllocationCreateInfo createInfo {};
	createInfo.memoryTypeBits = UINT32_MAX;

	// Device local memory
	createInfo.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE;
	createInfo.flags = 0;
	createInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
	createInfo.preferredFlags = 0;
	assert(vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &types->device) == VK_SUCCESS && "Failed to find DEVICE memory");

	// Upload memory
	createInfo.usage = VMA_MEMORY_USAGE_AUTO_PREFER_HOST;
	createInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT | VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;
	createInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	createInfo.preferredFlags = 0;
	assert(vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &types->upload) == VK_SUCCESS && "Failed to find UPLOAD memory");

	// Readback memory
	createInfo.usage = VMA_MEMORY_USAGE_AUTO_PREFER_HOST;
	createInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT | VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT;
	createInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	createInfo.preferredFlags = VK_MEMORY_PROPERTY_HOST_CACHED_BIT;
	if (vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &types->readback) != VK_SUCCESS) {
		types->readback = types->upload;
	}

	// Dynamic memory
	createInfo.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE;
	createInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT | VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;
	createInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
		VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	createInfo.preferredFlags = 0;
	if (vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &types->dynamic) != VK_SUCCESS) {
		types->dynamic = types->upload;
	}

	// Lazy memory
	createInfo.usage = VMA_MEMORY_USAGE_AUTO_PREFER_DEVICE;
	createInfo.flags = 0;
	createInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;
	createInfo.preferredFlags = 0;
	if (!vmaFindMemoryTypeIndex(allocator, UINT32_MAX, &createInfo, &types->lazy) != VK_SUCCESS) {
		types->lazy = UINT32_MAX;
	}
}

/////
ASTRUM_EXPORT VkResult VmaCreateImage(VmaAllocator allocator, const VkImageCreateInfo* pCreateInfo, uint32_t memoryIndex,
	VkBool32 dedicated, VkImage* pImage, VmaAllocation* pAllocation)
{
	VmaAllocationCreateInfo create {};
	create.flags = dedicated ? VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT : VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT;
	create.usage = VMA_MEMORY_USAGE_UNKNOWN; // Allows single memoryTypeBits selection
	create.memoryTypeBits = 1u << memoryIndex;
	return vmaCreateImage(allocator, pCreateInfo, &create, pImage, pAllocation, nullptr);
}

/////
ASTRUM_EXPORT void VmaDestroyImage(VmaAllocator allocator, VkImage image, VmaAllocation allocation)
{
	if (!allocator || !image || !allocation) return;
	vmaDestroyImage(allocator, image, allocation);
}

/////
ASTRUM_EXPORT VkResult VmaCreateBuffer(VmaAllocator allocator, const VkBufferCreateInfo* pCreateInfo, uint32_t memoryIndex,
	VkBool32 dedicated, VkBool32 mapped, VkBuffer* pBuffer, VmaAllocation* pAllocation, void** ppMapping)
{
	VmaAllocationCreateInfo create {};
	create.flags = 
		(dedicated ? VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT : VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT) |
		(mapped ? VMA_ALLOCATION_CREATE_MAPPED_BIT : 0);
	create.usage = VMA_MEMORY_USAGE_UNKNOWN; // Allows single memoryTypeBits selection
	create.memoryTypeBits = 1u << memoryIndex;

	VmaAllocationInfo info;
	const auto result = vmaCreateBuffer(allocator, pCreateInfo, &create, pBuffer, pAllocation, &info);
	if (result == VK_SUCCESS && ppMapping) *ppMapping = info.pMappedData;
	return result;
}

/////
ASTRUM_EXPORT void VmaDestroyBuffer(VmaAllocator allocator, VkBuffer buffer, VmaAllocation allocation)
{
	if (!allocator || !buffer || !allocation) return;
	vmaDestroyBuffer(allocator, buffer, allocation);
}

}
