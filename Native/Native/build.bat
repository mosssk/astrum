@rem
@rem Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
@rem This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
@rem the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
@rem

@echo off

cl /LD /O2 ^
    /I "..\..\External\Vulkan-Headers\include" /I "..\..\External\VulkanMemoryAllocator\include" ^
    Src\vk_mem_alloc.usage.cpp Src\test.cpp ^
    /link /out:./Lib/win-x64/astrum-runtime.dll
del *.lib *.exp *.obj
