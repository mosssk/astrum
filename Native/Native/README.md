# Native Libraries

This directory contains the sources, build scripts, and prebuilt binaries for the native libraries used by Astrum.

On top of third party libraries, Astrum includes a dedicated runtime library containing a collection of additional libraries compiled together as needed.

### Astrum Native Runtime

The native runtime library can be built using the `build.bat`/`build.sh` scripts found in this directory. The following toolchains are required:
- Windows: `cl.exe` - Visual 2022 Build Tools (**x64**) with the `Microsoft.VisualStudio.Component.Windows10SDK.20348` toolchain
- Linux: `g++` - GCC version 9.4.0 - newer versions should be avoided

This library includes:
- [VMA](https://gpuopen.com/vulkan-memory-allocator/) 3.1.0
