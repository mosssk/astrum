#!/bin/sh
#
# Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
# This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
# the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
#

g++ -shared -o ./Lib/linux-x64/libastrum-runtime.so -fPIC -m64 -O3 -Wno-attributes -flto \
    -I "../../External/Vulkan-Headers/include" -I "../../External/VulkanMemoryAllocator/include" \
    Src/vk_mem_alloc.usage.cpp Src/test.cpp
