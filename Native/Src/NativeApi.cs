﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Astrum.Native;


/// <summary>
/// Manages a loaded native shared library, operating with <see cref="NativeSymbolAttribute"/> to provide automatic
/// symbol loading.
/// </summary>
public abstract class NativeApi : IDisposable
{
	private static readonly Type INTPTR_T = typeof(nint);
	
	
	#region Fields
	/// <summary>The name of the native library.</summary>
	public readonly string LibraryName;

	/// <summary>The handle of the loaded native library. Returns <c>0</c> when disposed.</summary>
	public nint LibraryHandle { get; private set; }

	/// <summary>If the native API handle has been disposed.</summary>
	public bool IsDisposed => LibraryHandle == 0;
	#endregion // Fields

	
	/// <summary>Loads the native library handle, and initializes all <see cref="NativeSymbolAttribute"/> fields.</summary>
	/// <param name="libraryName">The root name of the library to load (without extensions or prefixes).</param>
	/// <param name="assembly">The assembly loading the native library.</param>
	/// <param name="searchPath">Search path flags for finding the native library.</param>
	/// <exception cref="DllNotFoundException">The native library could not be loaded.</exception>
	/// <exception cref="BadImageFormatException">The native library was not a valid shared library.</exception>
	/// <exception cref="SymbolLoadException">A non-optional symbol could not be loaded.</exception>
	protected NativeApi(string libraryName, Assembly assembly, DllImportSearchPath? searchPath = null)
	{
		ArgumentException.ThrowIfNullOrEmpty(libraryName);
		
		LibraryName = libraryName;
		LibraryHandle = NativeLibrary.Load(libraryName, assembly, 
			DllImportSearchPath.SafeDirectories | DllImportSearchPath.AssemblyDirectory);
		
		// Load field symbols
		var fields = GetType()
			.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
			.Where(f => f.FieldType.IsPointer || f.FieldType.IsUnmanagedFunctionPointer || f.FieldType == INTPTR_T)
			.Where(f => f.GetCustomAttribute<NativeSymbolAttribute>() is not null);
		foreach (var f in fields) {
			var attr = f.GetCustomAttribute<NativeSymbolAttribute>()!;
			if (!NativeLibrary.TryGetExport(LibraryHandle, attr.Name, out var symbol)) {
				if (!attr.Optional) throw new SymbolLoadException(attr.Name, LibraryName);
				symbol = 0;
			}
			f.SetValue(this, symbol);
		}
	}

	~NativeApi() => dispose(false);


	#region Dispose
	public void Dispose()
	{
		dispose(true);
		GC.SuppressFinalize(this);
	}

	private void dispose(bool disposing)
	{
		if (IsDisposed) return;
		OnDisposed(disposing);
		NativeLibrary.Free(LibraryHandle);
		LibraryHandle = 0;
	}
	
	/// <summary>Called when the native API is being disposed (before the library is freed).</summary>
	/// <param name="disposing">If <see cref="Dispose"/> has been called, <c>false</c> means finalization.</param>
	protected virtual void OnDisposed(bool disposing) { }
	#endregion // Dispose


	/// <summary>Thrown when a native library symbol cannot be loaded.</summary>
	public sealed class SymbolLoadException : Exception
	{
		#region Fields
		/// <summary>The name of the symbol that failed to load.</summary>
		public readonly string SymbolName;
		#endregion // Fields
		
		internal SymbolLoadException(string symbolName, string libraryName)
			: base($"Failed to load exported symbol '{symbolName}' from library '{libraryName}'") =>
			SymbolName = symbolName;
	}
}
