﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;

namespace Astrum.Native;


/// <summary>Used to label fields in <see cref="NativeApi"/> to be loaded as exported symbols during construction.</summary>
/// <remarks>Only fields that are <see cref="IntPtr"/>, pointers, or unmanaged function pointers are loaded.</remarks>
public sealed class NativeSymbolAttribute(string name) : Attribute
{
	#region Fields
	/// <summary>The name of the exported symbol to load.</summary>
	public readonly string Name = name;
	/// <summary>If the symbol is optional. Optional symbols will not throw an exception if loading them fails.</summary>
	public bool Optional { get; init; } = false;
	#endregion // Fields
}
