﻿/*
 * Microsoft Public License (Ms-PL) - Copyright (c) Astrum Project Contributors 2024
 * This file is subject to the terms and conditions of the Microsoft Public License, the text of which can be found in
 * the 'LICENSE' file at the root of this repository, or online at <https://opensource.org/licenses/MS-PL>.
 */

using System;
#pragma warning disable CS0649 // Field is never assigned to, and will always have its default value

namespace Astrum.Native;


// Tests for NativeApi
internal sealed unsafe class NativeApiTests
{
	[Test]
	public void Ctor_Normal() => Assert.DoesNotThrow(() => {
		using var api = new GoodApi();
		Assert.That(api.LibraryName, Is.EqualTo("astrum-runtime"));
		Assert.That(api.LibraryHandle, Is.Not.EqualTo(0));
		Assert.That(api.IsDisposed, Is.EqualTo(false));
		Assert.That(api.Test0, Is.Not.EqualTo((nint)0));
		Assert.That((nint)api.Test1, Is.Not.EqualTo((nint)0));
		Assert.That((nint)api.Test2, Is.Not.EqualTo((nint)0));
		Assert.That(api.DoesNotExist, Is.EqualTo((nint)0));
		Assert.That(api.InvalidFieldType, Is.EqualTo("test"));

		var test0 = (delegate* unmanaged<uint, uint, uint>)api.Test0;
		var test1 = (delegate* unmanaged<uint, uint*, void>)api.Test1;
		Assert.That(test0(10, 20), Is.EqualTo(30));
		uint r = 10;
		test1(0, &r);
		Assert.That(r, Is.EqualTo(0));
		api.Test2(30, 40, &r);
		Assert.That(r, Is.EqualTo(70));
	});

	[Test]
	public void Ctor_Failed() => Assert.Throws<DllNotFoundException>(() => {
		using var api = new BadApi();
	});

	[Test]
	public void Dispose()
	{
		var api = new GoodApi();
		api.Dispose();
		Assert.That(api.LibraryHandle, Is.EqualTo((nint)0));
		Assert.That(api.IsDisposed, Is.True);
	}


	// NativeApi that contains valid and optional but invalid fields
	private sealed class GoodApi() : NativeApi("astrum-runtime", typeof(GoodApi).Assembly)
	{
		[NativeSymbol("_Test0")]
		public readonly IntPtr Test0;
		[NativeSymbol("_Test1")]
		public readonly void* Test1;
		[NativeSymbol("_Test2")]
		public readonly delegate* unmanaged<uint, uint, uint*, void> Test2;
		[NativeSymbol("SymbolThatDoesNotExist", Optional = true)]
		public readonly IntPtr DoesNotExist;

		[NativeSymbol("InvalidFieldType")]
		public readonly string InvalidFieldType = "test";
	}
	
	// NativeApi that references an assembly that does not exist
	private sealed class BadApi() : NativeApi("does-not-exist", typeof(BadApi).Assembly);
}
